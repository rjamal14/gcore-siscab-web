/* eslint-disable import/order */
import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import Routes from "./routes";
import { BrowserRouter } from "react-router-dom";
import * as serviceWorker from "./serviceWorker";
import { MuiThemeProvider } from "@material-ui/core";
import theme from "./styles/theme";
import thunk from "redux-thunk";
import { createStore, applyMiddleware, compose } from "redux";
import rootReducer from "./redux/reducers";
import { Provider } from "react-redux";
import ActionCable from "actioncable";
import env from "./config/env";
import { ActionCableProvider } from "react-actioncable-provider";
import { SnackbarProvider } from "notistack";

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(
  rootReducer,
  /* preloadedState, */ composeEnhancers(applyMiddleware(thunk))
);
const cable = ActionCable.createConsumer(`${env.notifServer}`);
const snackbarProps = {
  maxSnack: 1,
  anchorOrigin: {
    vertical: "top",
    horizontal: "right",
  },
};

ReactDOM.render(
  <Provider store={store}>
    <ActionCableProvider cable={cable}>
      <BrowserRouter>
        <SnackbarProvider {...snackbarProps}>
          <MuiThemeProvider theme={theme}>
            <Routes />
          </MuiThemeProvider>
        </SnackbarProvider>
      </BrowserRouter>
    </ActionCableProvider>
  </Provider>,
  document.getElementById("root")
);
serviceWorker.unregister();
