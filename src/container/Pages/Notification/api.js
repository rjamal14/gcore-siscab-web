/* eslint-disable camelcase */
/* eslint-disable semi */
/* eslint-disable import/prefer-default-export */
import axios from 'axios';
import env from '../../../config/env';
import { SetAuthTokenRequest, checkTokenExpired } from '../../../config/interceptor';

const api = axios.create({
  baseURL: env.masterApi + env.apiPrefixV1
});
api.interceptors.request.use(SetAuthTokenRequest, null);
api.interceptors.response.use(null, checkTokenExpired);
const path = '/notifications';

const mapParams = (params) => {
  if (!params?.per_page) {
    params = {
      per_page: 10,
      page: 1
    }
  }
  return { params }
}

// eslint-disable-next-line camelcase
export const getApiList = (params) => api.get(path, mapParams(params));