/* eslint-disable no-unused-vars */
/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import Crud from '../../../components/Crud';
import { getApiList } from './api';
import columns from './columns';
import { changePageTitle } from '../../../redux/actions/changePageTitle';
import { changeNotifStatus } from '../../../redux/actions/notification';

const Notification = () => {
  const dispatch = useDispatch();
  const history = useHistory();

  useEffect(() => {
    dispatch(changePageTitle('Notifikasi'));
  }, []);

  const handleRowClick = (event, row, rowData) => {
    history.replace(`${row.url}`);
    dispatch(changeNotifStatus(row.id));
  };

  const customCollection = (data) => data.filter(val => val.type === 'siscab');

  return (
    <Crud
      columns={columns}
      getApi={getApiList}
      // showApi={showApi}
      disableAdd
      disableFilter
      disableExport
      disableSearch
      onRowClick={handleRowClick}
      customCollection={customCollection}
    />
  );
};

export default Notification;
