/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable camelcase */
/* eslint-disable consistent-return */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable semi */
import React, { useState } from 'react';
import { useFormikContext, useFormik } from 'formik';
import {
  Card,
  CardContent,
  Button,
  CircularProgress,
  Grid,
  TextField,
  InputAdornment,
} from '@material-ui/core';
import swal from 'sweetalert';
import useStyles from './profile-jss.js';
import { editApi } from './api';
import Icon from '../../../../components/icon';

const ChangePassword = () => {
  const classes = useStyles();
  const formikContext = useFormikContext();
  const [show, setShow] = useState({
    new_password: 'password',
    old_password: 'password',
    confirm_new_password: 'password',
  })
  const formik = useFormik({
    initialValues: {
      new_password: '',
      old_password: '',
      confirm_new_password: '',
    },
    initialErrors: {
      new_password: '',
      old_password: '',
      confirm_new_password: '',
    },
    validate: (values) => {
      const errors = {};
      const rgx = new RegExp('^(?=.*[0-9])(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z])[a-zA-Z0-9!@#$%^&*]{8,}$');

      if (!values.old_password) errors.old_password = 'Tidak boleh kosong';
      else if (!(rgx.test(values.old_password))) errors.old_password = 'Minimal 8 karakter kombinasi angka, simbol, huruf besar, dan kecil';

      if (!values.new_password) errors.new_password = 'Tidak boleh kosong';
      else if (!(rgx.test(values.new_password))) errors.new_password = 'Minimal 8 karakter kombinasi angka, simbol, huruf besar, dan kecil';

      if (!values.confirm_new_password) errors.confirm_new_password = 'Tidak boleh kosong';
      else if (!(rgx.test(values.confirm_new_password))) errors.confirm_new_password = 'Minimal 8 karakter kombinasi angka, simbol, huruf besar, dan kecil';
      else if (values.new_password !== values.confirm_new_password) errors.confirm_new_password = 'Kata Sandi tidak sama';

      console.log(values.confirm_new_password)
      console.log(values.new_password)
      return errors;
    },
    onSubmit: (values) => {
      const { old_password, new_password, confirm_new_password } = values
      const payload = { user: { old_password, new_password, confirm_new_password } }
      editApi(payload)
        .then((res) => {
          const response = res?.data || res?.response?.data;
          if (res?.status === 200 && response) {
            swal({
              title: 'Sukses',
              text: response?.message,
              icon: 'success',
              buttons: 'OK'
            });
          }
        });
    },
  });

  const changeShow = (name, type) => {
    const state = { ...show }
    state[name] = type
    setShow(state)
  }

  return (
    <div className={classes.root}>
      <Card classes={{ root: classes.root }}>
        <CardContent>
          <div className={classes.summaryTitle}>
            Ubah Kata Sandi
          </div>
          <form onSubmit={formik.handleSubmit} noValidate>
            <Grid container>
              <Grid item xs={12}>
                <div className={classes.form}>
                  <div className={classes.label}>
                    Kata Sandi Lama
                  </div>
                  <TextField
                    autoComplete="off"
                    type={show.old_password}
                    fullwidth
                    margin="normal"
                    size="small"
                    variant="outlined"
                    name="old_password"
                    value={formik.values.old_password}
                    onChange={formik.handleChange}
                    error={formik.touched.old_password && Boolean(formik.errors.old_password)}
                    helperText={formik.touched.old_password && formik.errors.old_password}
                    InputProps={{
                      endAdornment: show.old_password === 'password' ? (
                        <InputAdornment position="start">
                          <img
                            alt=""
                            onClick={() => changeShow('old_password', 'text')}
                            src={Icon.eye2}
                            style={{ cursor: 'pointer' }}
                          />
                        </InputAdornment>
                      ) : (
                        <InputAdornment position="start">
                          <img
                            alt=""
                            onClick={() => changeShow('old_password', 'password')}
                            src={Icon.eye}
                            style={{ cursor: 'pointer' }}
                          />
                        </InputAdornment>
                      ),
                    }}
                  />
                </div>
                <div className={classes.form}>
                  <div className={classes.label}>
                    Kata Sandi Baru
                  </div>
                  <TextField
                    autoComplete="off"
                    type={show.new_password}
                    fullwidth
                    margin="normal"
                    size="small"
                    variant="outlined"
                    name="new_password"
                    value={formik.values.new_password}
                    onChange={formik.handleChange}
                    error={formik.touched.new_password && Boolean(formik.errors.new_password)}
                    helperText={formik.touched.new_password && formik.errors.new_password}
                    InputProps={{
                      endAdornment: show.new_password === 'password' ? (
                        <InputAdornment position="start">
                          <img
                            alt=""
                            onClick={() => changeShow('new_password', 'text')}
                            src={Icon.eye2}
                            style={{ cursor: 'pointer' }}
                          />
                        </InputAdornment>
                      ) : (
                        <InputAdornment position="start">
                          <img
                            alt=""
                            onClick={() => changeShow('new_password', 'password')}
                            src={Icon.eye}
                            style={{ cursor: 'pointer' }}
                          />
                        </InputAdornment>
                      ),
                    }}
                  />
                </div>
                <div className={classes.form}>
                  <div className={classes.label}>
                    Konfirmasi Kata Sandi Baru
                  </div>
                  <TextField
                    autoComplete="off"
                    type={show.confirm_new_password}
                    fullwidth
                    margin="normal"
                    size="small"
                    variant="outlined"
                    name="confirm_new_password"
                    value={formik.values.confirm_new_password}
                    onChange={formik.handleChange}
                    error={formik.touched.confirm_new_password && Boolean(formik.errors.confirm_new_password)}
                    helperText={formik.touched.confirm_new_password && formik.errors.confirm_new_password}
                    InputProps={{
                      endAdornment: show.confirm_new_password === 'password' ? (
                        <InputAdornment position="start">
                          <img
                            alt=""
                            onClick={() => changeShow('confirm_new_password', 'text')}
                            src={Icon.eye2}
                            style={{ cursor: 'pointer' }}
                          />
                        </InputAdornment>
                      ) : (
                        <InputAdornment position="start">
                          <img
                            alt=""
                            onClick={() => changeShow('confirm_new_password', 'password')}
                            src={Icon.eye}
                            style={{ cursor: 'pointer' }}
                          />
                        </InputAdornment>
                      ),
                    }}
                  />
                </div>
                <div>
                  <Button
                    type="submit"
                    color="primary"
                    disabled={formikContext?.isSubmitting}
                    onClick={formikContext?.submitForm}
                    variant="contained"
                    style={{ color: 'white', marginTop: 12 }}
                  >
                    {
                      formikContext?.isSubmitting ? <CircularProgress color="secondary" size={24} /> : 'Simpan'
                    }
                  </Button>
                </div>
              </Grid>
            </Grid>
          </form>

        </CardContent>
      </Card>
    </div>
  );
};

export default ChangePassword;
