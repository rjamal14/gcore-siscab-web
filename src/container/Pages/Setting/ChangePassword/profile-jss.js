import { makeStyles } from '@material-ui/core';

const useStyles = makeStyles(() => ({
  root: {
    padding: '16px 16px 22px',
    borderRadius: 8,
    width: '100%',
    marginBottom: 16,
  },
  label: {
    margin: 'auto 0'
  },
  form: {
    marginBottom: 10,
  },
  userText: {
  },
  summaryTitle: {
    fontSize: '22px',
    fontWeight: '600',
    lineHeight: '2',
    marginBottom: 20,
  },
  photoContainer: {
    height: 250,
    width: 250,
    margin: '16px 0px',
    border: '2px dotted #666666',
  },
  photo: {
    maxHeight: 250,
    maxWidth: 250,
  },
}));

export default useStyles;
