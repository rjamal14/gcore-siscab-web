/* eslint-disable camelcase */
/* eslint-disable semi */
/* eslint-disable import/prefer-default-export */
import axios from 'axios';
import env from '../../../../config/env';
import { SetAuthTokenRequest, checkTokenExpired } from '../../../../config/interceptor';

const api = axios.create({
  baseURL: env.managementApi + env.apiPrefixV1
});
api.interceptors.request.use(SetAuthTokenRequest, null);
api.interceptors.response.use(response => response, checkTokenExpired);

// eslint-disable-next-line camelcase
/* --------------------------- api accounting ------------------------------------- */
export const getApi = (params) => api.get('/deviations', { params });
export const getApiDetail = (id) => api.get(`/deviations/${id}`);
export const approval = (id) => api.put(`/deviations/${id}/approve`);
export const rejection = (id, payload) => api.put(`/deviations/${id}/reject`, payload);
