/* eslint-disable react/jsx-one-expression-per-line */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable no-trailing-spaces */
/* eslint-disable semi */
/* eslint-disable react/prop-types */
import React from 'react';
import * as dayjs from 'dayjs'
import Func from '../../../../functions';
import ApproveAction from './approveAction';
import 'dayjs/locale/id'

const setColor = (val) => {
  let color = ''
  let backgroundColor = ''
  let status = ''

  if (val === 'waiting') {
    color = '#FFFFFF'
    backgroundColor = '#2196F3'
    status = 'Menunggu Persetujuan'
  } else if (val === 'approved') {
    color = '#FFFFFF'
    backgroundColor = '#4CAF50'
    status = 'Diterima'
  } else if (val === 'rejected') {
    color = '#FFFFFF'
    backgroundColor = '#F44336'
    status = 'Ditolak'
  }
  return (
    <div style={{
      backgroundColor,
      width: 'auto',
      color,
      borderRadius: 5,
      padding: 10,
      textAlign: 'center',
      fontSize: 10,
    }}
    >
      {status?.toUpperCase() || ''}
    </div>
  )
}

const setDeviasi = (val) => {
  let type = ''

  if (val === 'one_obligor') type = 'One Obligor'
  else if (val === 'ltv') type = 'LTV'
  else if (val === 'admin') type = 'Admin'
  else type = 'Rental'

  return type
}

const columns = [
  {
    name: 'id',
    label: 'ID',
    display: false,
    options: {
      filter: false,
      sort: false
    }
  },
  {
    name: 'cif_number',
    label: 'No. CIF',
    options: {
      filter: true,
      sort: false
    }
  },
  {
    name: 'sge',
    label: 'No. SGE',
    options: {
      filter: true,
      sort: false
    }
  },
  {
    name: 'contract_date',
    label: 'Tanggal',
    customBodyRender: (evt, val) => dayjs(val).locale('id').format('DD MMM YYYY'),
    options: {
      filter: true,
      sort: false
    }
  },
  {
    name: 'customer_name',
    label: 'Nama Customer',
    options: {
      filter: true,
      sort: false
    }
  },
  {
    name: 'deviation_type',
    label: 'Tipe Deviasi',
    customBodyRender: (evt, val) => setDeviasi(val),
    options: {
      filter: true,
      sort: false
    }
  },
  {
    name: 'loan_amount',
    label: 'Jumlah Pinjaman',
    customBodyRender: (evt, val) => Func.currencyFormatter(val),
    options: {
      filter: true,
      sort: false
    }
  },
  {
    name: 'status',
    label: 'Status',
    customBodyRender: (evt, val) => setColor(val),
    options: {
      filter: true,
      sort: false
    }
  },
  {
    name: 'action',
    label: 'Aksi',
    customBodyRender: (evt, val, rowItem) => ((rowItem.status === 'waiting') && <ApproveAction row={rowItem} />),
    options: {
      filter: true,
      sort: false
    }
  },
];

export default columns;
