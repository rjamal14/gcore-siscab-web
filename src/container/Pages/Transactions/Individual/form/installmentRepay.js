/* eslint-disable no-plusplus */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable no-redeclare */
/* eslint-disable block-scoped-var */
/* eslint-disable no-var */
/* eslint-disable vars-on-top */
/* eslint-disable array-callback-return */
/* eslint-disable react/button-has-type */
/* eslint-disable camelcase */
/* eslint-disable prefer-destructuring */
/* eslint-disable eqeqeq */
/* eslint-disable react/sort-comp */
/* eslint-disable react/no-access-state-in-setstate */
/* eslint-disable react/no-unused-state */
/* eslint-disable no-shadow */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable no-unused-vars */
import React from "react";
import { withStyles } from "@material-ui/core/styles";
import Divider from "@material-ui/core/Divider";
import Typography from "@material-ui/core/Typography";
import swal from "sweetalert";
import Grid from "@material-ui/core/Grid";
import DialogContentText from "@material-ui/core/DialogContentText";
import MuiDialogActions from "@material-ui/core/DialogActions";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import Button from "@material-ui/core/Button";
import { CircularProgress } from "@material-ui/core";
import Func from "../../../../../functions/index";
import styles from "../css";
import "../../../../../styles/date_field.css";
import env from "../../../../../config/env";

const DialogActions = withStyles((theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(1),
  },
}))(MuiDialogActions);

class Form extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: false,
      validator: [],
      value: { gender: { label: "" } },
      type: "",
      activeTabs: 0,
      Proses: false,
      ImgBase124: "",
      section: 0,
      id: null,
      redirect: false,
      date: new Date(),
      provin: [],
      data3: [],
      estimated_value: [],
      ltv_value: [],
      ltv: [],
      monthly_fee: [],
      dataBJ: [],
      cont: 0,
      approvals: [],
      download: null,
      payment_status: false,
      loader: false,
      installmentHistory: [],
    };
  }

  componentDidMount() {
    if (this.props.type == "Ubah") {
      // this.setState({ loader: true });
      fetch(
        process.env.REACT_APP_URL_MANAGEMENT +
          process.env.REACT_APP_API_PREFIX_V1 +
          "/installments/" +
          this.props.id +
          "/inquiry",
        {
          method: "GET",
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Bearer " + localStorage.getItem("token"),
          },
        }
      )
        .then((response) => response.json())
        .then((json) => {
          if (json.code == "403") {
            this.setState({ loader: false });
            if (Func.Clear_Token() == true) {
              if (!localStorage.getItem("token")) {
                this.setState({ redirect: true });
              }
            }
          }
          const { data } = json;
          const val = this.state.value;

          try {
            val.cif = data.cif;
            val.customer_name = data.customer_name;
            val.installment_amount = data.installment_amount;
            val.installment_fee = data.installment_fee;
            val.installment_interest = data.installment_interest;
            val.payment_amount = data.payment_amount;
            val.remaining_loan = data.remaining_loan;
            val.sge = data.sge;
            val.fine_amount = data?.fine_amount;
            val.installment_order = data?.installment_order;
          } catch (err) {
            this.setState({ loader: false });
            alert("terjadi kesalahan sistem");
          }

          this.setState({
            value: val,
            loader: false,
            show: true,
          });
        })
        .catch((error) => {})
        .finally(() => {});
    } else {
      this.addBJ();
    }
  }

  handleSubmit() {
    fetch(
      process.env.REACT_APP_URL_MANAGEMENT +
        process.env.REACT_APP_API_PREFIX_V1 +
        "/installments/" +
        this.props.id +
        "/install",
      {
        method: "PUT",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: "Bearer " + localStorage.getItem("token"),
        },
        body: JSON.stringify({
          monthly_installment: {
            payment_amount: this.state.value.payment_amount,
            fine_amount: this.state.value.fine_amount,
          },
        }),
      }
    )
      .then((response) => response.json())
      .then(() => {
        this.props.handleModal();
        this.props.OnNext("Cicilan berhasil dibayar");
      })
      .catch(() => {})
      .finally(() => {});
  }

  render() {
    const { classes } = this.props;
    const { value } = this.state;

    if (this.state.loader) {
      return (
        <div className={classes.root2}>
          <Dialog
            disablePortal
            disableEnforceFocus
            disableAutoFocus
            open
            scroll="paper"
            maxWidth="md"
            aria-labelledby="server-modal-title"
            aria-describedby="server-modal-description"
            container={() => {}}
          >
            <div
              style={{
                display: "flex",
                flexDirection: "column",
                alignItems: "center",
                padding: "20px",
                borderRadius: "50px",
              }}
            >
              <CircularProgress
                style={{ color: "#85203B", margin: "18px" }}
                size={40}
              />
              Mohon Tunggu
              <div
                style={{
                  marginTop: "10px",
                }}
              />
            </div>
          </Dialog>
        </div>
      );
    }

    return (
      <Dialog
        scroll="paper"
        open
        maxWidth="md"
        aria-labelledby="scroll-dialog-title"
        aria-describedby="scroll-dialog-description"
      >
        <DialogTitle id="scroll-dialog-title">
          <Typography variant="h7" className={classes.tittleModal}>
            Pembayaran Cicilan
          </Typography>
          <IconButton
            aria-label="close"
            className={classes.closeButton}
            onClick={() => {
              this.props.handleModal();
            }}
          >
            <CloseIcon />
          </IconButton>
        </DialogTitle>
        <DialogContent dividers>
          <DialogContentText id="scroll-dialog-description" tabIndex={-1}>
            <div className={classes.scrool}>
              <div>
                {Func.toLogin(this.state.redirect)}
                <div className={classes.root}>
                  <Grid
                    container
                    direction="row"
                    item
                    lg={12}
                    xl={12}
                    md={12}
                    xs={12}
                  >
                    <Grid item lg={12} xl={12} md={12} sm={12} xs={12}>
                      <Grid
                        container
                        direction="row"
                        item
                        lg={12}
                        xl={12}
                        md={12}
                        xs={12}
                      >
                        <Grid
                          container
                          item
                          lg={8}
                          xl={8}
                          md={8}
                          sm={8}
                          xs={12}
                        >
                          <Grid
                            container
                            item
                            lg={12}
                            xl={12}
                            md={12}
                            sm={12}
                            xs={12}
                          >
                            <Grid
                              item
                              lg={6}
                              xl={6}
                              md={6}
                              sm={6}
                              xs={12}
                              className={classes.formPad}
                            >
                              <div className={classes.label111}>
                                <text className={classes.label121}>
                                  No. CIF
                                </text>
                              </div>
                              <div className={classes.label1112}>
                                <text className={classes.label1}>
                                  {this.state.value.cif}
                                </text>
                              </div>
                            </Grid>
                            <Grid
                              item
                              lg={6}
                              xl={6}
                              md={6}
                              sm={6}
                              xs={12}
                              className={classes.formPad}
                            >
                              <div className={classes.label111}>
                                <text className={classes.label121}>
                                  Nomor SGE
                                </text>
                              </div>
                              <div className={classes.label1112}>
                                <text className={classes.label1}>
                                  {this.state.value.sge}
                                </text>
                              </div>
                            </Grid>
                          </Grid>
                          <Grid
                            container
                            item
                            lg={12}
                            xl={12}
                            md={12}
                            sm={12}
                            xs={12}
                          >
                            <Grid
                              item
                              lg={6}
                              xl={6}
                              md={6}
                              sm={6}
                              xs={12}
                              className={classes.formPad}
                            >
                              <div className={classes.label111}>
                                <text className={classes.label121}>
                                  Nama Customer
                                </text>
                              </div>
                              <div className={classes.label1112}>
                                <text className={classes.label1}>
                                  {this.state.value.customer_name}
                                </text>
                              </div>
                            </Grid>
                            <Grid
                              item
                              lg={6}
                              xl={6}
                              md={6}
                              sm={6}
                              xs={12}
                              className={classes.formPad}
                            >
                              <div className={classes.label111}>
                                <text className={classes.label121}>
                                  Jumlah Yang Harus Dibayar
                                </text>
                              </div>
                              <div className={classes.label1112}>
                                <text className={classes.label1}>
                                  {Func.currencyFormatter(
                                    this.state.value.payment_amount
                                  )}
                                </text>
                              </div>
                            </Grid>
                          </Grid>
                        </Grid>
                      </Grid>
                      <Divider className={classes.divider} />
                      <Grid
                        container
                        direction="row"
                        item
                        lg={12}
                        xl={12}
                        md={12}
                        xs={12}
                      >
                        <Grid
                          container
                          item
                          lg={8}
                          xl={8}
                          md={8}
                          sm={8}
                          xs={12}
                        >
                          <Grid
                            container
                            item
                            lg={12}
                            xl={12}
                            md={12}
                            sm={12}
                            xs={12}
                          >
                            <Grid
                              item
                              lg={4}
                              xl={4}
                              md={4}
                              sm={4}
                              xs={12}
                              className={classes.formPad}
                            >
                              <div className={classes.label111}>
                                <text className={classes.label121}>
                                  Cicilan Ke
                                </text>
                              </div>
                              <div className={classes.label1112}>
                                <text className={classes.label1}>
                                  {this.state.value.installment_order}
                                </text>
                              </div>
                            </Grid>
                            <Grid
                              item
                              lg={4}
                              xl={4}
                              md={4}
                              sm={4}
                              xs={12}
                              className={classes.formPad}
                            >
                              <div className={classes.label111}>
                                <text className={classes.label121}>
                                  Cicilan Pokok
                                </text>
                              </div>
                              <div className={classes.label1112}>
                                <text className={classes.label1}>
                                  {this.state.value.installment_amount}
                                </text>
                              </div>
                            </Grid>
                            <Grid
                              item
                              lg={4}
                              xl={4}
                              md={4}
                              sm={4}
                              xs={12}
                              className={classes.formPad}
                            >
                              <div className={classes.label111}>
                                <text className={classes.label121}>
                                  Bunga Cicilan
                                </text>
                              </div>
                              <div className={classes.label1112}>
                                <text className={classes.label1}>
                                  {this.state.value.installment_interest}
                                </text>
                              </div>
                            </Grid>
                          </Grid>
                          <Grid
                            container
                            item
                            lg={12}
                            xl={12}
                            md={12}
                            sm={12}
                            xs={12}
                          >
                            <Grid
                              item
                              lg={4}
                              xl={4}
                              md={4}
                              sm={4}
                              xs={12}
                              className={classes.formPad}
                            >
                              <div className={classes.label111}>
                                <text className={classes.label121}>
                                  Biaya Cicilan
                                </text>
                              </div>
                              <div className={classes.label1112}>
                                <text className={classes.label1}>
                                  {this.state.value.installment_fee}
                                </text>
                              </div>
                            </Grid>
                            <Grid
                              item
                              lg={4}
                              xl={4}
                              md={4}
                              sm={4}
                              xs={12}
                              className={classes.formPad}
                            >
                              <div className={classes.label111}>
                                <text className={classes.label121}>
                                  Sisa Cicilan
                                </text>
                              </div>
                              <div className={classes.label1112}>
                                <text className={classes.label1}>
                                  {Func.currencyFormatter(
                                    this.state.value.remaining_loan
                                  )}
                                </text>
                              </div>
                            </Grid>
                            <Grid
                              item
                              lg={4}
                              xl={4}
                              md={4}
                              sm={4}
                              xs={12}
                              className={classes.formPad}
                            >
                              <div className={classes.label111}>
                                <text className={classes.label121}>
                                  Denda
                                </text>
                              </div>
                              <div className={classes.label1112}>
                                <text className={classes.label1}>
                                  {Func.currencyFormatter(
                                    this.state.value.fine_amount
                                  )}
                                </text>
                              </div>
                            </Grid>
                          </Grid>
                        </Grid>
                      </Grid>
                    </Grid>
                  </Grid>
                </div>
              </div>
            </div>
          </DialogContentText>
        </DialogContent>

        <DialogActions>
          {this.state.loader === false && this.state.show ? (
            <Button
              disabled={this.state.loader_button}
              onClick={() => {
                swal({
                  title: "Apakah Anda Yakin?",
                  text: "Akan melakukan pembayaran cicilan",
                  icon: "info",
                  buttons: true,
                  dangerMode: true,
                }).then((willDelete) => {
                  if (willDelete) {
                    this.setState({ loader_button: true });
                    this.handleSubmit();
                  }
                });
              }}
              style={{
                backgroundColor: "#862C3A",
                color: "white",
              }}
            >
              <Typography variant="button" style={{ color: "#FFFFFF" }}>
                {this.state.loader_button ? (
                  <CircularProgress
                    style={{
                      color: "white",
                      marginLeft: "18px",
                      marginRight: "18px",
                      marginTop: 5,
                    }}
                    size={15}
                  />
                ) : (
                  "Bayar"
                )}
              </Typography>
            </Button>
          ) : null}

          <Button
            style={{
              color: "black",
            }}
            disabled={this.state.loader}
            onClick={() => {
              this.props.handleModal();
            }}
            variant="outlined"
          >
            <Typography variant="button">Batal</Typography>
          </Button>
        </DialogActions>
      </Dialog>
    );
  }
}

export default withStyles(styles.CoustomsStyles, { name: "Form" })(Form);
