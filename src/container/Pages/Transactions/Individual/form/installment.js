/* eslint-disable react/jsx-no-bind */
/* eslint-disable react/jsx-closing-tag-location */
/* eslint-disable react/jsx-indent */
/* eslint-disable indent */
/* eslint-disable consistent-return */
/* eslint-disable no-plusplus */
/* eslint-disable array-callback-return */
/* eslint-disable radix */
/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable class-methods-use-this */
/* eslint-disable react/button-has-type */
/* eslint-disable camelcase */
/* eslint-disable prefer-destructuring */
/* eslint-disable eqeqeq */
/* eslint-disable react/sort-comp */
/* eslint-disable react/no-access-state-in-setstate */
/* eslint-disable react/no-unused-state */
/* eslint-disable no-shadow */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable no-unused-vars */
import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import InputAdornment from '@material-ui/core/InputAdornment';
import Divider from '@material-ui/core/Divider';
import Typography from '@material-ui/core/Typography';
import swal from 'sweetalert';
import FormHelperText from '@material-ui/core/FormHelperText';
import Grid from '@material-ui/core/Grid';
import TextareaAutosize from '@material-ui/core/TextareaAutosize';
import DialogContentText from '@material-ui/core/DialogContentText';
import MuiDialogActions from '@material-ui/core/DialogActions';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Button from '@material-ui/core/Button';
import Box from '@material-ui/core/Box';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import { CircularProgress } from '@material-ui/core';
import Select2 from 'react-select';
import AsyncSelect from 'react-select/async';
import dayjs from 'dayjs';
import Func from '../../../../../functions/index';
import service from '../../../../../functions/service';
import Icon from '../../../../../components/icon';
import styles from '../css';
import env from '../../../../../config/env';
import '../../../../../styles/date_field.css';
import Webcam from './Webcam.jsx';

const DialogActions = withStyles((theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(1),
  },
}))(MuiDialogActions);

class Form extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: false,
      validator: [],
      value: [],
      loader: false,
      type: '',
      activeTabs: 0,
      Proses: false,
      ImgBase124: [],
      section: 0,
      id: null,
      redirect: false,
      date: new Date(),
      provin: [],
      data3: [],
      estimated_value: [],
      ltv_value: [],
      ltv: [],
      monthly_fee: [],
      dataBJ: [],
      cont: 0,
      brutoDisabled: false,
      insuranceItemVal: '',
      activeStep: 0,
      TIIA: [],
      installmentAttrb: [],
    };
    this.timeout = 0;
  }

  removeValidate(name) {
    const data = this.state.validator;
    delete data[name];
    this.setState({ validator: data });
  }

  handleChangeDesc(key) {
    const amount = this.state.value[`amount${key}`] || '';
    const insuranceItem = this.state.value[`product_insurance_item_id${key}`];
    const carats = this.state.value[`carats${key}`];
    const netto = this.state.value[`net_weight${key}`];
    let insuranceValue = '';
    if (insuranceItem === Object(insuranceItem)) insuranceValue = Object.values(insuranceItem)[1];
    else insuranceValue = '';
    const valState = this.state.value;
    valState[`description${key}`] = `${amount} ${insuranceValue} ${
      carats ? carats + ' karat' : ''
    } ${netto ? 'berat ' + netto + ' gram' : ''}`;
    this.setState({ value: valState });
  }

  handleNetto(insuranceType) {
    if (insuranceType.label === 'Logam Mulia') this.setState({ brutoDisabled: true });
    else this.setState({ brutoDisabled: false });
    this.setState({ insuranceItemVal: insuranceType.label });
  }

  handleChange(event, name) {
    if (name == 'customer_id') {
      this.getCustomerDetail(event.value);
    }

    if (name == 'insurance_item_id') {
      this.getInsuranceProd(event.value, '', '');
    }

    if (name == 'product_id') {
      const val = this.state.value;
      val.data3 = [];
      this.setState(
        {
          value: val,
        },
        () => {
          this.getDueDates(event.value);
          this.getInsurance(event.value);
        }
      );
    }

    const dataSet = this.state.value;
    dataSet[name] = event;
    this.setState({ value: dataSet });
  }

  getCustomerKTP(id) {
    fetch(
      process.env.REACT_APP_URL_MANAGEMENT
        + process.env.REACT_APP_API_PREFIX_V1
        + '/customer/identity_number/'
        + id,
      {
        method: 'GET',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + localStorage.getItem('token'),
        },
      }
    )
      .then((response) => response.json())
      .then((json) => {
        if (json.code == '403') {
          if (Func.Clear_Token() == true) {
            if (!localStorage.getItem('token')) {
              this.setState({ redirect: true });
            }
          }
        }
        if (json.code === 200) {
          const val = this.state.value;
          const { data } = json;
          val.phone_number = data.phone_number;
          val.gender = data.gender;
          val.customer_id = {
            value: data.id,
            label: data.name,
          };
          val.identity_type = data.identity_type;
          val.identity_number = data.identity_number;
          val.identity_address = data.address;
          this.setState({ value: val });
        } else this.setState({ value: [] });
      })
      .catch((error) => {})
      .finally(() => {});
  }

  getCustomerDetail(id) {
    fetch(
      process.env.REACT_APP_URL_MANAGEMENT
        + process.env.REACT_APP_API_PREFIX_V1
        + '/customer/'
        + id
        + '/transaction',
      {
        method: 'GET',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + localStorage.getItem('token'),
        },
      }
    )
      .then((response) => response.json())
      .then((json) => {
        if (json.code == '403') {
          if (Func.Clear_Token() == true) {
            if (!localStorage.getItem('token')) {
              this.setState({ redirect: true });
            }
          }
        }
        const val = this.state.value;
        const { data } = json;
        val.phone_number = data.phone_number;
        val.gender = data.gender;
        val.identity_type = data.identity_type;
        val.identity_number = data.identity_number;
        val.identity_address = data.address;
        this.setState({ value: val });
      })
      .catch((error) => {})
      .finally(() => {});
  }

  getInsurance(id) {
    fetch(
      process.env.REACT_APP_URL_MASTER
        + process.env.REACT_APP_API_PREFIX_V1
        + '/product/'
        + id
        + '/insurance_item/',
      {
        method: 'GET',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + localStorage.getItem('token'),
        },
      }
    )
      .then((response) => response.json())
      .then((json) => {
        if (json.code == '403') {
          if (Func.Clear_Token() == true) {
            if (!localStorage.getItem('token')) {
              this.setState({ redirect: true });
            }
          }
        }

        const datas = [];
        datas.push({
          value: '-',
          label:
            json.data.insurance_item_product.insurance_items.length > 0
              ? 'Pilih'
              : 'Tidak ditemukan',
          isDisabled: true,
        });

        json.data.insurance_item_product.insurance_items.map((value) => {
          datas.push({ value: value.id.$oid, label: value.name });
        });
        this.setState({
          data3: datas,
          data3_ori: json.data.insurance_item_product.insurance_items,
        });
      })
      .catch((error) => {})
      .finally(() => {});
  }

  getDueDates(id) {
    fetch(
      process.env.REACT_APP_URL_MASTER
        + process.env.REACT_APP_API_PREFIX_V1
        + '/due_dates?product_id='
        + id
        + '&contract_date='
        + dayjs().format('DD-MM-YYYY'),
      {
        method: 'GET',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + localStorage.getItem('token'),
        },
      }
    )
      .then((response) => response.json())
      .then((json) => {
        if (json.code == '403') {
          if (Func.Clear_Token() == true) {
            if (!localStorage.getItem('token')) {
              this.setState({ redirect: true });
            }
          }
        }

        const val = this.state.value;
        val.due_date = json.data.due_date;
        val.auction_date = json.data.auction_date;
        val.contract_date = dayjs().format('YYYY-MM-DD');
        this.setState({ value: val });
      })
      .catch((error) => {})
      .finally(() => {});
  }

  getInsuranceProd(id = '', id2, idx) {
    this.setState(
      {
        data5: [],
      },
      () => {
        fetch(
          process.env.REACT_APP_URL_MASTER
            + process.env.REACT_APP_API_PREFIX_V1
            + '/insurance_item/'
            + (id == '' ? this.state.value.insurance_item_id.value : id),
          {
            method: 'GET',
            headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json',
              Authorization: 'Bearer ' + localStorage.getItem('token'),
            },
          }
        )
          .then((response) => response.json())
          .then((json) => {
            if (json.code == '403') {
              if (Func.Clear_Token() == true) {
                if (!localStorage.getItem('token')) {
                  this.setState({ redirect: true });
                }
              }
            }
            const datas = [];
            datas.push({
              value: '-',
              label:
                json.data.insurance_item_types.length > 0
                  ? 'Pilih'
                  : 'Tidak ditemukan',
              isDisabled: true,
            });
            json.data.insurance_item_types.map((value) => {
              datas.push({ value: value._id.$oid, label: value.name });
            });
            this.setState({ data5: datas });

            const _id = id2;
            if (id2 !== '') {
              const search = json.data.insurance_item_types.find(
                (o) => o._id.$oid === _id
              );
              if (search !== undefined) {
                const val = this.state.value;
                val['product_insurance_item_id' + idx] = {
                  value: search._id.$oid,
                  label: search.name,
                };
                this.setState({ value: val });
              }
            }
          })
          .catch((error) => {})
          .finally(() => {});
      }
    );
  }

  handleChangeDate(date, name) {
    const dt = new Date(date);
    const dataSet = this.state.value;
    dataSet[name] = dt;
    this.setState({ value: dataSet });
  }

  componentDidMount() {
    this.setState({ loader: true });
    if (this.props.type == 'Ubah') {
      this.setState({ loader: true });
      fetch(
        process.env.REACT_APP_URL_MANAGEMENT
          + process.env.REACT_APP_API_PREFIX_V1
          + '/installments/'
          + this.props.id,
        {
          method: 'GET',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: 'Bearer ' + localStorage.getItem('token'),
          },
        }
      )
        .then((response) => response.json())
        .then((json) => {
          this.setState({ loader: false });
          if (json.code == '403') {
            if (Func.Clear_Token() == true) {
              if (!localStorage.getItem('token')) {
                this.setState({ redirect: true });
              }
            }
          }
          const data = json.data.installment;
          this.getCusto('', '', data.customer.name);
          this.getProd('', data.product_id, '');
          const val = this.state.value;
          try {
            val.customer_id = {
              value: data.customer.id,
              label: data.customer.name,
            };
            val.product_id = {
              value: data.product_id,
              label: data.product_name,
            };
            val.product_name = data.product_name;
            val.identity_address = data.customer.address;
            val.insurance_item_id = {
              value: data.insurance_item_id,
              label: data.insurance_item_name,
            };

            val.buy_price = data?.buy_price;
            val.estimated_value = data.estimate_value;
            val.ltv = data?.ltv;

            val.minimum_down_payment = 0;
            val.tenor = data.installment_detail.tenor;
            val.monthly_interest = data.installment_detail.monthly_interest;
            val.loan_amount = data.loan_amount;
            val.monthly_installment = data.installment_detail.monthly_installment;
            val.monthly_fee = data.installment_detail.monthly_fee;
            val.admin_fee = data.admin_fee;

            val.contract_date = data.contract_date;
            val.due_date = data.due_date;
            val.auction_date = data.auction_date;

            data.transaction_insurance_items.map((value, index) => {
              this.addBJ(value.id);
              this.getInsuranceProd(
                data.insurance_item_id,
                value.product_insurance_item_id,
                index
              );
              val['name' + index] = value.name;
              val['ownership' + index] = {
                value: value.ownership,
                label: value.ownership,
              };
              val['amount' + index] = value.amount;
              val['net_weight' + index] = value.net_weight;
              val['gross_weight' + index] = value.gross_weight;
              val['carats' + index] = value.carats;
              val['description' + index] = value.description;
              val['Insurance_item_image' + index] = value.insurance_item_image.url;
              val['buy_price' + index] = value.buy_price || 0;
            });
          } catch (err) {
            this.setState({ loader: false });
            alert('terjadi kesalahan sistem');
          }

          const mapData = Object.assign(val, data.customer);

          this.setState({
            loader: false,
            value: mapData,
            data4: data.customer,
            approvals: data.approvals,
            download: data.download == undefined ? null : data.download,
          });
        })
        .catch((error) => {})
        .finally(() => {});
    } else {
      const valueState = this.state.value;
      valueState.contract_date = dayjs().format('YYYY-MM-DD');
      this.setState({ valueState });
      this.getProd('', '', '');
      this.getCusto('', '', '');
      this.addBJ();
    }
  }

  getProd = (val, id, name) => {
    this.setState({ isloader: true });
    service
      .getProductInstallment()
      .then((response) => response.json())
      .then((res) => {
        if (res.code == '403') {
          if (Func.Clear_Token() == true) {
            if (!localStorage.getItem('token')) {
              this.setState({ redirect: true });
            }
          }
        } else {
          const datas = [];
          datas.push({
            value: '-',
            label: res.data.length > 0 ? 'Pilih' : 'Tidak ditemukan',
            isDisabled: true,
          });

          res.data.map((value) => {
            datas.push({ value: value.id.$oid, label: value.name });
          });
          if (id !== '') {
            const search = res.data.find((o) => o.id.$oid === id);
            if (search == undefined) {
              datas.push({ value: id, label: name });
            }
            if (search !== undefined) {
              const val = this.state.value;
              val.product_id = {
                value: search.id.$oid,
                label: search.name,
              };
            }
          }

          this.setState({
            data2: datas,
            data2_ori: res.data,
            loader: false,
          });
        }
      });
  };

  getCusto = (val, id, name) => {
    fetch(
      process.env.REACT_APP_URL_MANAGEMENT
        + process.env.REACT_APP_API_PREFIX_V1
        + '/customer/autocomplete'
        + '?query='
        + val
        + '&office_id='
        + localStorage.getItem('office_id'),
      {
        method: 'GET',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + localStorage.getItem('token'),
        },
      }
    )
      .then((response) => response.json())
      .then((json) => {
        if (json.code == '403') {
          if (Func.Clear_Token() == true) {
            if (!localStorage.getItem('token')) {
              this.setState({ redirect: true });
            }
          }
        } else {
          const datas = [];
          datas.push({
            value: '-',
            label: json.data.length > 0 ? 'Pilih' : 'Tidak ditemukan',
            isDisabled: true,
          });
          json.data.map((value) => {
            datas.push({ value: value.id, label: value.name });
          });
          this.setState({ data4: datas, data4_ori: json.data, loader: false });
        }
      })
      .catch((error) => {})
      .finally(() => {});
  };

  handleFirstStepForm() {
    const validator = [
      {
        name: 'insurance_item_id',
        type: 'required',
      },
      {
        name: 'customer_id',
        type: 'required',
      },
      {
        name: 'product_id',
        type: 'required',
      },
      {
        name: 'contract_date',
        type: 'required',
      },
      {
        name: 'due_date',
        type: 'required',
      },
      {
        name: 'auction_date',
        type: 'required',
      },
      {
        name: 'identity_number',
        type: 'required',
      },
      {
        name: 'identity_address',
        type: 'required',
      },
      {
        name: 'phone_number',
        type: 'required',
      },
      {
        name: 'gender',
        type: 'required',
      },
      {
        name: 'gender',
        type: 'required',
      },
      {
        name: 'gender',
        type: 'required',
      },
      {
        name: 'gender',
        type: 'required',
      },
    ];

    this.state.dataBJ.map((value, index) => {
      if (!value._destroy) {
        validator.push({
          name: 'product_insurance_item_id' + value.key,
          type: 'required',
        });
        validator.push({
          name: 'ownership' + value.key,
          type: 'required',
        });
        validator.push({
          name: 'amount' + value.key,
          type: 'required',
        });
        validator.push({
          name: 'gross_weight' + value.key,
          type: 'required|maxInt:1000',
        });
        validator.push({
          name: 'net_weight' + value.key,
          type: 'required|maxInt:1000',
        });
        validator.push({
          name: 'carats' + value.key,
          type: 'required|minInt:24',
        });
        validator.push({
          name: 'description' + value.key,
          type: 'required',
        });
        validator.push({
          name: 'Insurance_item_image' + value.key,
          type: 'required',
        });
        validator.push({
          name: 'buy_price' + value.key,
          type: 'required',
        });
      }
    });

    const validate = Func.Validator(this.state.value, validator);

    if (validate.success) {
      const stepCount = this.state.activeStep;
      this.setState({ activeStep: stepCount + 1 });

      const TIIA = [];
      this.state.dataBJ.map((value, index) => {
        const estimated_value = 0;
        if (value._destroy) {
          TIIA.push({ id: value.id, _destroy: value._destroy });
        } else if (value.id == null) {
          TIIA.push({
            name: this.state.value['product_insurance_item_id' + value.key]
              .label,
            product_insurance_item_id: this.state.value[
              'product_insurance_item_id' + value.key
            ].value,
            product_insurance_item_name: this.state.value[
              'product_insurance_item_id' + value.key
            ].label,
            ownership: this.state.value['ownership' + value.key].value,
            amount: this.state.value['amount' + value.key],
            gross_weight: this.state.value['gross_weight' + value.key],
            net_weight: this.state.value['net_weight' + value.key],
            carats: this.state.value['carats' + value.key],
            estimated_value: this.state.estimated_value[index],
            description: this.state.value['description' + value.key],
            insurance_item_image: this.state.ImgBase124[
              'Insurance_item_image' + value.key
            ],
            buy_price: this.state.value['buy_price' + value.key],
            _destroy: value._destroy,
          });
        } else {
          TIIA.push({
            id: value.id,
            name: this.state.value['product_insurance_item_id' + value.key]
              .label,
            product_insurance_item_id: this.state.value[
              'product_insurance_item_id' + value.key
            ].value,
            product_insurance_item_name: this.state.value[
              'product_insurance_item_id' + value.key
            ].label,
            ownership: this.state.value['ownership' + value.key].value,
            amount: this.state.value['amount' + value.key],
            gross_weight: this.state.value['gross_weight' + value.key],
            net_weight: this.state.value['net_weight' + value.key],
            carats: this.state.value['carats' + value.key],
            estimated_value: this.state.estimated_value[index],
            description: this.state.value['description' + value.key],
            insurance_item_image: this.state.ImgBase124[
              'Insurance_item_image' + value.key
            ],
            _destroy: value._destroy,
          });
        }
      });
      this.setState({ TIIA });

      const insuranceItem = [];
      this.state.dataBJ.map((value) => {
        if (!value._destroy) {
          const productName = this.state.value['product_insurance_item_id' + value.key]?.label
            || '';
          const upperCaseFirstLetter = productName
            ? productName[0].toUpperCase() + productName.slice(1)
            : '';
          insuranceItem.push({
            name: upperCaseFirstLetter,
            weight: parseInt(this.state.value['net_weight' + value.key]),
            amount: parseInt(this.state.value['amount' + value.key]),
            buy_price: parseInt(this.state.value['buy_price' + value.key]),
            karats: parseInt(this.state.value['carats' + value.key]),
          });
        }
      });
      this.setState({ installmentAttrb: insuranceItem });
      const params = {
        installment: {
          product_id: this.state.value.product_id?.value,
          insurance_item_id: this.state.value.insurance_item_id?.value,
          insurance_items: insuranceItem,
        },
      };
      service
        .checkInstallmentAttrb(params)
        .then((res) => res.json())
        .then((json) => {
          if (json.code == '403') {
            if (Func.Clear_Token() == true) {
              if (!localStorage.getItem('token')) {
                this.setState({ redirect: true });
              }
            }
          } else if (json.code == 400) {
            swal({
              title: 'Gagal mengecek barang jaminan',
              text: json.message,
              icon: 'error',
              buttons: 'OK',
            });
          } else {
            const val = this.state.value;
            const { data } = json;
            val.buy_price = data.buy_price;
            val.estimated_value = data.estimated_value;
            val.ltv = data.ltv;
            val.maximum_financing_amount = data.maximum_financing_amount;
            val.minimum_down_payment = data.minimum_down_payment;
            val.down_payment = data.minimum_down_payment;
            val.monthly_interest = data.monthly_interest;
            this.setState({ value: val });
          }
        });
    } else {
      this.setState({ validator: validate.error });
    }
  }

  getInstallmentCost(tenor) {
    const params = {
      installment: {
        product_id: this.state.value.product_id?.value,
        insurance_item_id: this.state.value.insurance_item_id?.value,
        tenor: tenor.value,
        down_payment: this.state.value.down_payment,
        insurance_items: this.state.installmentAttrb,
      },
    };
    service
      .installmentCostApi(params)
      .then((res) => res.json())
      .then((json) => {
        if (json.code == '403') {
          if (Func.Clear_Token() == true) {
            if (!localStorage.getItem('token')) {
              this.setState({ redirect: true });
            }
          }
        } else if (json.code == 400) {
          swal({
            title: 'Gagal mendapatkan biaya simulasi',
            text: json.message,
            icon: 'error',
            buttons: 'OK',
          });
        } else {
          const val = this.state.value;
          const { data } = json;
          val.loan_amount = data.loan_amount;
          val.monthly_installment = data.monthly_installment;
          val.installment_before_interest = data.installment_before_interest;
          val.total_down_payment = data.total_down_payment;
          val.monthly_fee = data.monthly_fee;
          val.admin_fee = data.admin_fee;
          this.setState({ value: val });
        }
      });
  }

  handleSubmit(type) {
    const { down_payment } = this.state.validator;

    if (!down_payment) {
      this.setState({ loader_button: true });
      fetch(
        process.env.REACT_APP_URL_MANAGEMENT
          + process.env.REACT_APP_API_PREFIX_V1
          + '/installments/'
          + (type == 'Tambah' ? '' : this.props.id),
        {
          method: type == 'Tambah' ? 'POST' : 'PUT',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: 'Bearer ' + localStorage.getItem('token'),
          },
          body: JSON.stringify({
            installment: {
              insurance_item_id: this.state.value.insurance_item_id.value,
              insurance_item_name: this.state.value.insurance_item_id.label,
              customer_id: this.state.value.customer_id.value,
              transaction_type: 'installment',
              product_id: this.state.value.product_id.value,
              product_name: this.state.value.product_id.label,
              status: 'waiting_approval',
              contract_date: this.state.value.contract_date,
              due_date: this.state.value.due_date,
              auction_date: this.state.value.auction_date,
              loan_amount: this.state.value.loan_amount,
              maximum_loan_percentage: this.state.value.ltv,
              maximum_loan: this.state.value.maximum_financing_amount,
              admin_fee: this.state.value.admin_fee,
              monthly_fee: this.state.value.monthly_fee,
              estimated_value: this.state.value.estimated_value,
              transaction_insurance_items_attributes: this.state.TIIA,
              installment_detail_attributes: {
                tenor: this.state.value.tenor,
                monthly_interest: this.state.value.monthly_interest,
                monthly_fee: this.state.value.monthly_fee,
                monthly_installment: this.state.value.monthly_installment,
              },
            },
          }),
        }
      )
        .then((response) => response.json())
        .then((json) => {
          if (json.code == '403') {
            Func.Refresh_Token();
            if (Func.Refresh_Token() == true) {
              this.handleSubmit();
            }
          } else if (json.code === 400) {
            swal({
              title: 'Kesalahan',
              text: json.message,
              icon: 'error',
            });
          }
          if (type == 'Tambah') {
            if (json.created) {
              this.props.OnNext('Transaksi berhasil dibuat');
              this.setState({ loader_button: false });
            } else if (json.code !== 400) {
              this.setState({ validator: json.status });
              this.setState({ loader_button: false });
            } else {
              this.setState({ loader_button: false });
            }
          } else if (json.code == 200) {
            this.props.OnNext(json.message);
            this.setState({ loader_button: false });
          } else {
            this.setState({ validator: json.status });
            this.setState({ loader_button: false });
          }
        })
        .catch((error) => {})
        .finally(() => {});
    }
  }

  handleWebcam(img64, stateName) {
    const dataSet = this.state.value;
    dataSet[stateName] = img64;
    this.setState({ value: dataSet });
    const { ImgBase124 } = this.state;
    ImgBase124[stateName] = img64;
    this.setState({ ImgBase124 });
  }

  handleChangeImg(event, name) {
    const file = event.target.files[0];
    if (file.size > 2.9e6) {
      swal({
        title: 'File Terlalu Besar',
        text: 'Maximal File 2Mb',
        icon: 'error',
        buttons: 'OK',
      });
      return false;
    }
    this.removeValidate('img');
    const dataSet = this.state.value;
    dataSet[name] = URL.createObjectURL(event.target.files[0]);
    this.setState({ value: dataSet });
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onloadend = () => {
      const { ImgBase124 } = this.state;
      ImgBase124[name] = reader.result;
      this.setState({ ImgBase124 });
    };
  }

  renderBJ() {
    const { classes } = this.props;
    const data = [];
    let key = 1;
    this.state.dataBJ.map((value, index) => {
      if (!value._destroy) {
        data.push(
          <div>
            <div
              className={classes.BodytitleMdl2}
              style={{
                marginTop: 50,
              }}
            >
              <text className={classes.titleMdl}>
                BARANG JAMINAN
                {key}
              </text>

              <text
                style={{
                  color: 'red',
                  marginLeft: 20,
                  cursor: 'default',
                  textDecoration: 'underline',
                }}
                onClick={() => {
                  if (key == 2) {
                    alert('Minimum 1');
                  } else {
                    const datas2 = this.state.dataBJ;
                    const val = this.state.value;
                    const est = this.state.estimated_value;
                    val['name' + datas2[index].key] = '';
                    val['product_insurance_item_id' + datas2[index].key] = '';
                    val['ownership' + datas2[index].key] = '';
                    val['amount' + datas2[index].key] = '';
                    val['weight' + datas2[index].key] = '';
                    val['carats' + datas2[index].key] = '';
                    val['description' + datas2[index].key] = '';
                    val['buy_price' + datas2[index].key] = '';
                    val['Insurance_item_image' + datas2[index].key] = '';
                    est[datas2[index].key] = 0;
                    let cont_estimated_value = 0;
                    for (let index = 0; index < est.length; index++) {
                      cont_estimated_value += est[index];
                    }
                    if (cont_estimated_value == 0) {
                      val.loan_amount = 0;
                      val.admin_fee = 0;
                      val.monthly_fee = 0;
                      this.removeValidate('loan_amount');
                    }
                    this.setState(
                      {
                        value: val,
                        estimated_value: est,
                      },
                      () => {
                        datas2[index]._destroy = true;
                        this.setState({ dataBJ: datas2 });
                      }
                    );
                  }
                }}
              >
                Hapus
              </text>
            </div>
            <Grid
              container
              direction="row"
              item
              lg={12}
              xl={12}
              md={12}
              xs={12}
            >
              <Grid container item lg={8} xl={8} md={8} sm={8} xs={12}>
                <Grid container item lg={12} xl={12} md={12} sm={12} xs={12}>
                  <Grid
                    item
                    lg={6}
                    xl={6}
                    md={6}
                    sm={6}
                    xs={12}
                    className={classes.formPad}
                  >
                    <div
                      style={{
                        marginBottom: 5,
                      }}
                    >
                      <text className={classes.label1}>
                        Kategori Barang Jaminan
                      </text>
                      <text className={classes.starts1}>*</text>
                    </div>
                    <div>
                      <Select2
                        name="form-field-name-error"
                        value={
                          this.state.value[
                            'product_insurance_item_id' + value.key
                          ] || ''
                        }
                        isDisabled={
                          this.state.value.product_id == undefined
                          || this.state.value.insurance_item_id == undefined
                          || this.state.value.product_id == ''
                          || this.state.value.insurance_item_id == ''
                        }
                        placeholder="Pilih"
                        onFocus={() => {
                          this.removeValidate(
                            'product_insurance_item_id' + value.key
                          );
                        }}
                        styles={{
                          control: (provided, state) => ({
                            ...provided,
                            borderColor: this.state.validator[
                              'product_insurance_item_id' + value.key
                            ]
                              ? 'red'
                              : '#CACACA',
                            borderRadius: '0.25rem',
                          }),
                        }}
                        className={classes.input21}
                        options={this.state.data5}
                        onChange={(val) => {
                          const setter = this.state.value;
                          setter['weight' + value.key] = '';
                          setter['carats' + value.key] = val.label === 'antam' ? '24' : '';
                          this.setState(
                            {
                              same: false,
                              value: setter,
                            },
                            () => {
                              this.handleChange(
                                val,
                                'product_insurance_item_id' + value.key
                              );
                              this.handleChangeDesc(value.key);
                            }
                          );
                        }}
                      />

                      <FormHelperText className={classes.error}>
                        {
                          this.state.validator[
                            'product_insurance_item_id' + value.key
                          ]
                        }
                      </FormHelperText>
                    </div>
                  </Grid>
                  <Grid
                    container
                    lg={6}
                    xl={6}
                    md={6}
                    sm={6}
                    xs={12}
                    className={classes.formPad}
                  >
                    <Grid
                      item
                      lg={6}
                      xl={6}
                      md={6}
                      sm={6}
                      xs={12}
                      className={classes.formPad}
                    >
                      <div>
                        <text className={classes.label1}>Jumlah</text>
                        <text className={classes.starts1}>*</text>
                      </div>
                      <div>
                        <TextField
                          size="small"
                          style={{
                            marginTop: 5,
                          }}
                          className={classes.input234}
                          variant="outlined"
                          type="number"
                          autoComplete="off"
                          onFocus={() => {
                            this.removeValidate('amount' + value.key);
                          }}
                          error={this.state.validator['amount' + value.key]}
                          value={this.state.value['amount' + value.key] || ''}
                          onChange={(event) => {
                            this.handleChange(
                              event.target.value,
                              'amount' + value.key
                            );
                            this.handleChangeDesc(value.key);
                          }}
                          name={'amount' + value.key}
                        />
                      </div>
                    </Grid>
                    <Grid
                      item
                      lg={6}
                      xl={6}
                      md={6}
                      sm={6}
                      xs={12}
                      className={classes.formPad}
                    >
                      <div>
                        <text className={classes.label1}>Karatase</text>
                        <text className={classes.starts1}>*</text>
                      </div>
                      <div>
                        <TextField
                          disabled={
                            this.state.value['carats' + value.key] === '24'
                            && this.state.brutoDisabled
                          }
                          size="small"
                          style={{
                            marginTop: 5,
                          }}
                          className={classes.input234}
                          variant="outlined"
                          type="number"
                          autoComplete="off"
                          onFocus={() => {
                            this.removeValidate('carats' + value.key);
                          }}
                          error={this.state.validator['carats' + value.key]}
                          value={this.state.value['carats' + value.key] || ''}
                          onChange={(event) => {
                            this.handleChange(
                              event.target.value,
                              'carats' + value.key
                            );
                            this.handleChangeDesc(value.key);
                          }}
                          name={'carats' + value.key}
                        />
                      </div>
                    </Grid>
                  </Grid>
                </Grid>
                <Grid container item lg={12} xl={12} md={12} sm={12} xs={12}>
                  <Grid
                    item
                    lg={6}
                    xl={6}
                    md={6}
                    sm={6}
                    xs={12}
                    className={classes.formPad}
                  >
                    <div>
                      <div className={classes.label111}>
                        <text className={classes.label1}>
                          Kepemilikan Barang Jaminan
                        </text>
                        <text className={classes.starts1}>*</text>
                      </div>
                      <Select2
                        name="form-field-name-error"
                        value={this.state.value['ownership' + value.key] || ''}
                        placeholder="Pilih"
                        onFocus={() => {
                          this.removeValidate('ownership' + value.key);
                        }}
                        styles={{
                          control: (provided, state) => ({
                            ...provided,
                            borderColor: this.state.validator[
                              'ownership' + value.key
                            ]
                              ? 'red'
                              : '#CACACA',
                            borderRadius: '0.25rem',
                          }),
                        }}
                        onChange={(data) => {
                          this.handleChange(data, 'ownership' + value.key);
                        }}
                        className={classes.input21}
                        options={[
                          {
                            value: 'Milik Sendiri',
                            label: 'Milik Sendiri',
                          },
                          {
                            value: 'Milik Keluarga',
                            label: 'Milik Keluarga',
                          },
                          {
                            value: 'Warisan',
                            label: 'Warisan',
                          },
                          {
                            value: 'Hasil Usaha',
                            label: 'Hasil Usaha',
                          },
                        ]}
                      />
                    </div>
                  </Grid>
                  <Grid
                    container
                    lg={6}
                    xl={6}
                    md={6}
                    sm={6}
                    xs={12}
                    className={classes.formPad}
                  >
                    <Grid
                      item
                      lg={6}
                      xl={6}
                      md={6}
                      sm={6}
                      xs={12}
                      className={classes.formPad}
                    >
                      <div>
                        <text className={classes.label1}>Berat Bersih</text>
                        <text className={classes.starts1}>*</text>
                      </div>
                      <div>
                        <TextField
                          size="small"
                          style={{
                            marginTop: 5,
                          }}
                          className={classes.input234}
                          variant="outlined"
                          type="number"
                          autoComplete="off"
                          onFocus={() => {
                            this.removeValidate('net_weight' + value.key);
                          }}
                          error={this.state.validator['net_weight' + value.key]}
                          value={
                            this.state.value['net_weight' + value.key] || ''
                          }
                          onChange={(event) => {
                            const setter = this.state.value;
                            const netValue = this.state.insuranceItemVal === 'Logam Mulia'
                                ? event.target.value
                                : '';
                            setter['net_weight' + value.key] = event.target.value;
                            setter['gross_weight' + value.key] = netValue;
                            this.setState({ value: setter });
                            this.handleChangeDesc(value.key);
                          }}
                          name={'net_weight' + value.key}
                        />
                      </div>
                    </Grid>
                    <Grid
                      item
                      lg={6}
                      xl={6}
                      md={6}
                      sm={6}
                      xs={12}
                      className={classes.formPad}
                    >
                      <div>
                        <text className={classes.label1}>Berat Kotor</text>
                        <text className={classes.starts1}>*</text>
                      </div>
                      <div>
                        <TextField
                          disabled={this.state.brutoDisabled}
                          size="small"
                          style={{
                            marginTop: 5,
                          }}
                          className={classes.input234}
                          variant="outlined"
                          type="number"
                          autoComplete="off"
                          onFocus={() => {
                            this.removeValidate('gross_weight' + value.key);
                          }}
                          error={
                            this.state.validator['gross_weight' + value.key]
                          }
                          value={
                            this.state.value['gross_weight' + value.key] || ''
                          }
                          onChange={(event) => {
                            this.handleChange(
                              event.target.value,
                              'gross_weight' + value.key
                            );
                          }}
                          name={'gross_weight' + value.key}
                        />
                      </div>
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
              <Grid
                container
                item
                lg={4}
                xl={4}
                md={4}
                sm={4}
                xs={12}
                spacing={3}
              >
                <Grid
                  item
                  lg={12}
                  xl={12}
                  md={12}
                  sm={12}
                  xs={12}
                  className={classes.formPad}
                >
                  <div>
                    <text className={classes.label1}>Deskripsi</text>
                    <text className={classes.starts1}>*</text>
                  </div>
                  <TextareaAutosize
                    className={
                      this.state.validator['description' + value.key]
                        ? classes.textArea2
                        : classes.textArea
                    }
                    variant="outlined"
                    margin="normal"
                    rows={4.8}
                    autoComplete="off"
                    onFocus={() => {
                      this.removeValidate('description' + value.key);
                    }}
                    error={this.state.validator['description' + value.key]}
                    value={this.state.value['description' + value.key] || ''}
                    onChange={(event) => {
                      this.handleChange(
                        event.target.value,
                        'description' + value.key
                      );
                    }}
                    name={'description' + value.key}
                    InputProps={{
                      endAdornment: this.state.validator[
                        'description' + value.key
                      ] ? (
                        <InputAdornment position="start">
                          <img src={Icon.warning} />
                        </InputAdornment>
                      ) : (
                        <div />
                      ),
                    }}
                  />
                  <FormHelperText className={classes.error}>
                    {this.state.validator['description' + value.key]}
                  </FormHelperText>
                </Grid>
              </Grid>
              <Grid
                container
                direction="row"
                item
                lg={12}
                xl={12}
                md={12}
                xs={12}
              >
                <Grid item lg={4} xl={4} md={4} sm={4} xs={12}>
                  <div className={classes.label111}>
                    <text className={classes.label1}>Foto Barang</text>
                    <text className={classes.starts1}>*</text>
                  </div>
                  <Box
                    borderColor={
                      this.state.validator['Insurance_item_image' + value.key]
                        ? 'error.main'
                        : 'grey.500'
                    }
                    border={1}
                    onClick={() => {
                      this.removeValidate('Insurance_item_image' + value.key);
                    }}
                    className={classes.imgScan}
                  >
                    {this.state.value['Insurance_item_image' + value.key] ? (
                      <img
                        className={classes.imgScanInstallment}
                        onClick={() => {
                          this.removeValidate(
                            'Insurance_item_image' + value.key
                          );
                        }}
                        src={
                          this.state.value['Insurance_item_image' + value.key]
                        }
                      />
                    ) : null}
                  </Box>
                  <FormHelperText className={classes.error}>
                    {this.state.validator['Insurance_item_image' + value.key]}
                  </FormHelperText>
                  <div className={classes.BodytitleMdl22}>
                    <img
                      src={Icon.deleteImg}
                      onClick={() => {
                        const dataSet = this.state.value;
                        dataSet['Insurance_item_image' + value.key] = null;
                        this.setState({ value: dataSet });
                      }}
                    />
                  </div>
                  <div className={classes.BodytitleMdl23}>
                    <input
                      type="file"
                      accept="image/*"
                      name="file"
                      title="Pilih Gambar"
                      onChange={(event) => {
                        this.handleChangeImg(
                          event,
                          'Insurance_item_image' + value.key
                        );
                      }}
                    />
                    Atau
{' '}
                    <Webcam
                      setState={this.setState.bind(this)}
                      handleWebcam={this.handleWebcam.bind(this)}
                      stateName={`Insurance_item_image${value.key}`}
                    />
                  </div>
                </Grid>
                <Grid item lg={4} xl={4} md={4} sm={4} xs={12}>
                  <div className={classes.label111}>
                    <text className={classes.label1}>Harga Beli</text>
                    <text className={classes.starts1}>*</text>
                  </div>
                  <TextField
                    size="small"
                    className={classes.input2}
                    variant="outlined"
                    autoComplete="off"
                    onFocus={() => {
                      this.removeValidate('buy_price' + value.key);
                    }}
                    error={this.state.validator['buy_price' + value.key]}
                    helperText={this.state.validator['buy_price' + value.key]}
                    value={
                      this.state.value['buy_price' + value.key] !== undefined
                        ? Func.FormatNumber(
                            this.state.value['buy_price' + value.key]
                          )
                        : ''
                    }
                    onChange={(event) => {
                      this.removeValidate('buy_price' + value.key);
                      this.handleChange(
                        Func.UnFormatRp(event.target.value),
                        'buy_price' + value.key
                      );
                    }}
                    InputProps={{
                      startAdornment: (
                        <InputAdornment position="start">Rp</InputAdornment>
                      ),
                    }}
                    name="buy_price"
                  />
                </Grid>
              </Grid>
            </Grid>
          </div>
        );
        key++;
      }
    });
    return data;
  }

  addBJ(id) {
    const data = this.state.dataBJ;
    data.push({
      _destroy: false,
      key: this.state.cont,
      id: id == undefined ? null : id,
    });
    this.setState({
      dataBJ: data,
      cont: this.state.cont + 1,
    });
  }

  render() {
    const ExampleCustomInput = ({ value, onClick }) => (
      <img src={Icon.icon_date} onClick={onClick} />
    );
    const { classes } = this.props;
    const { value, validator } = this.state;
    const loadOptions = (inputValue, callback) => {
      setTimeout(() => {
        callback(this.state.data2);
      }, 600);
    };

    const loadOptions3 = (inputValue, callback) => {
      setTimeout(() => {
        callback(this.state.data4);
      }, 600);
    };

    const labelGender = (value.gender === 'l' && 'Laki - laki')
      || (value.gender === 'p' && 'Perempuan');

    const labelTenor = value.tenor === 3
        ? '3 Bulan'
        : value.tenor === 6
        ? '6 Bulan'
        : value.tenor === 12
        ? '12 Bulan'
        : 'Pilih';

    const stepLabel = [
      'Data Nasabah dan Barang Jaminan',
      'Perhitungan Biaya-biaya',
    ];

    const downPayment = this.state.value.down_payment;
    const minDownPayment = this.state.value.minimum_down_payment;
    if (downPayment < minDownPayment) {
      validator.down_payment = `Minimal ${minDownPayment} !`;
    }

    if (this.state.loader) {
      return (
        <div className={classes.root2}>
          <Dialog
            disablePortal
            disableEnforceFocus
            disableAutoFocus
            open
            scroll="paper"
            maxWidth="md"
            aria-labelledby="server-modal-title"
            aria-describedby="server-modal-description"
            container={() => {}}
          >
            <div
              style={{
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
                padding: '20px',
                borderRadius: '50px',
              }}
            >
              <CircularProgress
                style={{ color: '#85203B', margin: '18px' }}
                size={40}
              />
              Mohon Tunggu
              <div
                style={{
                  marginTop: '10px',
                }}
              />
            </div>
          </Dialog>
        </div>
      );
    }
    return (
      <Dialog
        scroll="paper"
        open
        maxWidth="md"
        aria-labelledby="scroll-dialog-title"
        aria-describedby="scroll-dialog-description"
      >
        <DialogTitle id="scroll-dialog-title">
          <Typography variant="h7" className={classes.tittleModal}>
            {this.props.type + ' Transaksi Cicilan'}
          </Typography>
          <IconButton
            disabled={this.state.loader_button}
            aria-label="close"
            className={classes.closeButton}
            onClick={() => {
              this.props.handleModal();
            }}
          >
            <CloseIcon />
          </IconButton>
        </DialogTitle>
        <DialogContent dividers>
          <DialogContentText id="scroll-dialog-description" tabIndex={-1}>
            <div className={classes.scrool}>
              {Func.toLogin(this.state.redirect)}
              <div className={classes.root}>
                <Stepper
                  activeStep={this.state.activeStep}
                  style={{ padding: '0px 24px' }}
                >
                  {stepLabel.map((label) => (
                    <Step key={label}>
                      <StepLabel>
                        <text style={{ fontSize: '1rem' }}>{label}</text>
                      </StepLabel>
                    </Step>
                  ))}
                </Stepper>
                <Grid
                  container
                  direction="row"
                  item
                  lg={12}
                  xl={12}
                  md={12}
                  xs={12}
                >
                  {this.state.activeStep === 0 && (
                    <Grid item lg={12} xl={12} md={12} sm={12} xs={12}>
                      <Divider className={classes.divider} />
                      <div className={classes.BodytitleMdl2}>
                        <text className={classes.titleMdl}>Data Nasabah</text>
                      </div>
                      <Grid
                        container
                        direction="row"
                        item
                        lg={12}
                        xl={12}
                        md={12}
                        xs={12}
                      >
                        <Grid
                          container
                          item
                          lg={8}
                          xl={8}
                          md={8}
                          sm={8}
                          xs={12}
                        >
                          <Grid
                            container
                            item
                            lg={12}
                            xl={12}
                            md={12}
                            sm={12}
                            xs={12}
                          >
                            <Grid
                              item
                              lg={6}
                              xl={6}
                              md={6}
                              sm={6}
                              xs={12}
                              className={classes.formPad}
                            >
                              <div className={classes.label111}>
                                <text className={classes.label1}>
                                  Nama Lengkap
                                </text>
                                <text className={classes.starts1}>*</text>
                              </div>
                              <AsyncSelect
                                name="form-field-name-error"
                                value={this.state.value.customer_id || ''}
                                placeholder="Cari Nasabah"
                                onFocus={() => {
                                  this.removeValidate('customer_id');
                                }}
                                styles={{
                                  control: (provided, state) => ({
                                    ...provided,
                                    borderColor: this.state.validator
                                      .customer_id
                                      ? 'red'
                                      : '#CACACA',
                                    borderRadius: '0.25rem',
                                  }),
                                }}
                                onInputChange={(val) => {
                                  if (val) {
                                    if (this.timeout) clearTimeout(this.timeout);
                                    this.timeout = setTimeout(() => {
                                      this.getCusto(val, '');
                                    }, 500);
                                  }
                                }}
                                cacheOptions
                                loadOptions={loadOptions3}
                                defaultOptions
                                className={classes.input21}
                                options={this.state.data4}
                                onChange={(val) => {
                                  this.handleChange(val, 'customer_id');
                                }}
                              />
                            </Grid>
                            <Grid
                              item
                              lg={6}
                              xl={6}
                              md={6}
                              sm={6}
                              xs={12}
                              className={classes.formPad}
                            >
                              <div>
                                <text className={classes.label2}>
                                  Nomor ID KTP
                                </text>
                                <text className={classes.starts1}>*</text>
                              </div>
                              <TextField
                                size="small"
                                className={classes.input8}
                                variant="outlined"
                                autoComplete="off"
                                onFocus={() => {
                                  this.removeValidate('identity_number');
                                }}
                                error={this.state.validator.identity_number}
                                helperText={
                                  this.state.validator.identity_number
                                }
                                value={this.state.value.identity_number || ''}
                                onChange={(event) => {
                                  this.handleChange(
                                    event.target.value,
                                    'identity_number'
                                  );
                                  this.getCustomerKTP(event.target.value);
                                }}
                                name="identity_number"
                              />
                            </Grid>
                            <Grid
                              item
                              lg={6}
                              xl={6}
                              md={6}
                              sm={6}
                              xs={12}
                              className={classes.formPad}
                            >
                              <div>
                                <text className={classes.label1}>Nomor HP</text>
                                <text className={classes.starts1}>*</text>
                              </div>
                              <TextField
                                size="small"
                                disabled
                                className={classes.input2}
                                variant="outlined"
                                autoComplete="off"
                                onFocus={() => {
                                  this.removeValidate('phone_number');
                                }}
                                error={this.state.validator.phone_number}
                                helperText={this.state.validator.phone_number}
                                value={this.state.value.phone_number || ''}
                                onChange={(event) => {
                                  this.handleChange(
                                    event.target.value,
                                    'phone_number'
                                  );
                                }}
                                name="phone_number"
                              />
                            </Grid>
                            <Grid
                              item
                              lg={6}
                              xl={6}
                              md={6}
                              sm={6}
                              xs={12}
                              className={classes.formPad}
                            >
                              <div>
                                <text className={classes.label1}>
                                  Jenis Kelamin
                                </text>
                                <text className={classes.starts1}>*</text>
                              </div>
                              <div>
                                <Select2
                                  isDisabled
                                  name="form-field-name-error"
                                  value={{
                                    value: this.state.value.gender,
                                    label: labelGender,
                                  }}
                                  placeholder="Pilih"
                                  onFocus={() => {
                                    this.removeValidate('gender');
                                  }}
                                  error
                                  styles={{
                                    control: (provided, state) => ({
                                      ...provided,
                                      borderColor: this.state.validator.gender
                                        ? 'red'
                                        : '#CACACA',
                                      borderRadius: '0.25rem',
                                    }),
                                  }}
                                  className={classes.input2}
                                  options={[
                                    {
                                      value: '-',
                                      label: 'Pilih',
                                      isDisabled: true,
                                    },
                                    {
                                      value: 'l',
                                      label: 'Laki - Laki',
                                    },
                                    {
                                      value: 'p',
                                      label: 'Perempuan',
                                    },
                                  ]}
                                  onChange={(val) => {
                                    this.handleChange(val, 'gender');
                                  }}
                                />
                                <FormHelperText className={classes.error}>
                                  {this.state.validator.gender}
                                </FormHelperText>
                              </div>
                            </Grid>
                          </Grid>
                        </Grid>
                        <Grid
                          container
                          item
                          lg={4}
                          xl={4}
                          md={4}
                          sm={4}
                          xs={12}
                          spacing={3}
                        >
                          <Grid
                            item
                            lg={12}
                            xl={12}
                            md={12}
                            sm={12}
                            xs={12}
                            className={classes.formPad}
                          >
                            <div>
                              <text className={classes.label1}>Alamat</text>
                              <text className={classes.starts1}>*</text>
                            </div>
                            <TextareaAutosize
                              className={
                                this.state.validator.identity_address
                                  ? classes.textArea2
                                  : classes.textArea
                              }
                              variant="outlined"
                              disabled
                              margin="normal"
                              rows={4.5}
                              autoComplete="off"
                              onFocus={() => {
                                this.removeValidate('identity_address');
                              }}
                              error={this.state.validator.identity_address}
                              value={this.state.value.identity_address || ''}
                              onChange={(event) => {
                                this.handleChange(
                                  event.target.value,
                                  'identity_address'
                                );
                              }}
                              name="identity_address"
                              InputProps={{
                                endAdornment: this.state.validator
                                  .identity_address ? (
                                  <InputAdornment position="start">
                                    <img src={Icon.warning} />
                                  </InputAdornment>
                                ) : (
                                  <div />
                                ),
                              }}
                            />
                            <FormHelperText className={classes.error}>
                              {this.state.validator.identity_address}
                            </FormHelperText>
                          </Grid>
                        </Grid>
                      </Grid>
                      <Divider className={classes.divider} />
                      <Grid
                        container
                        direction="row"
                        item
                        lg={12}
                        xl={12}
                        md={12}
                        xs={12}
                      >
                        <Grid
                          container
                          item
                          lg={4}
                          xl={4}
                          md={4}
                          sm={4}
                          xs={12}
                        >
                          <div className={classes.label111}>
                            <text className={classes.label1}>Produk Gadai</text>
                            <text className={classes.starts1}>*</text>
                          </div>
                          <Select2
                            name="form-field-name-error"
                            value={this.state.value.product_id || ''}
                            placeholder="Pilih"
                            onFocus={() => {
                              this.removeValidate('product_id');
                            }}
                            styles={{
                              control: (provided, state) => ({
                                ...provided,
                                borderColor: this.state.validator.product_id
                                  ? 'red'
                                  : '#CACACA',
                                borderRadius: '0.25rem',
                              }),
                            }}
                            onChange={(val) => {
                              const setter = this.state.value;
                              for (
                                let index = 0;
                                index < this.state.dataBJ;
                                index++
                              ) {
                                setter['weight' + index] = '';
                                setter['carats' + index] = '';
                                setter['product_insurance_item_id' + index] = '';
                              }
                              this.setState(
                                {
                                  same: false,
                                  value: setter,
                                },
                                () => {
                                  this.handleChange(val, 'product_id');
                                }
                              );
                            }}
                            className={classes.input21}
                            options={this.state.data2}
                          />
                        </Grid>
                        <Grid
                          container
                          item
                          lg={4}
                          xl={4}
                          md={4}
                          sm={4}
                          xs={12}
                        >
                          <div className={classes.label111}>
                            <text className={classes.label1}>
                              Kategori Barang Jaminan
                            </text>
                            <text className={classes.starts1}>*</text>
                          </div>
                          <Select2
                            name="form-field-name-error"
                            value={this.state.value.insurance_item_id || ''}
                            isDisabled={
                              this.state.value.product_id == undefined
                            }
                            placeholder="Pilih"
                            onFocus={() => {
                              this.removeValidate('insurance_item_id');
                            }}
                            styles={{
                              control: (provided, state) => ({
                                ...provided,
                                borderColor: this.state.validator
                                  .insurance_item_id
                                  ? 'red'
                                  : '#CACACA',
                                borderRadius: '0.25rem',
                              }),
                            }}
                            onInputChange={(val) => {
                              if (val) this.getInsurance(val, '');
                            }}
                            className={classes.input21}
                            options={this.state.data3}
                            onChange={(val) => {
                              this.handleChange(val, 'insurance_item_id');
                              this.handleNetto(val);
                            }}
                          />
                        </Grid>
                      </Grid>
                      <div
                        style={{
                          marginBottom: 50,
                        }}
                      >
                        {this.renderBJ()}
                        <button
                          style={{
                            marginTop: 30,
                            marginLeft: 15,
                            backgroundColor: '#C4A643',
                            borderRadius: 50,
                            color: 'white',
                            width: 200,
                            height: 35,
                            fontWeight: '500',
                            fontSize: 14,
                          }}
                          onClick={() => {
                            this.addBJ();
                          }}
                        >
                          Tambah Barang Jaminan
                        </button>
                      </div>
                    </Grid>
                  )}
                  {this.state.activeStep === 1 && (
                    <Grid item lg={12} xl={12} md={12} sm={12} xs={12}>
                      <Divider className={classes.divider} />
                      <div className={classes.BodytitleMdl2}>
                        <text className={classes.titleMdl}>
                          Perhitungan Barang Jaminan
                        </text>
                      </div>
                      <Grid
                        container
                        direction="row"
                        item
                        lg={12}
                        xl={12}
                        md={12}
                        xs={12}
                      >
                        <Grid item lg={4} xl={4} md={4} sm={4} xs={12}>
                          <div className={classes.label111}>
                            <text className={classes.label1}>
                              Total Harga Beli
                            </text>
                            <text className={classes.starts1}>*</text>
                          </div>
                          <TextField
                            size="small"
                            className={classes.input2}
                            variant="outlined"
                            disabled
                            autoComplete="off"
                            onFocus={() => {
                              this.removeValidate('buy_price');
                            }}
                            error={this.state.validator.buy_price}
                            helperText={this.state.validator.buy_price}
                            value={
                              this.state.value.buy_price !== undefined
                                ? Func.FormatNumber(this.state.value.buy_price)
                                : ''
                            }
                            onChange={(event) => {
                              this.removeValidate('buy_price');
                              this.handleChange(
                                Func.UnFormatRp(event.target.value),
                                'buy_price'
                              );
                            }}
                            InputProps={{
                              startAdornment: (
                                <InputAdornment position="start">
                                  Rp
                                </InputAdornment>
                              ),
                            }}
                            name="buy_price"
                          />
                        </Grid>
                        <Grid
                          container
                          item
                          lg={4}
                          xl={4}
                          md={4}
                          sm={4}
                          xs={12}
                        >
                          {this.props.type !== 'Ubah' && (
                            <>
                              <div className={classes.label111}>
                                <text className={classes.label1}>
                                  Nilai Maksimum Pinjaman
                                </text>
                              </div>
                              <TextField
                                size="small"
                                disabled
                                className={classes.input2}
                                variant="outlined"
                                autoComplete="off"
                                onFocus={() => {
                                  this.removeValidate(
                                    'maximum_financing_amount'
                                  );
                                }}
                                error={
                                  this.state.validator.maximum_financing_amount
                                }
                                helperText={
                                  this.state.validator.maximum_financing_amount
                                }
                                value={
                                  this.state.value.maximum_financing_amount
                                    ? Func.FormatNumber(
                                        this.state.value
                                          .maximum_financing_amount
                                      )
                                    : ''
                                }
                                onChange={(event) => {
                                  this.handleChange(
                                    Func.UnFormatRp(event.target.value),
                                    'maximum_financing_amount'
                                  );
                                }}
                                InputProps={{
                                  startAdornment: (
                                    <InputAdornment position="start">
                                      Rp
                                    </InputAdornment>
                                  ),
                                }}
                                name="maximum_financing_amount"
                              />
                            </>
                          )}
                        </Grid>
                        <Grid
                          container
                          item
                          lg={4}
                          xl={4}
                          md={4}
                          sm={4}
                          xs={12}
                        />
                      </Grid>
                      <Grid
                        container
                        direction="row"
                        style={{
                          marginTop: 25,
                        }}
                        item
                        lg={12}
                        xl={12}
                        md={12}
                        xs={12}
                      >
                        <Grid
                          container
                          item
                          lg={4}
                          xl={4}
                          md={4}
                          sm={4}
                          xs={12}
                        >
                          <div className={classes.label111}>
                            <text className={classes.label1}>
                              Nilai Taksiran
                            </text>
                          </div>
                          <TextField
                            size="small"
                            disabled
                            className={classes.input2}
                            variant="outlined"
                            autoComplete="off"
                            onFocus={() => {
                              this.removeValidate('estimated_value');
                            }}
                            error={this.state.validator.estimated_value}
                            helperText={this.state.validator.estimated_value}
                            value={
                              this.state.value.estimated_value !== undefined
                                ? Func.FormatNumber(
                                    this.state.value.estimated_value
                                  )
                                : ''
                            }
                            onChange={(event) => {
                              this.handleChange(
                                Func.UnFormatRp(event.target.value),
                                'estimated_value'
                              );
                            }}
                            InputProps={{
                              startAdornment: (
                                <InputAdornment position="start">
                                  Rp
                                </InputAdornment>
                              ),
                            }}
                            name="estimated_value"
                          />
                        </Grid>
                        <Grid
                          container
                          item
                          lg={4}
                          xl={4}
                          md={4}
                          sm={4}
                          xs={12}
                        >
                          <div className={classes.label111}>
                            <text className={classes.label1}>
                              Rasio Maksimal Pinjaman
                            </text>
                            <text className={classes.starts1}>*</text>
                          </div>
                          <TextField
                            size="small"
                            disabled
                            className={classes.input2}
                            variant="outlined"
                            autoComplete="off"
                            InputProps={{
                              endAdornment: (
                                <InputAdornment position="end">
                                  %
                                </InputAdornment>
                              ),
                            }}
                            onFocus={() => {
                              this.removeValidate('ltv');
                            }}
                            error={this.state.validator.ltv}
                            helperText={this.state.validator.ltv}
                            value={this.state.value.ltv}
                            onChange={(event) => {
                              this.handleChange(
                                Func.UnFormatRp(event.target.value),
                                'ltv'
                              );
                            }}
                            name="ltv"
                          />
                        </Grid>
                        <Grid
                          container
                          item
                          lg={4}
                          xl={4}
                          md={4}
                          sm={4}
                          xs={12}
                        />
                      </Grid>
                      <Grid
                        container
                        direction="row"
                        style={{
                          marginTop: 25,
                        }}
                        item
                        lg={12}
                        xl={12}
                        md={12}
                        xs={12}
                      />
                      <Divider className={classes.divider} />
                      <div className={classes.BodytitleMdl2}>
                        <text className={classes.titleMdl}>
                          Perhitungan Cicilan
                        </text>
                      </div>
                      <Grid
                        container
                        direction="row"
                        item
                        lg={12}
                        xl={12}
                        md={12}
                        xs={12}
                      >
                        <Grid
                          container
                          item
                          lg={4}
                          xl={4}
                          md={4}
                          sm={4}
                          xs={12}
                        >
                          {this.props.type !== 'Ubah' && (
                            <>
                              <div className={classes.label111}>
                                <text className={classes.label1}>
                                  Uang Muka
                                </text>
                                <text className={classes.starts1}>*</text>
                              </div>
                              <TextField
                                size="small"
                                error={downPayment < minDownPayment}
                                className={classes.input2}
                                variant="outlined"
                                autoComplete="off"
                                InputProps={{
                                  startAdornment: (
                                    <InputAdornment position="start">
                                      Rp
                                    </InputAdornment>
                                  ),
                                }}
                                onFocus={() => {
                                  this.removeValidate('down_payment');
                                }}
                                helperText={this.state.validator.down_payment}
                                value={this.state.value.down_payment}
                                onChange={(event) => {
                                  this.handleChange(
                                    Func.UnFormatRp(event.target.value),
                                    'down_payment'
                                  );
                                }}
                                name="down_payment"
                              />
                            </>
                          )}
                        </Grid>
                        <Grid
                          container
                          item
                          lg={4}
                          xl={4}
                          md={4}
                          sm={4}
                          xs={12}
                        >
                          <div className={classes.label111}>
                            <text className={classes.label1}>Tenor</text>
                            <text className={classes.starts1}>*</text>
                          </div>
                          <Select2
                            value={{
                              value: this.state.value.tenor,
                              label: labelTenor,
                            }}
                            placeholder="Pilih"
                            onFocus={() => {
                              this.removeValidate('tenor');
                            }}
                            styles={{
                              control: (provided, state) => ({
                                ...provided,
                                borderColor: this.state.validator.tenor
                                  ? 'red'
                                  : '#CACACA',
                                borderRadius: '0.25rem',
                              }),
                            }}
                            onChange={(data) => {
                              this.handleChange(data.value, 'tenor');
                              this.getInstallmentCost(data);
                            }}
                            className={classes.input21}
                            options={[
                              {
                                value: '-',
                                label: 'Pilih',
                                isDisabled: true,
                              },
                              {
                                value: 3,
                                label: '3 Bulan',
                              },
                              {
                                value: 6,
                                label: '6 Bulan',
                              },
                              {
                                value: 12,
                                label: '12 Bulan',
                              },
                            ]}
                          />
                        </Grid>
                        <Grid
                          container
                          item
                          lg={4}
                          xl={4}
                          md={4}
                          sm={4}
                          xs={12}
                        >
<div className={classes.label111}>
                            <text className={classes.label1}>Biaya Admin</text>
                            <text className={classes.starts1}>*</text>
                          </div>
                          <TextField
                            size="small"
                            className={classes.input2}
                            disabled
                            variant="outlined"
                            autoComplete="off"
                            onFocus={() => {
                              this.removeValidate('admin_fee');
                            }}
                            error={this.state.validator.admin_fee}
                            helperText={this.state.validator.admin_fee}
                            value={
                              this.state.value.admin_fee !== undefined
                                ? Func.FormatNumber(this.state.value.admin_fee)
                                : ''
                            }
                            onChange={(event) => {
                              this.handleChange(
                                Func.UnFormatRp(event.target.value),
                                'admin_fee'
                              );
                            }}
                            InputProps={{
                              startAdornment: (
                                <InputAdornment position="start">
                                  Rp
                                </InputAdornment>
                              ),
                            }}
                            name="admin_fee"
                          />
                        </Grid>
                      </Grid>
                      <Grid
                        container
                        direction="row"
                        style={{
                          marginTop: 25,
                        }}
                        item
                        lg={12}
                        xl={12}
                        md={12}
                        xs={12}
                      >
                        <Grid
                          container
                          item
                          lg={4}
                          xl={4}
                          md={4}
                          sm={4}
                          xs={12}
                        >
                          <div className={classes.label111}>
                            <text className={classes.label1}>
                              Bunga Perbulan
                            </text>
                            <text className={classes.starts1}>*</text>
                          </div>
                          <TextField
                            size="small"
                            className={classes.input2}
                            disabled
                            variant="outlined"
                            autoComplete="off"
                            onFocus={() => {
                              this.removeValidate('monthly_interest');
                            }}
                            error={this.state.validator.monthly_interest}
                            helperText={this.state.validator.monthly_interest}
                            value={
                              this.state.value.monthly_interest !== undefined
                                ? Func.FormatNumber(
                                    this.state.value.monthly_interest
                                  )
                                : ''
                            }
                            onChange={(event) => {
                              this.handleChange(
                                Func.UnFormatRp(event.target.value),
                                'monthly_interest'
                              );
                            }}
                            InputProps={{
                              endAdornment: (
                                <InputAdornment position="end">
                                  %
                                </InputAdornment>
                              ),
                            }}
                            name="monthly_interest"
                          />
                        </Grid>
                        <Grid
                          container
                          item
                          lg={4}
                          xl={4}
                          md={4}
                          sm={4}
                          xs={12}
                        >
                          <div className={classes.label111}>
                            <text className={classes.label1}>
                              Pokok Pinjaman
                            </text>
                            <text className={classes.starts1}>*</text>
                          </div>
                          <TextField
                            size="small"
                            className={classes.input2}
                            disabled={!this.state.value.tenor?.value}
                            variant="outlined"
                            autoComplete="off"
                            onFocus={() => {
                              this.removeValidate('loan_amount');
                            }}
                            error={this.state.validator.loan_amount}
                            helperText={this.state.validator.loan_amount}
                            value={
                              this.state.value.loan_amount !== undefined
                                ? Func.FormatNumber(
                                    this.state.value.loan_amount
                                  )
                                : ''
                            }
                            onChange={(event) => {
                              this.removeValidate('loan_amount');
                              this.handleChange(
                                Func.UnFormatRp(event.target.value),
                                'loan_amount'
                              );
                            }}
                            InputProps={{
                              startAdornment: (
                                <InputAdornment position="start">
                                  Rp
                                </InputAdornment>
                              ),
                            }}
                            name="loan_amount"
                          />
                        </Grid>
                        <Grid
                          container
                          item
                          lg={4}
                          xl={4}
                          md={4}
                          sm={4}
                          xs={12}
                        >
                          <div className={classes.label111}>
                            <text className={classes.label1}>
                              Total Cicilan Perbulan
                            </text>
                            <text className={classes.starts1}>*</text>
                          </div>
                          <TextField
                            size="small"
                            className={classes.input2}
                            disabled
                            variant="outlined"
                            autoComplete="off"
                            onFocus={() => {
                              this.removeValidate('monthly_installment');
                            }}
                            error={this.state.validator.monthly_installment}
                            helperText={
                              this.state.validator.monthly_installment
                            }
                            value={
                              this.state.value.monthly_installment !== undefined
                                ? Func.FormatNumber(
                                    this.state.value.monthly_installment
                                  )
                                : ''
                            }
                            onChange={(event) => {
                              this.handleChange(
                                Func.UnFormatRp(event.target.value),
                                'monthly_installment'
                              );
                            }}
                            InputProps={{
                              startAdornment: (
                                <InputAdornment position="start">
                                  Rp
                                </InputAdornment>
                              ),
                            }}
                            name="monthly_installment"
                          />
                        </Grid>
                      </Grid>
                      <Grid
                        container
                        direction="row"
                        style={{
                          marginTop: 25,
                        }}
                        item
                        lg={12}
                        xl={12}
                        md={12}
                        xs={12}
                      >
                        <Grid
                          container
                          item
                          lg={4}
                          xl={4}
                          md={4}
                          sm={4}
                          xs={12}
                        >
                          <div className={classes.label111}>
                            <text className={classes.label1}>
                              Biaya Bunga Bulanan
                            </text>
                            <text className={classes.starts1}>*</text>
                          </div>
                          <TextField
                            size="small"
                            className={classes.input2}
                            disabled
                            variant="outlined"
                            autoComplete="off"
                            onFocus={() => {
                              this.removeValidate('monthly_fee');
                            }}
                            error={this.state.validator.monthly_fee}
                            helperText={this.state.validator.monthly_fee}
                            value={
                              this.state.value.monthly_fee !== undefined
                                ? Func.FormatNumber(
                                    this.state.value.monthly_fee
                                  )
                                : ''
                            }
                            onChange={(event) => {
                              this.handleChange(
                                Func.UnFormatRp(event.target.value),
                                'monthly_fee'
                              );
                            }}
                            InputProps={{
                              startAdornment: (
                                <InputAdornment position="start">
                                  Rp
                                </InputAdornment>
                              ),
                            }}
                            name="monthly_fee"
                          />
                        </Grid>
                        {this.props.type !== 'Ubah' && (
                          <>
                            <Grid
                              container
                              item
                              lg={4}
                              xl={4}
                              md={4}
                              sm={4}
                              xs={12}
                            >
                              <div className={classes.label111}>
                                <text className={classes.label1}>
                                  Cicilan Pokok
                                </text>
                                <text className={classes.starts1}>*</text>
                              </div>
                              <TextField
                                size="small"
                                className={classes.input2}
                                disabled
                                variant="outlined"
                                autoComplete="off"
                                onFocus={() => {
                                  this.removeValidate(
                                    'installment_before_interest'
                                  );
                                }}
                                error={
                                  this.state.validator
                                    .installment_before_interest
                                }
                                helperText={
                                  this.state.validator
                                    .installment_before_interest
                                }
                                value={
                                  this.state.value
                                    .installment_before_interest !== undefined
                                    ? Func.FormatNumber(
                                        this.state.value
                                          .installment_before_interest
                                      )
                                    : ''
                                }
                                onChange={(event) => {
                                  this.handleChange(
                                    Func.UnFormatRp(event.target.value),
                                    'installment_before_interest'
                                  );
                                }}
                                InputProps={{
                                  startAdornment: (
                                    <InputAdornment position="start">
                                      Rp
                                    </InputAdornment>
                                  ),
                                }}
                                name="installment_before_interest"
                              />
                            </Grid>
                            <Grid
                              container
                              item
                              lg={4}
                              xl={4}
                              md={4}
                              sm={4}
                              xs={12}
                            >
                              <div className={classes.label111}>
                                <text className={classes.label1}>
                                  Total Bayar Pertama
                                </text>
                                <text className={classes.starts1}>*</text>
                              </div>
                              <TextField
                                size="small"
                                className={classes.input2}
                                disabled
                                variant="outlined"
                                autoComplete="off"
                                onFocus={() => {
                                  this.removeValidate('total_down_payment');
                                }}
                                error={this.state.validator.total_down_payment}
                                helperText={
                                  this.state.validator.total_down_payment
                                }
                                value={
                                  this.state.value.total_down_payment
                                  !== undefined
                                    ? Func.FormatNumber(
                                        this.state.value.total_down_payment
                                      )
                                    : ''
                                }
                                onChange={(event) => {
                                  this.handleChange(
                                    Func.UnFormatRp(event.target.value),
                                    'total_down_payment'
                                  );
                                }}
                                InputProps={{
                                  startAdornment: (
                                    <InputAdornment position="start">
                                      Rp
                                    </InputAdornment>
                                  ),
                                }}
                                name="total_down_payment"
                              />
                            </Grid>
                          </>
                        )}
                        <Grid
                          container
                          item
                          lg={4}
                          xl={4}
                          md={4}
                          sm={4}
                          xs={12}
                        />
                        <Grid
                          container
                          item
                          lg={4}
                          xl={4}
                          md={4}
                          sm={4}
                          xs={12}
                        />
                      </Grid>
                      <Divider className={classes.divider} />
                      <div className={classes.BodytitleMdl2}>
                        <text className={classes.titleMdl}>
                          Tanggal-Tanggal Penting
                        </text>
                      </div>
                      <Grid
                        container
                        direction="row"
                        item
                        lg={12}
                        xl={12}
                        md={12}
                        xs={12}
                      >
                        <Grid
                          container
                          item
                          lg={4}
                          xl={4}
                          md={4}
                          sm={4}
                          xs={12}
                        >
                          <div>
                            <text className={classes.label1}>Tanggal Akad</text>
                            <text className={classes.starts1}>*</text>
                          </div>
                          <TextField
                            size="small"
                            className={classes.input2}
                            variant="outlined"
                            disabled
                            autoComplete="off"
                            type="date"
                            onFocus={() => {
                              this.removeValidate('contract_date');
                            }}
                            error={this.state.validator.contract_date}
                            helperText={this.state.validator.contract_date}
                            value={this.state.value.contract_date || ''}
                            onChange={(event) => {
                              this.handleChange(
                                event.target.value,
                                'contract_date'
                              );
                            }}
                            name="contract_date"
                            onKeyDown={(event) => event.preventDefault()}
                          />
                        </Grid>
                      </Grid>
                    </Grid>
                  )}
                </Grid>
              </div>
            </div>
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button
            disabled={this.state.activeStep === 0}
            onClick={() => {
              const stepCount = this.state.activeStep;
              this.setState({ activeStep: stepCount - 1 });
            }}
            variant="contained"
          >
            <Typography variant="button">Kembali</Typography>
          </Button>
          <Button
            style={{
              backgroundColor: '#862C3A',
              color: 'white',
            }}
            disabled={this.state.loader_button}
            onClick={() => {
              if (this.state.activeStep === 1) {
                swal({
                  title: 'Apa Anda yakin?',
                  text: 'Menyimpan data ini',
                  icon: 'info',
                  buttons: ['Batal', 'Simpan'],
                })
                .then((willDelete) => {
                  if (willDelete) {
                    this.handleSubmit(this.props.type);
                  }
                });
              } else this.handleFirstStepForm();
            }}
            variant="contained"
          >
            <Typography variant="button" style={{ color: '#FFFFFF' }}>
              {this.state.loader_button && (
                <CircularProgress
                  style={{
                    color: 'white',
                    marginLeft: '18px',
                    marginRight: '18px',
                    marginTop: 5,
                  }}
                  size={15}
                />
              )}
              {!this.state.loader_button && this.state.activeStep === 1
                ? 'Simpan'
                : !this.state.loader_button && this.state.activeStep === 0
                ? 'Selanjutnya'
                : ''}
            </Typography>
          </Button>
          <Button
            style={{
              color: 'black',
            }}
            disabled={this.state.loader_button}
            onClick={() => {
              this.props.handleModal();
            }}
            variant="outlined"
          >
            <Typography variant="button">Batal</Typography>
          </Button>
        </DialogActions>
      </Dialog>
    );
  }
}

export default withStyles(styles.CoustomsStyles, { name: 'Form' })(Form);
