/* eslint-disable react/jsx-one-expression-per-line */
/* eslint-disable func-names */
/* eslint-disable react/jsx-no-bind */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable radix */
/* eslint-disable consistent-return */
/* eslint-disable no-plusplus */
/* eslint-disable class-methods-use-this */
/* eslint-disable array-callback-return */
/* eslint-disable react/button-has-type */
/* eslint-disable camelcase */
/* eslint-disable prefer-destructuring */
/* eslint-disable eqeqeq */
/* eslint-disable react/sort-comp */
/* eslint-disable react/no-access-state-in-setstate */
/* eslint-disable react/no-unused-state */
/* eslint-disable no-shadow */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable no-unused-vars */
import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import InputAdornment from '@material-ui/core/InputAdornment';
import Divider from '@material-ui/core/Divider';
import Typography from '@material-ui/core/Typography';
import swal from 'sweetalert';
import FormHelperText from '@material-ui/core/FormHelperText';
import Grid from '@material-ui/core/Grid';
import TextareaAutosize from '@material-ui/core/TextareaAutosize';
import DialogContentText from '@material-ui/core/DialogContentText';
import MuiDialogActions from '@material-ui/core/DialogActions';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Button from '@material-ui/core/Button';
import Box from '@material-ui/core/Box';
import { CircularProgress } from '@material-ui/core';
import Select2 from 'react-select';
import Func from '../../../../../functions/index';
import Icon from '../../../../../components/icon';
import styles from '../css';
import env from '../../../../../config/env';
import '../../../../../styles/date_field.css';
import Webcam from './Webcam.jsx';

const DialogActions = withStyles((theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(1),
  },
}))(MuiDialogActions);

class Form extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: false,
      validator: [],
      value: {
        gender: {
          label: '',
        },
      },
      type: '',
      activeTabs: 0,
      Proses: false,
      ImgBase124: [],
      section: 0,
      id: null,
      redirect: false,
      date: new Date(),
      provin: [],
      data3: [],
      estimated_value: [],
      ltv_value: [],
      ltv: [],
      monthly_fee: [],
      dataBJ: [],
      cont: 0,
      approvals: [],
      download: null,
      loader: false,
      detailPembayaran: [],
      showAdd: false,
      setValue: [],
      changed: false,
    };
    this.handleChangeImg = this.handleChangeImg.bind(this);
  }

  removeValidate(name) {
    const data = this.state.validator;
    delete data[name];
    this.setState({ validator: data });
  }

  handleWebcam(img64, stateName) {
    const dataSet = this.state.setValue;
    dataSet[stateName] = img64;
    this.setState({ setValue: dataSet });
    const ImgBase124 = this.state.ImgBase124;
    ImgBase124[stateName + this.state.cont] = img64;
    this.setState({ ImgBase124 });
  }

  handleChange(event, name) {
    if (name == 'customer_id') {
      this.getCustomerDetail(event.value);
    }

    if (name == 'identity_number') {
      if (event.toString().length >= 16) {
        this.getCustomerKTP(event);
      }
    }

    if (name == 'insurance_item_id') {
      this.getInsuranceProd(event.value, '', '');
    }

    if (name == 'product_id') {
      const d = new Date();
      const val = this.state.value;
      val.contract_date = d.getFullYear()
        + '-'
        + (d.getMonth() + 1 > 9 ? d.getMonth() + 1 : '0' + d.getMonth() + 1)
        + '-'
        + d.getDate();
      val.data3 = [];
      this.setState(
        {
          value: val,
        },
        () => {
          this.getDueDates(event.value);
          this.getInsurance(event.value);
        }
      );
    }
    if (name == 'loan_amount') {
      this.getAdminFee(event);
      this.getRentalCosts(event);
    }

    const dataSet = this.state.setValue;
    dataSet[name] = event;
    this.setState({ setValue: dataSet });
  }

  getInsurance(id) {
    fetch(
      process.env.REACT_APP_URL_MASTER
        + process.env.REACT_APP_API_PREFIX_V1
        + '/product/'
        + id
        + '/insurance_item/',
      {
        method: 'GET',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + localStorage.getItem('token'),
        },
      }
    )
      .then((response) => response.json())
      .then((json) => {
        if (json.code == '403') {
          if (Func.Clear_Token() == true) {
            if (!localStorage.getItem('token')) {
              this.setState({ redirect: true });
            }
          }
        }

        const datas = [];
        datas.push({
          value: '-',
          label:
            json.data.insurance_item_product.insurance_items.length > 0
              ? 'Pilih'
              : 'Tidak ditemukan',
          isDisabled: true,
        });

        json.data.insurance_item_product.insurance_items.map((value) => {
          datas.push({ value: value.id.$oid, label: value.name });
        });
        this.setState({
          data3: datas,
          data3_ori: json.data.insurance_item_product.insurance_items,
        });
      })
      .catch((error) => {})
      .finally(() => {});
  }

  getRentalCosts(value) {
    const id = this.state.value.product_id.value;
    const insurance_item_id = this.state.value.insurance_item_id.value;
    fetch(
      process.env.REACT_APP_URL_MASTER
        + process.env.REACT_APP_API_PREFIX_V1
        + '/rental_costs?product_id='
        + id
        + '&insurance_item_id='
        + insurance_item_id
        + '&loan_value='
        + value,
      {
        method: 'GET',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + localStorage.getItem('token'),
        },
      }
    )
      .then((response) => response.json())
      .then((json) => {
        if (json.code == '403') {
          if (Func.Clear_Token() == true) {
            if (!localStorage.getItem('token')) {
              this.setState({ redirect: true });
            }
          }
        }
        const val = this.state.value;
        val.monthly_fee = Math.ceil(json.data.rental_cost_nominal);
        this.setState({ value: val });
      })
      .catch((error) => {})
      .finally(() => {});
  }

  getAdminFee(value) {
    const id = this.state.value.product_id.value;
    const insurance_item_id = this.state.value.insurance_item_id.value;
    fetch(
      process.env.REACT_APP_URL_MASTER
        + process.env.REACT_APP_API_PREFIX_V1
        + '/admin_fees?product_id='
        + id
        + '&insurance_item_id='
        + insurance_item_id
        + '&loan_nominal='
        + value,
      {
        method: 'GET',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + localStorage.getItem('token'),
        },
      }
    )
      .then((response) => response.json())
      .then((json) => {
        if (json.code == '403') {
          if (Func.Clear_Token() == true) {
            if (!localStorage.getItem('token')) {
              this.setState({ redirect: true });
            }
          }
        }
        const val = this.state.value;
        val.admin_fee = Math.ceil(
          value * parseFloat('0.' + json.data[0].cost_percentage)
        );
        this.setState({ value: val });
      })
      .catch((error) => {})
      .finally(() => {});
  }

  getDueDates(date) {
    const id = this.state.value.product_id.value;
    fetch(
      process.env.REACT_APP_URL_MASTER
        + process.env.REACT_APP_API_PREFIX_V1
        + '/due_dates?product_id='
        + id
        + '&contract_date='
        + date,
      {
        method: 'GET',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + localStorage.getItem('token'),
        },
      }
    )
      .then((response) => response.json())
      .then((json) => {
        if (json.code == '403') {
          if (Func.Clear_Token() == true) {
            if (!localStorage.getItem('token')) {
              this.setState({ redirect: true });
            }
          }
        }

        const val = this.state.value;
        val.due_date = json.data.due_date;
        val.auction_date = json.data.auction_date;
        this.setState({ value: val });
      })
      .catch((error) => {})
      .finally(() => {});
  }

  getInsuranceProd(id = '', id2, idx) {
    this.setState(
      {
        data5: [],
      },
      () => {
        fetch(
          process.env.REACT_APP_URL_MASTER
            + process.env.REACT_APP_API_PREFIX_V1
            + '/insurance_item/'
            + (id == '' ? this.state.value.insurance_item_id.value : id),
          {
            method: 'GET',
            headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json',
              Authorization: 'Bearer ' + localStorage.getItem('token'),
            },
          }
        )
          .then((response) => response.json())
          .then((json) => {
            if (json.code == '403') {
              if (Func.Clear_Token() == true) {
                if (!localStorage.getItem('token')) {
                  this.setState({ redirect: true });
                }
              }
            }
            const datas = [];
            datas.push({
              value: '-',
              label:
                json.data.insurance_item_types.length > 0
                  ? 'Pilih'
                  : 'Tidak ditemukan',
              isDisabled: true,
            });
            json.data.insurance_item_types.map((value) => {
              datas.push({ value: value._id.$oid, label: value.name });
            });
            this.setState({ data5: datas });
          })
          .catch((error) => {})
          .finally(() => {});
      }
    );
  }

  handleChangeDate(date, name) {
    const dt = new Date(date);
    const dataSet = this.state.value;
    dataSet[name] = dt;
    this.setState({ value: dataSet });
  }

  convertImgToBase64(url, callback, outputFormat) {
    let canvas = document.createElement('CANVAS');
    const ctx = canvas.getContext('2d');
    const img = new Image();
    img.crossOrigin = 'Anonymous';
    img.onload = function () {
      canvas.height = img.height;
      canvas.width = img.width;
      ctx.drawImage(img, 0, 0);
      const dataURL = canvas.toDataURL(outputFormat || 'image/png');
      callback.call(this, dataURL);
      // Clean up
      canvas = null;
    };
    img.src = url;
  }

  componentDidMount() {
    if (this.props.type == 'Ubah') {
      this.setState({ loader: true });
      fetch(
        process.env.REACT_APP_URL_MANAGEMENT
          + process.env.REACT_APP_API_PREFIX_V1
          + '/transactions/'
          + this.props.id,
        {
          method: 'GET',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: 'Bearer ' + localStorage.getItem('token'),
          },
        }
      )
        .then((response) => response.json())
        .then((json) => {
          if (json.code == '403') {
            if (Func.Clear_Token() == true) {
              if (!localStorage.getItem('token')) {
                this.setState({ redirect: true });
              }
            }
          }
          this.getCusto('', '', json.data.customer.name);
          this.getProd('', json.data.product_id, '');
          const val = this.state.value;
          try {
            val.customer_id = {
              value: json.data.customer.id,
              label: json.data.customer.name,
            };
            val.product_id = {
              value: json.data.product_id,
              label: json.data.product_name,
            };
            val.product_name = json.data.product_name;
            val.identity_address = json.data.customer.address;
            val.insurance_item_id = {
              value: json.data.insurance_item_id,
              label: json.data.insurance_item_name,
            };
            val.contract_date = json.data.contract_date;
            val.due_date = json.data.due_date;
            val.auction_date = json.data.auction_date;
            val.admin_fee = json.data.admin_fee;
            val.loan_amount = json.data.loan_amount;
            val.monthly_fee = json.data.monthly_fee;
            json.data.transaction_insurance_items.map((value, index) => {
              this.addBJ(value.id);
              this.getInsuranceProd(
                json.data.insurance_item_id,
                value.product_insurance_item_id,
                index
              );
              val['name' + index] = value.name;
              val['ownership' + index] = {
                value: value.ownership,
                label: value.ownership,
              };
              val['amount' + index] = value.amount;
              val['net_weight' + index] = value.net_weight;
              val['gross_weight' + index] = value.gross_weight;
              val['carats' + index] = value.carats;
              val['product_insurance_item_id' + index] = {
                value: value.product_insurance_item_id,
                label: value.product_insurance_item_name,
              };
              val['product_insurance_item_name' + index] = value.product_insurance_item_name;
              val['description' + index] = value.description;
              val['Insurance_item_image' + index] = value.insurance_item_image.url;
              const img = this.state.ImgBase124;
              this.convertImgToBase64(value.insurance_item_image.url,
                (base64Img) => {
                  img['Insurance_item_image' + index] = base64Img;
                }
              );
              this.setState({ ImgBase124: img });
            });
          } catch (err) {
            alert('terjadi kesalahan sistem');
          }

          const mapData = Object.assign(val, json.data.customer);

          this.setState(
            {
              value: mapData,
              data4: json.data.customer,
              approvals: json.data.approvals,
              download:
                json.data.download == undefined ? null : json.data.download,
            },
            () => {
              json.data.transaction_insurance_items.map((value, index) => {
                this.getEstimate(index);
              });
            }
          );
        })
        .catch((error) => {})
        .finally(() => {});

      fetch(
        process.env.REACT_APP_URL_MANAGEMENT
          + process.env.REACT_APP_API_PREFIX_V1
          + '/transactions/'
          + this.props.id
          + '/prolongations',
        {
          method: 'GET',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: 'Bearer ' + localStorage.getItem('token'),
          },
        }
      )
        .then((response) => response.json())
        .then((json) => {
          if (json.code == '403') {
            if (Func.Clear_Token() == true) {
              if (!localStorage.getItem('token')) {
                this.setState({ redirect: true });
              }
            }
          } else if (json.code === 400) {
            swal({
              title: 'Kesalahan',
              text: json.message,
              icon: 'error',
              buttons: 'OK',
            });
            this.props.handleModal();
          }

          const val = this.state.value;

          try {
            val.prolongation_admin_fee = json.data.admin_fee;
            val.prolongation_contract_date = Func.FormatDate(
              json.data.contract_date
            );
            val.prolongation_due_date = Func.FormatDate(json.data.due_date);
            val.prolongation_estimate_value = Func.FormatRp(
              json.data.estimate_value
            );
            val.prolongation_fine_amount = json.data.fine_amount;
            val.prolongation_loan_amount = json.data.loan_amount;
            val.prolongation_multiplier = json.data.multiplier;
            val.prolongation_number_of_days = json.data.number_of_days;
            val.prolongation_parent_sge = json.data.parent_sge;
            val.prolongation_payment_date = json.data.payment_date;
            val.prolongation_prolongation_order = json.data.prolongation_order;
            val.prolongation_rental_per_15_days = json.data.rental_per_15_days;
            val.prolongation_total_bill = json.data.total_bill;
            val.prolongation_total_rental_cost = json.data.total_rental_cost;
            val.prolongation_prolongation_date = json.data.prolongation_date;
            val.prolongation_monthly_interest = json.data.monthly_interest;
          } catch (err) {
            this.setState({ loader: false });
          }
          const mapData = Object.assign(val, json.data.customer);

          this.setState({
            value: mapData,
            data4: json.data.customer,
            approvals: json.data.approvals,
            download:
              json.data.download == undefined ? null : json.data.download,
          });
          this.setState({ loader: false });
        })
        .catch((error) => {})
        .finally(() => {});
    } else {
      this.getProd('', '', '');
      this.getCusto('', '', '');
      this.addBJ();
    }
  }

  getProd = (val, id, name) => {
    this.setState({ isloader: true });
    Func.getData('product', 10, 1, val).then((res) => {
      if (res.json.code == '403') {
        if (Func.Clear_Token() == true) {
          if (!localStorage.getItem('token')) {
            this.setState({ redirect: true });
          }
        }
      } else {
        const datas = [];
        datas.push({
          value: '-',
          label: res.json.data.length > 0 ? 'Pilih' : 'Tidak ditemukan',
          isDisabled: true,
        });

        res.json.data.map((value) => {
          datas.push({ value: value.id.$oid, label: value.name });
        });
        if (id != '') {
          const search = res.json.data.find((o) => o.id.$oid === id);
          if (search == undefined) {
            datas.push({ value: id, label: name });
          }
          if (search != undefined) {
            const val = this.state.value;
            val.product_id = {
              value: search.id.$oid,
              label: search.name,
            };
          }
        }

        this.setState({ data2: datas, data2_ori: res.json.data });
      }
    });
  };

  getCusto = (val, id, name) => {
    this.setState({ isloader: true });
    Func.getDataTransaction('customer', 10, 1, val).then((res) => {
      if (res.json.code == '403') {
        if (Func.Clear_Token() == true) {
          if (!localStorage.getItem('token')) {
            this.setState({ redirect: true });
          }
        }
      } else {
        const datas = [];
        datas.push({
          value: '-',
          label: res.json.data.length > 0 ? 'Pilih' : 'Tidak ditemukan',
          isDisabled: true,
        });
        res.json.data.map((value) => {
          datas.push({ value: value.id, label: value.name });
        });
        if (name != '') {
          const search = res.json.data.find((o) => o.name === name);
          if (search == undefined) {
            datas.push({ value: id, label: name });
          }

          if (search != undefined) {
            const val = this.state.value;
            val.customer_id = {
              value: search.id,
              label: search.name,
            };
          }
        }

        this.setState({ data4: datas, data4_ori: res.json.data });
      }
    });
  };

  handleSubmit(type) {
    this.setState({ loader_button: true });
    const TIIA = [];
    this.state.dataBJ.map((value, index) => {
      const estimated_value = 0;
      if (!value._destroy) {
        if (value.id == null) {
          this.setState({ changed: true });
          TIIA.push({
            name: this.state.value['product_insurance_item_id' + value.key]
              .label,
            product_insurance_item_id: this.state.value[
              'product_insurance_item_id' + value.key
            ].value,
            product_insurance_item_name: this.state.value[
              'product_insurance_item_id' + value.key
            ].label,
            ownership: this.state.value['ownership' + value.key].value,
            amount: this.state.value['amount' + value.key],
            gross_weight: this.state.value['gross_weight' + value.key],
            net_weight: this.state.value['net_weight' + value.key],
            carats: this.state.value['carats' + value.key],
            estimated_value: this.state.estimated_value[index],
            description: this.state.value['description' + value.key],
            insurance_item_image: this.state.ImgBase124[
              'Insurance_item_image' + value.key
            ],
            _destroy: value._destroy,
          });
        } else {
          TIIA.push({
            name: this.state.value['product_insurance_item_id' + value.key]
              .label,
            product_insurance_item_id: this.state.value[
              'product_insurance_item_id' + value.key
            ].value,
            product_insurance_item_name: this.state.value[
              'product_insurance_item_id' + value.key
            ].label,
            ownership: this.state.value['ownership' + value.key].value,
            amount: this.state.value['amount' + value.key],
            gross_weight: this.state.value['gross_weight' + value.key],
            net_weight: this.state.value['net_weight' + value.key],
            carats: this.state.value['carats' + value.key],
            estimated_value: this.state.estimated_value[index],
            description: this.state.value['description' + value.key],
            insurance_item_image: this.state.ImgBase124[
              'Insurance_item_image' + value.key
            ],
            _destroy: value._destroy,
          });
        }
      }
    });

    let ltv_value = 0;
    for (let index = 0; index < this.state.ltv_value.length; index++) {
      ltv_value += this.state.ltv_value[index];
    }

    let ltv = 0;
    for (let index = 0; index < this.state.ltv.length; index++) {
      ltv += this.state.ltv[index];
    }
    let Total = 0;
    Total += this.state.value.prolongation_fine_amount;
    Total += this.state.value.prolongation_total_rental_cost;
    Total += this.state.value.prolongation_admin_fee;
    this.state.detailPembayaran.map((data) => {
      Total += data.amount;
    });
    const { prolongation_loan_amount, prolongation_monthly_interest } = this.state.value;
    const monthly_fee = ((prolongation_loan_amount * prolongation_monthly_interest) / 100);
    fetch(
      process.env.REACT_APP_URL_MANAGEMENT
          + process.env.REACT_APP_API_PREFIX_V1
          + '/transactions/'
          + this.props.id
          + '/prolongations',
      {
        method: 'PUT',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + localStorage.getItem('token'),
        },
        body: JSON.stringify({
          transaction: {
            insurance_item_id: this.state.value.insurance_item_id.value,
            customer_id: this.state.value.customer_id.value,
            insurance_item_name: this.state.value.insurance_item_id.label,
            product_id: this.state.value.product_id.value,
            product_name: this.state.value.product_id.label,
            status: 'waiting_approval',
            maximum_loan: ltv_value,
            maximum_loan_percentage: ltv,
            contract_date: this.state.value.contract_date,
            due_date: this.state.value.due_date,
            auction_date: this.state.value.auction_date,
            loan_amount: this.state.value.loan_amount,
            admin_fee: this.state.value.prolongation_admin_fee,
            monthly_fee,
            disbursement_status: this.state.value.disbursement_status,
            transaction_insurance_items_attributes: TIIA,
            payment_amount: Total,
            changed: this.state.changed,
            interest_rate: this.state.value.prolongation_monthly_interest,
          },
        }),
      }
    )
      .then((response) => response.json())
      .then((json) => {
        if (json.code == '403') {
          Func.Refresh_Token();
          if (Func.Refresh_Token() == true) {
            this.handleSubmit();
          }
        }
        if (type == 'Tambah') {
          if (json.created) {
            window.open(
              process.env.REACT_APP_URL_MANAGEMENT
                  + process.env.REACT_APP_API_PREFIX_V1
                  + '/transactions/'
                  + this.props.id
                  + '/print_repayment.pdf?type=prolongation&token=' + localStorage.getItem('token')
            );
            window.location.reload();
          } else {
            this.setState({ validator: json.status });
            this.setState({ loader_button: false });
          }
        } else if (json.code == 200) {
          window.open(
            process.env.REACT_APP_URL_MANAGEMENT
                + process.env.REACT_APP_API_PREFIX_V1
                + '/transactions/'
                + this.props.id
                + '/print_repayment.pdf?type=prolongation&token=' + localStorage.getItem('token')
          );
          window.location.reload();
        } else {
          this.setState({ validator: json.status });
          this.setState({ loader_button: false });
        }
      })
      .catch((error) => {})
      .finally(() => {});
  }

  handleChangeImg(event, name) {
    const file = event.target.files[0];
    if (file.size > 2.9e6) {
      swal({
        title: 'File Terlalu Besar',
        text: 'Maximal File 2Mb',
        icon: 'error',
        buttons: 'OK',
      });
      return false;
    }
    this.removeValidate('img');
    const dataSet = this.state.setValue;
    dataSet[name] = URL.createObjectURL(event.target.files[0]);
    this.setState({ setValue: dataSet });
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onloadend = () => {
      const ImgBase124 = this.state.ImgBase124;
      ImgBase124[name + this.state.cont] = reader.result;
      this.setState({ ImgBase124 });
    };
  }

  getEstimate(index) {
    const id = this.state.value.product_id.value;
    const insurance_item_id = this.state.value.insurance_item_id.value;
    const weight = this.state.value['net_weight' + index];
    const carats = this.state.value['carats' + index];
    const amount = this.state.value['amount' + index];

    if (id && insurance_item_id && weight && carats && amount) {
      fetch(
        process.env.REACT_APP_URL_MASTER
          + process.env.REACT_APP_API_PREFIX_V1
          + '/estimate_values?carats='
          + carats
          + '&weight='
          + weight
          + '&product_insurance_item_id='
          + id
          + '&insurance_item_id='
          + insurance_item_id,
        {
          method: 'GET',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: 'Bearer ' + localStorage.getItem('token'),
          },
        }
      )
        .then((response) => response.json())
        .then((json) => {
          if (json.code == '403') {
            if (Func.Clear_Token() == true) {
              if (!localStorage.getItem('token')) {
                this.setState({ redirect: true });
              }
            }
          }

          const { estimated_value } = this.state;
          const { ltv } = this.state;
          const { ltv_value } = this.state;

          estimated_value[index] = Math.ceil(
            json.data.estimated_value * amount
          );
          ltv_value[index] = Math.ceil(json.data.ltv_value * amount);
          ltv[0] = Math.ceil(json.data.ltv);

          this.setState(
            {
              estimated_value,
              ltv_value,
              ltv,
            },
            () => {
              this.removeValidate('loan_amount');
            }
          );
        })
        .catch((error) => {})
        .finally(() => {});
    }
  }

  renderTotal() {
    let Total = 0;
    Total += this.state.value.prolongation_fine_amount;
    Total += this.state.value.prolongation_total_rental_cost;
    Total += this.state.value.prolongation_admin_fee;
    this.state.detailPembayaran.map((data) => {
      Total += data.amount;
    });
    return Func.FormatRp(Total);
  }

  renderPembayaran() {
    const { classes } = this.props;
    const Pembayaran = [];
    this.state.detailPembayaran.map((data) => {
      Pembayaran.push(
        <div>
          <Grid
            container
            direction="row"
            item
            lg={12}
            xl={12}
            md={12}
            xs={12}
          >
            <Grid container item lg={3} xl={3} md={3} sm={3} xs={12}>
              <div className={classes.label111}>
                <text className={classes.label1}>
                  Pembayaran
                  {data.name}
                </text>
              </div>
            </Grid>
            <Grid container item lg={3} xl={3} md={3} sm={3} xs={12} />
            <Grid container item lg={3} xl={3} md={3} sm={3} xs={12} />
            <Grid container item lg={3} xl={3} md={3} sm={3} xs={12}>
              <div className={classes.label111}>
                <text className={classes.label1}>
                  {Func.FormatRp(data.amount)}
                </text>
              </div>
            </Grid>
          </Grid>
          <Divider />
        </div>
      );
    });
    return Pembayaran;
  }

  renderBJ() {
    const { classes } = this.props;
    const data = [];
    let key = 1;
    this.state.dataBJ.map((value, index) => {
      if (!value._destroy) {
        data.push(
          <div>
            <div
              className={classes.BodytitleMdl2}
              style={{
                marginTop: 50,
              }}
            >
              <text className={classes.titleMdl}>
                BARANG JAMINAN
                {key}
              </text>

              <text
                style={{
                  color: 'red',
                  marginLeft: 20,
                  cursor: 'default',
                  textDecoration: 'underline',
                }}
                onClick={() => {
                  if (key == 2) {
                    swal({
                      title: 'Maaf',
                      text: 'Minimum 1 Barang Jaminan!',
                      icon: 'error',
                    });
                  } else {
                    const datas2 = this.state.dataBJ;
                    const val = this.state.value;
                    const est = this.state.estimated_value;
                    const ltv_value = this.state.ltv_value;
                    const ltv = this.state.ltv;
                    const detailPembayaran = this.state.detailPembayaran;
                    if (value.id != null) {
                      this.setState({ changed: true });
                      detailPembayaran.push({
                        name: val['name' + datas2[index].key],
                        amount: est[datas2[index].key],
                      });
                      const values = this.state.value;
                      values.loan_amount = parseInt(values.loan_amount)
                        - parseInt(est[datas2[index].key]);
                      this.setState({ value: values });
                    } else {
                      this.setState({ changed: false });
                    }
                    val['name' + datas2[index].key] = '';
                    val['product_insurance_item_id' + datas2[index].key] = '';
                    val['ownership' + datas2[index].key] = '';
                    val['amount' + datas2[index].key] = '';
                    val['gross_weight' + datas2[index].key] = '';
                    val['net_weight' + datas2[index].key] = '';
                    val['carats' + datas2[index].key] = '';
                    val['description' + datas2[index].key] = '';
                    val['Insurance_item_image' + datas2[index].key] = '';
                    est[datas2[index].key] = 0;
                    ltv[datas2[index].key] = 0;
                    ltv_value[datas2[index].key] = 0;
                    let cont_estimated_value = 0;
                    for (let index = 0; index < est.length; index++) {
                      cont_estimated_value += est[index];
                    }
                    if (cont_estimated_value == 0) {
                      val.loan_amount = 0;
                      val.admin_fee = 0;
                      val.monthly_fee = 0;
                      this.removeValidate('loan_amount');
                    }
                    this.setState(
                      {
                        value: val,
                        estimated_value: est,
                        ltv,
                        detailPembayaran,
                        ltv_value,
                      },
                      () => {
                        datas2[index]._destroy = true;
                        this.setState({ dataBJ: datas2 });
                      }
                    );
                  }
                }}
              >
                Hapus
              </text>
            </div>
            <Grid
              container
              direction="row"
              item
              lg={12}
              xl={12}
              md={12}
              xs={12}
            >
              <Grid container item lg={8} xl={8} md={8} sm={8} xs={12}>
                <Grid container item lg={12} xl={12} md={12} sm={12} xs={12}>
                  <Grid
                    item
                    lg={6}
                    xl={6}
                    md={6}
                    sm={6}
                    xs={12}
                    className={classes.formPad}
                  >
                    <div>
                      <text className={classes.label121}>
                        Kepemilikan Barang Jaminan
                      </text>
                    </div>
                    <div>
                      <div className={classes.label1112}>
                        <text className={classes.label1}>
                          {this.state.value['ownership' + value.key]
                          == undefined
                            ? ''
                            : this.state.value['ownership' + value.key].label}
                        </text>
                      </div>
                    </div>
                  </Grid>
                  <Grid
                    container
                    lg={6}
                    xl={6}
                    md={6}
                    sm={6}
                    xs={12}
                    className={classes.formPad}
                  >
                    <Grid
                      item
                      lg={6}
                      xl={6}
                      md={6}
                      sm={6}
                      xs={12}
                      className={classes.formPad}
                    >
                      <div>
                        <text className={classes.label121}>Jumlah</text>
                      </div>
                      <div>
                        <div className={classes.label1112}>
                          <text className={classes.label1}>
                            {this.state.value['amount' + value.key]}
                          </text>
                        </div>
                      </div>
                    </Grid>
                    <Grid
                      item
                      lg={6}
                      xl={6}
                      md={6}
                      sm={6}
                      xs={12}
                      className={classes.formPad}
                    >
                      <div>
                        <text className={classes.label121}>Karat</text>
                      </div>
                      <div>
                        <div className={classes.label1112}>
                          <text className={classes.label1}>
                            {this.state.value['carats' + value.key]}
                          </text>
                        </div>
                      </div>
                    </Grid>
                  </Grid>
                </Grid>
                <Grid container item lg={12} xl={12} md={12} sm={12} xs={12}>
                  <Grid
                    item
                    lg={6}
                    xl={6}
                    md={6}
                    sm={6}
                    xs={12}
                    className={classes.formPad}
                  >
                    <div>
                      <text className={classes.label121}>
                        Kategori Barang Jaminan
                      </text>
                    </div>
                    <div>
                      <div className={classes.label1112}>
                        <text className={classes.label1}>
                          {
                            this.state.value[
                              'product_insurance_item_name' + value.key
                            ]
                          }
                        </text>
                      </div>
                    </div>
                  </Grid>
                  <Grid
                    container
                    lg={6}
                    xl={6}
                    md={6}
                    sm={6}
                    xs={12}
                    className={classes.formPad}
                  >
                    <Grid
                      item
                      lg={6}
                      xl={6}
                      md={6}
                      sm={6}
                      xs={12}
                      className={classes.formPad}
                    >
                      <div>
                        <text className={classes.label121}>Berat Kotor</text>
                      </div>
                      <div>
                        <div className={classes.label1112}>
                          <text className={classes.label1}>
                            {this.state.value['gross_weight' + value.key]}
                          </text>
                        </div>
                      </div>
                    </Grid>
                    <Grid
                      item
                      lg={6}
                      xl={6}
                      md={6}
                      sm={6}
                      xs={12}
                      className={classes.formPad}
                    >
                      <div>
                        <text className={classes.label121}>Berat Bersih</text>
                      </div>
                      <div>
                        <div className={classes.label1112}>
                          <text className={classes.label1}>
                            {this.state.value['net_weight' + value.key]}
                          </text>
                        </div>
                      </div>
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
              <Grid
                container
                item
                lg={4}
                xl={4}
                md={4}
                sm={4}
                xs={12}
                spacing={3}
              >
                <Grid
                  item
                  lg={12}
                  xl={12}
                  md={12}
                  sm={12}
                  xs={12}
                  className={classes.formPad}
                >
                  <div>
                    <text className={classes.label121}>Deskripsi</text>
                  </div>
                  <div>
                    <div className={classes.label1112}>
                      <text className={classes.label1}>
                        {this.state.value['description' + value.key]}
                      </text>
                    </div>
                  </div>
                </Grid>
              </Grid>
            </Grid>
            <Grid item lg={4} xl={4} md={4} sm={4} xs={12}>
              <Box
                borderColor={
                  this.state.validator['Insurance_item_image' + value.key]
                    ? 'error.main'
                    : 'grey.500'
                }
                border={1}
                onClick={() => {
                  this.removeValidate('Insurance_item_image' + value.key);
                }}
                className={classes.imgScan}
              >
                {this.state.value['Insurance_item_image' + value.key] ? (
                  <img
                    className={classes.imgScan2}
                    onClick={() => {
                      this.removeValidate('Insurance_item_image' + value.key);
                    }}
                    src={this.state.value['Insurance_item_image' + value.key]}
                  />
                ) : null}
              </Box>
            </Grid>
          </div>
        );
        key++;
      }
    });
    return data;
  }

  addBJ(id) {
    const data = this.state.dataBJ;
    data.push({
      _destroy: false,
      key: this.state.cont,
      id: id == undefined ? null : id,
    });
    this.setState({
      dataBJ: data,
      cont: this.state.cont + 1,
    });
  }

  render() {
    let estimated_value = 0;
    for (let index = 0; index < this.state.estimated_value.length; index++) {
      estimated_value += this.state.estimated_value[index];
    }

    let ltv_value = 0;
    for (let index = 0; index < this.state.ltv_value.length; index++) {
      ltv_value += this.state.ltv_value[index];
    }

    let ltv = 0;
    for (let index = 0; index < this.state.ltv.length; index++) {
      ltv += this.state.ltv[index];
    }

    if (
      this.state.value.loan_amount > ltv_value
      && this.state.validator.loan_amount == undefined
    ) {
      const validate = this.state.validator;
      validate.loan_amount = 'Pinjaman melebihi batas maksimum';
      this.setState({ validator: validate });
    }

    const ExampleCustomInput = ({ value, onClick }) => (
      <img src={Icon.icon_date} onClick={onClick} />
    );
    const { classes } = this.props;
    const loadOptions = (inputValue, callback) => {
      setTimeout(() => {
        callback(this.state.data2);
      }, 1000);
    };
    const loadOptions3 = (inputValue, callback) => {
      setTimeout(() => {
        callback(this.state.data4);
      }, 2000);
    };

    const labelGender = (this.state.value.gender === 'l' && 'Laki - laki')
      || (this.state.value.gender === 'p' && 'Perempuan');

    if (this.state.loader) {
      return (
        <div className={classes.root2}>
          <Dialog
            disablePortal
            disableEnforceFocus
            disableAutoFocus
            open
            scroll="paper"
            maxWidth="md"
            aria-labelledby="server-modal-title"
            aria-describedby="server-modal-description"
            container={() => {}}
          >
            <div
              style={{
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
                padding: '20px',
                borderRadius: '50px',
              }}
            >
              <CircularProgress
                style={{ color: '#85203B', margin: '18px' }}
                size={40}
              />
              Mohon Tunggu
              <div
                style={{
                  marginTop: '10px',
                }}
              />
            </div>
          </Dialog>
        </div>
      );
    }
    return (
      <Dialog
        scroll="paper"
        open
        maxWidth="md"
        aria-labelledby="scroll-dialog-title"
        aria-describedby="scroll-dialog-description"
      >
        <DialogTitle id="scroll-dialog-title">
          <Typography variant="h7" className={classes.tittleModal}>
            Tambah Transaksi Perpanjangan
          </Typography>
          <IconButton
            aria-label="close"
            disabled={this.state.loader_button}
            className={classes.closeButton}
            onClick={() => {
              this.props.handleModal();
            }}
          >
            <CloseIcon />
          </IconButton>
        </DialogTitle>
        <DialogContent dividers>
          <DialogContentText id="scroll-dialog-description" tabIndex={-1}>
            <div className={classes.scrool}>
              <div>
                {Func.toLogin(this.state.redirect)}
                <div className={classes.root}>
                  <Grid
                    container
                    direction="row"
                    item
                    lg={12}
                    xl={12}
                    md={12}
                    xs={12}
                  >
                    <Grid item lg={12} xl={12} md={12} sm={12} xs={12}>
                      <div className={classes.BodytitleMdl2}>
                        <text className={classes.titleMdl}>
                          {this.state.value.prolongation_prolongation_order}
                        </text>
                      </div>
                      <Grid
                        container
                        direction="row"
                        item
                        lg={12}
                        xl={12}
                        md={12}
                        xs={12}
                      >
                        <Grid
                          container
                          item
                          lg={4}
                          xl={4}
                          md={4}
                          sm={4}
                          xs={12}
                        >
                          <div className={classes.label111}>
                            <text className={classes.label121}>
                              Tanggal Akad
                            </text>
                          </div>
                        </Grid>
                        <Grid
                          container
                          item
                          lg={4}
                          xl={4}
                          md={4}
                          sm={4}
                          xs={12}
                        >
                          <div className={classes.label111}>
                            <text className={classes.label121}>Taksiran</text>
                          </div>
                        </Grid>
                        <Grid
                          container
                          item
                          lg={4}
                          xl={4}
                          md={4}
                          sm={4}
                          xs={12}
                        >
                          <div className={classes.label111}>
                            <text className={classes.label121}>
                              Rate Sewa/15 Hari
                            </text>
                          </div>
                        </Grid>
                      </Grid>

                      <Grid
                        container
                        direction="row"
                        item
                        lg={12}
                        xl={12}
                        md={12}
                        xs={12}
                      >
                        <Grid
                          container
                          item
                          lg={4}
                          xl={4}
                          md={4}
                          sm={4}
                          xs={12}
                        >
                          <div className={classes.label111}>
                            <text className={classes.label1}>
                              {this.state.value.prolongation_contract_date}
                            </text>
                          </div>
                        </Grid>
                        <Grid
                          container
                          item
                          lg={4}
                          xl={4}
                          md={4}
                          sm={4}
                          xs={12}
                        >
                          <div className={classes.label111}>
                            <text className={classes.label1}>
                              {this.state.value.prolongation_estimate_value}
                            </text>
                          </div>
                        </Grid>
                        <Grid
                          container
                          item
                          lg={4}
                          xl={4}
                          md={4}
                          sm={4}
                          xs={12}
                        >
                          <div className={classes.label111}>
                            <text className={classes.label1}>
                              {this.state.value.prolongation_rental_per_15_days}
                            </text>
                          </div>
                        </Grid>
                      </Grid>

                      <Grid
                        container
                        direction="row"
                        item
                        lg={12}
                        xl={12}
                        md={12}
                        xs={12}
                      >
                        <Grid
                          container
                          item
                          lg={4}
                          xl={4}
                          md={4}
                          sm={4}
                          xs={12}
                        >
                          <div className={classes.label111}>
                            <text className={classes.label121}>
                              Tanggal Perpanjangan
                            </text>
                          </div>
                        </Grid>
                        <Grid
                          container
                          item
                          lg={4}
                          xl={4}
                          md={4}
                          sm={4}
                          xs={12}
                        >
                          <div className={classes.label111}>
                            <text className={classes.label121}>
                              Jumlah Hari
                            </text>
                          </div>
                        </Grid>
                        <Grid
                          container
                          item
                          lg={4}
                          xl={4}
                          md={4}
                          sm={4}
                          xs={12}
                        >
                          <div className={classes.label111}>
                            <text className={classes.label121}>
                              Total Biaya Sewa
                            </text>
                          </div>
                        </Grid>
                      </Grid>

                      <Grid
                        container
                        direction="row"
                        item
                        lg={12}
                        xl={12}
                        md={12}
                        xs={12}
                      >
                        <Grid
                          container
                          item
                          lg={4}
                          xl={4}
                          md={4}
                          sm={4}
                          xs={12}
                        >
                          <div className={classes.label111}>
                            <text className={classes.label1}>
                              {Func.FormatDate(new Date())}
                            </text>
                          </div>
                        </Grid>
                        <Grid
                          container
                          item
                          lg={4}
                          xl={4}
                          md={4}
                          sm={4}
                          xs={12}
                        >
                          <div className={classes.label111}>
                            <text className={classes.label1}>
                              {this.state.value.prolongation_number_of_days}
                            </text>
                          </div>
                        </Grid>
                        <Grid
                          container
                          item
                          lg={4}
                          xl={4}
                          md={4}
                          sm={4}
                          xs={12}
                        >
                          <div className={classes.label111}>
                            <text className={classes.label1}>
                              {this.state.value
                                .prolongation_total_rental_cost != undefined
                                ? Func.FormatRp(
                                  this.state.value
                                    .prolongation_total_rental_cost
                                )
                                : 'Rp 0'}
                            </text>
                          </div>
                        </Grid>
                      </Grid>
                      <Grid
                        container
                        direction="row"
                        item
                        lg={12}
                        xl={12}
                        md={12}
                        xs={12}
                      >
                        <Grid
                          container
                          item
                          lg={4}
                          xl={4}
                          md={4}
                          sm={4}
                          xs={12}
                        >
                          <div className={classes.label111}>
                            <text className={classes.label121}>
                              Biaya Admin Perpanjangan
                            </text>
                          </div>
                        </Grid>
                        <Grid
                          container
                          item
                          lg={4}
                          xl={4}
                          md={4}
                          sm={4}
                          xs={12}
                        >
                          <div className={classes.label111}>
                            <text className={classes.label121}>
                              Tanggal Jatuh Tempo
                            </text>
                          </div>
                        </Grid>
                        <Grid
                          container
                          item
                          lg={4}
                          xl={4}
                          md={4}
                          sm={4}
                          xs={12}
                        >
                          <div className={classes.label111}>
                            <text className={classes.label121}>
                              Jumlah Pinjaman
                            </text>
                          </div>
                        </Grid>
                      </Grid>

                      <Grid
                        container
                        direction="row"
                        item
                        lg={12}
                        xl={12}
                        md={12}
                        xs={12}
                      >
                        <Grid
                          container
                          item
                          lg={4}
                          xl={4}
                          md={4}
                          sm={4}
                          xs={12}
                        >
                          <div className={classes.label111}>
                            <text className={classes.label1}>
                              {this.state.value.prolongation_admin_fee
                              != undefined
                                ? Func.FormatRp(
                                  this.state.value.prolongation_admin_fee
                                )
                                : 'Rp 0'}
                            </text>
                          </div>
                        </Grid>
                        <Grid
                          container
                          item
                          lg={4}
                          xl={4}
                          md={4}
                          sm={4}
                          xs={12}
                        >
                          <div className={classes.label111}>
                            <text className={classes.label1}>
                              {this.state.value.prolongation_due_date}
                            </text>
                          </div>
                        </Grid>
                        <Grid
                          container
                          item
                          lg={4}
                          xl={4}
                          md={4}
                          sm={4}
                          xs={12}
                        >
                          <div className={classes.label111}>
                            <text className={classes.label1}>
                              {Func.FormatRp(this.state.value.prolongation_loan_amount)}
                            </text>
                          </div>
                        </Grid>
                      </Grid>
                      <Grid
                        container
                        direction="row"
                        item
                        lg={12}
                        xl={12}
                        md={12}
                        xs={12}
                      >
                        <Grid
                          container
                          item
                          lg={4}
                          xl={4}
                          md={4}
                          sm={4}
                          xs={12}
                        >
                          <div className={classes.label111}>
                            <text className={classes.label121}>Denda</text>
                          </div>
                        </Grid>
                        <Grid
                          container
                          item
                          lg={4}
                          xl={4}
                          md={4}
                          sm={4}
                          xs={12}
                        >
                          <div className={classes.label111}>
                            <text className={classes.label121}>
                              Pengali Tarif Sewa
                            </text>
                          </div>
                        </Grid>
                      </Grid>
                      <Grid
                        container
                        direction="row"
                        item
                        lg={12}
                        xl={12}
                        md={12}
                        xs={12}
                      >
                        <Grid
                          container
                          item
                          lg={4}
                          xl={4}
                          md={4}
                          sm={4}
                          xs={12}
                        >
                          <div className={classes.label111}>
                            <text className={classes.label1}>
                              {this.state.value.prolongation_fine_amount
                              != undefined
                                ? Func.FormatRp(
                                  this.state.value.prolongation_fine_amount
                                )
                                : 'Rp 0'}
                            </text>
                          </div>
                        </Grid>
                        <Grid
                          container
                          item
                          lg={4}
                          xl={4}
                          md={4}
                          sm={4}
                          xs={12}
                        >
                          <div className={classes.label111}>
                            <text className={classes.label1}>
                              {this.state.value.prolongation_multiplier}
                            </text>
                          </div>
                        </Grid>
                      </Grid>
                      <Divider className={classes.divider} />
                      <div
                        style={{
                          marginBottom: 50,
                        }}
                      />
                      <div
                        style={{
                          marginBottom: 50,
                        }}
                      >
                        {this.renderBJ()}
                        {this.state.showAdd ? (
                          <div>
                            <div
                              className={classes.BodytitleMdl2}
                              style={{
                                marginTop: 50,
                              }}
                            >
                              <text className={classes.titleMdl}>
                                BARANG JAMINAN
                              </text>
                            </div>
                            <Grid
                              container
                              direction="row"
                              item
                              lg={12}
                              xl={12}
                              md={12}
                              xs={12}
                            >
                              <Grid
                                container
                                item
                                lg={8}
                                xl={8}
                                md={8}
                                sm={8}
                                xs={12}
                              >
                                <Grid
                                  container
                                  item
                                  lg={12}
                                  xl={12}
                                  md={12}
                                  sm={12}
                                  xs={12}
                                >
                                  <Grid
                                    item
                                    lg={6}
                                    xl={6}
                                    md={6}
                                    sm={6}
                                    xs={12}
                                    className={classes.formPad}
                                  >
                                    <div
                                      style={{
                                        marginBottom: 5,
                                      }}
                                    >
                                      <text className={classes.label1}>
                                        Kategori Barang Jaminan
                                      </text>
                                      <text className={classes.starts1}>*</text>
                                    </div>
                                    <div>
                                      <Select2
                                        name="form-field-name-error"
                                        value={
                                          this.state.setValue
                                            .product_insurance_item_id
                                        }
                                        placeholder="Pilih"
                                        onFocus={() => {
                                          this.removeValidate(
                                            'product_insurance_item_id'
                                          );
                                        }}
                                        styles={{
                                          control: (provided, state) => ({
                                            ...provided,
                                            borderColor: this.state.validator
                                              .product_insurance_item_id
                                              ? 'red'
                                              : '#CACACA',
                                            borderRadius: '0.25rem',
                                          }),
                                        }}
                                        className={classes.input21}
                                        options={this.state.data5}
                                        onChange={(val) => {
                                          this.handleChange(
                                            val,
                                            'product_insurance_item_id'
                                          );
                                        }}
                                      />
                                      <FormHelperText className={classes.error}>
                                        {
                                          this.state.validator.product_insurance_item_id
                                        }
                                      </FormHelperText>
                                    </div>
                                  </Grid>
                                  <Grid
                                    container
                                    lg={6}
                                    xl={6}
                                    md={6}
                                    sm={6}
                                    xs={12}
                                    className={classes.formPad}
                                  >
                                    <Grid
                                      item
                                      lg={6}
                                      xl={6}
                                      md={6}
                                      sm={6}
                                      xs={12}
                                      className={classes.formPad}
                                    >
                                      <div>
                                        <text className={classes.label1}>
                                          Jumlah
                                        </text>
                                        <text className={classes.starts1}>
                                          *
                                        </text>
                                      </div>
                                      <div>
                                        <TextField
                                          size="small"
                                          style={{
                                            marginTop: 5,
                                          }}
                                          className={classes.input234}
                                          variant="outlined"
                                          type="number"
                                          autoComplete="off"
                                          onFocus={() => {
                                            this.removeValidate('amount');
                                          }}
                                          error={this.state.validator.amount}
                                          value={this.state.setValue.amount}
                                          onChange={(event) => {
                                            this.handleChange(
                                              event.target.value,
                                              'amount'
                                            );
                                            this.getEstimate();
                                          }}
                                          name="amount"
                                        />
                                      </div>
                                    </Grid>
                                    <Grid
                                      item
                                      lg={6}
                                      xl={6}
                                      md={6}
                                      sm={6}
                                      xs={12}
                                      className={classes.formPad}
                                    >
                                      <div>
                                        <text className={classes.label1}>
                                          Karatase
                                        </text>
                                        <text className={classes.starts1}>
                                          *
                                        </text>
                                      </div>
                                      <div>
                                        <TextField
                                          size="small"
                                          style={{
                                            marginTop: 5,
                                          }}
                                          className={classes.input234}
                                          variant="outlined"
                                          type="number"
                                          autoComplete="off"
                                          onFocus={() => {
                                            this.removeValidate('carats');
                                          }}
                                          error={this.state.validator.carats}
                                          value={this.state.setValue.carats}
                                          onChange={(event) => {
                                            this.handleChange(
                                              event.target.value,
                                              'carats'
                                            );
                                            this.getEstimate();
                                          }}
                                          name="carats"
                                        />
                                      </div>
                                    </Grid>
                                  </Grid>
                                </Grid>
                                <Grid
                                  container
                                  item
                                  lg={12}
                                  xl={12}
                                  md={12}
                                  sm={12}
                                  xs={12}
                                >
                                  <Grid
                                    item
                                    lg={6}
                                    xl={6}
                                    md={6}
                                    sm={6}
                                    xs={12}
                                    className={classes.formPad}
                                  >
                                    <div>
                                      <div className={classes.label111}>
                                        <text className={classes.label1}>
                                          Kepemilikan Barang Jaminan
                                        </text>
                                        <text className={classes.starts1}>
                                          *
                                        </text>
                                      </div>
                                      <Select2
                                        name="form-field-name-error"
                                        value={this.state.setValue.ownership}
                                        placeholder="Pilih"
                                        onFocus={() => {
                                          this.removeValidate('ownership');
                                        }}
                                        styles={{
                                          control: (provided, state) => ({
                                            ...provided,
                                            borderColor: this.state.validator.ownership
                                              ? 'red'
                                              : '#CACACA',
                                            borderRadius: '0.25rem',
                                          }),
                                        }}
                                        onChange={(data) => {
                                          this.handleChange(data, 'ownership');
                                        }}
                                        className={classes.input21}
                                        options={[
                                          {
                                            value: 'Milik Sendiri',
                                            label: 'Milik Sendiri',
                                          },
                                          {
                                            value: 'Milik Keluarga',
                                            label: 'Milik Keluarga',
                                          },
                                          {
                                            value: 'Warisan',
                                            label: 'Warisan',
                                          },
                                          {
                                            value: 'Hasil Usaha',
                                            label: 'Hasil Usaha',
                                          },
                                        ]}
                                      />
                                    </div>
                                  </Grid>
                                  <Grid
                                    container
                                    lg={6}
                                    xl={6}
                                    md={6}
                                    sm={6}
                                    xs={12}
                                    className={classes.formPad}
                                  >
                                    <Grid
                                      item
                                      lg={6}
                                      xl={6}
                                      md={6}
                                      sm={6}
                                      xs={12}
                                      className={classes.formPad}
                                    >
                                      <div>
                                        <text className={classes.label1}>
                                          Berat Kotor
                                        </text>
                                        <text className={classes.starts1}>
                                          *
                                        </text>
                                      </div>
                                      <div>
                                        <TextField
                                          size="small"
                                          style={{
                                            marginTop: 5,
                                          }}
                                          className={classes.input234}
                                          variant="outlined"
                                          type="number"
                                          autoComplete="off"
                                          onFocus={() => {
                                            this.removeValidate('gross_weight');
                                          }}
                                          error={
                                            this.state.validator.gross_weight
                                          }
                                          value={
                                            this.state.setValue.gross_weight
                                          }
                                          onChange={(event) => {
                                            this.handleChange(
                                              event.target.value,
                                              'gross_weight'
                                            );
                                            this.getEstimate();
                                          }}
                                          name="gross_weight"
                                        />
                                      </div>
                                    </Grid>
                                    <Grid
                                      item
                                      lg={6}
                                      xl={6}
                                      md={6}
                                      sm={6}
                                      xs={12}
                                      className={classes.formPad}
                                    >
                                      <div>
                                        <text className={classes.label1}>
                                          Berat Bersih
                                        </text>
                                        <text className={classes.starts1}>
                                          *
                                        </text>
                                      </div>
                                      <div>
                                        <TextField
                                          size="small"
                                          style={{
                                            marginTop: 5,
                                          }}
                                          className={classes.input234}
                                          variant="outlined"
                                          type="number"
                                          autoComplete="off"
                                          onFocus={() => {
                                            this.removeValidate('net_weight');
                                          }}
                                          error={
                                            this.state.validator.net_weight
                                          }
                                          value={
                                            this.state.setValue.net_weight
                                          }
                                          onChange={(event) => {
                                            this.handleChange(
                                              event.target.value,
                                              'net_weight'
                                            );
                                            this.getEstimate();
                                          }}
                                          name="net_weight"
                                        />
                                      </div>
                                    </Grid>
                                  </Grid>
                                </Grid>
                              </Grid>
                              <Grid
                                container
                                item
                                lg={4}
                                xl={4}
                                md={4}
                                sm={4}
                                xs={12}
                                spacing={3}
                              >
                                <Grid
                                  item
                                  lg={12}
                                  xl={12}
                                  md={12}
                                  sm={12}
                                  xs={12}
                                  className={classes.formPad}
                                >
                                  <div>
                                    <text className={classes.label1}>
                                      Deskripsi
                                    </text>
                                    <text className={classes.starts1}>*</text>
                                  </div>
                                  <TextareaAutosize
                                    className={
                                      this.state.validator.description
                                        ? classes.textArea2
                                        : classes.textArea
                                    }
                                    variant="outlined"
                                    margin="normal"
                                    rows={8}
                                    autoComplete="off"
                                    onFocus={() => {
                                      this.removeValidate('description');
                                    }}
                                    error={this.state.validator.description}
                                    value={this.state.setValue.description}
                                    onChange={(event) => {
                                      this.handleChange(
                                        event.target.value,
                                        'description'
                                      );
                                    }}
                                    name="description"
                                    InputProps={{
                                      endAdornment: this.state.validator.description ? (
                                        <InputAdornment position="start">
                                          <img src={Icon.warning} />
                                        </InputAdornment>
                                      ) : (
                                        <div />
                                      ),
                                    }}
                                  />
                                  <FormHelperText className={classes.error}>
                                    {this.state.validator.description}
                                  </FormHelperText>
                                </Grid>
                              </Grid>
                            </Grid>
                            <Grid item lg={4} xl={4} md={4} sm={4} xs={12}>
                              <Box
                                borderColor={
                                  this.state.validator.Insurance_item_image
                                    ? 'error.main'
                                    : 'grey.500'
                                }
                                border={1}
                                onClick={() => {
                                  this.removeValidate('Insurance_item_image');
                                }}
                                className={classes.imgScan}
                              >
                                {this.state.setValue.Insurance_item_image ? (
                                  <img
                                    className={classes.imgScan2}
                                    onClick={() => {
                                      this.removeValidate(
                                        'Insurance_item_image'
                                      );
                                    }}
                                    src={
                                      this.state.setValue.Insurance_item_image
                                    }
                                  />
                                ) : null}
                              </Box>
                              <FormHelperText className={classes.error}>
                                {this.state.validator.Insurance_item_image}
                              </FormHelperText>
                            </Grid>
                            <Grid item lg={4} xl={4} md={4} sm={4} xs={12}>
                              <div className={classes.BodytitleMdl22}>
                                <img
                                  src={Icon.deleteImg}
                                  onClick={() => {
                                    const dataSet = this.state.setValue;
                                    dataSet.Insurance_item_image = null;
                                    this.setState({ setValue: dataSet });
                                  }}
                                />
                              </div>
                              <div className={classes.BodytitleMdl23}>
                                <input
                                  type="file"
                                  accept="image/*"
                                  name="file"
                                  title="Pilih Gambar"
                                  onChange={(event) => {
                                    this.handleChangeImg(
                                      event,
                                      'Insurance_item_image'
                                    );
                                  }}
                                />
                                Atau
                                {' '}
                                <Webcam
                                  setState={this.setState.bind(this)}
                                  handleWebcam={this.handleWebcam.bind(this)}
                                  stateName="Insurance_item_image"
                                />
                              </div>
                            </Grid>
                          </div>
                        ) : null}
                        <button
                          style={{
                            marginTop: 30,
                            marginLeft: 15,
                            backgroundColor: '#C4A643',
                            borderRadius: 50,
                            color: 'white',
                            width: 200,
                            height: 35,
                            fontWeight: '500',
                            fontSize: 14,
                          }}
                          onClick={() => {
                            this.setState({ showAdd: !this.state.showAdd });
                          }}
                        >
                          {this.state.showAdd
                            ? 'Batal'
                            : 'Tambah Barang Jaminan'}
                        </button>
                        {this.state.showAdd ? (
                          <button
                            style={{
                              marginTop: 30,
                              marginLeft: 15,
                              backgroundColor: 'rgb(133, 32, 59)',
                              borderRadius: 50,
                              color: 'white',
                              width: 200,
                              height: 35,
                              fontWeight: '500',
                              fontSize: 14,
                            }}
                            onClick={() => {
                              const validator = [];
                              validator.push({
                                name: 'product_insurance_item_id',
                                type: 'required',
                              });
                              validator.push({
                                name: 'ownership',
                                type: 'required',
                              });
                              validator.push({
                                name: 'amount',
                                type: 'required',
                              });
                              validator.push({
                                name: 'gross_weight',
                                type: 'required|maxInt:1000',
                              });
                              validator.push({
                                name: 'net_weight',
                                type: 'required|maxInt:1000',
                              });
                              validator.push({
                                name: 'carats',
                                type: 'required|minInt:24',
                              });
                              validator.push({
                                name: 'description',
                                type: 'required',
                              });
                              validator.push({
                                name: 'Insurance_item_image',
                                type: 'required',
                              });
                              const validate = Func.Validator(
                                this.state.setValue,
                                validator
                              );
                              if (validate.success) {
                                const data = this.state.dataBJ;
                                const val = this.state.value;
                                val[
                                  'name' + this.state.cont
                                ] = this.state.setValue.product_insurance_item_id.label;
                                val[
                                  'product_insurance_item_id' + this.state.cont
                                ] = this.state.setValue.product_insurance_item_id;
                                val[
                                  'product_insurance_item_name'
                                    + this.state.cont
                                ] = this.state.setValue.product_insurance_item_id.label;
                                val[
                                  'ownership' + this.state.cont
                                ] = this.state.setValue.ownership;
                                val['amount' + this.state.cont] = parseInt(
                                  this.state.setValue.amount
                                );
                                val['net_weight' + this.state.cont] = parseInt(
                                  this.state.setValue.net_weight
                                );
                                val[
                                  'gross_weight' + this.state.cont
                                ] = parseInt(this.state.setValue.gross_weight);
                                val['carats' + this.state.cont] = parseInt(
                                  this.state.setValue.carats
                                );
                                val[
                                  'description' + this.state.cont
                                ] = this.state.setValue.description;
                                val[
                                  'Insurance_item_image' + this.state.cont
                                ] = this.state.setValue.Insurance_item_image;
                                this.setState(
                                  {
                                    value: val,
                                    setValue: [],
                                    showAdd: false,
                                  },
                                  () => {
                                    this.getEstimate(this.state.cont);
                                    data.push({
                                      _destroy: false,
                                      key: this.state.cont,
                                      id: null,
                                    });
                                    this.setState({
                                      dataBJ: data,
                                      cont: this.state.cont + 1,
                                      changed: true,
                                    });
                                  }
                                );
                              } else {
                                this.setState({ validator: validate.error });
                              }
                            }}
                          >
                            Simpan
                          </button>
                        ) : null}
                      </div>
                      <Divider className={classes.divider} />
                      <Grid
                        style={{ marginTop: 30 }}
                        container
                        direction="row"
                        item
                        lg={12}
                        xl={12}
                        md={12}
                        xs={12}
                      >
                        <Grid
                          container
                          item
                          lg={3}
                          xl={3}
                          md={3}
                          sm={3}
                          xs={12}
                        >
                          <div className={classes.label111}>
                            <text className={classes.label121}>
                              Detail Perpanjangan
                            </text>
                          </div>
                        </Grid>
                        <Grid
                          container
                          item
                          lg={3}
                          xl={3}
                          md={3}
                          sm={3}
                          xs={12}
                        />
                        <Grid
                          container
                          item
                          lg={3}
                          xl={3}
                          md={3}
                          sm={3}
                          xs={12}
                        />
                        <Grid
                          container
                          item
                          lg={3}
                          xl={3}
                          md={3}
                          sm={3}
                          xs={12}
                        />
                      </Grid>

                      <Grid
                        style={{ marginTop: 20 }}
                        container
                        direction="row"
                        item
                        lg={12}
                        xl={12}
                        md={12}
                        xs={12}
                      >
                        <Grid
                          container
                          item
                          lg={3}
                          xl={3}
                          md={3}
                          sm={3}
                          xs={12}
                        >
                          <div className={classes.label111}>
                            <text className={classes.label1}>Denda</text>
                          </div>
                        </Grid>
                        <Grid
                          container
                          item
                          lg={3}
                          xl={3}
                          md={3}
                          sm={3}
                          xs={12}
                        />
                        <Grid
                          container
                          item
                          lg={3}
                          xl={3}
                          md={3}
                          sm={3}
                          xs={12}
                        />
                        <Grid
                          container
                          item
                          lg={3}
                          xl={3}
                          md={3}
                          sm={3}
                          xs={12}
                        >
                          <div className={classes.label111}>
                            <text className={classes.label1}>
                              {this.state.value.prolongation_fine_amount
                              != undefined
                                ? Func.FormatRp(
                                  this.state.value.prolongation_fine_amount
                                )
                                : 'Rp 0'}
                            </text>
                          </div>
                        </Grid>
                      </Grid>
                      <Divider />
                      <Grid
                        container
                        direction="row"
                        item
                        lg={12}
                        xl={12}
                        md={12}
                        xs={12}
                      >
                        <Grid
                          container
                          item
                          lg={3}
                          xl={3}
                          md={3}
                          sm={3}
                          xs={12}
                        >
                          <div className={classes.label111}>
                            <text className={classes.label1}>
                              Total Biaya Sewa
                            </text>
                          </div>
                        </Grid>
                        <Grid
                          container
                          item
                          lg={3}
                          xl={3}
                          md={3}
                          sm={3}
                          xs={12}
                        />
                        <Grid
                          container
                          item
                          lg={3}
                          xl={3}
                          md={3}
                          sm={3}
                          xs={12}
                        />
                        <Grid
                          container
                          item
                          lg={3}
                          xl={3}
                          md={3}
                          sm={3}
                          xs={12}
                        >
                          <div className={classes.label111}>
                            <text className={classes.label1}>
                              {this.state.value
                                .prolongation_total_rental_cost != undefined
                                ? Func.FormatRp(
                                  this.state.value
                                    .prolongation_total_rental_cost
                                )
                                : 'Rp 0'}
                            </text>
                          </div>
                        </Grid>
                      </Grid>
                      <Divider />
                      <Grid
                        container
                        direction="row"
                        item
                        lg={12}
                        xl={12}
                        md={12}
                        xs={12}
                      >
                        <Grid
                          container
                          item
                          lg={3}
                          xl={3}
                          md={3}
                          sm={3}
                          xs={12}
                        >
                          <div className={classes.label111}>
                            <text className={classes.label1}>Admin</text>
                          </div>
                        </Grid>
                        <Grid
                          container
                          item
                          lg={3}
                          xl={3}
                          md={3}
                          sm={3}
                          xs={12}
                        />
                        <Grid
                          container
                          item
                          lg={3}
                          xl={3}
                          md={3}
                          sm={3}
                          xs={12}
                        />
                        <Grid
                          container
                          item
                          lg={3}
                          xl={3}
                          md={3}
                          sm={3}
                          xs={12}
                        >
                          <div className={classes.label111}>
                            <text className={classes.label1}>
                              {this.state.value.prolongation_admin_fee
                              != undefined
                                ? Func.FormatRp(
                                  this.state.value.prolongation_admin_fee
                                )
                                : 'Rp 0'}
                            </text>
                          </div>
                        </Grid>
                      </Grid>
                      <Divider />
                      {this.renderPembayaran()}
                      <Grid
                        container
                        direction="row"
                        item
                        lg={12}
                        xl={12}
                        md={12}
                        xs={12}
                      >
                        <Grid
                          container
                          item
                          lg={3}
                          xl={3}
                          md={3}
                          sm={3}
                          xs={12}
                        />
                        <Grid
                          container
                          item
                          lg={3}
                          xl={3}
                          md={3}
                          sm={3}
                          xs={12}
                        />
                        <Grid
                          container
                          item
                          lg={3}
                          xl={3}
                          md={3}
                          sm={3}
                          xs={12}
                        >
                          <div className={classes.label111} />
                        </Grid>
                        <Grid
                          container
                          item
                          lg={3}
                          xl={3}
                          md={3}
                          sm={3}
                          xs={12}
                        >
                          <div className={classes.label111}>
                            <text className={classes.label1} style={{ marginRight: '18px', marginLeft: -40 }}>Total</text>
                            {this.renderTotal()}
                          </div>
                        </Grid>
                      </Grid>
                    </Grid>
                  </Grid>
                </div>
              </div>
            </div>
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          {this.state.loader == false ? (
            <Button
              disabled={this.state.loader_button}
              onClick={() => {
                swal({
                  title: 'Apakah Anda Yakin?',
                  text: 'Akan memperpanjang transaksi',
                  icon: 'info',
                  buttons: true,
                  dangerMode: true,
                }).then((willDelete) => {
                  if (willDelete) {
                    this.handleSubmit();
                  }
                });
              }}
              style={{
                backgroundColor: '#862C3A',
                color: 'white',
              }}
            >
              <Typography variant="button" style={{ color: '#FFFFFF' }}>
                {this.state.loader_button ? (
                  <CircularProgress
                    style={{
                      color: 'white',
                      marginLeft: '18px',
                      marginRight: '18px',
                      marginTop: 5,
                    }}
                    size={15}
                  />
                ) : (
                  'Perpanjang'
                )}
              </Typography>
            </Button>
          ) : null}
          <Button
            style={{
              color: 'black',
            }}
            disabled={this.state.loader_button}
            onClick={() => {
              this.props.handleModal();
            }}
            variant="outlined"
          >
            <Typography variant="button">Batal</Typography>
          </Button>
        </DialogActions>
      </Dialog>
    );
  }
}

export default withStyles(styles.CoustomsStyles, { name: 'Form' })(Form);
