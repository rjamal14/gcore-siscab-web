/* eslint-disable react/no-access-state-in-setstate */
/* eslint-disable react/no-deprecated */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/sort-comp */
/* eslint-disable react/no-unused-state */
import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Func from '../../../../../functions/index';
import styles from '../css';
import Icon from '../../../../../components/icon';
import Field from './field';
import Approve from './approve';
import Repayment from './repayment';
import Extendeed from './extendeed';
import Installment from './installment';
import AddRepayment from './addRepayment';
import InstallmentApprove from './installmentApprove';
import InstallmentRepay from './installmentRepay';
import { withRouter } from "react-router";

class Form extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: false,
      modal2: false,
      modal3: false,
      modal4: false,
      modal5: false,
      modalInstallment: false,
      modalInstallment2: false,
      installmentRepay: false,
      validator: [],
      value: [],
      type: '',
      activeTabs: 0,
      Proses: 0,
      imgPath: Icon.blank,
      count: 0,
      id: null,
    };
    this.handleChangeImg = this.handleChangeImg.bind(this);
  }

  onPage(type) {
    let val = '';
    if (type === '-') {
      val = this.state.activeTabs - 1;
    } else {
      val = this.state.activeTabs + 1;
    }
    this.setState({
      activeTabs: val,
      Proses: val,
    });
  }

  componentWillReceiveProps() {
    if (this.props.row !== 'undefined') {
      if (this.props.modal === true) {
        this.setState({
          modal: true,
          modal2: false,
          modal3: false,
          modal4: false,
          modal5: false,
          modalInstallment: false,
          modalInstallment2: false,
          installmentRepay: false,
          type: this.props.type,
        });
      } else if (this.props.modal2 === true) {
        this.setState({
          modal: false,
          modal2: true,
          modal3: false,
          modal4: false,
          modal5: false,
          modalInstallment: false,
          modalInstallment2: false,
          installmentRepay: false,
          type: this.props.type,
        });
      } else if (this.props.modal3 === true) {
        this.setState({
          modal: false,
          modal2: false,
          modal3: true,
          modal4: false,
          modal5: false,
          modalInstallment: false,
          modalInstallment2: false,
          installmentRepay: false,
          type: this.props.type,
        });
      } else if (this.props.modal4 === true) {
        this.setState({
          modal: false,
          modal2: false,
          modal3: false,
          modal4: true,
          modal5: false,
          modalInstallment: false,
          modalInstallment2: false,
          installmentRepay: false,
          type: this.props.type,
        });
      } else if (this.props.modal5 === true) {
        this.setState({
          modal: false,
          modal2: false,
          modal3: false,
          modal4: false,
          modal5: true,
          modalInstallment: false,
          modalInstallment2: false,
          installmentRepay: false,
          type: this.props.type,
        });
      } else if (this.props.modalInstallment === true) {
        this.setState({
          modal: false,
          modal2: false,
          modal3: false,
          modal4: false,
          modal5: false,
          modalInstallment: true,
          modalInstallment2: false,
          installmentRepay: false,
          type: this.props.type,
        });
      } else if (this.props.modalInstallment2 === true) {
        this.setState({
          modal: false,
          modal2: false,
          modal3: false,
          modal4: false,
          modal5: false,
          modalInstallment: false,
          modalInstallment2: true,
          installmentRepay: false,
          type: this.props.type,
        });
      } else if (this.props.installmentRepay === true) {
        this.setState({
          modal: false,
          modal2: false,
          modal3: false,
          modal4: false,
          modal5: false,
          modalInstallment: false,
          modalInstallment2: false,
          installmentRepay: true,
          type: this.props.type,
        });
      }
    }
    if (this.props.type === 'Ubah' && this.props.row !== undefined) {
      this.setState({ id: this.props.row.id });
    }

    const { id, type } = this.props.match.params;
    if (id && type) this.setState({ id, type: 'Ubah' })
    if (this.state.id !== id) {
      if (type === 'installment') this.setState({ modalInstallment2: true })
    }
  }

  removeValidate(name) {
    const data = this.state.validator;
    delete data[name];
    this.setState({ validator: data });
  }

  handleModal() {
    this.setState({
      modal: false,
      modal2: false,
      modal3: false,
      modal4: false,
      modal5: false,
      modalInstallment: false,
      modalInstallment2: false,
      installmentRepay: false,
    });
  }

  handleChange(event, name) {
    const dataSet = this.state.value;
    dataSet[name] = event.target.value;
    this.setState({ value: dataSet });
  }

  handleSubmit() {
    if (this.state.activeTabs === 6) {
      this.setState({
        modal: !this.state.modal,
        activeTabs: 0,
      });
    } else {
      this.setState({
        Proses: this.state.activeTabs,
      });
    }
  }

  handleChangeImg(event) {
    this.setState({
      imgPath: URL.createObjectURL(event.target.files[0]),
    });
  }

  render() {
    if (this.state.modal) {
      return (
        <Field
          Proses={this.state.Proses}
          value={this.state.value}
          type={this.state.type}
          id={this.state.id}
          count={this.state.count}
          handleModal={() => {
            this.handleModal();
          }}
          OnNext={(res) => {
            this.setState({ modal: false });
            Func.AlertForm('Berhasil', res, 'success');
          }}
        />
      );
    }
    if (this.state.modal2) {
      return (
        <Approve
          Proses={this.state.Proses}
          value={this.state.value}
          type={this.state.type}
          id={this.state.id}
          count={this.state.count}
          handleModal={() => {
            this.handleModal();
          }}
          OnNext={(res) => {
            this.setState({ modal: false, modal2: false });
            Func.AlertForm('Berhasil', res, 'success');
          }}
          closeModal={() => this.setState({ modal: false, modal2: false })}
        />
      );
    }
    if (this.state.modal3) {
      return (
        <Repayment
          Proses={this.state.Proses}
          value={this.state.value}
          type={this.state.type}
          id={this.state.id}
          count={this.state.count}
          handleModal={() => {
            this.handleModal();
          }}
          OnNext={(res) => {
            this.setState({ modal: false, modal2: false });
            Func.AlertForm('Berhasil', res, 'success');
          }}
        />
      );
    }
    if (this.state.modal4) {
      return (
        <Extendeed
          Proses={this.state.Proses}
          value={this.state.value}
          type={this.state.type}
          id={this.state.id}
          count={this.state.count}
          handleModal={() => {
            this.handleModal();
          }}
          OnNext={(res) => {
            this.setState({ modal: false, modal2: false });
            Func.AlertForm('Berhasil', res, 'success');
          }}
        />
      );
    }
    if (this.state.modalInstallment) {
      return (
        <Installment
          Proses={this.state.Proses}
          value={this.state.value}
          type={this.state.type}
          id={this.state.id}
          count={this.state.count}
          handleModal={() => {
            this.handleModal();
          }}
          OnNext={(res) => {
            this.setState({ modalInstallment: false });
            Func.AlertForm('Berhasil', res, 'success');
          }}
        />
      );
    }
    if (this.state.modalInstallment2) {
      return (
        <InstallmentApprove
          Proses={this.state.Proses}
          value={this.state.value}
          type={this.state.type}
          id={this.state.id}
          count={this.state.count}
          handleModal={() => {
            this.handleModal();
          }}
          OnNext={(res) => {
            this.setState({ modalInstallment2: false });
            Func.AlertForm('Berhasil', res, 'success');
          }}
        />
      );
    }
    if (this.state.installmentRepay) {
      return (
        <InstallmentRepay
          Proses={this.state.Proses}
          value={this.state.value}
          type={this.state.type}
          id={this.state.id}
          count={this.state.count}
          handleModal={() => {
            this.handleModal();
          }}
          OnNext={(res) => {
            this.setState({ installmentRepay: false });
            Func.AlertForm('Berhasil', res, 'success');
          }}
        />
      );
    }
    if (this.state.modal5) {
      return (
        <AddRepayment
          Proses={this.state.Proses}
          value={this.state.value}
          type={this.state.type}
          id={this.state.id}
          count={this.state.count}
          handleModal={() => {
            this.handleModal();
          }}
          OnNext={(res) => {
            this.setState({ modal: false, modal2: false });
            Func.AlertForm('Berhasil', res, 'success');
          }}
        />
      );
    }
    return <div />;
  }
}

export default withRouter(withStyles(styles.CoustomsStyles, {
  name: 'Form',
})(Form));
