/* eslint-disable no-octal-escape */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable react/no-access-state-in-setstate */
/* eslint-disable no-plusplus */
/* eslint-disable no-unused-vars */
/* eslint-disable camelcase */
/* eslint-disable array-callback-return */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/sort-comp */
/* eslint-disable react/no-unused-state */
import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Divider from '@material-ui/core/Divider';
import Typography from '@material-ui/core/Typography';
import swal from 'sweetalert';
import FormHelperText from '@material-ui/core/FormHelperText';
import Grid from '@material-ui/core/Grid';
import DialogContentText from '@material-ui/core/DialogContentText';
import MuiDialogActions from '@material-ui/core/DialogActions';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Button from '@material-ui/core/Button';
import Box from '@material-ui/core/Box';
import { CircularProgress } from '@material-ui/core';
import AsyncSelect from 'react-select/async';
import Func from '../../../../../functions/index';
import Icon from '../../../../../components/icon';
import styles from '../css';
import '../../../../../styles/date_field.css';

const DialogActions = withStyles((theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(1),
  },
}))(MuiDialogActions);

class Form extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: false,
      validator: [],
      value: { gender: { label: '' } },
      type: '',
      activeTabs: 0,
      Proses: false,
      ImgBase124: '',
      section: 0,
      id: null,
      redirect: false,
      date: new Date(),
      provin: [],
      data3: [],
      estimated_value: [],
      ltv_value: [],
      ltv: [],
      monthly_fee: [],
      dataBJ: [],
      cont: 0,
      approvals: [],
      download: null,
      loader: false,
      transaction_id: null,
      show: false,
    };
    this.timeout = 0;
  }

  componentDidMount() {}

  handleSubmit() {
    fetch(
      process.env.REACT_APP_URL_MANAGEMENT
        + process.env.REACT_APP_API_PREFIX_V1
        + '/transactions/'
        + this.state.transaction_id
        + '/repayments',
      {
        method: 'PUT',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + localStorage.getItem('token'),
        },
      }
    )
      .then((response) => response.json())
      .then(() => {
        window.open(process.env.REACT_APP_BASE_URL_MANAJEMENT + process.env.REACT_APP_API_PREFIX_V1 + '/transactions/' + this.state.transaction_id + '/print_repayment.pdf?token=' + localStorage.getItem('token'));
        this.props.handleModal();
        this.props.OnNext('Pelunsan berhasil dibuat');
      })
      .catch(() => {})
      .finally(() => {});
  }

  handleChange(val) {
    this.setState(
      {
        transaction_id: val.value,
      },
      () => {
        this.getDatail(this.state.transaction_id);
      }
    );
  }

  getDatail(id) {
    fetch(
      process.env.REACT_APP_URL_MANAGEMENT
        + process.env.REACT_APP_API_PREFIX_V1
        + '/transactions/'
        + id
        + '/repayments.json?token='
        + localStorage.getItem('token'),
      {
        method: 'GET',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
      }
    )
      .then((response) => response.json())
      .then((json) => {
        this.setState({ loader: false });
        if (json.code === '403') {
          if (Func.Clear_Token() === true) {
            if (!localStorage.getItem('token')) {
              this.setState({ redirect: true });
            }
          }
        }
        const val = this.state.value;

        try {
          val.loan_amount = Func.FormatRp(json.data.loan_amount);
          val.estimate_value = Func.FormatRp(json.data.estimate_value);
          val.rental_per_15_days = json.data.rental_per_15_days;
          val.contract_date = Func.FormatDate(json.data.contract_date);
          val.due_date = Func.FormatDate(json.data.due_date);
          val.total_rental_cost = Func.FormatRp(json.data.total_rental_cost);
          val.fine_amount = Func.FormatRp(json.data.fine_amount);
          val.number_of_days = json.data.number_of_days;
          val.multiplier = json.data.multiplier;
          val.payment_date = Func.FormatDate(json.data.payment_date);
          val.total_bill = Func.FormatRp(json.data.total_bill);
          val.total = Func.FormatRp(
            json.data.loan_amount
              + json.data.total_rental_cost
              + json.data.fine_amount
          );

          this.setState({
            value: val,
            show: true,
          });
        } catch (err) {
          this.setState({ loader: false });
          alert(err);
        }
      })
      .catch(() => {})
      .finally(() => {});
  }

  getSGE(val) {
    fetch(
      process.env.REACT_APP_URL_MANAGEMENT
        + process.env.REACT_APP_API_PREFIX_V1
        + '/transactions/repayment/autocomplete'
        + '?query='
        + val
        + '&office_id='
        + localStorage.getItem('office_id'),
      {
        method: 'GET',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + localStorage.getItem('token'),
        },
      }
    )
      .then((response) => response.json())
      .then((json) => {
        if (json.code === '403') {
          if (Func.Clear_Token() === true) {
            if (!localStorage.getItem('token')) {
              this.setState({ redirect: true });
            }
          }
        } else {
          const datas = [];
          datas.push({
            value: '-',
            label: json.data.length > 0 ? 'Pilih' : 'Tidak ditemukan',
            isDisabled: true,
          });
          json.data.map((value) => {
            datas.push({
              value: value.id,
              label: value.sge + ' ( ' + value.name + ' )',
            });
          });
          this.setState({ data4: datas, data4_ori: json.data, loader: false });
        }
      })
      .catch(() => {})
      .finally(() => {});
  }

  render() {
    let estimated_value = 0;
    for (let index = 0; index < this.state.estimated_value.length; index++) {
      estimated_value += this.state.estimated_value[index];
    }

    let ltv_value = 0;
    for (let index = 0; index < this.state.ltv_value.length; index++) {
      ltv_value += this.state.ltv_value[index];
    }

    let ltv = 0;
    for (let index = 0; index < this.state.ltv.length; index++) {
      ltv += this.state.ltv[index];
    }

    if (
      this.state.value.loan_amount > ltv_value
      && this.state.validator.loan_amount === undefined
    ) {
      const validate = this.state.validator;
      validate.loan_amount = 'Pinjaman melebihi batas maksimum';
      this.setState({ validator: validate });
    }

    const ExampleCustomInput = ({ value, onClick }) => (
      <img src={Icon.icon_date} onClick={onClick} />
    );
    const { classes } = this.props;
    const loadOptions = (inputValue, callback) => {
      setTimeout(() => {
        callback(this.state.data2);
      }, 600);
    };
    const loadOptions3 = (inputValue, callback) => {
      setTimeout(() => {
        callback(this.state.data4);
      }, 600);
    };

    const labelGender = (this.state.value.gender === 'l' && 'Laki - laki')
      || (this.state.value.gender === 'p' && 'Perempuan');

    if (this.state.loader) {
      return (
        <div className={classes.root2}>
          <Dialog
            disablePortal
            disableEnforceFocus
            disableAutoFocus
            open
            scroll="paper"
            maxWidth="md"
            aria-labelledby="server-modal-title"
            aria-describedby="server-modal-description"
            container={() => {}}
          >
            <div
              style={{
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
                padding: '20px',
                borderRadius: '50px',
              }}
            >
              <CircularProgress
                style={{ color: '#85203B', margin: '18px' }}
                size={40}
              />
              Mohon Tunggu
              <div
                style={{
                  marginTop: '10px',
                }}
              />
            </div>
          </Dialog>
        </div>
      );
    }
    return (
      <Dialog
        scroll="paper"
        open
        maxWidth="md"
        aria-labelledby="scroll-dialog-title"
        aria-describedby="scroll-dialog-description"
      >
        <DialogTitle id="scroll-dialog-title">
          <Typography variant="h7" className={classes.tittleModal}>
            Tambah Transaksi Pelunasan
          </Typography>
          <IconButton
            disabled={this.state.loader_button}
            aria-label="close"
            className={classes.closeButton}
            onClick={() => {
              this.props.handleModal();
            }}
          >
            <CloseIcon />
          </IconButton>
        </DialogTitle>
        <DialogContent dividers>
          <DialogContentText id="scroll-dialog-description" tabIndex={-1}>
            <div className={classes.scrool}>
              <Grid container item lg={12} xl={12} md={12} sm={12} xs={12}>
                <Grid
                  item
                  lg={12}
                  xl={12}
                  md={12}
                  sm={12}
                  xs={12}
                  className={classes.formPad}
                >
                  <AsyncSelect
                    name="form-field-name-error"
                    placeholder="Masukan No. SGE"
                    styles={{
                      control: (provided, state) => ({
                        ...provided,
                        borderColor: this.state.validator.customer_id
                          ? 'red'
                          : '#CACACA',
                        borderRadius: '0.25rem',
                      }),
                    }}
                    onInputChange={(val) => {
                      if (val) {
                        if (this.timeout) clearTimeout(this.timeout);
                        this.timeout = setTimeout(() => {
                          this.getSGE(val);
                        }, 500);
                      }
                    }}
                    fullWidth
                    cacheOptions
                    loadOptions={loadOptions3}
                    defaultOptions
                    options={this.state.data4}
                    onChange={(val) => {
                      this.handleChange(val);
                    }}
                  />
                </Grid>
              </Grid>
              {this.state.show ? (
                <div style={{ marginTop: 30 }}>
                  <div>
                    {Func.toLogin(this.state.redirect)}
                    <div className={classes.root}>
                      <div className={classes.BodytitleMdl2}>
                        <text className={classes.titleMdl}>Data Transaksi</text>
                      </div>
                      {' '}
                      <Grid
                        container
                        direction="row"
                        item
                        lg={12}
                        xl={12}
                        md={12}
                        xs={12}
                      >
                        <Grid item lg={12} xl={12} md={12} sm={12} xs={12}>
                          <Grid
                            container
                            direction="row"
                            item
                            lg={12}
                            xl={12}
                            md={12}
                            xs={12}
                          >
                            <Grid
                              container
                              item
                              lg={12}
                              xl={12}
                              md={12}
                              sm={12}
                              xs={12}
                            >
                              <Grid
                                container
                                item
                                lg={12}
                                xl={12}
                                md={12}
                                sm={12}
                                xs={12}
                              >
                                <Grid
                                  item
                                  lg={6}
                                  xl={6}
                                  md={6}
                                  sm={6}
                                  xs={12}
                                  className={classes.formPad}
                                >
                                  <div className={classes.label111}>
                                    <text className={classes.label121}>
                                      Tanggal Akad
                                    </text>
                                  </div>
                                  <div className={classes.label1112}>
                                    <text className={classes.label1}>
                                      {this.state.value.contract_date}
                                    </text>
                                  </div>
                                </Grid>
                                <Grid
                                  item
                                  lg={6}
                                  xl={6}
                                  md={6}
                                  sm={6}
                                  xs={12}
                                  className={classes.formPad}
                                >
                                  <div className={classes.label111}>
                                    <text className={classes.label121}>
                                      Tanggal Jatuh Tempo
                                    </text>
                                  </div>
                                  <div className={classes.label1112}>
                                    <text className={classes.label1}>
                                      {this.state.value.due_date}
                                    </text>
                                  </div>
                                </Grid>
                              </Grid>
                            </Grid>
                          </Grid>
                          <Divider className={classes.divider} />
                          <div className={classes.BodytitleMdl2}>
                            <text className={classes.titleMdl}>
                              Data Pinjaman
                            </text>
                          </div>
                          <Grid
                            container
                            direction="row"
                            item
                            lg={12}
                            xl={12}
                            md={12}
                            xs={12}
                          >
                            <Grid
                              container
                              item
                              lg={3}
                              xl={3}
                              md={3}
                              sm={3}
                              xs={12}
                            >
                              <div className={classes.label111}>
                                <text className={classes.label121}>
                                  Nilai Taksiran
                                </text>
                              </div>
                            </Grid>
                            <Grid
                              container
                              item
                              lg={3}
                              xl={3}
                              md={3}
                              sm={3}
                              xs={12}
                            >
                              <div className={classes.label111}>
                                <text className={classes.label121}>
                                  Pinjaman yang Diajukan
                                </text>
                              </div>
                            </Grid>
                            <Grid
                              container
                              item
                              lg={3}
                              xl={3}
                              md={3}
                              sm={3}
                              xs={12}
                            >
                              <div className={classes.label111}>
                                <text className={classes.label121}> Rate Sewa\15 Hari </text>
                              </div>
                            </Grid>
                            <Grid
                              container
                              item
                              lg={3}
                              xl={3}
                              md={3}
                              sm={3}
                              xs={12}
                            >
                              <div className={classes.label111}>
                                <text className={classes.label121}>Denda</text>
                              </div>
                            </Grid>
                          </Grid>

                          <Grid
                            container
                            direction="row"
                            item
                            lg={12}
                            xl={12}
                            md={12}
                            xs={12}
                          >
                            <Grid
                              container
                              item
                              lg={3}
                              xl={3}
                              md={3}
                              sm={3}
                              xs={12}
                            >
                              <div className={classes.label111}>
                                <text className={classes.label1}>
                                  {this.state.value.estimate_value}
                                </text>
                              </div>
                            </Grid>
                            <Grid
                              container
                              item
                              lg={3}
                              xl={3}
                              md={3}
                              sm={3}
                              xs={12}
                            >
                              <div className={classes.label111}>
                                <text className={classes.label1}>
                                  {this.state.value.loan_amount}
                                </text>
                              </div>
                            </Grid>
                            <Grid
                              container
                              item
                              lg={3}
                              xl={3}
                              md={3}
                              sm={3}
                              xs={12}
                            >
                              <div className={classes.label111}>
                                <text className={classes.label1}>
                                  {this.state.value.rental_per_15_days}
                                </text>
                              </div>
                            </Grid>
                            <Grid
                              container
                              item
                              lg={3}
                              xl={3}
                              md={3}
                              sm={3}
                              xs={12}
                            >
                              <div className={classes.label111}>
                                <text className={classes.label1}>
                                  {this.state.value.fine_amount}
                                </text>
                              </div>
                            </Grid>
                          </Grid>
                          <Divider className={classes.divider} />
                          <div className={classes.BodytitleMdl2}>
                            <text className={classes.titleMdl}>
                              Data Pelunasan
                            </text>
                          </div>
                          <Grid
                            container
                            direction="row"
                            item
                            lg={12}
                            xl={12}
                            md={12}
                            xs={12}
                          >
                            <Grid
                              container
                              item
                              lg={4}
                              xl={4}
                              md={4}
                              sm={4}
                              xs={12}
                            >
                              <div className={classes.label111}>
                                <text className={classes.label121}>
                                  Tanggal Pelunasan
                                </text>
                              </div>
                            </Grid>
                            <Grid
                              container
                              item
                              lg={4}
                              xl={4}
                              md={4}
                              sm={4}
                              xs={12}
                            >
                              <div className={classes.label111}>
                                <text className={classes.label121}>
                                  Jumlah Hari
                                </text>
                              </div>
                            </Grid>
                            <Grid
                              container
                              item
                              lg={4}
                              xl={4}
                              md={4}
                              sm={4}
                              xs={12}
                            >
                              <div className={classes.label111}>
                                <text className={classes.label121}>
                                  Total Biaya Sewa
                                </text>
                              </div>
                            </Grid>
                          </Grid>

                          <Grid
                            container
                            direction="row"
                            item
                            lg={12}
                            xl={12}
                            md={12}
                            xs={12}
                          >
                            <Grid
                              container
                              item
                              lg={4}
                              xl={4}
                              md={4}
                              sm={4}
                              xs={12}
                            >
                              <div className={classes.label111}>
                                <text className={classes.label1}>
                                  {this.state.value.payment_date}
                                </text>
                              </div>
                            </Grid>
                            <Grid
                              container
                              item
                              lg={4}
                              xl={4}
                              md={4}
                              sm={4}
                              xs={12}
                            >
                              <div className={classes.label111}>
                                <text className={classes.label1}>
                                  {this.state.value.number_of_days}
                                </text>
                              </div>
                            </Grid>
                            <Grid
                              container
                              item
                              lg={4}
                              xl={4}
                              md={4}
                              sm={4}
                              xs={12}
                            >
                              <div className={classes.label111}>
                                <text className={classes.label1}>
                                  {this.state.value.total_rental_cost}
                                </text>
                              </div>
                            </Grid>
                          </Grid>

                          <Grid
                            style={{ marginTop: 30 }}
                            container
                            direction="row"
                            item
                            lg={12}
                            xl={12}
                            md={12}
                            xs={12}
                          >
                            <Grid
                              container
                              item
                              lg={3}
                              xl={3}
                              md={3}
                              sm={3}
                              xs={12}
                            >
                              <div className={classes.label111}>
                                <text className={classes.label121}>
                                  Detail Pelunasan
                                </text>
                              </div>
                            </Grid>
                            <Grid
                              container
                              item
                              lg={3}
                              xl={3}
                              md={3}
                              sm={3}
                              xs={12}
                            />
                            <Grid
                              container
                              item
                              lg={3}
                              xl={3}
                              md={3}
                              sm={3}
                              xs={12}
                            />
                            <Grid
                              container
                              item
                              lg={3}
                              xl={3}
                              md={3}
                              sm={3}
                              xs={12}
                            />
                          </Grid>

                          <Grid
                            style={{ marginTop: 20 }}
                            container
                            direction="row"
                            item
                            lg={12}
                            xl={12}
                            md={12}
                            xs={12}
                          >
                            <Grid
                              container
                              item
                              lg={3}
                              xl={3}
                              md={3}
                              sm={3}
                              xs={12}
                            >
                              <div className={classes.label111}>
                                <text className={classes.label1}>Pinjaman</text>
                              </div>
                            </Grid>
                            <Grid
                              container
                              item
                              lg={3}
                              xl={3}
                              md={3}
                              sm={3}
                              xs={12}
                            />
                            <Grid
                              container
                              item
                              lg={3}
                              xl={3}
                              md={3}
                              sm={3}
                              xs={12}
                            />
                            <Grid
                              container
                              item
                              lg={3}
                              xl={3}
                              md={3}
                              sm={3}
                              xs={12}
                            >
                              <div className={classes.label111}>
                                <text className={classes.label1}>
                                  {this.state.value.loan_amount}
                                </text>
                              </div>
                            </Grid>
                          </Grid>
                          <Divider />
                          <Grid
                            container
                            direction="row"
                            item
                            lg={12}
                            xl={12}
                            md={12}
                            xs={12}
                          >
                            <Grid
                              container
                              item
                              lg={3}
                              xl={3}
                              md={3}
                              sm={3}
                              xs={12}
                            >
                              <div className={classes.label111}>
                                <text className={classes.label1}>
                                  Biaya Sewa
                                </text>
                              </div>
                            </Grid>
                            <Grid
                              container
                              item
                              lg={3}
                              xl={3}
                              md={3}
                              sm={3}
                              xs={12}
                            />
                            <Grid
                              container
                              item
                              lg={3}
                              xl={3}
                              md={3}
                              sm={3}
                              xs={12}
                            />
                            <Grid
                              container
                              item
                              lg={3}
                              xl={3}
                              md={3}
                              sm={3}
                              xs={12}
                            >
                              <div className={classes.label111}>
                                <text className={classes.label1}>
                                  {this.state.value.total_rental_cost}
                                </text>
                              </div>
                            </Grid>
                          </Grid>
                          <Divider />
                          <Grid
                            container
                            direction="row"
                            item
                            lg={12}
                            xl={12}
                            md={12}
                            xs={12}
                          >
                            <Grid
                              container
                              item
                              lg={3}
                              xl={3}
                              md={3}
                              sm={3}
                              xs={12}
                            >
                              <div className={classes.label111}>
                                <text className={classes.label1}>Denda</text>
                              </div>
                            </Grid>
                            <Grid
                              container
                              item
                              lg={3}
                              xl={3}
                              md={3}
                              sm={3}
                              xs={12}
                            />
                            <Grid
                              container
                              item
                              lg={3}
                              xl={3}
                              md={3}
                              sm={3}
                              xs={12}
                            />
                            <Grid
                              container
                              item
                              lg={3}
                              xl={3}
                              md={3}
                              sm={3}
                              xs={12}
                            >
                              <div className={classes.label111}>
                                <text className={classes.label1}>
                                  {this.state.value.fine_amount}
                                </text>
                              </div>
                            </Grid>
                          </Grid>
                          <Divider />
                          <Grid
                            container
                            direction="row"
                            item
                            lg={12}
                            xl={12}
                            md={12}
                            xs={12}
                          >
                            <Grid
                              container
                              item
                              lg={3}
                              xl={3}
                              md={3}
                              sm={3}
                              xs={12}
                            />
                            <Grid
                              container
                              item
                              lg={3}
                              xl={3}
                              md={3}
                              sm={3}
                              xs={12}
                            />
                            <Grid
                              container
                              item
                              lg={3}
                              xl={3}
                              md={3}
                              sm={3}
                              xs={12}
                            >
                              <div className={classes.label111} />
                            </Grid>
                            <Grid
                              container
                              item
                              lg={3}
                              xl={3}
                              md={3}
                              sm={3}
                              xs={12}
                            >
                              <div className={classes.label111}>
                                <text
                                  className={classes.label1}
                                  style={{
                                    marginRight: '12px',
                                    marginLeft: -50,
                                  }}
                                >
                                  Total
                                </text>
                                <text className={classes.label1}>
                                  {this.state.value.total}
                                </text>
                              </div>
                            </Grid>
                          </Grid>
                        </Grid>
                      </Grid>
                    </div>
                  </div>
                  <div>
                    {this.state.approvals.length > 0 ? (
                      <div>
                        <Divider className={classes.divider} />
                        <div className={classes.BodytitleMdl2}>
                          <text className={classes.titleMdl}>Lampiran</text>
                        </div>
                        <div className={classes.BodytitleMdl}>
                          <Box
                            borderColor={
                              this.state.validator.img
                                ? 'error.main'
                                : 'grey.500'
                            }
                            border={1}
                            onClick={() => {
                              this.removeValidate('img');
                            }}
                            className={classes.imgScan3}
                          >
                            {this.state.value.img ? (
                              <img
                                className={classes.imgScan4}
                                onClick={() => {
                                  this.removeValidate('img');
                                }}
                                src={this.state.value.img}
                              />
                            ) : null}
                          </Box>
                          <FormHelperText className={classes.error22}>
                            {this.state.validator.img}
                          </FormHelperText>
                        </div>
                        <div className={classes.BodytitleMdl22}>
                          <img
                            src={Icon.deleteImg}
                            onClick={() => {
                              const dataSet = this.state.value;
                              dataSet.img = null;
                              this.setState({ value: dataSet, ImgBase44: '' });
                            }}
                          />
                          <div />
                          <input
                            type="file"
                            accept="image/*"
                            name="file"
                            title="Pilih Gambar"
                            onChange={this.handleChangeImg}
                          />
                        </div>
                      </div>
                    ) : null}
                  </div>
                </div>
              ) : this.state.show === false
                && this.state.transaction_id != null ? (
                  <div
                    style={{
                      marginTop: 80,
                      display: 'flex',
                      flexDirection: 'column',
                      alignItems: 'center',
                      padding: '20px',
                      borderRadius: '50px',
                    }}
                  >
                    <CircularProgress
                      style={{ color: '#85203B', margin: '18px' }}
                      size={40}
                    />
                    Mohon Tunggu
                    <div
                      style={{
                        marginTop: '10px',
                      }}
                    />
                  </div>
                ) : null}
            </div>
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          {this.state.loader === false && this.state.show ? (
            <Button
              disabled={this.state.loader_button}
              onClick={() => {
                swal({
                  title: 'Apakah Anda Yakin?',
                  text: 'Akan melakukan pelunasan transaksi',
                  icon: 'info',
                  buttons: true,
                  dangerMode: true,
                }).then((willDelete) => {
                  if (willDelete) {
                    this.setState({ loader_button: true });
                    this.handleSubmit();
                  }
                });
              }}
              style={{
                backgroundColor: '#862C3A',
                color: 'white',
              }}
            >
              <Typography variant="button" style={{ color: '#FFFFFF' }}>
                {this.state.loader_button ? (
                  <CircularProgress
                    style={{
                      color: 'white',
                      marginLeft: '18px',
                      marginRight: '18px',
                      marginTop: 5,
                    }}
                    size={15}
                  />
                ) : (
                  'Lunasi'
                )}
              </Typography>
            </Button>
          ) : null}
          {this.state.show ? (
            <Button
              style={{
                backgroundColor: '#117a8b',
                color: 'white',
              }}
              disabled={this.state.loader_button}
              onClick={() => {
                window.open(
                  process.env.REACT_APP_URL_MANAGEMENT
                    + process.env.REACT_APP_API_PREFIX_V1
                    + '/transactions/'
                    + this.state.transaction_id
                    + '/repayments.pdf?token='
                    + localStorage.getItem('token')
                );
              }}
              variant="outlined"
            >
              <Typography variant="button">Cetak (Preview)</Typography>
            </Button>
          ) : null}
          <Button
            style={{
              color: 'black',
            }}
            disabled={this.state.loader_button}
            onClick={() => {
              this.props.handleModal();
            }}
            variant="outlined"
          >
            <Typography variant="button">Batal</Typography>
          </Button>
        </DialogActions>
      </Dialog>
    );
  }
}

export default withStyles(styles.CoustomsStyles, { name: 'Form' })(Form);
