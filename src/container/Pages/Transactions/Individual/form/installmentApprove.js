/* eslint-disable no-plusplus */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable no-redeclare */
/* eslint-disable block-scoped-var */
/* eslint-disable no-var */
/* eslint-disable vars-on-top */
/* eslint-disable array-callback-return */
/* eslint-disable react/button-has-type */
/* eslint-disable camelcase */
/* eslint-disable prefer-destructuring */
/* eslint-disable eqeqeq */
/* eslint-disable react/sort-comp */
/* eslint-disable react/no-access-state-in-setstate */
/* eslint-disable react/no-unused-state */
/* eslint-disable no-shadow */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable no-unused-vars */
import React from "react";
import { withStyles } from "@material-ui/core/styles";
import Divider from "@material-ui/core/Divider";
import Typography from "@material-ui/core/Typography";
import swal from "sweetalert";
import FormHelperText from "@material-ui/core/FormHelperText";
import Grid from "@material-ui/core/Grid";
import DialogContentText from "@material-ui/core/DialogContentText";
import MuiDialogActions from "@material-ui/core/DialogActions";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import Button from "@material-ui/core/Button";
import { CircularProgress } from "@material-ui/core";
import Box from "@material-ui/core/Box";
import Func from "../../../../../functions/index";
import Icon from "../../../../../components/icon";
import styles from "../css";
import "../../../../../styles/date_field.css";
import service from "../../../../../functions/service";

const DialogActions = withStyles((theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(1),
  },
}))(MuiDialogActions);

class Form extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: false,
      validator: [],
      value: { gender: { label: "" } },
      type: "",
      activeTabs: 0,
      Proses: false,
      ImgBase124: "",
      section: 0,
      id: null,
      redirect: false,
      date: new Date(),
      provin: [],
      data3: [],
      estimated_value: [],
      ltv_value: [],
      ltv: [],
      monthly_fee: [],
      dataBJ: [],
      cont: 0,
      approvals: [],
      download: null,
      payment_status: false,
      loader: false,
      installmentHistory: [],
      installmentSimulation: [],
    };
  }

  componentDidMount() {
    if (this.props.type == "Ubah") {
      this.setState({ loader: true });
      fetch(
        process.env.REACT_APP_URL_MANAGEMENT +
          process.env.REACT_APP_API_PREFIX_V1 +
          "/installments/" +
          this.props.id,
        {
          method: "GET",
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Bearer " + localStorage.getItem("token"),
          },
        }
      )
        .then((response) => response.json())
        .then((json) => {
          if (json.code == "403") {
            this.setState({ loader: false });
            if (Func.Clear_Token() == true) {
              if (!localStorage.getItem("token")) {
                this.setState({ redirect: true });
              }
            }
          } else if (json.code == "404" || json.code == "500") {
            this.props.handleModal();
            swal(
              '', 
              'Terjadi kesalahan pada server',
              'error'
            )
          }
          const data = json.data.installment;
          const val = this.state.value;
          const { installmentHistory } = this.state;
          this.getProd("", data.product_id, "");

          try {
            val.customer_id = {
              value: data.customer.id,
              label: data.customer.name,
            };
            val.product_name = data.product_name;
            val.insurance_item_id = {
              value: data.insurance_item_id,
              label: data.insurance_item_name,
            };

            val.loan_amount = data.loan_amount;
            val.tenor = data.installment_detail.tenor;
            val.monthly_installment =
              data.installment_detail.monthly_installment;
            val.monthly_fee = data.installment_detail.monthly_fee;
            val.remaining_loan = data.installment_detail.remaining_loan;

            val.next_installment_due_date = data.next_installment_due_date;
            data.transaction_insurance_items.map((value, index) => {
              this.addBJ(value.id);
              val["name" + index] = value.name;
              val["ownership" + index] = value.ownership;
              val["amount" + index] = value.amount;
              val["gross_weight" + index] = value.gross_weight;
              val["net_weight" + index] = value.net_weight;
              val["carats" + index] = value.carats;
              val["description" + index] = value.description;
              val["Insurance_item_image" + index] =
                value.insurance_item_image.url;
              val["product_insurance_item_name" + index] =
                val.product_insurance_item_name;
              val["buy_price" + index] = value.buy_price;
            });
          } catch (err) {
            this.setState({ loader: false });
            alert("terjadi kesalahan sistem");
          }
          const mapData = Object.assign(val, data.customer);

          this.setState({
            value: mapData,
            installmentHistory: data.installment_payment_history,
            installmentSimulation: data.installment_date_simulation,
            data4: data.customer,
            approvals: data.approvals,
            download: data.download == undefined ? null : data.download,
            payment_status: data.payment_status,
            loader: false,
          });
        })
        .catch((error) => {})
        .finally(() => {});
    } else {
      this.addBJ();
    }
  }

  getProd = (val, id, name) => {
    this.setState({ isloader: true });
    service
      .getProductInstallment()
      .then((response) => response.json())
      .then((res) => {
        if (res.code == "403") {
          if (Func.Clear_Token() == true) {
            if (!localStorage.getItem("token")) {
              this.setState({ redirect: true });
            }
          }
        } else {
          const datas = [];
          datas.push({
            value: "-",
            label: res.data.length > 0 ? "Pilih" : "Tidak ditemukan",
            isDisabled: true,
          });

          res.data.map((value) => {
            datas.push({ value: value.id.$oid, label: value.name });
          });
          if (id !== "") {
            const search = res.data.find((o) => o.id.$oid === id);
            if (search == undefined) {
              datas.push({ value: id, label: name });
            }
            if (search !== undefined) {
              const val = this.state.value;
              val.product_id = {
                value: search.id.$oid,
                label: search.name,
              };
            }
          }

          this.setState({
            data2: datas,
            data2_ori: res.data,
            loader: false,
          });
        }
      });
  };

  handleSubmit(type) {
    if (this.state.value.img == undefined) {
      var bodys = {
        transaction_approval: {
          attachment: "",
        },
      };
    } else {
      var bodys = {
        transaction_approval: {
          attachment: this.state.ImgBase44,
        },
      };
    }

    fetch(type, {
      method: "PUT",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: "Bearer " + localStorage.getItem("token"),
      },
      body: JSON.stringify(bodys),
    })
      .then((response) => response.json())
      .then((json) => {
        if (json.code == "403") {
          Func.Refresh_Token();
          if (Func.Refresh_Token() == true) {
            this.handleSubmit();
          }
        }
        if (json.code == 200) {
          this.props.OnNext(json.message);
        } else if (json.code === 400) {
          this.props.closeModal();
          swal({
            title: "Kesalahan",
            text: json.message,
            icon: "error",
          });
        } else {
          this.setState({ validator: json.status });
        }
      })
      .catch((error) => {})
      .finally(() => {});
  }

  removeValidate(name) {
    const data = this.state.validator;
    delete data[name];
    this.setState({ validator: data });
  }

  renderBJ() {
    const { classes } = this.props;
    const data = [];
    let key = 1;
    this.state.dataBJ.map((value, index) => {
      if (!value._destroy) {
        data.push(
          <div>
            <div
              className={classes.BodytitleMdl2}
              style={{
                marginTop: 50,
              }}
            >
              <text className={classes.titleMdl}>
                BARANG JAMINAN
                {key}
              </text>
            </div>
            <Grid
              container
              direction="row"
              item
              lg={12}
              xl={12}
              md={12}
              xs={12}
            >
              <Grid container item lg={8} xl={8} md={8} sm={8} xs={12}>
                <Grid container item lg={12} xl={12} md={12} sm={12} xs={12}>
                  <Grid
                    item
                    lg={6}
                    xl={6}
                    md={6}
                    sm={6}
                    xs={12}
                    className={classes.formPad}
                  >
                    <div>
                      <text className={classes.label121}>
                        Kepemilikan Barang Jaminan
                      </text>
                    </div>
                    <div>
                      <div className={classes.label1112}>
                        <text className={classes.label1}>
                          {this.state.value["ownership" + value.key]}
                        </text>
                      </div>
                    </div>
                  </Grid>
                  <Grid
                    container
                    lg={6}
                    xl={6}
                    md={6}
                    sm={6}
                    xs={12}
                    className={classes.formPad}
                  >
                    <Grid
                      item
                      lg={6}
                      xl={6}
                      md={6}
                      sm={6}
                      xs={12}
                      className={classes.formPad}
                    >
                      <div>
                        <text className={classes.label121}>Jumlah</text>
                      </div>
                      <div>
                        <div className={classes.label1112}>
                          <text className={classes.label1}>
                            {this.state.value["amount" + value.key]}
                          </text>
                        </div>
                      </div>
                    </Grid>
                    <Grid
                      item
                      lg={6}
                      xl={6}
                      md={6}
                      sm={6}
                      xs={12}
                      className={classes.formPad}
                    >
                      <div>
                        <text className={classes.label121}>Karat</text>
                      </div>
                      <div>
                        <div className={classes.label1112}>
                          <text className={classes.label1}>
                            {this.state.value["carats" + value.key]}
                          </text>
                        </div>
                      </div>
                    </Grid>
                  </Grid>
                </Grid>
                <Grid container item lg={12} xl={12} md={12} sm={12} xs={12}>
                  <Grid
                    item
                    lg={6}
                    xl={6}
                    md={6}
                    sm={6}
                    xs={12}
                    className={classes.formPad}
                  >
                    <div>
                      <text className={classes.label121}>
                        Kategori Barang Jaminan
                      </text>
                    </div>
                    <div>
                      <div className={classes.label1112}>
                        <text className={classes.label1}>
                          {
                            this.state.value[
                              "product_insurance_item_name" + value.key
                            ]
                          }
                        </text>
                      </div>
                    </div>
                  </Grid>
                  <Grid
                    container
                    lg={6}
                    xl={6}
                    md={6}
                    sm={6}
                    xs={12}
                    className={classes.formPad}
                  >
                    <Grid
                      item
                      lg={6}
                      xl={6}
                      md={6}
                      sm={6}
                      xs={12}
                      className={classes.formPad}
                    >
                      <div>
                        <text className={classes.label121}>Berat Kotor</text>
                      </div>
                      <div>
                        <div className={classes.label1112}>
                          <text className={classes.label1}>
                            {this.state.value["gross_weight" + value.key]}
                          </text>
                        </div>
                      </div>
                    </Grid>
                    <Grid
                      item
                      lg={6}
                      xl={6}
                      md={6}
                      sm={6}
                      xs={12}
                      className={classes.formPad}
                    >
                      <div>
                        <text className={classes.label121}>Berat Bersih</text>
                      </div>
                      <div>
                        <div className={classes.label1112}>
                          <text className={classes.label1}>
                            {this.state.value["net_weight" + value.key]}
                          </text>
                        </div>
                      </div>
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
              <Grid
                container
                item
                lg={4}
                xl={4}
                md={4}
                sm={4}
                xs={12}
                spacing={3}
              >
                <Grid
                  item
                  lg={12}
                  xl={12}
                  md={12}
                  sm={12}
                  xs={12}
                  className={classes.formPad}
                >
                  <div>
                    <text className={classes.label121}>Deskripsi</text>
                  </div>
                  <div>
                    <div className={classes.label1112}>
                      <text className={classes.label1}>
                        {this.state.value["description" + value.key]}
                      </text>
                    </div>
                  </div>
                </Grid>
              </Grid>
            </Grid>
            <Grid item lg={4} xl={4} md={4} sm={4} xs={12}>
              <Box
                borderColor={
                  this.state.validator["Insurance_item_image" + value.key]
                    ? "error.main"
                    : "grey.500"
                }
                border={1}
                onClick={() => {
                  this.removeValidate("Insurance_item_image" + value.key);
                }}
                className={classes.imgScan}
              >
                {this.state.value["Insurance_item_image" + value.key] ? (
                  <img
                    className={classes.imgScan2}
                    onClick={() => {
                      this.removeValidate("Insurance_item_image" + value.key);
                    }}
                    src={this.state.value["Insurance_item_image" + value.key]}
                  />
                ) : null}
              </Box>
            </Grid>
          </div>
        );
        key++;
      }
    });
    return data;
  }

  addBJ(id) {
    const data = this.state.dataBJ;
    data.push({
      _destroy: false,
      key: this.state.cont,
      id: id == undefined ? null : id,
    });
    this.setState({
      dataBJ: data,
      cont: this.state.cont + 1,
    });
  }

  render() {
    const { classes } = this.props;
    const { value } = this.state;

    if (this.state.loader) {
      return (
        <div className={classes.root2}>
          <Dialog
            disablePortal
            disableEnforceFocus
            disableAutoFocus
            open
            scroll="paper"
            maxWidth="md"
            aria-labelledby="server-modal-title"
            aria-describedby="server-modal-description"
            container={() => {}}
          >
            <div
              style={{
                display: "flex",
                flexDirection: "column",
                alignItems: "center",
                padding: "20px",
                borderRadius: "50px",
              }}
            >
              <CircularProgress
                style={{ color: "#85203B", margin: "18px" }}
                size={40}
              />
              Mohon Tunggu
              <div
                style={{
                  marginTop: "10px",
                }}
              />
            </div>
          </Dialog>
        </div>
      );
    }
    const labelTenor =
      value.tenor === 3
        ? "3 Bulan"
        : value.tenor === 6
        ? "6 Bulan"
        : value.tenor === 12
        ? "12 Bulan"
        : "-";
    return (
      <Dialog
        scroll="paper"
        open
        maxWidth="md"
        aria-labelledby="scroll-dialog-title"
        aria-describedby="scroll-dialog-description"
      >
        <DialogTitle id="scroll-dialog-title">
          <Typography variant="h7" className={classes.tittleModal}>
            Informasi Cicilan
          </Typography>
          <IconButton
            aria-label="close"
            className={classes.closeButton}
            onClick={() => {
              this.props.handleModal();
            }}
          >
            <CloseIcon />
          </IconButton>
        </DialogTitle>
        <DialogContent dividers>
          <DialogContentText id="scroll-dialog-description" tabIndex={-1}>
            <div className={classes.scrool}>
              <div>
                {Func.toLogin(this.state.redirect)}
                <div className={classes.root}>
                  <div className={classes.BodytitleMdl2}>
                    <text className={classes.titleMdl}>Data Nasabah</text>
                  </div>
                  <Grid
                    container
                    direction="row"
                    item
                    lg={12}
                    xl={12}
                    md={12}
                    xs={12}
                  >
                    <Grid item lg={12} xl={12} md={12} sm={12} xs={12}>
                      <Grid
                        container
                        direction="row"
                        item
                        lg={12}
                        xl={12}
                        md={12}
                        xs={12}
                      >
                        <Grid
                          container
                          item
                          lg={8}
                          xl={8}
                          md={8}
                          sm={8}
                          xs={12}
                        >
                          <Grid
                            container
                            item
                            lg={12}
                            xl={12}
                            md={12}
                            sm={12}
                            xs={12}
                          >
                            <Grid
                              item
                              lg={6}
                              xl={6}
                              md={6}
                              sm={6}
                              xs={12}
                              className={classes.formPad}
                            >
                              <div className={classes.label111}>
                                <text className={classes.label121}>
                                  Nama Lengkap
                                </text>
                              </div>
                              <div className={classes.label1112}>
                                <text className={classes.label1}>
                                  {this.state.value.name}
                                </text>
                              </div>
                            </Grid>
                            <Grid
                              item
                              lg={6}
                              xl={6}
                              md={6}
                              sm={6}
                              xs={12}
                              className={classes.formPad}
                            >
                              <div className={classes.label111}>
                                <text className={classes.label121}>
                                  Nomor HP
                                </text>
                              </div>
                              <div className={classes.label1112}>
                                <text className={classes.label1}>
                                  {this.state.value.phone_number}
                                </text>
                              </div>
                            </Grid>
                          </Grid>
                          <Grid
                            container
                            item
                            lg={12}
                            xl={12}
                            md={12}
                            sm={12}
                            xs={12}
                          >
                            <Grid
                              item
                              lg={6}
                              xl={6}
                              md={6}
                              sm={6}
                              xs={12}
                              className={classes.formPad}
                            >
                              <div className={classes.label111}>
                                <text className={classes.label121}>
                                  Jenis Kelamin
                                </text>
                              </div>
                              <div className={classes.label1112}>
                                <text className={classes.label1}>
                                  {this.state.value.gender === "p" &&
                                    "Perempuan"}
                                  {this.state.value.gender === "l" &&
                                    "Laki - laki"}
                                </text>
                              </div>
                            </Grid>
                            <Grid
                              item
                              lg={6}
                              xl={6}
                              md={6}
                              sm={6}
                              xs={12}
                              className={classes.formPad}
                            >
                              <div className={classes.label111}>
                                <text className={classes.label121}>
                                  Nomor ID
                                </text>
                              </div>
                              <div className={classes.label1112}>
                                <text className={classes.label1}>
                                  {this.state.value.identity_number}
                                </text>
                              </div>
                            </Grid>
                          </Grid>
                          <Grid
                            container
                            item
                            lg={12}
                            xl={12}
                            md={12}
                            sm={12}
                            xs={12}
                          />
                        </Grid>
                        <Grid
                          container
                          item
                          lg={4}
                          xl={4}
                          md={4}
                          sm={4}
                          xs={12}
                          spacing={3}
                        >
                          <Grid
                            item
                            lg={12}
                            xl={12}
                            md={12}
                            sm={12}
                            xs={12}
                            className={classes.formPad}
                          >
                            <div className={classes.label111}>
                              <text className={classes.label121}>Alamat</text>
                            </div>
                            <div className={classes.label1112}>
                              <text className={classes.label1}>
                                {this.state.value.address}
                              </text>
                            </div>
                          </Grid>
                        </Grid>
                      </Grid>
                      <Divider className={classes.divider} />
                      <Grid
                        container
                        direction="row"
                        item
                        lg={12}
                        xl={12}
                        md={12}
                        xs={12}
                      >
                        <Grid
                          container
                          item
                          lg={4}
                          xl={4}
                          md={4}
                          sm={4}
                          xs={12}
                        >
                          <div className={classes.label111}>
                            <text className={classes.label121}>
                              Produk Gadai
                            </text>
                          </div>
                        </Grid>
                        <Grid
                          container
                          item
                          lg={4}
                          xl={4}
                          md={4}
                          sm={4}
                          xs={12}
                        >
                          <div className={classes.label111}>
                            <text className={classes.label121}>
                              Kategori Barang Jaminan
                            </text>
                          </div>
                        </Grid>
                      </Grid>
                      <Grid
                        container
                        direction="row"
                        item
                        lg={12}
                        xl={12}
                        md={12}
                        xs={12}
                      >
                        <Grid
                          container
                          item
                          lg={4}
                          xl={4}
                          md={4}
                          sm={4}
                          xs={12}
                        >
                          <div className={classes.label1112}>
                            <text className={classes.label1}>
                              {this.state.value.product_id?.label}
                            </text>
                          </div>
                        </Grid>
                        <Grid
                          container
                          item
                          lg={4}
                          xl={4}
                          md={4}
                          sm={4}
                          xs={12}
                        >
                          <div className={classes.label1112}>
                            <text className={classes.label1}>
                              {this.state.value.insurance_item_id == undefined
                                ? ""
                                : this.state.value.insurance_item_id.label}
                            </text>
                          </div>
                        </Grid>
                      </Grid>
                      <div
                        style={{
                          marginBottom: 50,
                        }}
                      >
                        {this.renderBJ()}
                      </div>
                      <div className={classes.BodytitleMdl2}>
                        <text className={classes.titleMdl}>
                          Perhitungan Barang Jaminan
                        </text>
                      </div>
                      <Grid
                        container
                        direction="row"
                        item
                        lg={12}
                        xl={12}
                        md={12}
                        xs={12}
                      >
                        <Grid
                          container
                          item
                          lg={4}
                          xl={4}
                          md={4}
                          sm={4}
                          xs={12}
                        >
                          <div className={classes.label111}>
                            <text className={classes.label121}>
                              Pinjaman yang Diajukan
                            </text>
                          </div>
                        </Grid>
                        <Grid
                          container
                          item
                          lg={4}
                          xl={4}
                          md={4}
                          sm={4}
                          xs={12}
                        >
                          <div className={classes.label111}>
                            <text className={classes.label121}>Tenor</text>
                          </div>
                        </Grid>
                        <Grid
                          container
                          item
                          lg={4}
                          xl={4}
                          md={4}
                          sm={4}
                          xs={12}
                        >
                          <div className={classes.label111}>
                            <text className={classes.label121}>
                              Jumlah Sisa Pinjaman
                            </text>
                          </div>
                        </Grid>
                      </Grid>

                      <Grid
                        container
                        direction="row"
                        item
                        lg={12}
                        xl={12}
                        md={12}
                        xs={12}
                      >
                        <Grid
                          container
                          item
                          lg={4}
                          xl={4}
                          md={4}
                          sm={4}
                          xs={12}
                        >
                          <div className={classes.label111}>
                            <text className={classes.label1}>
                              Rp{" "}
                              {this.state.value.loan_amount !== undefined
                                ? Func.FormatNumber(
                                    this.state.value.loan_amount
                                  )
                                : ""}
                            </text>
                          </div>
                        </Grid>
                        <Grid
                          container
                          item
                          lg={4}
                          xl={4}
                          md={4}
                          sm={4}
                          xs={12}
                        >
                          <div className={classes.label111}>
                            <text className={classes.label1}>{labelTenor}</text>
                          </div>
                        </Grid>
                        <Grid
                          container
                          item
                          lg={4}
                          xl={4}
                          md={4}
                          sm={4}
                          xs={12}
                        >
                          <div className={classes.label111}>
                            <text className={classes.label1}>
                              Rp{" "}
                              {this.state.value.remaining_loan !== undefined
                                ? Func.FormatNumber(
                                    this.state.value.remaining_loan
                                  )
                                : ""}
                            </text>
                          </div>
                        </Grid>
                      </Grid>

                      <Grid
                        container
                        direction="row"
                        item
                        lg={12}
                        xl={12}
                        md={12}
                        xs={12}
                      >
                        <Grid
                          container
                          item
                          lg={4}
                          xl={4}
                          md={4}
                          sm={4}
                          xs={12}
                        >
                          <div className={classes.label111}>
                            <text className={classes.label121}>
                              Cicilan Perbulan
                            </text>
                          </div>
                        </Grid>
                        <Grid
                          container
                          item
                          lg={4}
                          xl={4}
                          md={4}
                          sm={4}
                          xs={12}
                        >
                          <div className={classes.label111}>
                            <text className={classes.label121}>
                              Biaya Perbulan
                            </text>
                          </div>
                        </Grid>
                      </Grid>

                      <Grid
                        container
                        direction="row"
                        item
                        lg={12}
                        xl={12}
                        md={12}
                        xs={12}
                      >
                        <Grid
                          container
                          item
                          lg={4}
                          xl={4}
                          md={4}
                          sm={4}
                          xs={12}
                        >
                          <div className={classes.label111}>
                            <text className={classes.label1}>
                              Rp{" "}
                              {this.state.value.monthly_installment !==
                              undefined
                                ? Func.FormatNumber(
                                    this.state.value.monthly_installment
                                  )
                                : ""}
                            </text>
                          </div>
                        </Grid>
                        <Grid
                          container
                          item
                          lg={4}
                          xl={4}
                          md={4}
                          sm={4}
                          xs={12}
                        >
                          <div className={classes.label111}>
                            <text className={classes.label1}>
                              Rp{" "}
                              {this.state.value.monthly_fee !== undefined
                                ? Func.FormatNumber(
                                    this.state.value.monthly_fee
                                  )
                                : ""}
                            </text>
                          </div>
                        </Grid>
                      </Grid>
                      <Divider className={classes.divider} />
                      <div className={classes.BodytitleMdl2}>
                        <text className={classes.titleMdl}>
                          Tanggal-Tanggal Penting
                        </text>
                      </div>
                      <Grid
                        container
                        direction="row"
                        item
                        lg={12}
                        xl={12}
                        md={12}
                        xs={12}
                      >
                        <Grid
                          container
                          item
                          lg={4}
                          xl={4}
                          md={4}
                          sm={4}
                          xs={12}
                        >
                          <div className={classes.label111}>
                            <text className={classes.label121}>
                              Tanggal Jatuh Tempo
                            </text>
                          </div>
                        </Grid>
                      </Grid>

                      <Grid
                        container
                        direction="row"
                        item
                        lg={12}
                        xl={12}
                        md={12}
                        xs={12}
                      >
                        <Grid
                          container
                          item
                          lg={4}
                          xl={4}
                          md={4}
                          sm={4}
                          xs={12}
                        >
                          <div className={classes.label1112}>
                            <text className={classes.label1}>
                              {Func.FormatDate(
                                this.state.value.next_installment_due_date
                              )}
                            </text>
                          </div>
                        </Grid>
                      </Grid>
                      {this.state.installmentHistory?.length >= 1 && (
                        <>
                          <Divider className={classes.divider} />
                          <div className={classes.BodytitleMdl2}>
                            <text className={classes.titleMdl}>
                              Riwayat Cicilan
                            </text>
                          </div>
                          <Grid
                            container
                            direction="row"
                            item
                            lg={12}
                            xl={12}
                            md={12}
                            xs={12}
                          >
                            <Grid
                              container
                              item
                              lg={2}
                              xl={2}
                              md={2}
                              sm={2}
                              xs={12}
                            >
                              <div className={classes.label1112}>
                                <text className={classes.label1}>
                                  Cicilan Ke
                                </text>
                              </div>
                            </Grid>
                            <Grid
                              container
                              item
                              lg={2}
                              xl={2}
                              md={2}
                              sm={2}
                              xs={12}
                            >
                              <div className={classes.label1112}>
                                <text className={classes.label1}>
                                  Tanggal Cicilan
                                </text>
                              </div>
                            </Grid>
                            <Grid
                              container
                              item
                              lg={3}
                              xl={3}
                              md={3}
                              sm={3}
                              xs={12}
                            >
                              <div className={classes.label1112}>
                                <text className={classes.label1}>
                                  Jumlah Cicilan
                                </text>
                              </div>
                            </Grid>
                            <Grid
                              container
                              item
                              lg={3}
                              xl={3}
                              md={3}
                              sm={3}
                              xs={12}
                            >
                              <div className={classes.label1112}>
                                <text className={classes.label1}>Denda</text>
                              </div>
                            </Grid>
                            <Grid
                              container
                              item
                              lg={2}
                              xl={2}
                              md={2}
                              sm={2}
                              xs={12}
                            >
                              <div className={classes.label1112}>
                                <text className={classes.label1}>
                                  Status
                                </text>
                              </div>
                            </Grid>
                          </Grid>
                        </>
                      )}

                      {this.state.installmentHistory?.length >= 1 &&
                        this.state.installmentHistory.map((val) => (
                          <Grid
                            container
                            direction="row"
                            item
                            lg={12}
                            xl={12}
                            md={12}
                            xs={12}
                          >
                            <Grid
                              container
                              item
                              lg={2}
                              xl={2}
                              md={2}
                              sm={2}
                              xs={12}
                            >
                              <div className={classes.label1112}>
                                <text className={classes.label1}>
                                  {val?.installment_order}
                                </text>
                              </div>
                            </Grid>
                            <Grid
                              container
                              item
                              lg={2}
                              xl={2}
                              md={2}
                              sm={2}
                              xs={12}
                            >
                              <div className={classes.label1112}>
                                <text className={classes.label1}>
                                  {(val?.payment_date !== '-') ? Func.FormatDate(val?.payment_date) : '-'}
                                </text>
                              </div>
                            </Grid>
                            <Grid
                              container
                              item
                              lg={3}
                              xl={3}
                              md={3}
                              sm={3}
                              xs={12}
                            >
                              <div className={classes.label1112}>
                                <text className={classes.label1}>
                                  {Func.currencyFormatter(val?.payment_amount)}
                                </text>
                              </div>
                            </Grid>
                            <Grid
                              container
                              item
                              lg={3}
                              xl={3}
                              md={3}
                              sm={3}
                              xs={12}
                            >
                              <div className={classes.label1112}>
                                <text className={classes.label1}>
                                  {Func.currencyFormatter(val?.fine_amount)}
                                </text>
                              </div>
                            </Grid>
                            <Grid
                              container
                              item
                              lg={2}
                              xl={2}
                              md={2}
                              sm={2}
                              xs={12}
                            >
                              <div className={classes.label1112}>
                                <text className={classes.label1}>
                                  {(val?.payment_status) ? 'Sudah Dibayar' : 'Belum Dibayar'}
                                </text>
                              </div>
                            </Grid>
                          </Grid>
                        ))}

                      {this.state.installmentSimulation?.length >= 1 && (
                        <>
                          <Divider className={classes.divider} />
                          <div className={classes.BodytitleMdl2}>
                            <text className={classes.titleMdl}>
                              Jadwal Angsuran
                            </text>
                          </div>
                          <Grid
                            container
                            direction="row"
                            item
                            lg={12}
                            xl={12}
                            md={12}
                            xs={12}
                          >
                            <Grid
                              container
                              item
                              lg={6}
                              xl={6}
                              md={6}
                              sm={6}
                              xs={12}
                            >
                              <div className={classes.label1112}>
                                <text className={classes.label1}>
                                  Cicilan Ke
                                </text>
                              </div>
                            </Grid>
                            <Grid
                              container
                              item
                              lg={6}
                              xl={6}
                              md={6}
                              sm={6}
                              xs={12}
                            >
                              <div className={classes.label1112}>
                                <text className={classes.label1}>
                                  Tanggal Jatuh Tempo
                                </text>
                              </div>
                            </Grid>
                          </Grid>
                        </>
                      )}

                      {this.state.installmentSimulation?.length >= 1 &&
                        this.state.installmentSimulation.map((val) => (
                          <Grid
                            container
                            direction="row"
                            item
                            lg={12}
                            xl={12}
                            md={12}
                            xs={12}
                          >
                            <Grid
                              container
                              item
                              lg={6}
                              xl={6}
                              md={6}
                              sm={6}
                              xs={12}
                            >
                              <div className={classes.label1112}>
                                <text className={classes.label1}>
                                  {val?.installment_order}
                                </text>
                              </div>
                            </Grid>
                            <Grid
                              container
                              item
                              lg={6}
                              xl={6}
                              md={6}
                              sm={6}
                              xs={12}
                            >
                              <div className={classes.label1112}>
                                <text className={classes.label1}>
                                  {Func.FormatDate(val?.installment_date)}
                                </text>
                              </div>
                            </Grid>
                          </Grid>
                        ))}
                    </Grid>
                  </Grid>
                </div>
              </div>
            </div>
          </DialogContentText>
        </DialogContent>

        <DialogActions>
          {this.state.approvals.length > 0 ? (
            <Button
              style={{
                backgroundColor: "#862C3A",
                color: "white",
              }}
              onClick={() => {
                swal({
                  title: "Apakah Anda Yakin?",
                  text: "Menyetujui transaksi ini?",
                  icon: "info",
                  buttons: true,
                  dangerMode: true,
                }).then((willDelete) => {
                  if (willDelete) {
                    this.setState({ loader: true });
                    this.handleSubmit(this.state.approvals[0].approval_url);
                  }
                });
              }}
              variant="contained"
            >
              <Typography variant="button" style={{ color: "#FFFFFF" }}>
                Setujui
              </Typography>
            </Button>
          ) : null}
          {Func.checkPermission("transaction#individual#print-sge") &&
          this.state.download !== null ? (
            <Button
              style={{
                backgroundColor: "#862C3A",
                color: "white",
              }}
              onClick={() => {
                swal({
                  title: "Apakah Anda Yakin?",
                  text: "Akan mencetak",
                  icon: "info",
                  buttons: true,
                  dangerMode: true,
                }).then((willDelete) => {
                  if (willDelete) {
                    this.setState({ loader: true });
                    window.open(this.state.download);
                    window.location.reload();
                  }
                });
              }}
              variant="contained"
            >
              <Typography variant="button" style={{ color: "#FFFFFF" }}>
                Cetak
              </Typography>
            </Button>
          ) : null}
          <Button
            style={{
              color: "black",
            }}
            disabled={this.state.loader}
            onClick={() => {
              this.props.handleModal();
            }}
            variant="outlined"
          >
            <Typography variant="button">Batal</Typography>
          </Button>
        </DialogActions>
      </Dialog>
    );
  }
}

export default withStyles(styles.CoustomsStyles, { name: "Form" })(Form);
