/* eslint-disable no-param-reassign */
/* eslint-disable semi */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react/button-has-type */
import React, { useCallback, useRef, useState } from 'react';
import Wcam from 'react-webcam';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import { Button } from '@material-ui/core';

function Webcam({ setState, handleWebcam, stateName }) {
  const webcamRef = useRef(null)

  const [dialogBox, setDialogBox] = useState(false)

  const capture = useCallback(() => {
    const ImgBase124 = webcamRef.current.getScreenshot()
    setState({ ImgBase124 })
    handleWebcam(ImgBase124, stateName)
    setDialogBox(false)
  }, [webcamRef])

  const videoConstraints = {
    facingMode: 'user'
  }

  return (
    <>
      <button onClick={() => setDialogBox(true)}>Ambil Foto Langsung</button>
      <Dialog open={dialogBox} onClose={() => setDialogBox(false)}>
        <DialogTitle>Ambil foto</DialogTitle>
        <DialogContent>
          <Wcam
            audio={false}
            height={300}
            ref={webcamRef}
            screenshotFormat="image/jpeg"
            width={400}
            videoConstraints={videoConstraints}
          />
        </DialogContent>

        <DialogActions>
          <Button color="primary" onClick={capture}>
            Ambil foto
          </Button>
          <Button color="primary" onClick={() => setDialogBox(false)}>
            Tutup
          </Button>
        </DialogActions>
      </Dialog>
    </>
  )
}

export default Webcam;
