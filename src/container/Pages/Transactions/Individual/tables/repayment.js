/* eslint-disable consistent-return */
/* eslint-disable no-redeclare */
/* eslint-disable block-scoped-var */
/* eslint-disable no-var */
/* eslint-disable vars-on-top */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable react/no-access-state-in-setstate */
/* eslint-disable no-unused-vars */
/* eslint-disable array-callback-return */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable no-useless-concat */
/* eslint-disable class-methods-use-this */
/* eslint-disable react/sort-comp */
/* eslint-disable react/no-unused-state */
/* eslint-disable camelcase */
import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import Swal from 'sweetalert2';
import Pagination from '@material-ui/lab/Pagination';
import { Hidden, Button } from '@material-ui/core';
import BeatLoader from 'react-spinners/BeatLoader';
import PrintIcon from '@material-ui/icons/Print';
import Tooltip from '@material-ui/core/Tooltip';
import Func from '../../../../../functions/index';
import Icon from '../../../../../components/icon';
import FormCountry from '../form';
import styles from '../css';
import BootstrapInput from './style/BootstrapInput';
import Filter from './Filter.jsx';

function createData(id, insurance_item_name, cif_number, sge, name, loan_amount, contract_date, status,) {
  return {
    id,
    insurance_item_name,
    cif_number,
    sge,
    name,
    loan_amount,
    contract_date,
    status
  };
}

const headCells = [
  {
    id: 'insurance_item_name',
    numeric: false,
    disablePadding: false,
    label: 'KategoriBJ'
  }, {
    id: 'cif_number',
    numeric: false,
    disablePadding: false,
    label: 'No. CIF'
  }, {
    id: 'sge',
    numeric: false,
    disablePadding: false,
    label: 'No. SGE'
  }, {
    id: 'name',
    numeric: false,
    disablePadding: false,
    label: 'Nasabah'
  }, {
    id: 'loan_amount',
    numeric: false,
    disablePadding: false,
    label: 'Jum. Pinjaman'
  }, {
    id: 'contract_date',
    numeric: false,
    disablePadding: false,
    label: 'Tgl. Dibuat'
  }, {
    id: 'status',
    numeric: false,
    disablePadding: false,
    label: 'Status'
  }, {
    id: 'action',
    numeric: false,
    disablePadding: false,
    label: ''
  }
];

class Tables extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      total_data: 0,
      total_page: 1,
      current_page: 1,
      prev_page: null,
      data: [],
      next_page: 1,
      rowsPerPage: 10,
      openSearch: false,
      rowFocus: '',
      page: 1,
      count: 1,
      loading: true,
      modal: false,
      selectAll: false,
      order: 'asc',
      rowSelected: [],
      bulk: false,
      redirect: false,
      failure: false
    };
    this.ChangePage = this
      .ChangePage
      .bind(this);
  }

  handleClick(id) {
    alert(id);
  }

  ChangePage(event, page) {
    this.setState({
      page
    }, () => {
      this.getData();
    });
  }

  setFocus(row) {
    this.setState({ rowFocus: row.id });
  }

  setUnfocus() {
    setTimeout(() => {
      this.setState({ rowFocus: '' });
    }, 5500);
  }

  handleModal(row) {
    this.setState({
      modal3: false,
      modal2: false,
      modal: true,
      row
    }, () => {
      this.setState({ modal: false, bulk: false });
    });
  }

  handleRepayments(row) {
    this.setState({
      modal3: true,
      modal2: false,
      modal: false,
      row
    }, () => {
      this.setState({ modal3: false, bulk: false });
    });
  }

  handleApprove(row) {
    this.setState({
      modal3: false,
      modal2: true,
      modal: false,
      row
    }, () => {
      this.setState({ modal2: false, bulk: false });
    });
  }

  componentDidMount() {
    if (localStorage.getItem('delete_product') != null) {
      this.handleDelete(localStorage.getItem('delete_product'));
    } else {
      this.getData('first');
    }
  }

    handleClickDelete = (row) => {
      Swal
        .fire({
          title: 'Apakah Anda yakin?',
          text: 'Akan menghapus data yang dipilih',
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Ya, Hapus!',
          cancelButtonText: 'Batal'
        })
        .then((result) => {
          if (result.value) {
            fetch(process.env.REACT_APP_URL_MANAGEMENT + process.env.REACT_APP_API_PREFIX_V1 + '/transactions/' + row, {
              method: 'DELETE',
              headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + localStorage.getItem('token')
              }
            }).then((response) => response.json()).then((json) => {
              this.getData('first');
              this.setState({ rowSelected: [] });
              Swal.fire('Berhasil!', 'Data berhasil dihapus.', 'success');
            }).catch((error) => {})
              . finally(() => {});
          }
        });
    };

    handleDelete = (row) => {
      fetch(process.env.REACT_APP_URL_MANAGEMENT + process.env.REACT_APP_API_PREFIX_V1 + '/transactions/' + row, {
        method: 'DELETE',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + localStorage.getItem('token')
        }
      }).then((response) => response.json()).then((json) => {
        localStorage.removeItem('delete_product');
        this.getData('first');
      }).catch((error) => {})
        . finally(() => {});
    };

    getData = () => {
      this.setState({ loading: true });
      Func
        .getDataTransaction(this.props.path, this.state.rowsPerPage, this.state.page, this.state.search)
        .then((res) => {
          if (res.json.code === '403') {
            if (Func.Clear_Token() === true) {
              if (!localStorage.getItem('token')) {
                this.setState({ redirect: true });
              }
            }
          } else if (res.json.status === 500) {
            this.setState({ failure: true });
          } else {
            const datas = [];
            res
              .json
              .data
              .map((data) => {
                if (data.company === null) {
                  datas.push(createData(
                    data.id,
                    data.insurance_item_name,
                    data.customer ? data.customer.cif_number : '',
                    data.sge,
                    data.customer ? data.customer.name : '',
                    data.loan_amount,
                    data.contract_date,
                    data.payment_status
                  ));
                }
              });
            this.setState({
              con: res.json.data.length,
              data: datas,
              loading: false,
              total_data: res.json.total_data,
              page: res.json.page,
              next_page: res.json.next_page,
              prev_page: res.json.prev_page,
              current_page: res.json.current_page,
              total_page: res.json.total_page
            });
          }
        });
    };

    Short(orderKey) {
      if (orderKey === this.state.order) {
        this.setState({ order: '' });
        var library = this.state.data;
        library.sort((a, b) => (a[orderKey] < b[orderKey]
          ? 1
          : b[orderKey] < a[orderKey]
            ? -1
            : 0));
      } else {
        this.setState({ order: orderKey });
        var library = this.state.data;
        library.sort((a, b) => (a[orderKey] > b[orderKey]
          ? 1
          : b[orderKey] > a[orderKey]
            ? -1
            : 0));
      }
    }

    render() {
      const { classes } = this.props;
      let con = 0;
      this
        .state
        .data
        .map((row) => {
          if (this.state.rowSelected.indexOf(row.id) > -1) {
            con += 1;
          }
        });
      return (
        <div>
          {Func.toLogin(this.state.redirect)}
          <TableContainer
            style={{
              width: this.props.open
                ? window.innerWidth - this.props.width
                : window.innerWidth - 115
            }}
            component={Paper}
          >
            <div style={{
              width: '100%'
            }}
            >
              <Box display="flex">
                <Box flexGrow={1}>
                  <Typography variant="h6" className={classes.paginationTxt2}>
                    {this.props.title}
                  </Typography>
                </Box>
              </Box>
            </div>
            <div style={{
              width: '100%'
            }}
            >
              <Box display="flex">
                <Box flexGrow={1}>
                  <Typography className={classes.paginationTxt}>
                    {this.props.subtitle}
                  </Typography>
                </Box>
              </Box>
            </div>
            <Hidden smUp>
              <div
                style={{
                  width: '100%'
                }}
              >
                <Box display="flex">
                  <Box flexGrow={1}>
                    <Typography variant="h9" className={classes.textperdata}>
                      Tampilkan
                    </Typography>
                    <Select
                      size="small"
                      className={classes.selectperdata}
                      labelId="demo-customized-select-label"
                      id="demo-customized-select"
                      value={this.state.rowsPerPage}
                      onChange={(val) => {
                        this.setState({
                          rowsPerPage: val.target.value
                        }, () => {
                          this.getData();
                        });
                      }}
                      input={<BootstrapInput />}
                    >
                      <MenuItem value={10}>10 data</MenuItem>
                      <MenuItem value={50}>50 data</MenuItem>
                      <MenuItem value={100}>100 data</MenuItem>
                    </Select>
                    <Typography variant="h9" className={classes.textperdata}>
                      Total:
                      {' '}
                      {' '}
                      {this.state.total_data}
                      {' '}
                      Transaksi
                    </Typography>
                  </Box>
                </Box>
              </div>
              <div
                style={{
                  width: '100%'
                }}
              >
                <Box display="flex">
                  <Box>
                    <BootstrapInput
                      style={{
                        marginBottom: 20,
                        marginLeft: -0
                      }}
                      value={this.state.search}
                      onChange={(event) => {
                        this.setState({
                          search: event.target.value
                        }, () => {
                          this.getData();
                        });
                      }}
                      on
                      className={classes.search2}
                      id="demo-customized-textbox"
                    />
                  </Box>
                </Box>
              </div>
            </Hidden>
            <Hidden xsDown>
              <div
                style={{
                  width: '100%'
                }}
              >
                <Box display="flex">
                  <Box flexGrow={1}>
                    <Typography variant="h9" className={classes.textperdata}>
                      Tampilkan
                    </Typography>
                    <Select
                      size="small"
                      className={classes.selectperdata}
                      labelId="demo-customized-select-label"
                      id="demo-customized-select"
                      value={this.state.rowsPerPage}
                      onChange={(val) => {
                        this.setState({
                          rowsPerPage: val.target.value
                        }, () => {
                          this.getData();
                        });
                      }}
                      input={<BootstrapInput />}
                    >
                      <MenuItem value={10}>10 data</MenuItem>
                      <MenuItem value={50}>50 data</MenuItem>
                      <MenuItem value={100}>100 data</MenuItem>
                    </Select>
                    <Typography variant="h9" className={classes.textperdata}>
                      Total:
                      {' '}
                      {' '}
                      {this.state.total_data}
                      {' '}
                      Transaksi
                    </Typography>
                  </Box>
                  <Box>
                    <Filter handleOnSubmit={createData} />
                  </Box>
                  <Box>
                    <BootstrapInput
                      placeholder="Pencarian No. SGE"
                      value={this.state.search}
                      style={{
                        marginBottom: 20,
                        marginLeft: -0
                      }}
                      onChange={(event) => {
                        this.setState({
                          search: event.target.value
                        }, () => {
                          this.getData();
                        });
                      }}
                      on
                      className={classes.search2}
                      id="demo-customized-textbox"
                    />
                  </Box>
                </Box>
              </div>
            </Hidden>
            <FormCountry
              type="Ubah"
              modal={this.state.modal}
              modal2={this.state.modal2}
              modal3={this.state.modal3}
              row={this.state.row}
            />
          </TableContainer>
          <TableContainer
            style={{
              width: this.props.open
                ? window.innerWidth - this.props.width
                : window.innerWidth - 115
            }}
            component={Paper}
          >
            <Table
              size="small"
              onMouseOut={() => {
                this.setUnfocus();
              }}
              className={classes.table}
              aria-label="simple table"
            >
              <TableHead className={classes.headTable}>
                <TableRow>
                  {headCells.map((headCell) => (
                    <TableCell
                      key={headCell.id}
                      align={headCell.numeric
                        ? 'right'
                        : 'left'}
                      sortDirection={this.state.order === headCell.id
                        ? this.state.order
                        : false}
                    >
                      <TableSortLabel
                        active={this.state.order === headCell.id}
                        direction={this.state.order === headCell.id
                          ? this.state.order
                          : 'asc'}
                        onClick={() => {
                          this.Short(headCell.id);
                        }}
                      >
                        {headCell.label}
                        {this.state.order === headCell.id
                          ? (<span className={classes.visuallyHidden} />)
                          : null}
                      </TableSortLabel>
                    </TableCell>
                  ))}
                </TableRow>
              </TableHead>
              {this.state.data.length === 0 && !this.state.loading
                ? (
                  <TableBody>
                    <TableRow>
                      <TableCell colSpan={9} align="center">
                        <Typography>
                          Tidak Ada Data
                        </Typography>
                      </TableCell>
                    </TableRow>
                  </TableBody>
                )
                : null}
              <TableBody>
                {!this.state.loading
                  ? this
                    .state
                    .data
                    .map((row) => (
                      <TableRow
                        hover
                        onMouseOver={() => {
                          this.setFocus(row);
                        }}
                        role="checkbox"
                        tabIndex={-1}
                        key={row.id}
                      >
                        <TableCell
                          style={{ cursor: 'pointer' }}
                          onClick={() => {
                            this.handleApprove(row);
                          }}
                          align="left"
                        >
                          {row.insurance_item_name}
                        </TableCell>
                        <TableCell
                          style={{ cursor: 'pointer' }}
                          onClick={() => {
                            this.handleApprove(row);
                          }}
                          align="left"
                        >
                          {row.cif_number}
                        </TableCell>
                        <TableCell
                          style={{ cursor: 'pointer' }}
                          onClick={() => {
                            this.handleApprove(row);
                          }}
                          align="left"
                        >
                          {row.sge}
                        </TableCell>
                        <TableCell
                          style={{ cursor: 'pointer' }}
                          onClick={() => {
                            this.handleApprove(row);
                          }}
                          align="left"
                        >
                          {row.name}
                        </TableCell>
                        <TableCell
                          style={{ cursor: 'pointer' }}
                          onClick={() => {
                            this.handleApprove(row);
                          }}
                          align="left"
                        >
                          {Func.FormatRp(row.loan_amount)}
                        </TableCell>
                        <TableCell
                          style={{ cursor: 'pointer' }}
                          onClick={() => {
                            this.handleApprove(row);
                          }}
                          align="left"
                        >
                          {Func.FormatDate(row.contract_date)}
                        </TableCell>
                        <TableCell align="left">
                          <div
                            style={{
                              backgroundColor: '#28a745',
                              width: '160px',
                              height: '40px',
                              color: 'white',
                              borderRadius: 5,
                              padding: '10px 0',
                              textAlign: 'center'
                            }}
                          >
                            Sudah Dibayar
                          </div>
                        </TableCell>
                        <TableCell align="left">
                          <div>
                            <Tooltip title="Cetak Struk" aria-label="Cetak">
                              <IconButton
                                onClick={() => {
                                  window.open(process.env.REACT_APP_BASE_URL_MANAJEMENT + process.env.REACT_APP_API_PREFIX_V1 + '/transactions/' + row.id + '/print_repayment.pdf?token=' + localStorage.getItem('token'));
                                }}
                              >
                                <PrintIcon />
                              </IconButton>
                            </Tooltip>
                          </div>
                        </TableCell>
                      </TableRow>
                    ))

                  : null}
              </TableBody>
            </Table>
            {this.state.loading
              ? (
                <div className={classes.loader}>
                  <BeatLoader size={15} color="#3F3F3F" loading />
                </div>
              )
              : null}
          </TableContainer>
          <TableContainer
            style={{
              width: this.props.open
                ? window.innerWidth - this.props.width
                : window.innerWidth - 115,
              marginBottom: 40
            }}
            component={Paper}
          >
            <div className={classes.col}>
              <text>
                Halaman
                {' '}
                {' '}
                {' ' + this.state.current_page + ' '}
                dari
                {' '}
                {' '}
                {' ' + this.state.total_page + ' '}
                halaman
              </text>
            </div>
            <Pagination
              color="secondary"
              className={classes.row}
              count={this.state.total_page}
              defaultPage={this.state.page}
              onChange={this.ChangePage}
              siblingCount={0}
            />
          </TableContainer>
        </div>
      );
    }
}

export default withStyles(styles.CoustomsStyles, { name: 'Tables' })(Tables);
