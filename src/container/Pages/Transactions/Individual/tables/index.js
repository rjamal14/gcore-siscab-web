/* eslint-disable no-redeclare */
/* eslint-disable block-scoped-var */
/* eslint-disable no-var */
/* eslint-disable vars-on-top */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable react/no-access-state-in-setstate */
/* eslint-disable no-unused-vars */
/* eslint-disable array-callback-return */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable no-useless-concat */
/* eslint-disable class-methods-use-this */
/* eslint-disable react/sort-comp */
/* eslint-disable react/no-unused-state */
/* eslint-disable camelcase */
import React from 'react';
import Container from '@material-ui/core/Container';
import Hidden from '@material-ui/core/Hidden';
import Grid from '@material-ui/core/Grid';
import { withStyles } from '@material-ui/core/styles';
import styles from '../css';
import Melting from './melting';
import Repayment from './repayment';
import Extension from './extension';
import Auction from './auction';
import Cancle from './cancle';
import Installment from './installment';

class Index extends React.Component {
  render() {
    const { classes } = this.props;
    let tbl = '';

    if (this.props.active === 'Pencairan') {
      const ttl = 'Pencairan';
      const subtl = 'pencairan.';
      tbl = (
        <div>
          <Hidden only={['lg', 'xl']}>
            <Melting
              width={60}
              open={this.props.open}
              filter={false}
              title={'Transaksi ' + ttl}
              subtitle=""
              path="transaction_disbursements?type=customer"
            />
          </Hidden>
          <Hidden only={['sm', 'md', 'xs']}>
            <Melting
              width={285}
              open={this.props.open}
              filter={false}
              title={'Transaksi ' + ttl}
              subtitle=""
              path="transaction_disbursements?type=customer"
            />
          </Hidden>
        </div>
      );
    } else if (this.props.active === 'Pelunasan') {
      const ttl = 'Pelunasan';
      const subtl = 'pelunasan.';
      tbl = (
        <div>
          <Hidden only={['lg', 'xl']}>
            <Repayment
              width={60}
              open={this.props.open}
              filter={false}
              title={'Transaksi ' + ttl}
              subtitle=""
              path="transaction_repayments?type=customer"
            />
          </Hidden>
          <Hidden only={['sm', 'md', 'xs']}>
            <Repayment
              width={285}
              open={this.props.open}
              filter={false}
              title={'Transaksi ' + ttl}
              subtitle=""
              path="transaction_repayments?type=customer"
            />
          </Hidden>
        </div>
      );
    } else if (this.props.active === 'Perpanjangan') {
      const ttl = 'Perpanjangan';
      const subtl = 'perpanjangan.';
      tbl = (
        <div>
          <Hidden only={['lg', 'xl']}>
            <Extension
              width={60}
              open={this.props.open}
              filter={false}
              title={'Transaksi ' + ttl}
              subtitle=""
              path="transaction_time_extensions?type=customer"
            />
          </Hidden>
          <Hidden only={['sm', 'md', 'xs']}>
            <Extension
              width={285}
              open={this.props.open}
              filter={false}
              title={'Transaksi ' + ttl}
              subtitle=""
              path="transaction_time_extensions?type=customer"
            />
          </Hidden>
        </div>
      );
    } else if (this.props.active === 'Cicilan') {
      const ttl = 'Cicilan';
      const subtl = 'cicilan.';
      tbl = (
        <div>
          <Hidden only={['lg', 'xl']}>
            <Installment
              width={60}
              open={this.props.open}
              filter={false}
              title={'Transaksi ' + ttl}
              subtitle=""
              path="transaction_installment?customer"
            />
          </Hidden>
          <Hidden only={['sm', 'md', 'xs']}>
            <Installment
              width={285}
              open={this.props.open}
              filter={false}
              title={'Transaksi ' + ttl}
              subtitle=""
              path="transaction_installment?customer"
            />
          </Hidden>
        </div>
      );
    } else if (this.props.active === 'Lelang') {
      const ttl = 'Lelang';
      const subtl = 'lelang.';
      tbl = (
        <div>
          <Hidden only={['lg', 'xl']}>
            <Auction
              width={60}
              open={this.props.open}
              filter={false}
              title={'Transaksi ' + ttl}
              subtitle=""
              path="transaction_auctions?type=customer"
            />
          </Hidden>
          <Hidden only={['sm', 'md', 'xs']}>
            <Auction
              width={285}
              open={this.props.open}
              filter={false}
              title={'Transaksi ' + ttl}
              subtitle=""
              path="transaction_auctions?type=customer"
            />
          </Hidden>
        </div>
      );
    } else if (this.props.active === 'Dibatalkan') {
      const ttl = 'Dibatalkan';
      const subtl = 'dibatalkan.';
      tbl = (
        <div>
          <Hidden only={['lg', 'xl']}>
            <Cancle
              width={60}
              open={this.props.open}
              filter={false}
              title={'Transaksi ' + ttl}
              subtitle=""
              path="transaction_cancels?customer"
            />
          </Hidden>
          <Hidden only={['sm', 'md', 'xs']}>
            <Cancle
              width={285}
              open={this.props.open}
              filter={false}
              title={'Transaksi ' + ttl}
              subtitle=""
              path="transaction_cancels?customer"
            />
          </Hidden>
        </div>
      );
    }
    return (
      <div>
        {tbl}
      </div>
    );
  }
}

export default withStyles(styles.CoustomsStyles, { name: 'Index' })(Index);
