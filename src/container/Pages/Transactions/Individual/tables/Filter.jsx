/* eslint-disable camelcase */
/* eslint-disable no-shadow */
/* eslint-disable no-param-reassign */
/* eslint-disable react/jsx-one-expression-per-line */
/* eslint-disable no-unused-vars */
/* eslint-disable semi */
import React, { useState, useEffect } from 'react';
import { Button } from '@material-ui/core';
import FilterListIcon from '@material-ui/icons/FilterList';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import Autocomplete from '@material-ui/lab/Autocomplete';
import TextField from '@material-ui/core/TextField';
import Swal from 'sweetalert2';

import service from '../../../../../functions/service'

function Filter({ handleOnSubmit }) {
  const [dialogBox, setDialogBox] = useState(false)
  const [filterData, setFilterData] = useState({
    customerId: '',
    productId: ''
  })
  const [customerOption, setCustomerOption] = useState([])
  const [productOption, setProductOption] = useState([])

  useState(() => {
    service.getCustomer()
      .then(res => res.json())
      .then(resjson => {
        if (resjson.code === 200) setCustomerOption(resjson.data)
      })

    service.getProduct()
      .then(res => res.json())
      .then(resjson => {
        if (resjson.code === 200) setProductOption(resjson.data)
      })
  }, [customerOption])

  const getFilterData = () => {
    if (filterData.customerId && filterData.productId) {
      service.getTransactionFilter(filterData)
        .then(res => res.json())
        .then(resjson => {
          if (resjson.code === 200) {
            if (resjson.data.length > 0) {
              handleOnSubmit(resjson.data)
            } else {
              setDialogBox(false)
              setFilterData((filterData) => {
                filterData.customerId = '';
                filterData.productId = '';
                return filterData
              })
              Swal.fire('Data tidak ditemukan')
            }
          }
        })
    } else setDialogBox(false)
  }

  return (
    <>
      <Button
        name="filterButton"
        style={{ marginRight: 15, marginBottom: 15 }}
        startIcon={<FilterListIcon />}
        variant="outlined"
        color="primary"
        onClick={() => setDialogBox(true)}
      >
        Filter
      </Button>

      <Dialog open={dialogBox} onClose={() => setDialogBox(false)}>
        <DialogTitle>Filter</DialogTitle>
        <DialogContent>
          <Autocomplete
            options={
              customerOption.map(({ id, name }) => (
                {
                  id,
                  name
                }
              ))
            }
            onChange={(evt, val) => {
              setFilterData((filterData) => {
                filterData.customerId = val;
                return filterData
              })
            }}
            getOptionLabel={(option) => option.name}
            style={{ width: 300 }}
            renderInput={(params) => <TextField {...params} label="Nama Customer" variant="outlined" />}
            noOptionsText="Data Kosong"
          />
          <br /><br />
          <Autocomplete
            options={
              productOption.map((val) => (
                {
                  id: val.id.$oid,
                  name: val.name
                }
              ))
            }
            onChange={(evt, val) => {
              setFilterData((filterData) => {
                filterData.productId = val;
                return filterData
              })
            }}
            getOptionLabel={(option) => option.name}
            style={{ width: 300 }}
            renderInput={(params) => <TextField {...params} label="Jenis Barang Jaminan" variant="outlined" />}
          />
        </DialogContent>
        <DialogActions>
          <Button color="primary" onClick={getFilterData}>
            Filter
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
}

export default Filter;
