/* eslint-disable react/no-access-state-in-setstate */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/no-unused-state */
/* eslint-disable react/sort-comp */
import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Hidden from '@material-ui/core/Hidden';
import AddIcon from '@material-ui/icons/Add';
import Fab from '@material-ui/core/Fab';
import Box from '@material-ui/core/Box';
import swal from 'sweetalert';
import { withRouter } from 'react-router';
import styles from './css';
import Tables from './tables/index';
import Form from './form/index';
import Func from '../../../../functions/index';
import { getCashStatus2 } from '../../Cashflow/Cashflow/api';

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      open: true,
      master: false,
      validator: [],
      value: [],
      mdlShow: true,
      mobile: false,
      redirect: false,
      show: 'password',
      id_cust: null,
      cashStatus: 'close',
      active: localStorage.getItem('activeTransacForm') || 'Pencairan'
    };
  }

  handleModal() {
    const { active } = this.state;
    if (active === 'Pencairan') {
      this.setState({
        modal: true,
        modal5: false
      }, () => {
        this.setState({ modal: false });
      });
    } else if (active === 'Cicilan') {
      this.setState({
        modalInstallment: true,
        modalInstallment2: false,
      }, () => {
        this.setState({ modalInstallment: false });
      });
    }
  }

  handleRepayments() {
    this.setState({
      modal5: true,
      modal: false,
    }, () => {
      this.setState({ modal5: false });
    });
  }

  handleDrawer() {
    this.setState({
      open: !this.state.open
    });
  }

  componentDidMount() {
    // cek ke api apa hr ini sudah tutup kas atau belum
    const { id, type } = this.props.match.params;

    if (!id && !type) {
      getCashStatus2(localStorage.getItem('office_id'))
        .then(res => {
          const response = res?.data || res?.response?.data;
          if (res?.status !== 200) {
            swal({
              title: 'Tidak Bisa Melakukan Transaksi',
              text: response?.message,
              icon: 'info',
              buttons: 'OK'
            });
          } else this.setState({ cashStatus: 'open' });
        });
    }

    if (type === 'installment') {
      this.setState({ active: 'Cicilan' });
      localStorage.setItem('activeTransacForm', 'Cicilan');
    }
  }

  checkActive() {
    const { active } = this.state;
    if ((active === 'Pencairan' || active === 'Cicilan' || active === 'Pelunasan')) {
      return true;
    } return false;
  }

  render() {
    const { classes } = this.props;
    const {
      modalInstallment,
      modalInstallment2,
      installmentRepay,
    } = this.state;
    const { active } = this.state;

    return (
      <main>
        <Hidden only={['lg', 'xl']}>
          <div>
            <Box
              display="flex"
              justifyContent="flex-start"
              p={1}
              style={{ marginBottom: 8 }}
              bgcolor="background.paper"
            >
              <Box
                onClick={() => {
                  this.setState({ active: 'Pencairan' });
                  localStorage.setItem('activeTransacForm', 'Pencairan');
                }}
                style={{
                  cursor: 'pointer'
                }}
                ml={1}
                p={1}
                borderBottom={active === 'Pencairan'
                  ? 3
                  : 0}
                color={active === 'Pencairan'
                  ? '#85203B'
                  : '#95A1A7'}
              >
                Pencairan
              </Box>
              <Box
                onClick={() => {
                  this.setState({ active: 'Pelunasan' });
                  localStorage.setItem('activeTransacForm', 'Pelunasan');
                }}
                style={{
                  cursor: 'pointer'
                }}
                ml={1}
                p={1}
                borderBottom={active === 'Pelunasan'
                  ? 3
                  : 0}
                color={active === 'Pelunasan'
                  ? '#85203B'
                  : '#95A1A7'}
              >
                Pelunasan
              </Box>
              <Box
                onClick={() => {
                  this.setState({ active: 'Perpanjangan' });
                  localStorage.setItem('activeTransacForm', 'Perpanjangan');
                }}
                style={{
                  cursor: 'pointer'
                }}
                ml={1}
                p={1}
                borderBottom={active === 'Perpanjangan'
                  ? 3
                  : 0}
                color={active === 'Perpanjangan'
                  ? '#85203B'
                  : '#95A1A7'}
              >
                Perpanjangan
              </Box>
              <Box
                onClick={() => {
                  this.setState({ active: 'Cicilan' });
                  localStorage.setItem('activeTransacForm', 'Cicilan');
                }}
                style={{
                  cursor: 'pointer'
                }}
                ml={1}
                p={1}
                borderBottom={active === 'Cicilan'
                  ? 3
                  : 0}
                color={active === 'Cicilan'
                  ? '#85203B'
                  : '#95A1A7'}
              >
                Cicilan
              </Box>
              <Box
                onClick={() => {
                  this.setState({ active: 'Lelang' });
                  localStorage.setItem('activeTransacForm', 'Lelang');
                }}
                style={{
                  cursor: 'pointer'
                }}
                ml={1}
                p={1}
                borderBottom={active === 'Lelang'
                  ? 3
                  : 0}
                color={active === 'Lelang'
                  ? '#85203B'
                  : '#95A1A7'}
              >
                Lelang
              </Box>
              <Box
                onClick={() => {
                  this.setState({ active: 'Dibatalkan' });
                  localStorage.setItem('activeTransacForm', 'DiBatalkan');
                }}
                style={{
                  cursor: 'pointer'
                }}
                ml={1}
                p={1}
                borderBottom={active === 'Dibatalkan'
                  ? 3
                  : 0}
                color={active === 'Dibatalkan'
                  ? '#85203B'
                  : '#95A1A7'}
              >
                Dibatalkan
              </Box>
            </Box>
          </div>
        </Hidden>
        <Hidden only={['sm', 'md', 'xs']}>
          <div>
            <Box
              display="flex"
              justifyContent="flex-start"
              p={1}
              style={{ marginBottom: 8 }}
              bgcolor="background.paper"
            >
              <Box
                onClick={() => {
                  this.setState({ active: 'Pencairan' });
                  localStorage.setItem('activeTransacForm', 'Pencairan');
                }}
                style={{
                  cursor: 'pointer'
                }}
                ml={1}
                p={1}
                borderBottom={active === 'Pencairan'
                  ? 3
                  : 0}
                color={active === 'Pencairan'
                  ? '#85203B'
                  : '#95A1A7'}
              >
                Pencairan
              </Box>
              <Box
                onClick={() => {
                  this.setState({ active: 'Pelunasan' });
                  localStorage.setItem('activeTransacForm', 'Pelunasan');
                }}
                style={{
                  cursor: 'pointer'
                }}
                ml={1}
                p={1}
                borderBottom={active === 'Pelunasan'
                  ? 3
                  : 0}
                color={active === 'Pelunasan'
                  ? '#85203B'
                  : '#95A1A7'}
              >
                Pelunasan
              </Box>
              <Box
                onClick={() => {
                  this.setState({ active: 'Perpanjangan' });
                  localStorage.setItem('activeTransacForm', 'Perpanjangan');
                }}
                style={{
                  cursor: 'pointer'
                }}
                ml={1}
                p={1}
                borderBottom={active === 'Perpanjangan'
                  ? 3
                  : 0}
                color={active === 'Perpanjangan'
                  ? '#85203B'
                  : '#95A1A7'}
              >
                Perpanjangan
              </Box>
              <Box
                onClick={() => {
                  this.setState({ active: 'Cicilan' });
                  localStorage.setItem('activeTransacForm', 'Cicilan');
                }}
                style={{
                  cursor: 'pointer'
                }}
                ml={1}
                p={1}
                borderBottom={active === 'Cicilan'
                  ? 3
                  : 0}
                color={active === 'Cicilan'
                  ? '#85203B'
                  : '#95A1A7'}
              >
                Cicilan
              </Box>
              <Box
                onClick={() => {
                  this.setState({ active: 'Lelang' });
                  localStorage.setItem('activeTransacForm', 'Lelang');
                }}
                style={{
                  cursor: 'pointer'
                }}
                ml={1}
                p={1}
                borderBottom={active === 'Lelang'
                  ? 3
                  : 0}
                color={active === 'Lelang'
                  ? '#85203B'
                  : '#95A1A7'}
              >
                Lelang
              </Box>
              <Box
                onClick={() => {
                  this.setState({ active: 'Dibatalkan' });
                  localStorage.setItem('activeTransacForm', 'Dibatalkan');
                }}
                style={{
                  cursor: 'pointer'
                }}
                ml={1}
                p={1}
                borderBottom={active === 'Dibatalkan'
                  ? 3
                  : 0}
                color={active === 'Dibatalkan'
                  ? '#85203B'
                  : '#95A1A7'}
              >
                Dibatalkan
              </Box>
            </Box>
          </div>
        </Hidden>
        <Form
          type="Tambah"
          modal={this.state.modal}
          modal2={this.state.modal2}
          modal3={this.state.modal3}
          modal4={this.state.modal4}
          modal5={this.state.modal5}
          modalInstallment={modalInstallment}
          modalInstallment2={modalInstallment2}
          installmentRepay={installmentRepay}
        />
        <div style={{ display: 'flex', justifyContent: 'center' }}>
          <Tables open={this.state.open} active={active} />
        </div>
        {
          (Func.checkPermission('transaction#individual#create'))
            && (this.state.cashStatus === 'open')
            && (this.checkActive())
            && (
              <Fab
                className={classes.fab}
                color="#85203B"
                aria-label="add"
                onClick={() => {
                  if ((active === 'Pencairan' || active === 'Cicilan')) {
                    this.handleModal();
                  } else if ((active === 'Pelunasan')) {
                    this.handleRepayments();
                  }
                }}
              >
                <AddIcon />
              </Fab>
            )
        }
      </main>
    );
  }
}

export default withRouter(withStyles(styles.CoustomsStyles, { name: 'Login' })(Login));
