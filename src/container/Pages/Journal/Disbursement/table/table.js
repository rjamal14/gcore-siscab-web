/* eslint-disable radix */
/* eslint-disable no-unused-vars */
/* eslint-disable react/no-access-state-in-setstate */
/* eslint-disable eqeqeq */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable class-methods-use-this */
/* eslint-disable react/sort-comp */
/* eslint-disable react/no-unused-state */
/* eslint-disable no-sparse-arrays */
/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable array-callback-return */
import React from 'react';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import Select2 from 'react-select';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import IconButton from '@material-ui/core/IconButton';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import InputBase from '@material-ui/core/InputBase';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import Swal from 'sweetalert2';
import Pagination from '@material-ui/lab/Pagination';
import { Hidden, Button } from '@material-ui/core';
import BeatLoader from 'react-spinners/BeatLoader';
import FilterListIcon from '@material-ui/icons/FilterList';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Divider from '@material-ui/core/Divider';
import Collapse from '@material-ui/core/Collapse';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import env from '../../../../../config/env';
import Func from '../../../../../functions/index';
import Icon from '../../../../../components/icon';
import styles from './css';

function createData(name, description, date, history, detais, debit, credit) {
  return {
    name,
    description,
    date,
    debit,
    credit,
    history,
    detais
  };
}
const useRowStyles = makeStyles({
  root: {
    '& > *': {
      borderBottom: 'unset'
    }
  }
});

function Row2(props) {
  const { row } = props;
  const [open, setOpen] = React.useState(false);
  const classes = useRowStyles();

  return (
    <React.Fragment>
      <TableRow className={classes.root} style={{ cursor: 'pointer' }} onClick={() => setOpen(!open)}>
        <Table size="small" aria-label="purchases">
          <TableHead>
            <TableRow>
              <TableCell style={{ width: 15 }}>
                <IconButton
                  aria-label="expand row"
                  size="small"
                >
                  {open ? <img src={Icon.dash} style={{ width: 15 }} /> : <img src={Icon.dash} style={{ width: 15 }} />}
                </IconButton>
              </TableCell>
              <TableCell style={{ fontWeight: 'bold', backgroundColor: '#EBEBEB', }}>No Akun</TableCell>
              <TableCell style={{ fontWeight: 'bold', backgroundColor: '#EBEBEB', }}>Nama Akun</TableCell>
              <TableCell style={{ fontWeight: 'bold', backgroundColor: '#EBEBEB', }}>Keterangan</TableCell>
              <TableCell style={{ fontWeight: 'bold', backgroundColor: '#EBEBEB', }}>Debit</TableCell>
              <TableCell style={{ fontWeight: 'bold', backgroundColor: '#EBEBEB', }}>Kredit</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {row.detail.map((detailRow) => (
              <TableRow key={detailRow.no_akun}>
                <TableCell style={{ width: '1%' }} />
                <TableCell style={{ width: '10%' }}>{detailRow.no_akun}</TableCell>
                <TableCell style={{ width: '18%' }}>{detailRow.account_item}</TableCell>
                <TableCell component="th" scope="row" style={{ width: '22%' }}>
                  {detailRow.name}
                </TableCell>
                <TableCell style={{ width: '13%' }}>{detailRow.debit}</TableCell>
                <TableCell style={{ width: '13%' }}>{detailRow.credit}</TableCell>
              </TableRow>
            ))}
            <TableRow>
              <TableCell colSpan={3} />
              <TableCell style={{ backgroundColor: '#EBEBEB', fontWeight: 'bold' }}>
                Blance
              </TableCell>
              <TableCell style={{ backgroundColor: '#EBEBEB', fontWeight: 'bold' }}>
                {Func.FormatRp(row.debit)}
              </TableCell>
              <TableCell style={{ backgroundColor: '#EBEBEB', fontWeight: 'bold' }}>
                {Func.FormatRp(row.credit)}
              </TableCell>
            </TableRow>
          </TableBody>
        </Table>
      </TableRow>
    </React.Fragment>
  );
}

function Row(props) {
  const { row } = props;
  const [open, setOpen] = React.useState(false);
  const classes = useRowStyles();

  return (
    <React.Fragment>
      <TableRow className={classes.root} style={{ cursor: 'pointer' }} onClick={() => setOpen(!open)}>
        <TableCell style={{ width: 15 }}>
          <IconButton
            aria-label="expand row"
            size="small"
          >
            {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
          </IconButton>
        </TableCell>
        <TableCell>{row.name}</TableCell>
        <TableCell>{row.description}</TableCell>
        <TableCell>{row.date}</TableCell>
      </TableRow>
      <TableRow>
        <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
          <Collapse in={open} timeout="auto" unmountOnExit>
            <Box>
              <Table size="small" aria-label="purchases">
                <TableBody>
                  {row.history.map((historyRow) => (
                    <Row2 key={historyRow.name} row={historyRow} />
                  ))}
                </TableBody>
              </Table>
            </Box>
          </Collapse>
        </TableCell>
      </TableRow>
    </React.Fragment>
  );
}

const headCells = [
  {
    id: 'name',
    numeric: false,
    disablePadding: false,
    label: 'Nama'
  }, {
    id: 'type',
    numeric: false,
    disablePadding: false,
    label: 'Keterangan'
  }, {
    id: 'date',
    numeric: false,
    disablePadding: false,
    label: 'Tanggal'
  },
];

const BootstrapInput = withStyles((theme) => ({
  root: {
    'label + &': {
      marginTop: theme.spacing(3)
    }
  },
  input: {
    borderRadius: 4,
    position: 'relative',
    backgroundColor: theme.palette.background.paper,
    border: '1px solid #ced4da',
    fontSize: 16,
    padding: '10px 26px 10px 12px',
    transition: theme
      .transitions
      .create(['border-color', 'box-shadow']),
    fontFamily: [
      '-apple-system',
      'BlinkMacSystemFont',
      '"Segoe UI"',
      'Roboto',
      '"Helvetica Neue"',
      'Arial',
      'sans-serif',
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"'
    ].join(','),
    '&:focus': {
      borderRadius: 4,
      borderColor: '#80bdff',
      boxShadow: '0 0 0 0.2rem rgba(0,123,255,.25)'
    }
  }
}))(InputBase);

class Tables extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      total_data: 0,
      total_page: 1,
      current_page: 1,
      prev_page: null,
      data: [],
      next_page: 1,
      rowsPerPage: 10,
      openSearch: false,
      rowFocus: '',
      page: 1,
      filter: false,
      count: 1,
      // eslint-disable-next-line no-dupe-keys
      data: [],
      filterValue: [],
      loading: true,
      modal: false,
      selectAll: false,
      order: 'asc',
      rowSelected: [],
      bulk: false,
      redirect: false,
      failure: false
    };
    this.ChangePage = this
      .ChangePage
      .bind(this);
  }

  handleClick(id) {
    alert(id);
  }

  ChangePage(event, page) {
    this.setState({
      page
    }, () => {
      this.getData();
    });
  }

  setFocus(row) {
    this.setState({ rowFocus: row.id });
  }

  setUnfocus() {
    setTimeout(() => {
      this.setState({ rowFocus: '' });
    }, 5500);
  }

  handleModal(row) {
    this.setState({
      modal: true,
      row
    }, () => {
      this.setState({ modal: false, bulk: false });
    });
  }

  componentDidMount() {
    const d = new Date();
    this.setState({
      startDate: new Date(parseInt(d.getMonth()) + 1 + '-01-' + d.getFullYear()),
      endDate: d
    }, () => {
      this.getData('first');
    });
  }

    handleClickDelete = (row) => {
      Swal
        .fire({
          title: 'Apakah Anda yakin?',
          text: 'Akan menghapus data yang dipilih',
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Ya, Hapus!',
          cancelButtonText: 'Batal'
        })
        .then((result) => {
          if (result.value) {
            fetch(env.managementApi + env.apiPrefixV1 + '/' + this.props.path + '/' + row, {
              method: 'DELETE',
              headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + localStorage.getItem('token')
              }
            }).then((response) => response.json()).then((json) => {
              if (json.success) {
                this.setState({ rowSelected: [] });
                Swal.fire('Berhasil!', 'Data berhasil dihapus.', 'success');
              } else {
                Swal.fire('Gagal!', json.message, 'warning');
              }
              this.getData('first');
            }).catch(() => {})
              .finally(() => {});
          }
        });
    };

    formatDate(date) {
      const d = new Date(date);
      let month = '' + (d.getMonth() + 1);
      let day = '' + d.getDate();
      const year = d.getFullYear();

      if (month.length < 2) month = '0' + month;
      if (day.length < 2) day = '0' + day;

      return [year, month, day].join('-');
    }

    getData = () => {
      this.setState({ loading: true });
      Func.getDataJournalV2('journals?type=Pencairan&q[publish_date_lteq]=' + this.formatDate(this.state.endDate) + '&q[publish_date_gteq]=' + this.formatDate(this.state.startDate), this.state.rowsPerPage, this.state.page, this.state.search)
        .then((res) => {
          if (res.json.code === '403') {
            if (Func.Clear_Token() === true) {
              if (!localStorage.getItem('token')) {
                this.setState({ redirect: true });
              }
            }
          } else if (res.json.status === 500) {
            this.setState({ failure: true });
          } else {
            const datas = [];
            res
              .json
              .data
              .map((data) => {
                const History = [];
                const debit = 0;
                const credit = 0;
                const detailss = [];
                data.journal_entries.map((data2) => {
                  detailss.push({
                    name: data2.description,
                    no_akun: data2.account_number,
                    account_item: data2.account_name,
                    debit: data2.type === 'debit' ? Func.FormatRp(data2.amount) : '',
                    credit: data2.type != 'debit' ? Func.FormatRp(data2.amount) : ''
                  });
                });
                History.push({
                  name: data.name,
                  detail: detailss,
                  debit: parseFloat(data.balance_debit),
                  credit: parseFloat(data.balance_credit)
                });
                datas.push(createData(data.name, data.description, Func.FormatDate(data.publish_date), History, debit, credit));
              });

            this.setState({
              con: res.json.data.length,
              data: datas,
              loading: false,
              total_data: res.json.total_data,
              page: res.json.current_page,
              next_page: res.json.next_page,
              prev_page: res.json.prev_page,
              current_page: res.json.current_page,
              total_page: res.json.total_page,
              filter: false
            });
          }
        });
    };

    Short(orderKey) {
      if (orderKey === this.state.order) {
        this.setState({ order: '' });
        const library = this.state.data;
        library.sort((a, b) => (a[orderKey] < b[orderKey]
          ? 1
          : b[orderKey] < a[orderKey]
            ? -1
            : 0));
      } else {
        this.setState({ order: orderKey });
        // eslint-disable-next-line no-redeclare
        const library = this.state.data;
        library.sort((a, b) => (a[orderKey] > b[orderKey]
          ? 1
          : b[orderKey] > a[orderKey]
            ? -1
            : 0));
      }
    }

    handleChange(event, name) {
      const dataSet = this.state.filterValue;
      dataSet[name] = event;
      this.setState({ filterValue: dataSet });
    }

    handleClickDeleteAll = () => {
      Swal
        .fire({
          title: 'Apakah Anda yakin?',
          text: 'Akan menghapus data yang dipilih',
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Ya, Hapus!',
          cancelButtonText: 'Batal'
        })
        .then((result) => {
          if (result.value) {
            this
              .state
              .rowSelected
              .map((row) => {
                fetch(env.managementApi + env.apiPrefixV1 + '/' + this.props.path + '/' + row, {
                  method: 'DELETE',
                  headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: 'Bearer ' + localStorage.getItem('token')
                  }
                }).then((response) => response.json()).then(() => {
                }).catch(() => {})
                  .finally(() => {});
              });
            Swal.fire('Berhasil!', 'Data berhasil dihapus.', 'success');
            this.getData();
            this.setState({ rowSelected: [] });
          }
        });
    };

    DeleteBulk() {
      this.handleClickDeleteAll();
    }

    render() {
      const { classes } = this.props;
      let con = 0;
      this.state.data.map((row) => {
        if (this.state.rowSelected.indexOf(row.id) > -1) {
          con += 1;
        }
      });
      return (
        <div>
          <TableContainer
            style={{ width: this.props.open ? window.innerWidth - this.props.width : window.innerWidth - 115 }}
            component={Paper}
          >
            <div style={{
              width: '100%'
            }}
            >
              <Box display="flex">
                <Box flexGrow={1}>
                  <Typography variant="h6" className={classes.paginationTxt2}>
                    {this.props.title}
                  </Typography>
                </Box>
              </Box>
            </div>
            <div style={{
              width: '100%'
            }}
            >
              <Box display="flex">
                <Box flexGrow={1}>
                  <Typography className={classes.paginationTxt}>
                    {this.props.subtitle}
                  </Typography>
                </Box>
              </Box>
            </div>
            <Hidden smUp>
              <div
                style={{
                  width: '100%'
                }}
              >
                <Box display="flex">
                  <Box flexGrow={1}>
                    <Typography variant="h9" className={classes.textperdata}>
                      Tampilkan
                    </Typography>
                    <Select
                      size="small"
                      className={classes.selectperdata}
                      labelId="demo-customized-select-label"
                      id="demo-customized-select"
                      value={this.state.rowsPerPage}
                      onChange={(val) => {
                        this.setState({
                          rowsPerPage: val.target.value
                        }, () => {
                          this.getData();
                        });
                      }}
                      input={<BootstrapInput />}
                    >
                      <MenuItem value={10}>10 data</MenuItem>
                      <MenuItem value={50}>50 data</MenuItem>
                      <MenuItem value={100}>100 data</MenuItem>
                    </Select>
                    <Typography variant="h9" className={classes.textperdata}>
                      Total:
                      {' '}
                      {this.state.total_data}
                      {' '}
                      Jurnal Pencarian
                    </Typography>
                  </Box>
                </Box>
              </div>
              <div
                style={{
                  width: '100%'
                }}
              >
                <Box display="flex">
                  <Box>
                    <Button
                      name="filterButton"
                      style={{ marginRight: 15, marginBottom: 15 }}
                      startIcon={<FilterListIcon />}
                      variant="outlined"
                      onClick={() => {
                        this.setState({
                          filter: !this.state.filter
                        });
                      }}
                      color="primary"
                    >
                      Filter
                    </Button>
                  </Box>
                </Box>
              </div>
            </Hidden>
            <Dialog
              open={this.state.filter}
              onClose={() => {
                this.setState({
                  filter: !this.state.filter
                });
              }}
              aria-labelledby="alert-dialog-title"
              aria-describedby="alert-dialog-description"
            >
              <DialogTitle id="alert-dialog-title">Filter</DialogTitle>
              <Divider style={{ marginBottom: 20 }} />
              <DialogContent>
                <DialogContentText id="alert-dialog-description">
                  <div>
                    <div>
                      <Select2
                        name="form-field-name-error"
                        value={this.state.filterValue.status_true}
                        placeholder="Pilih"
                        error
                        styles={{
                          control: (provided, state) => ({
                            ...provided,
                            borderColor: this.state.filterValue.status_true
                              ? 'red'
                              : '#CACACA',
                            borderRadius: '0.25rem'
                          })
                        }}
                        className={classes.search2}
                        options={[
                          {
                            value: '-',
                            label: 'Pilih Status',
                            isDisabled: true
                          }, {
                            value: 1,
                            label: 'Aktif'
                          }, {
                            value: 0,
                            label: 'Tidak Aktif'
                          }
                        ]}
                        onChange={(val) => {
                          this.handleChange(val, 'status_true');
                        }}
                      />
                    </div>
                    <div>
                      <BootstrapInput
                        value={this.state.filterValue.name_cont}
                        placeholder="Nama Nasabah"
                        onChange={(event) => {
                          this.handleChange(event.target.value, 'name_cont');
                        }}
                        on
                        className={classes.search2}
                        id="demo-customized-textbox"
                      />
                    </div>
                    <div>
                      <BootstrapInput
                        placeholder="Email"
                        value={this.state.filterValue.email_cont}
                        onChange={(event) => {
                          this.handleChange(event.target.value, 'email_cont');
                        }}
                        on
                        className={classes.search2}
                        id="demo-customized-textbox"
                      />
                    </div>
                    <div>
                      <BootstrapInput
                        value={this.state.filterValue.customer_contact_phone_number_cont}
                        placeholder="No. Telepon"
                        onChange={(event) => {
                          this.handleChange(event.target.value, 'customer_contact_phone_number_cont');
                        }}
                        on
                        className={classes.search2}
                        id="demo-customized-textbox"
                      />
                    </div>
                    <div>
                      <BootstrapInput
                        value={this.state.filterValue.identity_number}
                        placeholder="No. Indentitas"
                        onChange={(event) => {
                          this.handleChange(event.target.value, 'identity_number');
                        }}
                        on
                        className={classes.search2}
                        id="demo-customized-textbox"
                      />
                    </div>
                    <div>
                      <BootstrapInput
                        value={this.state.filterValue.cif_number_cont}
                        placeholder="No. CIF"
                        onChange={(event) => {
                          this.handleChange(event.target.value, 'cif_number_cont');
                        }}
                        on
                        className={classes.search2}
                        id="demo-customized-textbox"
                      />
                    </div>
                  </div>
                </DialogContentText>
              </DialogContent>
              <DialogActions>
                <Button
                  onClick={() => {
                    this.setState({
                      filterValue: []
                    }, () => {
                      this.getData();
                    });
                  }}
                  color="primary"
                >
                  Hapus
                </Button>
                <Button
                  onClick={() => {
                    this.getData();
                  }}
                  color="primary"
                  autoFocus
                >
                  Filter
                </Button>
              </DialogActions>
            </Dialog>
            <Hidden xsDown>
              <div
                style={{
                  width: '100%'
                }}
              >
                <Box display="flex">
                  <Box flexGrow={1}>
                    <Typography variant="h9" className={classes.textperdata}>
                      Tampilkan
                    </Typography>
                    <Select
                      size="small"
                      className={classes.selectperdata}
                      labelId="demo-customized-select-label"
                      id="demo-customized-select"
                      value={this.state.rowsPerPage}
                      onChange={(val) => {
                        this.setState({
                          rowsPerPage: val.target.value
                        }, () => {
                          this.getData();
                        });
                      }}
                      input={<BootstrapInput />}
                    >
                      <MenuItem value={10}>10 data</MenuItem>
                      <MenuItem value={50}>50 data</MenuItem>
                      <MenuItem value={100}>100 data</MenuItem>
                    </Select>
                    <Typography variant="h9" className={classes.textperdata}>
                      Total:
                      {' '}
                      {this.state.total_data}
                      {' '}
                      Jurnal Pencarian
                    </Typography>
                  </Box>
                  <Box>
                    <DatePicker
                      className={classes.Startdate}
                      selected={this.state.startDate}
                      onChange={(startDate) => {
                        if (Object.prototype.toString.call(startDate) === '[object Date]') {
                          this.setState({ startDate });
                        }
                      }}
                      selectsStart
                      selectsEnd
                      disabledKeyboardNavigation
                      value={Func.FormatDate(this.state.startDate)}
                      placeholderText="Tanggal Awal"
                      startDate={this.state.startDate}
                      endDate={this.state.endDate}
                    />
                  </Box>
                  <Box>
                    <DatePicker
                      className={classes.date}
                      selected={this.state.endDate}
                      selectsEnd
                      onChange={(endDate) => {
                        if (Object.prototype.toString.call(endDate) === '[object Date]') {
                          this.setState({ endDate });
                        }
                      }}
                      value={Func.FormatDate(this.state.endDate)}
                      placeholderText="Tanggal Akhir"
                      startDate={this.state.startDate}
                      endDate={this.state.endDate}
                      disabledKeyboardNavigation
                      minDate={this.state.startDate}
                    />
                  </Box>
                  <Box style={{ marginRight: 20 }}>
                    <Button
                      variant="contained"
                      onClick={() => {
                        this.getData();
                      }}
                    >
                      Terapkan
                    </Button>
                  </Box>
                  <Box>
                    <Button
                      name="filterButton"
                      style={{ marginRight: 15, marginBottom: 15 }}
                      startIcon={<FilterListIcon />}
                      variant="outlined"
                      color="primary"
                      onClick={() => {
                        this.setState({
                          filter: !this.state.filter
                        });
                      }}
                    >
                      Filter
                    </Button>
                  </Box>
                </Box>
              </div>
            </Hidden>
          </TableContainer>
          <TableContainer style={{ width: this.props.open ? window.innerWidth - this.props.width : window.innerWidth - 115 }} component={Paper}>
            <Table
              size="small"
              onMouseOut={() => {
                this.setUnfocus();
              }}
              className={classes.table}
              aria-label="simple table"
            >
              <TableHead className={classes.headTable}>
                <TableRow>
                  <TableCell />
                  {headCells.map((headCell) => (
                    <TableCell
                      key={headCell.id}
                      align={headCell.numeric
                        ? 'right'
                        : 'left'}
                      sortDirection={this.state.order === headCell.id
                        ? this.state.order
                        : false}
                    >
                      <TableSortLabel
                        active={this.state.order === headCell.id}
                        direction={this.state.order === headCell.id
                          ? this.state.order
                          : 'asc'}
                        onClick={() => {
                          this.Short(headCell.id);
                        }}
                      >
                        {headCell.label}
                        {this.state.order === headCell.id
                          ? (
                            <span className={classes.visuallyHidden} />
                          )
                          : null}
                      </TableSortLabel>
                    </TableCell>
                  ))}
                </TableRow>
              </TableHead>
              {this.state.data.length == 0 && !this.state.loading
                ? (
                  <TableBody>
                    <TableRow>
                      <TableCell colSpan={7} align="center">
                        <Typography>
                          Tidak Ada Data
                        </Typography>
                      </TableCell>
                    </TableRow>
                  </TableBody>
                )
                : null}
              <TableBody>
                {!this.state.loading
                  ? this
                    .state
                    .data
                    .map((row) => (
                      <Row key={row.name} row={row} />
                    ))

                  : null}
              </TableBody>
            </Table>
            {this.state.loading
              ? (
                <div className={classes.loader}>
                  <BeatLoader size={15} color="#3F3F3F" loading />
                </div>
              )
              : null}
          </TableContainer>
          <TableContainer style={{ width: this.props.open ? window.innerWidth - this.props.width : window.innerWidth - 115, marginBottom: 40 }} component={Paper}>
            <div className={classes.col}>
              <text>
                Halaman
                {' '}
                {' ' + this.state.current_page + ' '}
                dari
                {' '}
                {' ' + this.state.total_page + ' '}
                halaman
              </text>
            </div>
            <Pagination
              color="secondary"
              className={classes.row}
              count={this.state.total_page}
              defaultPage={this.state.page}
              onChange={this.ChangePage}
              siblingCount={0}
            />
          </TableContainer>
          {this.state.rowSelected.length > 0
            ? (
              <div className={classes.popupv2}>
                <Box display="flex" justifyContent="center" flexWrap="wrap">
                  <Box flexWrap="wrap">
                    <div className={classes.popup}>
                      <Box display="flex" justifyContent="center" flexWrap="wrap">
                        <Box>
                          <Typography className={classes.popupTxt}>
                            Pilih aksi:
                          </Typography>
                        </Box>
                        <Box>
                          <IconButton
                            onClick={() => {
                              this.DeleteBulk();
                            }}
                            aria-label="Cari"
                          >
                            <img src={Icon.popupDelete} />
                          </IconButton>
                        </Box>
                        <Box>
                          <IconButton
                            className={classes.popupClose}
                            onClick={() => {
                              this.setState({ rowSelected: [] });
                            }}
                            aria-label="Cari"
                          >
                            <img src={Icon.close} />
                          </IconButton>
                        </Box>
                      </Box>
                    </div>
                  </Box>
                </Box>
              </div>
            )
            : null}
        </div>
      );
    }
}

export default withStyles(styles.CoustomsStyles, { name: 'Tables' })(Tables);
