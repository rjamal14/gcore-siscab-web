import React, { Fragment, useState } from 'react';
// import PropTypes from 'prop-types';
import Tables from './table';

const JournalPage = () => {
  const [tabValue] = useState(0);

  return (
    <Fragment>
      <Tables tabValue={tabValue} index={0} />
    </Fragment>
  );
};

JournalPage.propTypes = {
};

export default JournalPage;
