/* eslint-disable react/jsx-one-expression-per-line */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable camelcase */
/* eslint-disable react-hooks/exhaustive-deps */
import React, { Fragment, useState, useEffect } from 'react';
import {
  Paper,
  TableContainer,
  TextField,
  TableRow,
  Table,
  withStyles,
} from '@material-ui/core';
import MaterialTable, { MTableToolbar } from 'material-table';
import Autocomplete from '@material-ui/lab/Autocomplete';
import MuiTableCell from '@material-ui/core/TableCell';
import swal from 'sweetalert';
import Alert from '@material-ui/lab/Alert';
import { useHistory } from 'react-router-dom';
import useStyles from './component-jss.js';
import { getAccounts, postJournal } from './api';
import entryJournalColumns from './entryJournalColumns';
import Func from '../../../../functions';

const TableCell = withStyles({
  root: {
    borderBottom: 'none',
  },
})(MuiTableCell);

const EntryJournalForm = () => {
  const classes = useStyles();
  const [data, setData] = useState([]);
  const [debit, setDebit] = useState(0);
  const [credit, setCredit] = useState(0);
  const [journal, setJournal] = useState({ name: '', desc: '' });
  const [columns, setColumns] = useState(entryJournalColumns);
  const [validator, setValidator] = useState([]);
  const [collection, setCollection] = useState([]);
  const history = useHistory();
  const schema = [
    {
      name: 'name',
      type: 'required',
    },
    {
      name: 'desc',
      type: 'required',
    },
  ];

  const onRowAdd = (newData) => new Promise((resolve) => {
    const datanew = newData;
    const { accNum, item, id } = newData.accNumber;
    datanew.accId = id;
    datanew.accNumber = accNum;
    datanew.accName = item;
    setTimeout(() => {
      setData([...data, datanew]);
      resolve();
    }, 1000);
  });

  const onRowUpdate = (newData, oldData) => new Promise((resolve) => {
    const dataUpdate = [...data];
    const index = oldData.tableData.id;
    const datanew = newData;
    let account;
    if (!newData.accNumber?.accNum) {
      account = collection.find((v) => v.account_number === newData.accNumber);
    } else account = newData.accNumber;

    const { accNum, item, id, account_number } = account;
    datanew.accId = id;
    datanew.accNumber = accNum || account_number;
    datanew.accName = item;
    setTimeout(() => {
      dataUpdate[index] = datanew;
      setData([...dataUpdate]);
      resolve();
    }, 1000);
  });

  const onRowDelete = (oldData) => new Promise((resolve) => {
    setTimeout(() => {
      const dataDelete = [...data];
      const index = oldData.tableData.id;
      dataDelete.splice(index, 1);
      setData([...dataDelete]);
      resolve();
    }, 1000);
  });

  useEffect(() => {
    getAccounts().then((res) => {
      const response = res?.data || res?.response?.data;
      if (res?.status === 200) {
        const col = columns;
        const { data: accData } = response;
        setCollection(accData);
        col[1].editComponent = (props) => {
          const val = props.value?.accNum || props.value;
          return (
            <Autocomplete
              value={accData.find((v) => v.account_number === val)}
              options={accData.map((v) => ({
                id: v.id,
                item: v.item,
                accNum: v.account_number,
              }))}
              onChange={(evt, value) => props.onChange(value)}
              getOptionLabel={({ item }) => item}
              size="small"
              renderInput={(params) => (
                <TextField
                  {...params}
                  error={props.error}
                  helperText={props.helperText}
                  variant="outlined"
                />
              )}
            />
          );
        };
        setColumns(col);
      }
    });
  }, []);

  useEffect(() => {
    if (data.length > 0) {
      const deb = data.reduce((a, b) => a + parseInt(b?.debit || 0, 0), 0);
      const cre = data.reduce((a, b) => a + parseInt(b?.credit || 0, 0), 0);
      setDebit(deb);
      setCredit(cre);
    }
  }, [data]);

  const handleChange = (evt) => {
    const { name: fieldName, value } = evt.target;
    const state = journal;
    state[fieldName] = value;
    setJournal(state);
  };

  const handleSubmit = () => {
    const validate = Func.Validator(journal, schema);
    if (validate.success && data.length > 0 && debit === credit) {
      const journalAttrb = data.map((val) => ({
        account_id: val.accId,
        amount: val?.debit || val?.credit,
        transaction_type: (val?.debit ? 'debit' : 'credit'),
        description: val.desc,
        ref_code: val.reference,
        document_number: val.document_number,
      }));
      const payload = {
        name: journal.name,
        description: journal.desc,
        journal_entries_attributes: journalAttrb
      };
      swal({
        title: 'Apa Anda yakin?',
        text: 'Menyimpan data ini',
        icon: 'info',
        buttons: ['Batal', 'Simpan'],
      })
        .then((willDelete) => {
          if (willDelete) {
            postJournal(payload)
              .then((res) => {
                const response = res?.data || res?.response?.data;
                if (res?.status === 201) {
                  swal('Sukses!', '', 'success')
                    .then(() => { history.replace('/app/journal/all'); });
                } else swal('Kesalahan!', response?.message, 'error');
              });
          }
        });
    } else setValidator(validate.error);
  };

  const Form = () => (
    <Table style={{ width: 0, }}>
      <TableRow>
        <TableCell><b>Nama</b></TableCell>
        <TableCell>
          <TextField
            name="name"
            variant="outlined"
            onChange={handleChange}
            size="small"
            defaultValue={journal?.name}
            helperText={validator.name}
            error={validator.name}
          />
        </TableCell>
        <TableCell><b>Debit</b></TableCell>
        <TableCell>
          {Func.currencyFormatter(debit || 0)}
        </TableCell>
        <TableCell>
          {(debit || credit)
            ? (debit === credit)
              ? (<Alert severity="success">Balance</Alert>)
              : (<Alert severity="error">Tidak Balance</Alert>) : ''}
          {data.length === 0 && (<Alert severity="error">Tabel jurnal kosong, mohon isi dahulu</Alert>)}
        </TableCell>
      </TableRow>
      <TableRow>
        <TableCell><b>Deskripsi</b></TableCell>
        <TableCell>
          <TextField
            name="desc"
            variant="outlined"
            onChange={handleChange}
            size="small"
            defaultValue={journal?.desc}
            helperText={validator.desc}
            error={validator.desc}
          />
        </TableCell>
        <TableCell><b>Kredit</b></TableCell>
        <TableCell>
          {Func.currencyFormatter(credit || 0)}
        </TableCell>
      </TableRow>
    </Table>
  );

  return (
    <Fragment>
      <div className={classes.root}>
        <TableContainer component={Paper}>
          <MaterialTable
            title={(<b>Entry Jurnal</b>)}
            columns={columns}
            data={data}
            editable={{
              onRowAdd: (newData) => onRowAdd(newData),
              onRowUpdate: (newData, oldData) => onRowUpdate(newData, oldData),
              onRowDelete: (oldData) => onRowDelete(oldData),
            }}
            options={{
              search: false,
              paging: false,
            }}
            components={{
              Toolbar: (props) => (
                <div>
                  <MTableToolbar {...props} />
                  <Form />
                </div>
              ),
            }}
            actions={[
              {
                icon: 'save',
                tooltip: 'Simpan Data',
                isFreeAction: true,
                onClick: handleSubmit,
              },
            ]}
          />
        </TableContainer>
      </div>
    </Fragment>
  );
};

export default EntryJournalForm;
