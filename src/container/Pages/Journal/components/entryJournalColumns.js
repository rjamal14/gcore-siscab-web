/* eslint-disable react/destructuring-assignment */
/* eslint-disable semi */
import React from 'react'
import { TextField, InputAdornment } from '@material-ui/core'
import Func from '../../../../functions'

const entryJournalColumns = [
  {
    title: 'id',
    field: 'accId',
    hidden: true,
  },
  {
    title: 'Nomor Akun',
    field: 'accNumber',
    validate: rowData => ((rowData.accNumber !== undefined && !rowData.accNumber)
      ? 'Tidak boleh kosong' : (rowData.accNumber !== undefined && rowData.accNumber)
        ? true : ''),
  },
  {
    title: 'Nama Akun',
    field: 'accName',
    editable: 'never',
  },
  {
    title: 'Kode Referensi',
    field: 'reference',
    initialEditValue: '-',
  },
  {
    title: 'No. Dokumen',
    field: 'document_number',
    initialEditValue: '-',
  },
  {
    title: 'Deskripsi',
    field: 'desc',
    initialEditValue: '-',
  },
  {
    title: 'Debit',
    field: 'debit',
    type: 'currency',
    currencySetting: {
      locale: 'id-ID',
      currencyCode: 'IDR',
      minimumFractionDigits: 0
    },
    validate: rowData => ((rowData.debit !== undefined && (!rowData.debit && !rowData.credit))
      ? 'Isi salah satu' : (rowData.debit !== undefined && rowData.debit && !rowData.credit)
        ? true : (rowData.credit && !rowData.debit) ? true : ''),
    editComponent: props => (
      <TextField
        size="small"
        autoComplete="off"
        error={props.error}
        helperText={props.helperText}
        value={Func.FormatNumber(props.value)}
        onChange={(event) => props.onChange(Func.UnFormatRp(event.target.value))}
        InputProps={{
          startAdornment: (<InputAdornment position="start">Rp</InputAdornment>),
        }}
      />
    )
  },
  {
    title: 'Kredit',
    field: 'credit',
    type: 'currency',
    currencySetting: {
      locale: 'id-ID',
      currencyCode: 'IDR',
      minimumFractionDigits: 0
    },
    validate: rowData => ((rowData.debit !== undefined && (!rowData.debit && !rowData.credit))
      ? 'Isi salah satu' : (rowData.debit && rowData.credit)
        ? 'Kredit harus kosong jika debit terisi' : (rowData.credit !== undefined && rowData.credit && !rowData.debit)
          ? true : (!rowData.credit && rowData.debit) ? true : ''),
    editComponent: props => (
      <TextField
        size="small"
        autoComplete="off"
        error={props.error}
        helperText={props.helperText}
        value={Func.FormatNumber(props.value)}
        onChange={(event) => props.onChange(Func.UnFormatRp(event.target.value))}
        InputProps={{
          startAdornment: (<InputAdornment position="start">Rp</InputAdornment>),
        }}
      />
    )
  },
];

export default entryJournalColumns;
