import { makeStyles } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    padding: theme.spacing(2),
  },
  tableToolbar: {
    display: 'flex',
    alignItems: 'flex-end',
    padding: theme.spacing(2),
    borderRadius: '12px 12px 0px 0px',
    [theme.breakpoints.down('sm')]: {
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'center'
    }
  },
  tableLeftContent: {
    display: 'flex',
    flex: 1,
    flexDirection: 'column',
    [theme.breakpoints.down('sm')]: {
      alignItems: 'center'
    }
  },
  title: {
    marginBottom: theme.spacing(2)
  },
}));

export default useStyles;
