/* eslint-disable react/no-access-state-in-setstate */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/sort-comp */
/* eslint-disable react/no-unused-state */
import React, { Fragment } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Hidden from '@material-ui/core/Hidden';
import styles from './css';
import Tables from './table';
class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      open: true,
      master: false,
      validator: [],
      value: [],
      mdlShow: true,
      mobile: false,
      redirect: false,
      show: 'password',
      id_cust: null
    };
  }

  handleModal() {
    this.setState({
      modal: true
    }, () => {
      this.setState({ modal: false });
    });
  }

  handleDrawer() {
    this.setState({
      open: !this.state.open
    });
  }

  componentDidMount() {}

  render() {
    return (
      <Fragment>
        <div style={{ display: 'flex', justifyContent: 'center', paddingTop: 8 }}>
          <Hidden only={['lg', 'xl']}>
            <Tables
              width={60}
              open={this.state.open}
              filter={false}
              title="Jurnal Akun"
              subtitle=""
              path="journals?type=Perpanjangan"
            />
          </Hidden>
          <Hidden only={['sm', 'md', 'xs']}>
            <Tables
              width={270}
              open={this.state.open}
              filter={false}
              title="Jurnal Akun"
              subtitle=""
              path="journals?type=Perpanjangan"
            />
          </Hidden>
        </div>
      </Fragment>
    );
  }
}

export default withStyles(styles.CoustomsStyles, { name: 'Login' })(Login);
