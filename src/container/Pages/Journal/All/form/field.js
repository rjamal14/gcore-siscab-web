/* eslint-disable react/button-has-type */
/* eslint-disable no-plusplus */
/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable consistent-return */
/* eslint-disable camelcase */
/* eslint-disable eqeqeq */
/* eslint-disable array-callback-return */
/* eslint-disable radix */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/sort-comp */
/* eslint-disable react/no-access-state-in-setstate */
/* eslint-disable react/no-unused-state */
import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import InputAdornment from '@material-ui/core/InputAdornment';
import Typography from '@material-ui/core/Typography';
import FormHelperText from '@material-ui/core/FormHelperText';
import Grid from '@material-ui/core/Grid';
import TextareaAutosize from '@material-ui/core/TextareaAutosize';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import CloseIcon from '@material-ui/icons/Close';
import IconButton from '@material-ui/core/IconButton';
import Button from '@material-ui/core/Button';
import Divider from '@material-ui/core/Divider';
import swal from 'sweetalert';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import { CircularProgress } from '@material-ui/core';
import AsyncSelect from 'react-select/async';
import Select2 from 'react-select';
import Link from '@material-ui/core/Link';
import env from '../../../../../config/env';
import styles from './css';
import Icon from '../../../../../components/icon';
import Func from '../../../../../functions/index';

class Form extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: false,
      validator: [],
      journal_entries_attributes: [
        {
          account_id: '',
          amount: '',
          transaction_type: '',
          description: '',
          ref_code: '',
          _destroy: false
        }, {
          account_id: '',
          amount: '',
          transaction_type: '',
          description: '',
          ref_code: '',
          _destroy: false
        }
      ],
      data4: [],
      data5: [],
      value: {
        email: '',
        nominal: ''
      },
      office_name: '',
      name: ''
    };
    this.handleChangeImg = this
      .handleChangeImg
      .bind(this);
    this.timeout = 0;
  }

  removeValidate(name) {
    const data = this.state.validator;
    delete data[name];
    this.setState({ validator: data });
  }

  handleChange(event, name) {
    const dataSet = this.state.value;
    dataSet[name] = event;
    this.setState({ value: dataSet });
  }

  handleChangeDate(date, name, name2) {
    const dt = new Date(date);
    const dataSet = this.state.value;
    dataSet[name2] = dt;
    dataSet[name] = dt.getFullYear() + '-' + parseInt(dt.getMonth() + 1) + '-' + dt.getDate();
    this.setState({ value: dataSet });
  }

  getAkun(val) {
    fetch(env.financialApi + env.apiPrefixV1 + '/accounts/autocomplete?query=' + val, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + localStorage.getItem('token')
      }
    }).then((response) => response.json()).then((json) => {
      if (json.code == '403') {
        if (Func.Clear_Token() == true) {
          if (!localStorage.getItem('token')) {
            this.setState({ redirect: true });
          }
        }
      } else {
        const datas = [];
        datas.push({
          value: '-',
          label: json.data.length > 0
            ? 'Pilih'
            : 'Tidak ditemukan',
          isDisabled: true
        });
        json
          .data
          .map((value) => {
            datas.push({ label: value.account_number, value: value.id });
          });

        this.setState({ data5: datas, data5_ori: json.data });
      }
    }).catch(() => {})
      . finally(() => {});
  }

  getType(val, name) {
    fetch(env.financialApi + env.apiPrefixV1 + '/journal_types/autocomplete?query=' + val, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + localStorage.getItem('token')
      }
    }).then((response) => response.json()).then((json) => {
      if (json.code === '403') {
        if (Func.Clear_Token() === true) {
          if (!localStorage.getItem('token')) {
            this.setState({ redirect: true });
          }
        }
      } else {
        const datas = [];
        datas.push({
          value: '-',
          label: json.data.length > 0
            ? 'Pilih'
            : 'Tidak ditemukan',
          isDisabled: true
        });

        if (name != '') {
          const search = json
            .data
            .find((o) => o.name === name);
          if (search === undefined) {
            datas.push({ value: name, label: name });
          }
        }
        json
          .data
          .map((value) => {
            datas.push({ label: value.name, value: value.id });
          });

        this.setState({ data4: datas, data4_ori: json.data });
      }
    }).catch(() => {})
      . finally(() => {});
  }

  componentDidMount() {
    this.getType('', '');
    this.getAkun('', '');
    const { value } = this.state;
    value.transaction_type0 = {
      value: 'credit',
      label: 'Credit '
    };
    value.transaction_type1 = {
      value: 'debit',
      label: 'Debit '
    };
    this.setState({ value });
  }

  handleSubmit(type) {
    this.setState({ loader_button: true });
    const validator = [
      {
        name: 'name',
        type: 'required'
      }, {
        name: 'description',
        type: 'required'
      }, {
        name: 'journal_type',
        type: 'required'
      }
    ];

    this
      .state
      .journal_entries_attributes
      .map((value, index) => {
        if (!value._destroy) {
          validator.push({
            name: 'account_id' + index,
            type: 'required'
          });
          validator.push({
            name: 'amount' + index,
            type: 'required'
          });
          validator.push({
            name: 'transaction_type' + index,
            type: 'required'
          });
          validator.push({
            name: 'description' + index,
            type: 'required'
          });
          validator.push({
            name: 'ref_code' + index,
            type: 'required'
          });
        }
      });

    const validate = Func.Validator(this.state.value, validator);
    if (validate.success) {
      const journal_entries_attributes = [];
      this
        .state
        .journal_entries_attributes
        .map((value, index) => {
          if (value._destroy == false) {
            const item_journal_entries_attributes = {
              account_id: this.state.value['account_id' + index].value,
              amount: parseInt(this.state.value['amount' + index]),
              transaction_type: this.state.value['transaction_type' + index].value,
              description: this.state.value['description' + index],
              ref_code: this.state.value['ref_code' + index]
            };
            journal_entries_attributes.push(item_journal_entries_attributes);
          }
        });
      fetch(env.financialApi + env.apiPrefixV1 + '/journals', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + localStorage.getItem('token')
        },
        body: JSON.stringify({ name: this.state.value.name, description: this.state.value.description, journal_type_name: this.state.value.journal_type.label, journal_type_id: this.state.value.journal_type.value, journal_entries_attributes })
      }).then((response) => response.json()).then((json) => {
        if (json.code == '403') {
          Func.Refresh_Token();
          if (Func.Refresh_Token() == true) {
            this.handleSubmit();
          }
        }
        if (type == 'Tambah') {
          if (json.created) {
            this
              .props
              .OnNext(json.message);
            this.setState({ loader_button: false });
          } else {
            Func.AlertError(json.message);
            this.setState({ loader_button: false });
          }
        } else {
          Func.AlertError(json.message);
          this.setState({ loader_button: false });
        }
      }).catch(() => {})
        . finally(() => {});
    } else {
      this.setState({ validator: validate.error });
      this.setState({ loader_button: false });
    }
  }

  handleChangeImg(event) {
    const file = event.target.files[0];
    if (file.size > 2.9e6) {
      swal({ title: 'File Terlalu Besar', text: 'Maximal File 2Mb', icon: 'error', buttons: 'OK' });
      return false;
    }
    this.removeValidate('img');
    const dataSet = this.state.value;
    dataSet.img = URL.createObjectURL(event.target.files[0]);
    this.setState({ value: dataSet });
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onloadend = () => {
      this.setState({ ImgBase44: reader.result });
    };
  }

  addChild() {
    const detail = this.state.journal_entries_attributes;
    detail.push({
      account_id: '',
      amount: '',
      transaction_type: '',
      description: '',
      ref_code: '',
      _destroy: false
    });
    this.setState({ journal_entries_attributes: detail });
  }

  renderChild() {
    const loadOptions2 = (inputValue, callback) => {
      setTimeout(() => {
        callback(this.state.data5);
      }, 600);
    };
    const { classes } = this.props;
    const dataLIst = [];
    let key = 1;
    this
      .state
      .journal_entries_attributes
      .map((value, index) => {
        if (value._destroy == false) {
          dataLIst.push(
            <div
              style={{
                marginTop: 25
              }}
            >
              <div
                style={{
                  marginBottom: 25
                }}
              >
                <text className={classes.titleMdl}>
                  Detail
                  {' '}
                  {key}
                </text>
                <Link
                  className={classes.deleteRec}
                  onClick={() => {
                    if (key <= 2) {
                      swal({ title: 'Opps!', text: 'Harus Ada Data Detail Minimum 1', icon: 'error', buttons: 'OK' });
                    } else if (index == 0) {
                      const datas = this.state.journal_entries_attributes;
                      const datas2 = this.state.journal_entries_attributes[index];
                      datas2._destroy = true;
                      datas.push(datas2);
                      this.setState({ journal_entries_attributes: datas });
                    } else {
                      const datas = this.state.journal_entries_attributes;
                      const datas2 = this.state.journal_entries_attributes[index];
                      datas2._destroy = true;
                      datas.push(datas2);
                      this.setState({ journal_entries_attributes: datas });
                    }
                  }}
                  color="inherit"
                >
                  Hapus
                </Link>
              </div>
              <Grid container direction="row" item lg={12} xl={12} md={12} xs={12}>
                <Grid container item lg={12} xl={12} md={12} sm={12} xs={12}>
                  <Grid
                    container
                    item
                    lg={12}
                    xl={12}
                    md={12}
                    sm={12}
                    xs={12}
                    style={{
                      marginTop: '20px'
                    }}
                  >
                    <Grid item lg={6} xl={6} md={6} sm={12} xs={12}>
                      <div>
                        <text className={classes.label1}>Akun</text>
                        <text className={classes.starts1}>*</text>
                      </div>
                      <AsyncSelect
                        name="form-field-name-error"
                        placeholder="Cari Akun"
                        value={this.state.value['account_id' + index]}
                        onFocus={() => {
                          this.removeValidate('account_id' + index);
                        }}
                        styles={{
                          control: (provided) => ({
                            ...provided,
                            borderColor: this.state.validator['account_id' + index]
                              ? 'red'
                              : '#CACACA',
                            borderRadius: '0.25rem'
                          })
                        }}
                        onInputChange={(val) => {
                          if (val) {
                            if (this.timeout) clearTimeout(this.timeout);
                            this.timeout = setTimeout(() => {
                              this.getAkun(val, '');
                            }, 500);
                          }
                        }}
                        cacheOptions
                        loadOptions={loadOptions2}
                        defaultOptions
                        className={classes.input2}
                        options={this.state.data5}
                        onChange={(val) => {
                          this.handleChange(val, 'account_id' + index);
                        }}
                      />
                    </Grid>
                    <Grid item lg={6} xl={6} md={6} sm={12} xs={12}>
                      <div>
                        <text className={classes.label1}>Nominal</text>
                        <text className={classes.starts1}>*</text>
                      </div>
                      <TextField
                        size="small"
                        className={classes.input2}
                        fullWidth
                        variant="outlined"
                        autoComplete="off"
                        onFocus={() => {
                          this.removeValidate('amount' + index);
                        }}
                        error={this.state.validator['amount' + index]}
                        helperText={this.state.validator['amount' + index]}
                        value={this.state.value['amount' + index] != undefined
                          ? Func.FormatNumber(this.state.value['amount' + index])
                          : ''}
                        onChange={(event) => {
                          this.removeValidate('amount' + index);
                          this.handleChange(Func.UnFormatRp(event.target.value), 'amount' + index);
                        }}
                        name={'amount' + index}
                      />
                    </Grid>
                  </Grid>
                  <Grid
                    container
                    item
                    lg={12}
                    xl={12}
                    md={12}
                    sm={12}
                    xs={12}
                    style={{
                      marginTop: '20px'
                    }}
                  >
                    <Grid item lg={6} xl={6} md={6} sm={12} xs={12}>
                      <div>
                        <text className={classes.label1}>Jenis Transaksi</text>
                        <text className={classes.starts1}>*</text>
                      </div>
                      <Select2
                        name="form-field-name-error"
                        placeholder="Pilih"
                        value={this.state.value['transaction_type' + index]}
                        onFocus={() => {
                          this.removeValidate('transaction_type' + index);
                        }}
                        error
                        styles={{
                          control: (provided) => ({
                            ...provided,
                            borderColor: this.state.validator['transaction_type' + index]
                              ? 'red'
                              : '#CACACA',
                            borderRadius: '0.25rem'
                          })
                        }}
                        className={classes.input2}
                        options={[
                          {
                            value: '-',
                            label: 'Pilih',
                            isDisabled: true
                          }, {
                            value: 'debit',
                            label: 'Debit'
                          }, {
                            value: 'credit',
                            label: 'Credit '
                          }
                        ]}
                        onChange={(val) => {
                          this.handleChange(val, 'transaction_type' + index);
                        }}
                      />
                      <FormHelperText className={classes.error}>
                        {this.state.validator['transaction_type' + index]}
                      </FormHelperText>
                    </Grid>
                    <Grid item lg={6} xl={6} md={6} sm={12} xs={12}>
                      <div>
                        <text className={classes.label1}>Kode Referal</text>
                        <text className={classes.starts1}>*</text>
                      </div>
                      <TextField
                        size="small"
                        className={classes.input2}
                        fullWidth
                        variant="outlined"
                        autoComplete="off"
                        onFocus={() => {
                          this.removeValidate('ref_code' + index);
                        }}
                        error={this.state.validator['ref_code' + index]}
                        helperText={this.state.validator['ref_code' + index]}
                        value={this.state.value['ref_code' + index]}
                        onChange={(event) => {
                          this.handleChange(event.target.value, 'ref_code' + index);
                        }}
                        name={'ref_code' + index}
                      />
                    </Grid>
                  </Grid>
                  <Grid
                    container
                    item
                    lg={4}
                    xl={4}
                    md={4}
                    sm={4}
                    xs={12}
                    style={{
                      marginTop: '20px'
                    }}
                  >
                    <Grid item lg={12} xl={12} md={12} sm={12} xs={12}>
                      <div>
                        <text className={classes.label1}>Deskripsi</text>
                        <text className={classes.starts1}>*</text>
                      </div>
                      <TextareaAutosize
                        className={this.state.validator['description' + index]
                          ? classes.textArea2
                          : classes.textArea}
                        variant="outlined"
                        margin="normal"
                        rows={3}
                        autoComplete="off"
                        fullWidth
                        onFocus={() => {
                          this.removeValidate('description' + index);
                        }}
                        error={this.state.validator['description' + index]}
                        value={this.state.value['description' + index]}
                        onChange={(event) => {
                          this.handleChange(event.target.value, 'description' + index);
                        }}
                        name={'description' + index}
                        InputProps={{
                          endAdornment: this.state.validator['description' + index]
                            ? (
                              <InputAdornment position="start">
                                <img src={Icon.warning} />
                              </InputAdornment>
                            )
                            : (<div />)
                        }}
                      />
                      <FormHelperText className={classes.error}>
                        {this.state.validator['description' + index]}
                      </FormHelperText>
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
              <Divider className={classes.divider} />
            </div>
          );
          key++;
        }
      });
    return dataLIst;
  }

  render() {
    const loadOptions = (inputValue, callback) => {
      setTimeout(() => {
        callback(this.state.data4);
      }, 1000);
    };

    const { classes } = this.props;
    if (this.state.loader) {
      return (
        <div className={classes.root2}>
          <Dialog
            disablePortal
            disableEnforceFocus
            disableAutoFocus
            open
            scroll="paper"
            maxWidth="md"
            aria-labelledby="server-modal-title"
            aria-describedby="server-modal-description"
            container={() => {}}
          >
            <div
              style={{
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
                padding: '20px',
                borderRadius: '50px',
              }}
            >
              <CircularProgress
                style={{ color: '#85203B', margin: '18px' }}
                size={40}
              />
              Mohon Tunggu
              <div
                style={{
                  marginTop: '10px',
                }}
              />
            </div>
          </Dialog>
        </div>
      );
    }

    return (
      <Dialog
        scroll="paper"
        open
        maxWidth="md"
        aria-labelledby="scroll-dialog-title"
        aria-describedby="scroll-dialog-description"
      >
        <DialogTitle id="scroll-dialog-title">
          <Typography variant="h7" className={classes.tittleModal}>
            {this.props.type + ' Permintaan Kantor Cabang'}
          </Typography>
          <IconButton
            disabled={this.state.loader_button}
            aria-label="close"
            className={classes.closeButton}
            onClick={() => {
              this.props.handleModal();
            }}
          >
            <CloseIcon />
          </IconButton>
        </DialogTitle>
        <DialogContent dividers>
          <DialogContentText id="scroll-dialog-description" tabIndex={-1}>
            <div className={classes.scrool}>
              {Func.toLogin(this.state.redirect)}
              <div className={classes.root}>
                <Grid container direction="row" item lg={12} xl={12} md={12} xs={12}>
                  <Divider className={classes.divider} />
                  <Grid container direction="row" item lg={12} xl={12} md={12} xs={12}>
                    <Grid container item lg={12} xl={12} md={12} sm={12} xs={12}>
                      <Grid
                        container
                        item
                        lg={12}
                        xl={12}
                        md={12}
                        sm={12}
                        xs={12}
                        style={{
                          marginTop: '20px'
                        }}
                      >
                        <Grid item lg={6} xl={6} md={6} sm={12} xs={12}>
                          <div>
                            <text className={classes.label1}>Nama</text>
                            <text className={classes.starts1}>*</text>
                          </div>
                          <TextField
                            size="small"
                            className={classes.input2}
                            fullWidth
                            variant="outlined"
                            autoComplete="off"
                            onFocus={() => {
                              this.removeValidate('name');
                            }}
                            error={this.state.validator.name}
                            helperText={this.state.validator.name}
                            value={this.state.value.name}
                            onChange={(event) => {
                              this.handleChange(event.target.value, 'name');
                            }}
                            name="name"
                          />
                        </Grid>
                        <Grid item lg={6} xl={6} md={6} sm={12} xs={12}>
                          <div>
                            <text className={classes.label1}>Tipe Jurnal</text>
                            <text className={classes.starts1}>*</text>
                          </div>
                          <AsyncSelect
                            name="form-field-name-error"
                            value={this.state.value.journal_type}
                            placeholder="Cari Tipe Jurnal"
                            onFocus={() => {
                              this.removeValidate('journal_type');
                            }}
                            styles={{
                              control: (provided) => ({
                                ...provided,
                                borderColor: this.state.validator.journal_type
                                  ? 'red'
                                  : '#CACACA',
                                borderRadius: '0.25rem'
                              })
                            }}
                            onInputChange={(val) => {
                              if (val) {
                                if (this.timeout) clearTimeout(this.timeout);
                                this.timeout = setTimeout(() => {
                                  this.getType(val, '');
                                }, 500);
                              }
                            }}
                            cacheOptions
                            loadOptions={loadOptions}
                            defaultOptions
                            className={classes.input2}
                            options={this.state.data4}
                            onChange={(val) => {
                              this.handleChange(val, 'journal_type');
                            }}
                          />
                          <FormHelperText className={classes.error}>
                            {this.state.validator.journal_type}
                          </FormHelperText>
                        </Grid>
                      </Grid>
                      <Grid
                        container
                        item
                        lg={4}
                        xl={4}
                        md={4}
                        sm={4}
                        xs={12}
                        style={{
                          marginTop: '20px'
                        }}
                      >
                        <Grid item lg={12} xl={12} md={12} sm={12} xs={12}>
                          <div>
                            <text className={classes.label1}>Deskripsi</text>
                            <text className={classes.starts1}>*</text>
                          </div>
                          <TextareaAutosize
                            className={this.state.validator.description
                              ? classes.textArea2
                              : classes.textArea}
                            variant="outlined"
                            margin="normal"
                            rows={3}
                            autoComplete="off"
                            fullWidth
                            onFocus={() => {
                              this.removeValidate('description');
                            }}
                            error={this.state.validator.description}
                            value={this.state.value.description}
                            onChange={(event) => {
                              this.handleChange(event.target.value, 'description');
                            }}
                            name="description"
                            InputProps={{
                              endAdornment: this.state.validator.description
                                ? (
                                  <InputAdornment position="start">
                                    <img src={Icon.warning} />
                                  </InputAdornment>
                                )
                                : (<div />)
                            }}
                          />
                          <FormHelperText className={classes.error}>
                            {this.state.validator.description}
                          </FormHelperText>
                        </Grid>
                      </Grid>
                    </Grid>
                  </Grid>
                  <Divider className={classes.divider} />
                  {' '}
                  {this.renderChild()}
                  <button
                    style={{
                      backgroundColor: '#C4A443',
                      borderRadius: 50,
                      color: 'white',
                      width: 150,
                      height: 35,
                      marginLeft: 10,
                      marginTop: 5,
                      marginBottom: 15,
                      fontWeight: '500',
                      fontSize: 14
                    }}
                    onClick={() => {
                      this.addChild();
                    }}
                  >
                    Tambah Detail
                  </button>
                </Grid>
              </div>
            </div>
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button
            style={{
              backgroundColor: '#862C3A',
              color: 'white',
            }}
            disabled={this.state.loader_button}
            onClick={() => {
              this.handleSubmit(this.props.type);
            }}
            variant="contained"
          >
            <Typography variant="button" style={{ color: '#FFFFFF' }}>
              {this.state.loader_button ? (
                <CircularProgress
                  style={{
                    color: 'white',
                    marginLeft: '18px',
                    marginRight: '18px',
                    marginTop: 5,
                  }}
                  size={15}
                />
              ) : (
                'Simpan'
              )}
            </Typography>
          </Button>
          <Button
            style={{
              color: 'black',
            }}
            disabled={this.state.loader_button}
            onClick={() => {
              this.props.handleModal();
            }}
            variant="outlined"
          >
            <Typography variant="button">Batal</Typography>
          </Button>
        </DialogActions>
      </Dialog>
    );
  }
}

export default withStyles(styles.CoustomsStyles, { name: 'Form' })(Form);
