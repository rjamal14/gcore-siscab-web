/* eslint-disable camelcase */
/* eslint-disable no-trailing-spaces */
import Loadable from 'react-loadable';
import { Loader as Loading } from '../../components/Loader';

// Dashboard

export const DashboardPage = Loadable({
  loader: () => import('./Dashboard'),
  loading: Loading
});

// Data Nasabah

export const CustomerIndividual = Loadable({
  loader: () => import('./Customers/Individual'),
  loading: Loading
});

// Manajemen Transaksi

export const TransactionIndividual = Loadable({
  loader: () => import('./Transactions/Individual'),
  loading: Loading
});

export const DeviationApproval = Loadable({
  loader: () => import('./Transactions/Deviation'),
  loading: Loading
});

// Finansial Manajemen 

export const TransactionalReports = Loadable({
  loader: () => import('./FinancialManagement/TransactionalReports'),
  loading: Loading
});

export const Request = Loadable({
  loader: () => import('./FinancialManagement/Request'),
  loading: Loading
});

export const MiscellaneousExpense = Loadable({
  loader: () => import('./FinancialManagement/MiscellaneousExpense'),
  loading: Loading
});

// Jurnal

export const Journal_All = Loadable({
  loader: () => import('./Journal/All'),
  loading: Loading
});

export const Journal_Disbursement = Loadable({
  loader: () => import('./Journal/Disbursement'),
  loading: Loading
});

export const Journal_Repayment = Loadable({
  loader: () => import('./Journal/Repayment'),
  loading: Loading
});

export const Journal_Extension = Loadable({
  loader: () => import('./Journal/Extension'),
  loading: Loading
});

export const Journal_nonTransactional = Loadable({
  loader: () => import('./Journal/nonTransactional'),
  loading: Loading
});

export const EntryJournalForm = Loadable({
  loader: () => import('./Journal/components/EntryJournalForm'),
  loading: Loading
});

// Laporan Transaksi

export const TranscationReport = Loadable({
  loader: () => import('./Report/All'),
  loading: Loading
});

export const TranscationReport_disbursement = Loadable({
  loader: () => import('./Report/Disbursement'),
  loading: Loading
});

export const TranscationReport_prolongation = Loadable({
  loader: () => import('./Report/Prolongation'),
  loading: Loading
});

export const TranscationReport_repayment = Loadable({
  loader: () => import('./Report/Repayment'),
  loading: Loading
});

export const TranscationReport_outstanding = Loadable({
  loader: () => import('./Report/Outstanding'),
  loading: Loading
});

// Manajemen Kas

export const Cashflow = Loadable({
  loader: () => import('./Cashflow/Cashflow'),
  loading: Loading
});

export const CashDisbursements = Loadable({
  loader: () => import('./Cashflow/CashDisbursements'),
  loading: Loading
});

export const CashJournal = Loadable({
  loader: () => import('./Cashflow/CashJournal'),
  loading: Loading
});

// Modal Kerja

export const MokerHistory = Loadable({
  loader: () => import('./Workcap/Moker'),
  loading: Loading
});

export const WorkcapApproval = Loadable({
  loader: () => import('./Workcap/Approval'),
  loading: Loading
});

// Setting

export const Profile = Loadable({
  loader: () => import('./Setting/Profile'),
  loading: Loading
});

export const ChangePassword = Loadable({
  loader: () => import('./Setting/ChangePassword'),
  loading: Loading
});

// etc

export const accessDened = Loadable({
  loader: () => import('../../routes/accessDened'),
  loading: Loading
});

export const AllNotification = Loadable({
  loader: () => import('./Notification'),
  loading: Loading
});
