/* eslint-disable array-callback-return */
/* eslint-disable eqeqeq */
/* eslint-disable react/no-unused-state */
/* eslint-disable react/no-access-state-in-setstate */
/* eslint-disable react/no-deprecated */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/sort-comp */
/* eslint-disable jsx-a11y/alt-text */
import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Dialog from '@material-ui/core/Dialog';
import { CircularProgress } from '@material-ui/core';
import Func from '../../../../../functions/index';
import styles from '../css';
import Field from './field';
import env from '../../../../../config/env';

// eslint-disable-next-line no-unused-vars
function TabPanel(props) {
  const {
    children,
    value,
    index,
    ...other
  } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`full-width-tabpanel-${index}`}
      aria-labelledby={`full-width-tab-${index}`}
      {...other}
    >
      {value === index && children}
    </div>
  );
}

class Form extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: false,
      validator: [],
      value: [],
      type: '',
      activeTabs: 0,
      Proses: 0,
      count: 0,
      id_cust: null,
      state: 1,
      loader: false
    };
  }

  onPage(type) {
    let val = '';
    if (type === '-') {
      val = this.state.activeTabs - 1;
    } else {
      val = this.state.activeTabs + 1;
    }
    this.setState({ activeTabs: val, Proses: val });
  }

  componentWillReceiveProps() {
    if (this.props.row !== 'undefined' && this.props.modal) {
      this.setState({ modal: this.props.modal, type: this.props.type });
    }
    if (this.props.type === 'Ubah' && this.props.row !== undefined && !this.state.loader && this.state.value.identity_number === undefined) {
      this.setState({ id_cust: this.props.row.id });
      this.getData();
    }
  }

  removeValidate(name) {
    const data = this.state.validator;
    delete data[name];
    this.setState({ validator: data });
  }

  handleModal() {
    this.setState({
      modal: false,
      value: [],
      loader: false
    });
  }

  handleChange(event, name) {
    const dataSet = this.state.value;
    dataSet[name] = event.target.value;
    this.setState({ value: dataSet });
  }

  handleSubmit() {
    if (this.state.activeTabs === 6) {
      this.setState({
        modal: !this.state.modal,
        activeTabs: 0
      });
    } else {
      this.setState({ Proses: this.state.activeTabs });
    }
  }

  getData() {
    this.setState({ loader: true });
    fetch(
      env.managementApi + env.apiPrefixV1 + '/customer/' + this.props.row.id,
      {
        method: 'GET',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + localStorage.getItem('token'),
        },
      }
    )
      .then((response) => response.json())
      .then((json) => {
        if (json.code === '403') {
          if (Func.Clear_Token() === true) {
            if (!localStorage.getItem('token')) {
              this.setState({ redirect: true });
            }
          }
        }
        const val = [];
        const arrSalary = [
          'Kurang dari Rp 3.000.000',
          'Diatas dari Rp 3.000.000',
          'Diatas dari Rp 6.000.000',
        ];
        val.name = json.customer_detail.name;
        val.birth_place = json.customer_detail.birth_place;
        val.birth_date2 = new Date(json.customer_detail.birth_date);
        val.birth_date = json.customer_detail.birth_date;
        val.identity_type = json.customer_detail.identity_type;
        val.identity_number = json.customer_detail.identity_number;
        const marital = json.customer_detail.marital_status;
        val.marital_status = {
          value: marital,
          label: (marital === 'single') ? 'Belum Kawin' : (marital === 'married') ? 'Kawin' : 'Cerai'
        };
        val.degree = {
          value: json.customer_detail.degree,
          label: json.customer_detail.degree,
        };
        val.gender = {
          value: json.customer_detail.gender,
          label:
            json.customer_detail.gender === 'l' ? 'Laki Laki' : 'Perempuan',
        };
        val.expired_date = json.customer_detail.expired_date;
        val.expired_date2 = new Date(json.customer_detail.expired_date);
        val.email = json.customer_detail.email;

        val.img = json.customer_detail.id_scan_image.url;

        val.residence_province = {
          value:
            json.customer_detail.customer_contact_data.residence_province,
          label:
            json.customer_detail.customer_contact_data
              .residence_province_name,
        };
        val.residence_city = {
          value: json.customer_detail.customer_contact_data.residence_city,
          label:
            json.customer_detail.customer_contact_data.residence_city_name,
        };
        val.residence_region = {
          value: json.customer_detail.customer_contact_data.residence_region,
          label:
            json.customer_detail.customer_contact_data.residence_region_name,
        };
        val.residence_urbans = {
          value:
            json.customer_detail.customer_contact_data.identity_subdistric,
          label:
            json.customer_detail.customer_contact_data
              .identity_subdistric_name,
        };
        val.residence_postal_code = json.customer_detail.customer_contact_data.residence_postal_code;
        val.residence_address = json.customer_detail.customer_contact_data.residence_address;

        val.identity_province = {
          value: json.customer_detail.customer_contact_data.identity_province,
          label:
            json.customer_detail.customer_contact_data.identity_province_name,
        };
        val.identity_city = {
          value: json.customer_detail.customer_contact_data.identity_city,
          label:
            json.customer_detail.customer_contact_data.identity_city_name,
        };
        val.identity_region = {
          value: json.customer_detail.customer_contact_data.identity_region,
          label:
            json.customer_detail.customer_contact_data.identity_region_name,
        };
        val.identity_urbans = {
          value:
            json.customer_detail.customer_contact_data.identity_subdistric,
          label:
            json.customer_detail.customer_contact_data
              .identity_subdistric_name,
        };
        val.identity_postal_code = json.customer_detail.customer_contact_data.identity_postal_code;
        val.identity_address = json.customer_detail.customer_contact_data.identity_address;

        val.phone_number = json.customer_detail.customer_contact_data.phone_number;
        val.telephone_number = json.customer_detail.customer_contact_data.telephone_number;

        val.job_type = {
          value: json.customer_detail.customer_job.job_type,
          label: json.customer_detail.customer_job.job_type,
        };
        val.income_source = {
          value: json.customer_detail.customer_job.income_source,
          label: json.customer_detail.customer_job.income_source,
        };
        val.company_name = json.customer_detail.customer_job.company_name;
        val.financing_purpose = {
          value: json.customer_detail.customer_job.financing_purpose,
          label: json.customer_detail.customer_job.financing_purpose,
        };
        val.tax_number = json.customer_detail.customer_job.tax_number;
        val.mother_name = json.customer_detail.mother_name;
        val.salary_amount = {
          value: json.customer_detail.customer_job.salary_amount,
          label:
            arrSalary[json.customer_detail.customer_job.salary_amount - 1],
        };

        json.customer_detail.customer_bank_accounts.map((value, index) => {
          if (index != 0) {
            this.addRekening();
          }
          val['id_Rek' + index] = value.id;
          val['account_number' + index] = value.account_number;
          val['account_name' + index] = value.account_name;
          val['bank_name' + index] = {
            value: value.bank_name,
            label: value.bank_name,
          };
          val['branch_name' + index] = value.branch_name;

          if (value.main_account) {
            this.setState({ rekeningActive: index });
          }
        });

        json.customer_detail.customer_emergency_contacts.map(
          (value, index) => {
            if (index != 0) {
              this.addContact();
            }
            val['emergency_id' + index] = value.id;
            val['emergency_name' + index] = value.name;
            val['emergency_relation' + index] = {
              value: value.relation,
              label: value.relation,
            };
            val['emergency_job_name' + index] = {
              value: value.job_name,
              label: value.job_name,
            };
            val['emergency_nationality' + index] = {
              value: value.nationality,
              label: value.nationality,
            };
            val['emergency_address' + index] = value.address;
            val['emergency_phone_number' + index] = value.phone_number == undefined ? '' : value.phone_number;
            val['emergency_birth_date' + index] = value.birth_date == undefined ? '' : value.birth_date;
            val['emergency_birth_place' + index] = value.birth_place == undefined ? '' : value.birth_place;
          }
        );
        val.img = json.customer_detail.id_scan_image.url;
        this.setState({
          value: val,
        });
        this.setState({ loader: false, modal: true });
      })
      .catch(() => {
      })
      .finally(() => {});
  }

  componentDidMount() {
    if (this.props.type === 'Ubah' && this.props.row !== undefined) {
      this.getData();
    }
  }

  render() {
    const { classes } = this.props;
    if (this.state.loader) {
      return (
        <div className={classes.root2}>
          <Dialog
            disablePortal
            disableEnforceFocus
            disableAutoFocus
            open
            scroll="paper"
            maxWidth="md"
            aria-labelledby="server-modal-title"
            aria-describedby="server-modal-description"
            container={() => {}}
          >
            <div
              style={{
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
                padding: '20px',
                borderRadius: '50px',
              }}
            >
              <CircularProgress style={{ color: '#85203B', margin: '18px' }} size={40} />
              Mohon Tunggu
              <div style={{
                marginTop: '10px',
              }}
              />
            </div>
          </Dialog>
        </div>
      );
    } if (this.state.modal) {
      return (
        <Field
          Proses={this.state.Proses}
          value={this.state.value}
          type={this.state.type}
          id_cust={this.state.id_cust}
          state={this.state.state}
          count={this.state.count}
          handleModal={() => {
            this.handleModal();
          }}
          OnNext={(res) => {
            if (res === '2') {
              this.setState({ state: 2 });
            } else if (res === '1') {
              this.setState({ state: 1 });
            } else {
              this.setState({ modal: false });
              Func.AlertForm('Berhasil', res, 'success');
            }
          }}
        />
      );
    }
    return <div />;
  }
}

export default withStyles(styles.CoustomsStyles, { name: 'Form' })(Form);
