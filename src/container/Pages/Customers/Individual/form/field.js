/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable react/jsx-no-duplicate-props */
/* eslint-disable react/jsx-indent */
/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable no-plusplus */
/* eslint-disable no-empty */
/* eslint-disable consistent-return */
/* eslint-disable no-shadow */
/* eslint-disable no-redeclare */
/* eslint-disable array-callback-return */
/* eslint-disable radix */
/* eslint-disable vars-on-top */
/* eslint-disable no-var */
/* eslint-disable block-scoped-var */
/* eslint-disable eqeqeq */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/no-access-state-in-setstate */
/* eslint-disable react/sort-comp */
/* eslint-disable react/no-unused-state */
import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import InputAdornment from '@material-ui/core/InputAdornment';
import Divider from '@material-ui/core/Divider';
import Typography from '@material-ui/core/Typography';
import swal from 'sweetalert';
import FormHelperText from '@material-ui/core/FormHelperText';
import Grid from '@material-ui/core/Grid';
import TextareaAutosize from '@material-ui/core/TextareaAutosize';
import DialogContentText from '@material-ui/core/DialogContentText';
import MuiDialogActions from '@material-ui/core/DialogActions';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Link from '@material-ui/core/Link';
import Button from '@material-ui/core/Button';
import Box from '@material-ui/core/Box';
import Select2 from 'react-select';
import AsyncSelect from 'react-select/async';
import { CircularProgress } from '@material-ui/core';
import InputMask from 'react-input-mask';
import Func from '../../../../../functions/index';
import Icon from '../../../../../components/icon';
import styles from '../css';
import env from '../../../../../config/env';
import '../../../../../styles/date_field.css';
import service from '../../../../../functions/service';

const DialogActions = withStyles((theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(1),
  },
}))(MuiDialogActions);

class Form extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      modal: false,
      validator: [],
      value: { email: '', identity_number: '' },
      type: '',
      activeTabs: 0,
      Proses: false,
      ImgBase44: '',
      section: 0,
      id_cust: null,
      redirect: false,
      date: new Date(),
      cities: [],
      same: false,
      cities2: [],
      province: [],
      rekeningActive: 0,
      rekeningN0: 1,
      contactN0: 1,
      loader: false,
      form: 1,
      contact: [
        {
          emergency_id: 0,
          emergency_name: '',
          emergency_relation: '',
          emergency_job_name: '',
          emergency_nationality: '',
          emergency_address: '',
          _destroy: false,
        },
      ],
      rekening: [
        {
          id_Rek: 0,
          norek: '',
          rek_name: '',
          bank_name: '',
          bank_branch: '',
          _destroy: false,
        },
      ],
      job: [],
    };
    this.handleChangeImg = this.handleChangeImg.bind(this);
    this.getJob();
    this.timeout = 0;
  }

  getJob() {
    service
      .getJob()
      .then((res) => res.json())
      .then((resjson) => {
        if (resjson.code === 200) this.setState({ job: resjson.data });
      });
  }

  removeValidate(name) {
    const data = this.state.validator;
    delete data[name];
    this.setState({ validator: data });
  }

  handleChange(event, name) {
    const dataSet = this.state.value;

    if (name == 'identity_type') {
      dataSet.identity_number = '';
    }
    if (name == 'identity_province') {
      this.getIdentityCity('', event.id);
      dataSet.identity_city = '-';
      dataSet.identity_region = '';
      dataSet.identity_urbans = '';
      dataSet.identity_postal_code = '';
      dataSet.identity_address = '';
    }
    if (name == 'residence_province') {
      this.getResidenceCity('', event.id);
      dataSet.residence_city = '';
      dataSet.residence_region = '';
      dataSet.residence_urbans = '';
      dataSet.residence_postal_code = '';
      dataSet.residence_address = '';
    }

    if (name == 'identity_city') {
      this.getIdentityRegion('', event.id);
      dataSet.identity_region = '';
      dataSet.identity_urbans = '';
      dataSet.identity_postal_code = '';
      dataSet.identity_address = '';
    }
    if (name == 'residence_city') {
      this.getResidenceRegion('', event.id);
      dataSet.residence_region = '';
      dataSet.residence_urbans = '';
      dataSet.residence_postal_code = '';
      dataSet.residence_address = '';
    }
    if (name == 'identity_region') {
      this.getIdentityUrbans('', event.id);
      dataSet.identity_urbans = '';
      dataSet.identity_postal_code = '';
      dataSet.identity_address = '';
    }
    if (name == 'residence_region') {
      this.getResidenceUrbans('', event.id);
      dataSet.residence_urbans = '';
      dataSet.residence_postal_code = '';
      dataSet.residence_address = '';
    }
    if (name == 'identity_urbans') {
      dataSet.identity_postal_code = event.postal_code;
    }
    if (name == 'residence_urbans') {
      dataSet.residence_postal_code = event.postal_code;
    }

    dataSet[name] = event;
    this.setState({ value: dataSet });
  }

  handleChangeDate(date, name, name2) {
    const dt = new Date(date);
    const dataSet = this.state.value;
    dataSet[name2] = dt;
    dataSet[name] = dt.getFullYear() + '-' + parseInt(dt.getMonth() + 1) + '-' + dt.getDate();
    this.setState({ value: dataSet });
  }

  addRekening() {
    const data = this.state.rekening;
    data.push({
      id_Rek: this.state.rekeningN0,
      norek: '',
      rek_name: '',
      bank_name: '',
      bank_branch: '',
      _destroy: false,
    });
    this.setState({
      rekening: data,
      rekeningN0: this.state.rekeningN0 + 1,
    });
  }

  addContact() {
    const data = this.state.contact;
    data.push({
      emergency_id: this.state.contactN0,
      emergency_name: '',
      emergency_relation: '',
      emergency_job_name: '',
      emergency_nationality: '',
      emergency_address: '',
      _destroy: false,
    });
    this.setState({
      contact: data,
      contactN0: this.state.contactN0 + 1,
    });
  }

  getBank = (val, id, name) => {
    this.setState({ isLoading: true });
    fetch(
      process.env.REACT_APP_URL_MASTER
      + process.env.REACT_APP_API_PREFIX_V1
      + '/banks/autocomplete?query='
      + val,
      {
        method: 'GET',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + localStorage.getItem('token'),
        },
      }
    )
      .then((response) => response.json())
      .then((json) => {
        this.setState({ isLoading: false });
        if (json.code == '403') {
          if (Func.Clear_Token() == true) {
            if (!localStorage.getItem('token')) {
              this.setState({ redirect: true });
            }
          }
        }
        const datas = [];
        datas.push({
          value: '-',
          label: json.data.length > 0 ? 'Pilih' : 'Tidak ditemukan',
          isDisabled: true,
        });
        json.data.map((value) => {
          datas.push({ value: value.name, label: value.name });
        });
        if (id != '') {
          const search = json.data.find((o) => o._id.$oid === id);
          if (search == undefined) {
            datas.push({ value: name, label: name });
          }
        }

        this.setState({ data4: datas, data4_ori: json.data });
      })
      .catch(() => {
        this.setState({ isLoading: false });
      })
      .finally(() => { });
  };

  componentDidMount() {
    this.getBank('', '', '');
    this.getProvince('', '');
    if (this.props.type == 'Ubah') {
      this.setState({ value: this.props.value });
    }
  }

  getIdentityUrbans(val, id) {
    fetch(
      env.masterApi
      + env.apiPrefixV1
      + '/urbans/autocomplete?district_id='
      + id
      + '&query='
      + val,
      {
        method: 'GET',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + localStorage.getItem('token'),
        },
      }
    )
      .then((response) => response.json())
      .then((json) => {
        if (json.code == '403') {
          if (Func.Clear_Token() == true) {
            if (!localStorage.getItem('token')) {
              this.setState({ redirect: true });
            }
          }
        }
        const datas = [];
        datas.push({
          value: '-',
          label: json.data.length > 0 ? 'Pilih' : 'Tidak ditemukan',
          isDisabled: true,
        });
        json.data.map((value) => {
          datas.push({
            postal_code: value.postal_code,
            label: value.name,
            value: value._id.$oid,
          });
        });
        this.setState({ urbans: datas });
      })
      .catch(() => { })
      .finally(() => { });
  }

  getResidenceUrbans(val, id) {
    fetch(
      env.masterApi
      + env.apiPrefixV1
      + '/urbans/autocomplete?district_id='
      + id
      + '&query='
      + val,
      {
        method: 'GET',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + localStorage.getItem('token'),
        },
      }
    )
      .then((response) => response.json())
      .then((json) => {
        if (json.code == '403') {
          if (Func.Clear_Token() == true) {
            if (!localStorage.getItem('token')) {
              this.setState({ redirect: true });
            }
          }
        }
        const datas = [];
        datas.push({
          value: '-',
          label: json.data.length > 0 ? 'Pilih' : 'Tidak ditemukan',
          isDisabled: true,
        });
        json.data.map((value) => {
          datas.push({
            postal_code: value.postal_code,
            label: value.name,
            value: value._id.$oid,
          });
        });
        this.setState({ urbans2: datas });
      })
      .catch(() => { })
      .finally(() => { });
  }

  getIdentityRegion(val, id) {
    fetch(
      env.masterApi
      + env.apiPrefixV1
      + '/districts/autocomplete?city_id='
      + id
      + '&query='
      + val,
      {
        method: 'GET',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + localStorage.getItem('token'),
        },
      }
    )
      .then((response) => response.json())
      .then((json) => {
        if (json.code == '403') {
          if (Func.Clear_Token() == true) {
            if (!localStorage.getItem('token')) {
              this.setState({ redirect: true });
            }
          }
        }
        const datas = [];
        datas.push({
          value: '-',
          label: json.data.length > 0 ? 'Pilih' : 'Tidak ditemukan',
          isDisabled: true,
        });
        json.data.map((value) => {
          datas.push({
            id: value.district_code,
            label: value.name,
            value: value._id.$oid,
          });
        });
        this.setState({ regional: datas });
      })
      .catch(() => { })
      .finally(() => { });
  }

  getResidenceRegion(val, id) {
    fetch(
      env.masterApi
      + env.apiPrefixV1
      + '/districts/autocomplete?city_id='
      + id
      + '&query='
      + val,
      {
        method: 'GET',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + localStorage.getItem('token'),
        },
      }
    )
      .then((response) => response.json())
      .then((json) => {
        if (json.code == '403') {
          if (Func.Clear_Token() == true) {
            if (!localStorage.getItem('token')) {
              this.setState({ redirect: true });
            }
          }
        }
        const datas = [];
        datas.push({
          value: '-',
          label: json.data.length > 0 ? 'Pilih' : 'Tidak ditemukan',
          isDisabled: true,
        });
        json.data.map((value) => {
          datas.push({
            id: value.district_code,
            label: value.name,
            value: value._id.$oid,
          });
        });
        this.setState({ regional2: datas });
      })
      .catch(() => { })
      .finally(() => { });
  }

  getIdentityCity(val, id) {
    fetch(
      env.masterApi
      + env.apiPrefixV1
      + '/cities/autocomplete?province_id='
      + id
      + '&query='
      + val,
      {
        method: 'GET',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + localStorage.getItem('token'),
        },
      }
    )
      .then((response) => response.json())
      .then((json) => {
        if (json.code == '403') {
          if (Func.Clear_Token() == true) {
            if (!localStorage.getItem('token')) {
              this.setState({ redirect: true });
            }
          }
        }
        const datas = [];
        datas.push({
          value: '-',
          label: json.data.length > 0 ? 'Pilih' : 'Tidak ditemukan',
          isDisabled: true,
        });
        json.data.map((value) => {
          datas.push({
            id: value.city_code,
            label: value.name,
            value: value._id.$oid,
          });
        });
        this.setState({ cities: datas });
      })
      .catch(() => { })
      .finally(() => { });
  }

  getResidenceCity(val, id) {
    fetch(
      env.masterApi
      + env.apiPrefixV1
      + '/cities/autocomplete?province_id='
      + id
      + '&query='
      + val,
      {
        method: 'GET',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + localStorage.getItem('token'),
        },
      }
    )
      .then((response) => response.json())
      .then((json) => {
        if (json.code == '403') {
          if (Func.Clear_Token() == true) {
            if (!localStorage.getItem('token')) {
              this.setState({ redirect: true });
            }
          }
        }
        const datas = [];
        datas.push({
          value: '-',
          label: json.data.length > 0 ? 'Pilih' : 'Tidak ditemukan',
          isDisabled: true,
        });
        json.data.map((value) => {
          datas.push({
            id: value.city_code,
            label: value.name,
            value: value._id.$oid,
          });
        });
        this.setState({ cities2: datas });
      })
      .catch(() => { })
      .finally(() => { });
  }

  getProvince(val, name) {
    fetch(
      env.masterApi + env.apiPrefixV1 + '/provinces/autocomplete?query=' + val,
      {
        method: 'GET',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + localStorage.getItem('token'),
        },
      }
    )
      .then((response) => response.json())
      .then((json) => {
        if (json.code == '403') {
          if (Func.Clear_Token() == true) {
            if (!localStorage.getItem('token')) {
              this.setState({ redirect: true });
            }
          }
        } else {
          const datas = [];
          datas.push({
            value: '-',
            label: json.data.length > 0 ? 'Pilih' : 'Tidak ditemukan',
            isDisabled: true,
          });

          if (name != '') {
            const search = json.data.find((o) => o.name === name);
            if (search == undefined) {
              datas.push({ value: name, label: name });
            }
          }
          json.data.map((value) => {
            datas.push({
              id: value.province_code,
              label: value.name,
              value: value._id.$oid,
            });
          });

          this.setState({
            province: datas,
            province_ori: json.data,
          });
        }
      })
      .catch(() => { })
      .finally(() => { });
  }

  handleSubmit(type) {
    this.setState({ loader: true });
    if (this.state.form == 1) {
      var validator = [
        {
          name: 'name',
          type: 'required',
        },
        {
          name: 'identity_number',
          type:
            this.state.value.identity_type == undefined
              ? 'required|min:16'
              : this.state.value.identity_type == 'ktp'
                ? 'required|min:16'
                : 'required|min:8',
        },
        {
          name: 'birth_place',
          type: 'required',
        },
        {
          name: 'birth_date',
          type: 'required',
        },
        {
          name: 'degree',
          type: 'required',
        },
        {
          name: 'gender',
          type: 'required',
        },
        {
          name: 'identity_province',
          type: 'required',
        },
        {
          name: 'identity_city',
          type: 'required',
        },
        {
          name: 'identity_region',
          type: 'required',
        },
        {
          name: 'identity_urbans',
          type: 'required',
        },
        {
          name: 'identity_postal_code',
          type: 'required',
        },
        {
          name: 'identity_address',
          type: 'required',
        },
        {
          name: 'residence_province',
          type: 'required',
        },
        {
          name: 'residence_city',
          type: 'required',
        },
        {
          name: 'residence_region',
          type: 'required',
        },
        {
          name: 'residence_urbans',
          type: 'required',
        },
        {
          name: 'residence_postal_code',
          type: 'required',
        },
        {
          name: 'residence_address',
          type: 'required',
        },
        {
          name: 'phone_number',
          type: 'required',
        },
        {
          name: 'income_source',
          type: 'required',
        },
        {
          name: 'job_type',
          type: 'required',
        },
        {
          name: 'financing_purpose',
          type: 'required',
        },
        {
          name: 'mother_name',
          type: 'required',
        },
        {
          name: 'salary_amount',
          type: 'required',
        },
        {
          name: 'img',
          type: 'required',
        },
        {
          name: 'marital_status',
          type: 'required',
        },
      ];
    } else {
      var validator = [];
      this.state.contact.map((value, index) => {
        if (!value._destroy) {
          validator.push({
            name: 'emergency_name' + index,
            type: 'required',
          });
          validator.push({
            name: 'emergency_job_name' + index,
            type: 'required',
          });
          validator.push({
            name: 'emergency_relation' + index,
            type: 'required',
          });
          validator.push({
            name: 'emergency_nationality' + index,
            type: 'required',
          });
          validator.push({
            name: 'emergency_address' + index,
            type: 'required',
          });
          validator.push({
            name: 'emergency_phone_number' + index,
            type: 'required',
          });
          if (this.state.value['emergency_birth_place' + index]) {
            validator.push({
              name: 'emergency_birth_date' + index,
              type: 'required',
            });
          }
          if (this.state.value['emergency_birth_date' + index]) {
            validator.push({
              name: 'emergency_birth_place' + index,
              type: 'required',
            });
          }
        }
      });
    }
    const validate = Func.Validator(this.state.value, validator);
    let payload;
    if (validate.success) {
      if (this.state.form == 1) {
        payload = {
          email: this.state.value.email,
          branch_office_id: '1231294',
          branch_office_name: 'Bandung',
          name: this.state.value.name,
          gender: this.state.value.gender.value,
          identity_number: this.state.value.identity_number,
          identity_type:
            this.state.value.identity_type == undefined
              ? 'ktp'
              : this.state.value.identity_type,
          expired_date: this.state.value.expired_date,
          birth_date: this.state.value.birth_date,
          birth_place: this.state.value.birth_place,
          marital_status: this.state.value.marital_status.value,
          degree: this.state.value.degree.value,
          mother_name: this.state.value.mother_name,
          customer_contact_attributes: {
            residence_address: this.state.value.residence_address,
            residence_province: this.state.value.residence_province.value,
            residence_city: this.state.value.residence_city.value,
            residence_region: this.state.value.residence_region.value,
            residence_subdistric: this.state.value.residence_urbans.value,
            residence_postal_code: this.state.value.residence_postal_code,
            identity_address: this.state.value.identity_address,
            identity_province: this.state.value.identity_province.value,
            identity_city: this.state.value.identity_city.value,
            identity_region: this.state.value.identity_region.value,
            identity_subdistric: this.state.value.identity_urbans.value,
            identity_postal_code: this.state.value.identity_postal_code,
            phone_number: this.state.value.phone_number,
            telephone_number: this.state.value.telephone_number,
          },
          customer_job_attributes: {
            job_type: this.state.value.job_type.value,
            company_name: this.state.value.company_name,
            tax_number: this.state.value.tax_number,
            customer_type: 'Perorangan',
            disbursement_type: 'Tunai',
            income_source: this.state.value.income_source.value,
            financing_purpose: this.state.value.financing_purpose.value,
            salary_amount: this.state.value.salary_amount.value,
          },
          customer_notes_attributes: [
            {
              subject: '-',
              note_description: '-',
            },
          ],
          id_scan_image: this.state.ImgBase44,
        };
      } else {
        const Emergency = [];
        this.state.contact.map((value, index) => {
          if (value._destroy) {
            if (this.state.value['emergency_id' + index] != undefined) {
              var itemEmergency = {
                id: this.state.value['emergency_id' + index],
                nationality: this.state.value['emergency_nationality' + index]
                  .value,
                address: this.state.value['emergency_address' + index],
                _destroy: value._destroy,
              };
            }
          } else {
            var itemEmergency = {
              id: this.state.value['emergency_id' + index],
              name: this.state.value['emergency_name' + index],
              relation: this.state.value['emergency_relation' + index].value,
              job_name: this.state.value['emergency_job_name' + index].value,
              nationality: this.state.value['emergency_nationality' + index]
                .value,
              address: this.state.value['emergency_address' + index],
              phone_number: this.state.value['emergency_phone_number' + index],
              birth_date: this.state.value['emergency_birth_date' + index],
              birth_place: this.state.value['emergency_birth_place' + index],
              _destroy: value._destroy,
            };
          }

          Emergency.push(itemEmergency);
        });

        payload = {
          customer_emergency_contacts_attributes: Emergency,
        };
      }
      const { id_cust: idProps } = this.props;
      const { id_cust: idState, form } = this.state;
      fetch(
        env.managementApi
        + env.apiPrefixV1
        + '/customer/'
        + ((type == 'Tambah' && form === 1) ? '' : (idState || idProps)),
        {
          method: (type == 'Tambah' && form === 1) ? 'POST' : 'PUT',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: 'Bearer ' + localStorage.getItem('token'),
          },
          body: JSON.stringify({
            customer: payload,
          }),
        }
      )
        .then((response) => response.json())
        .then((json) => {
          if (json.code == '403') {
            Func.Refresh_Token();
            if (Func.Refresh_Token() == true) {
            }
          }
          if (type == 'Tambah') {
            if (json.created) {
              this.setState({ loader: false, form: 2, id_cust: json.data.id });
            } else if (json.code == 200) {
              this.props.OnNext(json.message);
              this.setState({ loader: false });
            } else {
              Func.AlertError(json.message);
              var validate = [];
              if (json.status.identity_number != undefined) {
                validate.identity_number = 'ID Nomor KTP ' + json.status.identity_number[0];
              }
              if (json.status['customer_contact.phone_number'] != undefined) {
                validate.phone_number = 'Nomor Hp ' + json.status['customer_contact.phone_number'][0];
              }
              if (json.status.email != undefined) {
                validate.email = 'Email ' + json.status.email[0];
              }
              this.setState({ validator: validate });
              this.setState({ loader: false });
            }
          } else if (json.code == 200) {
            if (form === 1) {
              this.setState({ loader: false, form: 2, id_cust: json.data.id });
            } else {
              this.props.OnNext(json.message);
              this.setState({ loader: false });
            }
          } else {
            Func.AlertError(json.message);
            this.setState({ loader: false });
            var validate = [];
            if (json.status.identity_number != undefined) {
              validate.identity_number = 'ID Nomor KTP ' + json.status.identity_number[0];
            }
            if (json.status['customer_contact.phone_number'] != undefined) {
              validate.phone_number = 'Nomor Hp ' + json.status['customer_contact.phone_number'][0];
            }

            if (json.status.email != undefined) {
              validate.email = 'Email ' + json.status.email[0];
            }

            this.setState({ validator: validate });
          }
        })
        .catch(() => { })
        .finally(() => { });
    } else {
      this.setState({ validator: validate.error });
      this.setState({ loader: false });
    }
  }

  handleChangeImg(event) {
    const file = event.target.files[0];
    if (file.size > 2.9e6) {
      swal({
        title: 'File Terlalu Besar',
        text: 'Maximal File 2Mb',
        icon: 'error',
        buttons: 'OK',
      });
      return false;
    }
    this.removeValidate('img');
    const dataSet = this.state.value;
    dataSet.img = URL.createObjectURL(event.target.files[0]);
    this.setState({ value: dataSet });
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onloadend = () => {
      this.setState({ ImgBase44: reader.result });
    };
  }

  renderListRekening() {
    const loadOptions2 = (inputValue, callback) => {
      setTimeout(() => {
        callback(this.state.data4);
      }, 1000);
    };
    const { classes } = this.props;
    const dataLIst = [];
    let key = 1;
    this.state.rekening.map((value, index) => {
      if (value._destroy == false) {
        dataLIst.push(
          <div>
            <div className={classes.BodytitleMdl2}>
              <text className={classes.titleMdl} />
            </div>
            <div className={classes.BodytitleMdl2}>
              <text className={classes.titleMdl}>
                Rekening
                {key}
              </text>
              {value.id_Rek == this.state.rekeningActive ? null : (
                <Link
                  className={classes.deleteRec}
                  onClick={() => {
                    if (value.id_Rek == this.state.rekeningActive) {
                    } else if (index == 0) {
                      var datas = this.state.rekening;
                      var datas2 = this.state.rekening[index];
                      datas2._destroy = true;
                      datas.push(datas2);
                      this.setState({ rekening: datas });
                    } else {
                      var datas = this.state.rekening;
                      var datas2 = this.state.rekening[index];
                      datas2._destroy = true;
                      datas.push(datas2);
                      this.setState({ rekening: datas });
                    }
                  }}
                  color="inherit"
                >
                  Hapus
                </Link>
              )}
              {value.id_Rek == this.state.rekeningActive ? (
                <Checkbox checked value="remember" color="primary" />
              ) : null}
              {value.id_Rek == this.state.rekeningActive ? (
                <text className={classes.makePrio1}>Utama</text>
              ) : (
                <Link
                  className={classes.makePrio}
                  onClick={() => {
                    this.setState({ rekeningActive: value.id_Rek });
                  }}
                  color="inherit"
                >
                  Jadikan Utama
                </Link>
              )}
            </div>
            <Grid
              container
              direction="row"
              item
              lg={12}
              xl={12}
              md={12}
              xs={12}
            >
              <Grid
                item
                lg={3}
                xl={3}
                md={3}
                sm={3}
                xs={12}
                className={classes.formPad}
              >
                <div>
                  <text className={classes.label1}>Nomor Rekening</text>
                  <text className={classes.starts1}>*</text>
                </div>
                <div>
                  <TextField
                    size="small"
                    className={classes.input222}
                    variant="outlined"
                    autoComplete="off"
                    type="number"
                    onFocus={() => {
                      this.removeValidate('account_number' + index);
                    }}
                    error={this.state.validator['account_number' + index]}
                    helperText={this.state.validator['account_number' + index]}
                    value={this.state.value['account_number' + index]}
                    onChange={(event) => {
                      this.handleChange(
                        event.target.value,
                        'account_number' + index
                      );
                    }}
                    name={'account_number' + index}
                  />
                </div>
              </Grid>
              <Grid
                item
                lg={3}
                xl={3}
                md={3}
                sm={3}
                xs={12}
                className={classes.formPad}
              >
                <div>
                  <text className={classes.label1}>Nama Pemilik</text>
                  <text className={classes.starts1}>*</text>
                </div>
                <div>
                  <TextField
                    size="small"
                    className={classes.input222}
                    variant="outlined"
                    autoComplete="off"
                    onFocus={() => {
                      this.removeValidate('account_name' + index);
                    }}
                    error={this.state.validator['account_name' + index]}
                    helperText={this.state.validator['account_name' + index]}
                    value={this.state.value['account_name' + index]}
                    onChange={(event) => {
                      this.handleChange(
                        event.target.value,
                        'account_name' + index
                      );
                    }}
                    name={'account_name' + index}
                  />
                </div>
              </Grid>
              <Grid
                item
                lg={3}
                xl={3}
                md={3}
                sm={3}
                xs={12}
                className={classes.formPad}
              >
                <div>
                  <text className={classes.label1}>Nama Bank</text>
                  <text className={classes.starts1}>*</text>
                </div>
                <div>
                  <AsyncSelect
                    name="form-field-name-error"
                    value={this.state.value['bank_name' + index]}
                    placeholder="Pilih"
                    onFocus={() => {
                      this.removeValidate('bank_name' + index);
                    }}
                    styles={{
                      control: (provided) => ({
                        ...provided,
                        borderColor: this.state.validator['bank_name' + index]
                          ? 'red'
                          : '#CACACA',
                        borderRadius: '0.25rem',
                      }),
                    }}
                    onInputChange={(val) => {
                      if (val) {
                        if (this.timeout) clearTimeout(this.timeout);
                        this.timeout = setTimeout(() => {
                          this.getBank(val, '', '');
                        }, 500);
                      }
                    }}
                    cacheOptions
                    loadOptions={loadOptions2}
                    defaultOptions
                    className={classes.input222}
                    options={this.state.data4}
                    onChange={(val) => {
                      this.handleChange(val, 'bank_name' + index);
                    }}
                  />
                  <FormHelperText className={classes.error}>
                    {this.state.validator['bank_name' + index]}
                  </FormHelperText>
                </div>
              </Grid>
              <Grid
                item
                lg={3}
                xl={3}
                md={3}
                sm={3}
                xs={12}
                className={classes.formPad}
              >
                <div>
                  <text className={classes.label1}>Cabang</text>
                  <text className={classes.starts1}>*</text>
                </div>
                <div>
                  <TextField
                    size="small"
                    className={classes.input222}
                    variant="outlined"
                    autoComplete="off"
                    onFocus={() => {
                      this.removeValidate('branch_name' + index);
                    }}
                    error={this.state.validator['branch_name' + index]}
                    helperText={this.state.validator['branch_name' + index]}
                    value={this.state.value['branch_name' + index]}
                    onChange={(event) => {
                      this.handleChange(
                        event.target.value,
                        'branch_name' + index
                      );
                    }}
                    name={'branch_name' + index}
                  />
                </div>
              </Grid>
            </Grid>
          </div>
        );
        key++;
      }
    });
    return dataLIst;
  }

  renderListContact() {
    const { classes } = this.props;
    const dataLIst = [];
    let key = 1;
    this.state.contact.map((value, index) => {
      if (value._destroy == false) {
        dataLIst.push(
          <div style={{ marginTop: 25 }}>
            <div className={classes.BodytitleMdl2}>
              <text className={classes.titleMdl} />
            </div>
            <div className={classes.BodytitleMdl2}>
              <text className={classes.titleMdl}>
                KONTAK
                {key}
              </text>
              <Link
                className={classes.deleteRec}
                onClick={() => {
                  if (key <= 2) {
                    alert('Minimum 1');
                  } else if (index == 0) {
                    var datas = this.state.contact;
                    var datas2 = this.state.contact[index];
                    datas2._destroy = true;
                    datas.push(datas2);
                    this.setState({ contact: datas });
                  } else {
                    var datas = this.state.contact;
                    var datas2 = this.state.contact[index];
                    datas2._destroy = true;
                    datas.push(datas2);
                    this.setState({ contact: datas });
                  }
                }}
                color="inherit"
              >
                Hapus
              </Link>
            </div>
            <Grid
              container
              direction="row"
              item
              lg={12}
              xl={12}
              md={12}
              xs={12}
            >
              <Grid container item lg={8} xl={8} md={8} sm={8} xs={12}>
                <Grid container item lg={12} xl={12} md={12} sm={12} xs={12}>
                  <Grid
                    item
                    lg={6}
                    xl={6}
                    md={6}
                    sm={6}
                    xs={12}
                    className={classes.formPad}
                  >
                    <div>
                      <text className={classes.label1}>Nama Lengkap</text>
                      <text className={classes.starts1}>*</text>
                    </div>
                    <TextField
                      size="small"
                      className={classes.input2}
                      variant="outlined"
                      autoComplete="off"
                      onFocus={() => {
                        this.removeValidate('emergency_name' + index);
                      }}
                      error={this.state.validator['emergency_name' + index]}
                      helperText={
                        this.state.validator['emergency_name' + index]
                      }
                      value={this.state.value['emergency_name' + index]}
                      onChange={(event) => {
                        this.handleChange(
                          event.target.value,
                          'emergency_name' + index
                        );
                      }}
                      name={'emergency_name' + index}
                    />
                  </Grid>
                  <Grid
                    item
                    lg={6}
                    xl={6}
                    md={6}
                    sm={6}
                    xs={12}
                    className={classes.formPad}
                  >
                    <div>
                      <text className={classes.label1}>Pekerjaan</text>
                      <text className={classes.starts1}>*</text>
                    </div>
                    <Select2
                      name="form-field-name-error"
                      value={this.state.value['emergency_job_name' + index]}
                      placeholder="Pilih"
                      onFocus={() => {
                        this.removeValidate('emergency_job_name' + index);
                      }}
                      error
                      styles={{
                        control: (provided) => ({
                          ...provided,
                          borderColor: this.state.validator[
                            'emergency_job_name' + index
                          ]
                            ? 'red'
                            : '#CACACA',
                          borderRadius: '0.25rem',
                        }),
                      }}
                      className={classes.input2}
                      options={[
                        {
                          value: '-',
                          label: 'Pilih',
                          isDisabled: true,
                        },
                        {
                          value: 'Karyawan Swasta',
                          label: 'Karyawan Swasta',
                        },
                        {
                          value: 'Dokter',
                          label: 'Dokter',
                        },
                        {
                          value: 'Pegawai Negri Sipil',
                          label: 'Pegawai Negri Sipil',
                        },
                        {
                          value: 'Fresh Graduated',
                          label: 'Fresh Graduated',
                        },
                        {
                          value: 'Wirausaha',
                          label: 'Wirausaha',
                        },
                        {
                          value: 'Ibu Rumah Tangga',
                          label: 'Ibu Rumah Tangga',
                        },
                        {
                          value: 'Pensiunan',
                          label: 'Pensiunan',
                        },
                        {
                          value: 'Pelajar/Mahasiswa',
                          label: 'Pelajar/Mahasiswa',
                        },
                      ]}
                      onChange={(val) => {
                        this.handleChange(val, 'emergency_job_name' + index);
                      }}
                    />
                  </Grid>
                </Grid>
                <Grid container item lg={12} xl={12} md={12} sm={12} xs={12}>
                  <Grid
                    item
                    lg={6}
                    xl={6}
                    md={6}
                    sm={6}
                    xs={12}
                    className={classes.formPad}
                  >
                    <div>
                      <text className={classes.label1}>Hubungan</text>
                      <text className={classes.starts1}>*</text>
                    </div>
                    <div>
                      <Select2
                        name="form-field-name-error"
                        value={this.state.value['emergency_relation' + index]}
                        placeholder="Pilih"
                        onFocus={() => {
                          this.removeValidate('emergency_relation' + index);
                        }}
                        error
                        styles={{
                          control: (provided) => ({
                            ...provided,
                            borderColor: this.state.validator[
                              'emergency_relation' + index
                            ]
                              ? 'red'
                              : '#CACACA',
                            borderRadius: '0.25rem',
                          }),
                        }}
                        className={classes.input2}
                        options={[
                          {
                            value: '-',
                            label: 'Pilih',
                            isDisabled: true,
                          },
                          {
                            value: 'Pasangan (suami/istri)',
                            label: 'Pasangan (suami/istri)',
                          },
                          {
                            value: 'Anak',
                            label: 'Anak',
                          },
                          {
                            value: 'Sepupu',
                            label: 'Sepupu',
                          },
                          {
                            value: 'Orang Tua',
                            label: 'Orang Tua',
                          },
                          {
                            value: 'Paman',
                            label: 'Paman',
                          },
                          {
                            value: 'Bibi',
                            label: 'Bibi',
                          },
                          {
                            value: 'Teman',
                            label: 'Teman',
                          },
                        ]}
                        onChange={(val) => {
                          this.handleChange(val, 'emergency_relation' + index);
                        }}
                      />
                      <FormHelperText className={classes.error}>
                        {this.state.validator['emergency_relation' + index]}
                      </FormHelperText>
                    </div>
                  </Grid>
                  <Grid
                    item
                    lg={6}
                    xl={6}
                    md={6}
                    sm={6}
                    xs={12}
                    className={classes.formPad}
                  >
                    <div>
                      <text className={classes.label1}>Kewarganegaraan</text>
                      <text className={classes.starts1}>*</text>
                    </div>
                    <div>
                      <Select2
                        name="form-field-name-error"
                        value={
                          this.state.value['emergency_nationality' + index]
                        }
                        placeholder="Pilih"
                        onFocus={() => {
                          this.removeValidate('emergency_nationality' + index);
                        }}
                        error
                        styles={{
                          control: (provided) => ({
                            ...provided,
                            borderColor: this.state.validator[
                              'emergency_nationality' + index
                            ]
                              ? 'red'
                              : '#CACACA',
                            borderRadius: '0.25rem',
                          }),
                        }}
                        className={classes.input2}
                        options={[
                          {
                            value: '-',
                            label: 'Pilih',
                            isDisabled: true,
                          },
                          {
                            value: 'WNI',
                            label: 'WNI',
                          },
                          {
                            value: 'WNA',
                            label: 'WNA',
                          },
                        ]}
                        onChange={(val) => {
                          this.handleChange(
                            val,
                            'emergency_nationality' + index
                          );
                        }}
                      />
                      <FormHelperText className={classes.error}>
                        {this.state.validator['emergency_nationality' + index]}
                      </FormHelperText>
                    </div>
                  </Grid>
                </Grid>
              </Grid>
              <Grid
                container
                item
                lg={4}
                xl={4}
                md={4}
                sm={4}
                xs={12}
                spacing={3}
              >
                <Grid
                  item
                  lg={12}
                  xl={12}
                  md={12}
                  sm={12}
                  xs={12}
                  className={classes.formPad}
                >
                  <div>
                    <text className={classes.label1}>Alamat</text>
                    <text className={classes.starts1}>*</text>
                  </div>
                  <TextareaAutosize
                    className={
                      this.state.validator['emergency_address' + index]
                        ? classes.textArea2
                        : classes.textArea
                    }
                    variant="outlined"
                    margin="normal"
                    rows={8}
                    autoComplete="off"
                    onFocus={() => {
                      this.removeValidate('emergency_address' + index);
                    }}
                    error={this.state.validator['emergency_address' + index]}
                    value={this.state.value['emergency_address' + index]}
                    onChange={(event) => {
                      this.handleChange(
                        event.target.value,
                        'emergency_address' + index
                      );
                    }}
                    name={'emergency_address' + index}
                    InputProps={{
                      endAdornment: this.state.validator[
                        'emergency_address' + index
                      ] ? (
                          <InputAdornment position="start">
                            <img src={Icon.warning} />
                          </InputAdornment>
                        ) : (
                          <div />
                        ),
                    }}
                  />
                  <FormHelperText className={classes.error}>
                    {this.state.validator['emergency_address' + index]}
                  </FormHelperText>
                </Grid>
              </Grid>
            </Grid>
            <Grid container item lg={12} xl={12} md={12} sm={12} xs={12}>
              <Grid item lg={4} xl={4} md={4} sm={4} xs={12}>
                <div>
                  <text className={classes.label1}>Nomor Hp</text>
                  <text className={classes.starts1}>*</text>
                </div>
                <InputMask
                  mask="9999 9999 999999"
                  value={this.state.validator['emergency_phone_number' + index]}
                  onFocus={() => {
                    this.removeValidate('emergency_phone_number' + index);
                  }}
                  error={this.state.validator['emergency_phone_number' + index]}
                  helperText={
                    this.state.validator['emergency_phone_number' + index]
                  }
                  value={this.state.value['emergency_phone_number' + index]}
                  onChange={(event) => {
                    this.handleChange(
                      event.target.value.toString(),
                      'emergency_phone_number' + index
                    );
                  }}
                  name={'emergency_phone_number' + index}
                  disabled={false}
                  maskChar=" "
                >
                  {() => (
                    <TextField
                      size="small"
                      className={classes.input2}
                      variant="outlined"
                    />
                  )}
                </InputMask>
              </Grid>
              <Grid
                item
                lg={4}
                xl={4}
                md={4}
                sm={4}
                xs={12}
                className={classes.formPad}
              >
                <div>
                  <text className={classes.label1}>Tempat Lahir</text>
                  <text className={classes.starts1}>*</text>
                </div>
                <TextField
                  size="small"
                  className={classes.input2}
                  variant="outlined"
                  autoComplete="off"
                  onFocus={() => {
                    this.removeValidate('emergency_birth_place' + index);
                  }}
                  error={this.state.validator['emergency_birth_place' + index]}
                  helperText={this.state.validator['emergency_birth_place' + index]}
                  value={this.state.value['emergency_birth_place' + index]}
                  onChange={(event) => {
                    this.handleChange(
                      event.target.value,
                      'emergency_birth_place' + index
                    );
                  }}
                  name={'emergency_birth_place' + index}
                />
              </Grid>
              <Grid item lg={4} xl={4} md={4} sm={4} xs={12}>
                <div>
                  <text className={classes.label1}>Tanggal Lahir</text>
                  <text className={classes.starts1}>*</text>
                </div>
                <TextField
                  size="small"
                  className={classes.input2}
                  variant="outlined"
                  autoComplete="off"
                  type="date"
                  onFocus={() => {
                    this.removeValidate('emergency_birth_date' + index);
                  }}
                  error={this.state.validator['emergency_birth_date' + index]}
                  helperText={this.state.validator['emergency_birth_date' + index]}
                  value={this.state.value['emergency_birth_date' + index]}
                  onChange={(event) => {
                    this.handleChange(event.target.value, 'emergency_birth_date' + index);
                  }}
                  name={'emergency_birth_date' + index}
                  onKeyDown={(event) => event.preventDefault()}
                />
              </Grid>
            </Grid>
          </div>
        );
        key++;
      }
    });
    return dataLIst;
  }

  render() {
    const loadOptionsProvince1 = (inputValue, callback) => {
      setTimeout(() => {
        callback(this.state.province);
      }, 600);
    };
    const loadOptionsCity1 = (inputValue, callback) => {
      setTimeout(() => {
        callback(this.state.cities);
      }, 600);
    };
    const loadOptionsRegional = (inputValue, callback) => {
      setTimeout(() => {
        callback(this.state.regional);
      }, 600);
    };
    const loadOptionsUrbans = (inputValue, callback) => {
      setTimeout(() => {
        callback(this.state.urbans);
      }, 600);
    };

    const loadOptionsCity2 = (inputValue, callback) => {
      setTimeout(() => {
        callback(this.state.cities2);
      }, 600);
    };
    const loadOptionsRegional2 = (inputValue, callback) => {
      setTimeout(() => {
        callback(this.state.regional2);
      }, 600);
    };
    const loadOptionsUrbans2 = (inputValue, callback) => {
      setTimeout(() => {
        callback(this.state.urbans2);
      }, 600);
    };

    const { classes } = this.props;
    if (this.state.form == 1) {
      var content = (
        <div className={classes.scrool}>
          {Func.toLogin(this.state.redirect)}
          <div className={classes.root}>
            <div className={classes.BodytitleMdl2}>
              <text className={classes.titleMdl}>Data Perorangan</text>
            </div>
            <Grid
              container
              direction="row"
              item
              lg={12}
              xl={12}
              md={12}
              xs={12}
              xs={12}
            >
              <Grid container item lg={12} xl={12} md={12} sm={12} xs={12}>
                <Grid container item lg={12} xl={12} md={12} sm={12} xs={12}>
                  <Grid
                    item
                    lg={4}
                    xl={4}
                    md={4}
                    sm={4}
                    xs={12}
                    className={classes.formPad}
                  >
                    <div>
                      <text className={classes.label1}>
                        Nama Lengkap Sesuai ID
                      </text>
                      <text className={classes.starts1}>*</text>
                    </div>
                    <TextField
                      size="small"
                      className={classes.input2}
                      variant="outlined"
                      autoComplete="off"
                      onFocus={() => {
                        this.removeValidate('name');
                      }}
                      error={this.state.validator.name}
                      helperText={this.state.validator.name}
                      value={this.state.value.name}
                      onChange={(event) => {
                        this.handleChange(event.target.value, 'name');
                      }}
                      name="name"
                    />
                  </Grid>
                  <Grid
                    item
                    lg={4}
                    xl={4}
                    md={4}
                    sm={4}
                    xs={12}
                    className={classes.formPad}
                  >
                    <div>
                      <text className={classes.label1}>Tempat Lahir</text>
                      <text className={classes.starts1}>*</text>
                    </div>
                    <TextField
                      size="small"
                      className={classes.input2}
                      variant="outlined"
                      autoComplete="off"
                      onFocus={() => {
                        this.removeValidate('birth_place');
                      }}
                      error={this.state.validator.birth_place}
                      helperText={this.state.validator.birth_place}
                      value={this.state.value.birth_place}
                      onChange={(event) => {
                        this.handleChange(event.target.value, 'birth_place');
                      }}
                      name="birth_place"
                    />
                  </Grid>
                  <Grid
                    item
                    lg={4}
                    xl={4}
                    md={4}
                    sm={4}
                    xs={12}
                    className={classes.formPad}
                  >
                    <div>
                      <text className={classes.label2}>ID Nomor KTP</text>
                      <text className={classes.starts1}>*</text>
                    </div>
                    <TextField
                      size="small"
                      className={classes.input8}
                      variant="outlined"
                      inputProps={{
                        maxLength:
                          this.state.value.identity_type == undefined
                            ? 16
                            : this.state.value.identity_type == 'ktp'
                              ? 16
                              : 14,
                      }}
                      autoComplete="off"
                      onFocus={() => {
                        this.removeValidate('identity_number');
                      }}
                      error={this.state.validator.identity_number}
                      helperText={this.state.validator.identity_number}
                      value={this.state.value.identity_number}
                      onChange={(event) => {
                        if (this.state.value.identity_type == undefined) {
                          if (/^\d*\.?\d*$/.test(event.target.value)) {
                            this.handleChange(
                              event.target.value,
                              'identity_number'
                            );
                          }
                        } else if (this.state.value.identity_type == 'ktp') {
                          if (/^\d*\.?\d*$/.test(event.target.value)) {
                            this.handleChange(
                              event.target.value,
                              'identity_number'
                            );
                          }
                        } else {
                          this.handleChange(
                            event.target.value,
                            'identity_number'
                          );
                        }
                      }}
                      name="identity_number"
                    />
                  </Grid>
                </Grid>
                <Grid
                  container
                  style={{ marginTop: 15 }}
                  item
                  lg={12}
                  xl={12}
                  md={12}
                  sm={12}
                  xs={12}
                >
                  <Grid
                    container
                    item
                    lg={4}
                    xl={4}
                    md={4}
                    sm={4}
                    xs={12}
                    className={classes.formPad}
                  >
                    <Grid
                      item
                      lg={6}
                      xl={6}
                      md={6}
                      sm={6}
                      xs={12}
                      className={classes.formPad}
                    >
                      <div>
                        <text className={classes.label1}>Gelar</text>
                        <text className={classes.starts1}>*</text>
                      </div>
                      <div>
                        <Select2
                          name="form-field-name-error"
                          value={this.state.value.degree}
                          placeholder="Pilih"
                          onFocus={() => {
                            this.removeValidate('degree');
                          }}
                          error
                          styles={{
                            control: (provided) => ({
                              ...provided,
                              borderColor: this.state.validator.degree
                                ? 'red'
                                : '#CACACA',
                              borderRadius: '0.25rem',
                            }),
                          }}
                          className={classes.input9}
                          options={[
                            {
                              value: '-',
                              label: 'Pilih',
                              isDisabled: true,
                            },
                            {
                              value: 'S3',
                              label: 'S3',
                            },
                            {
                              value: 'S2',
                              label: 'S2',
                            },
                            {
                              value: 'S1',
                              label: 'S1',
                            },
                            {
                              value: 'D1',
                              label: 'D1',
                            },
                            {
                              value: 'D2',
                              label: 'D2',
                            },
                            {
                              value: 'D3',
                              label: 'D3',
                            },
                            {
                              value: 'SMA/SMK',
                              label: 'SMA/SMK',
                            },
                            {
                              value: 'SMP',
                              label: 'SMP',
                            },
                            {
                              value: 'SD',
                              label: 'SD',
                            },
                          ]}
                          onChange={(val) => {
                            this.handleChange(val, 'degree');
                          }}
                        />
                        <FormHelperText className={classes.error}>
                          {this.state.validator.degree}
                        </FormHelperText>
                      </div>
                    </Grid>
                    <Grid
                      item
                      lg={6}
                      xl={6}
                      md={6}
                      sm={6}
                      xs={12}
                      className={classes.formPad}
                    >
                      <div>
                        <text className={classes.label1}>Jenis Kelamin</text>
                        <text className={classes.starts1}>*</text>
                      </div>
                      <div>
                        <Select2
                          name="form-field-name-error"
                          value={this.state.value.gender}
                          placeholder="Pilih"
                          onFocus={() => {
                            this.removeValidate('gender');
                          }}
                          error
                          styles={{
                            control: (provided) => ({
                              ...provided,
                              borderColor: this.state.validator.gender
                                ? 'red'
                                : '#CACACA',
                              borderRadius: '0.25rem',
                            }),
                          }}
                          className={classes.input9}
                          options={[
                            {
                              value: '-',
                              label: 'Pilih',
                              isDisabled: true,
                            },
                            {
                              value: 'l',
                              label: 'Laki Laki',
                            },
                            {
                              value: 'p',
                              label: 'Perempuan',
                            },
                          ]}
                          onChange={(val) => {
                            this.handleChange(val, 'gender');
                          }}
                        />
                        <FormHelperText className={classes.error}>
                          {this.state.validator.gender}
                        </FormHelperText>
                      </div>
                    </Grid>
                  </Grid>
                  <Grid
                    item
                    lg={4}
                    xl={4}
                    md={4}
                    sm={4}
                    xs={12}
                    className={classes.formPad}
                  >
                    <div>
                      <text className={classes.label1}>Tanggal Berlaku ID</text>
                      <text className={classes.starts1}>*</text>
                    </div>
                    <TextField
                      size="small"
                      className={classes.input2}
                      variant="outlined"
                      autoComplete="off"
                      type="date"
                      onFocus={() => {
                        this.removeValidate('expired_date');
                      }}
                      error={this.state.validator.expired_date}
                      helperText={this.state.validator.expired_date}
                      value={this.state.value.expired_date}
                      onChange={(event) => {
                        this.handleChange(event.target.value, 'expired_date');
                      }}
                      name="expired_date"
                      onKeyDown={(event) => event.preventDefault()}
                    />
                    <FormHelperText className={classes.txtHelper}>
                      Kosongkan jika masa berlaku seumur hidup
                    </FormHelperText>
                  </Grid>
                  <Grid
                    item
                    lg={4}
                    xl={4}
                    md={4}
                    sm={4}
                    xs={12}
                    className={classes.formPad}
                  >
                    <div>
                      <text className={classes.label1}>Tanggal Lahir</text>
                      <text className={classes.starts1}>*</text>
                    </div>
                    <TextField
                      size="small"
                      className={classes.input2}
                      variant="outlined"
                      autoComplete="off"
                      type="date"
                      onFocus={() => {
                        this.removeValidate('birth_date');
                      }}
                      error={this.state.validator.birth_date}
                      helperText={this.state.validator.birth_date}
                      value={this.state.value.birth_date}
                      onChange={(event) => {
                        this.handleChange(event.target.value, 'birth_date');
                      }}
                      name="birth_date"
                      onKeyDown={(event) => event.preventDefault()}
                    />
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
            <Divider className={classes.divider} />
            <Grid container lg={12} xl={12} md={12} xs={12} xs={12}>
              <Grid item lg={4} xl={4} md={4} xs={4} xs={12}>
                <div className={classes.label111}>
                  <text className={classes.label1}>Alamat Sesuai ID</text>
                  <text className={classes.starts1}>*</text>
                </div>
                <Grid
                  item
                  lg={12}
                  xl={12}
                  md={12}
                  xs={12}
                  xs={12}
                  style={{ marginTop: 10 }}
                >
                  <AsyncSelect
                    name="form-field-name-error"
                    value={this.state.value.identity_province}
                    placeholder="Pilih Provinsi"
                    onFocus={() => {
                      this.removeValidate('identity_province');
                    }}
                    styles={{
                      control: (provided) => ({
                        ...provided,
                        borderColor: this.state.validator.identity_province
                          ? 'red'
                          : '#CACACA',
                        borderRadius: '0.25rem',
                      }),
                    }}
                    onInputChange={(val) => {
                      if (val) {
                        if (this.timeout) clearTimeout(this.timeout);
                        this.timeout = setTimeout(() => {
                          this.getProvince(val, '');
                        }, 500);
                      }
                    }}
                    cacheOptions
                    loadOptions={loadOptionsProvince1}
                    defaultOptions
                    className={classes.input21}
                    options={this.state.province}
                    onChange={(val) => {
                      this.handleChange(val, 'identity_province');
                      this.setState({ same: false });
                    }}
                  />
                </Grid>
                <Grid
                  item
                  lg={12}
                  xl={12}
                  md={12}
                  xs={12}
                  xs={12}
                  style={{ marginTop: 10 }}
                >
                  <Select2
                    name="form-field-name-error"
                    isDisabled={
                      this.state.value.identity_province == undefined
                      || this.state.value.identity_province == '-'
                    }
                    value={this.state.value.identity_city}
                    placeholder="Pilih Kota"
                    onFocus={() => {
                      this.removeValidate('identity_city');
                    }}
                    error
                    styles={{
                      control: (provided) => ({
                        ...provided,
                        borderColor: this.state.validator.identity_city
                          ? 'red'
                          : '#CACACA',
                        borderRadius: '0.25rem',
                      }),
                    }}
                    className={classes.input21}
                    onInputChange={(val) => {
                      this.getIdentityCity(
                        val,
                        this.state.value.identity_province.id
                      );
                    }}
                    cacheOptions
                    loadOptions={loadOptionsCity1}
                    defaultOptions
                    className={classes.input21}
                    options={this.state.cities}
                    onChange={(val) => {
                      this.setState({ same: false });
                      this.handleChange(val, 'identity_city');
                    }}
                  />
                </Grid>
                <Grid
                  item
                  lg={12}
                  xl={12}
                  md={12}
                  xs={12}
                  xs={12}
                  style={{ marginTop: 10 }}
                >
                  <Select2
                    name="form-field-name-error"
                    isDisabled={
                      this.state.value.identity_city == undefined
                      || this.state.value.identity_city == '-'
                    }
                    value={this.state.value.identity_region}
                    placeholder="Pilih Kecamatan"
                    onFocus={() => {
                      this.removeValidate('identity_region');
                    }}
                    error
                    styles={{
                      control: (provided) => ({
                        ...provided,
                        borderColor: this.state.validator.identity_region
                          ? 'red'
                          : '#CACACA',
                        borderRadius: '0.25rem',
                      }),
                    }}
                    className={classes.input21}
                    onInputChange={(val) => {
                      this.getIdentityRegion(
                        val,
                        this.state.value.identity_city.id
                      );
                    }}
                    cacheOptions
                    loadOptions={loadOptionsRegional}
                    defaultOptions
                    className={classes.input21}
                    options={this.state.regional}
                    onChange={(val) => {
                      this.setState({ same: false });
                      this.handleChange(val, 'identity_region');
                    }}
                  />
                </Grid>
                <Grid
                  item
                  lg={12}
                  xl={12}
                  md={12}
                  xs={12}
                  xs={12}
                  style={{ marginTop: 10 }}
                >
                  <Select2
                    name="form-field-name-error"
                    isDisabled={
                      this.state.value.identity_region == undefined
                      || this.state.value.identity_region == '-'
                      || this.state.value.identity_region == ''
                    }
                    value={this.state.value.identity_urbans}
                    placeholder="Pilih Kelurahan"
                    onFocus={() => {
                      this.removeValidate('identity_urbans');
                    }}
                    error
                    styles={{
                      control: (provided) => ({
                        ...provided,
                        borderColor: this.state.validator.identity_urbans
                          ? 'red'
                          : '#CACACA',
                        borderRadius: '0.25rem',
                      }),
                    }}
                    className={classes.input21}
                    onInputChange={(val) => {
                      this.getIdentityUrbans(
                        val,
                        this.state.value.identity_region.id
                      );
                    }}
                    cacheOptions
                    loadOptions={loadOptionsUrbans}
                    defaultOptions
                    className={classes.input21}
                    options={this.state.urbans}
                    onChange={(val) => {
                      this.setState({ same: false });
                      this.handleChange(val, 'identity_urbans');
                    }}
                  />
                </Grid>
                <Grid
                  item
                  lg={12}
                  xl={12}
                  md={12}
                  xs={12}
                  xs={12}
                  style={{ marginTop: 10 }}
                >
                  <div>
                    <TextField
                      size="small"
                      className={classes.input21}
                      disabled={
                        this.state.value.identity_urbans == undefined
                        || this.state.value.identity_urbans == '-'
                        || this.state.value.identity_urbans == ''
                      }
                      variant="outlined"
                      autoComplete="off"
                      placeholder="Kode Pos"
                      onFocus={() => {
                        this.removeValidate('identity_postal_code');
                      }}
                      error={this.state.validator.identity_postal_code}
                      value={this.state.value.identity_postal_code}
                      onChange={(event) => {
                        this.setState({ same: false });
                        this.handleChange(
                          event.target.value,
                          'identity_postal_code'
                        );
                      }}
                      name="identity_postal_code"
                    />
                  </div>
                </Grid>
                <div style={{ marginTop: 10 }}>
                  <text className={classes.label1}>Jalan dan Nomor Rumah</text>
                  <text className={classes.starts1}>*</text>
                </div>
                <Grid
                  item
                  lg={12}
                  xl={12}
                  md={12}
                  xs={12}
                  xs={12}
                  style={{ marginTop: 10 }}
                >
                  <TextareaAutosize
                    className={
                      this.state.validator.identity_address
                        ? classes.textArea2
                        : classes.textArea
                    }
                    variant="outlined"
                    margin="normal"
                    rows={4}
                    autoComplete="off"
                    onFocus={() => {
                      this.removeValidate('identity_address');
                    }}
                    error={this.state.validator.identity_address}
                    value={this.state.value.identity_address}
                    onChange={(event) => {
                      this.setState({ same: false });
                      this.handleChange(event.target.value, 'identity_address');
                    }}
                    name="identity_address"
                    InputProps={{
                      endAdornment: this.state.validator.identity_address ? (
                        <InputAdornment position="start">
                          <img src={Icon.warning} />
                        </InputAdornment>
                      ) : (
                        <div />
                      ),
                    }}
                  />
                </Grid>
                <FormHelperText className={classes.error}>
                  {this.state.validator.identity_province
                    || this.state.validator.identity_city
                    || this.state.validator.identity_region
                    || this.state.validator.identity_postal_code
                    || this.state.validator.identity_address
                    ? 'Harap isi kotak berwarna merah'
                    : ''}
                </FormHelperText>
              </Grid>
              <Grid item lg={4} xl={4} md={4} xs={4} xs={12}>
                <div className={classes.label111}>
                  <text className={classes.label1}>Alamat Saat ini</text>
                  <text className={classes.starts1}>*</text>
                </div>
                <Grid
                  item
                  lg={12}
                  xl={12}
                  md={12}
                  xs={12}
                  xs={12}
                  style={{ marginTop: 10 }}
                >
                  <AsyncSelect
                    name="form-field-name-error"
                    value={this.state.value.residence_province}
                    placeholder="Pilih Provinsi"
                    onFocus={() => {
                      this.removeValidate('residence_province');
                    }}
                    styles={{
                      control: (provided) => ({
                        ...provided,
                        borderColor: this.state.validator.residence_province
                          ? 'red'
                          : '#CACACA',
                        borderRadius: '0.25rem',
                      }),
                    }}
                    onInputChange={(val) => {
                      if (val) {
                        if (this.timeout) clearTimeout(this.timeout);
                        this.timeout = setTimeout(() => {
                          this.getProvince(val, '');
                        }, 500);
                      }
                    }}
                    cacheOptions
                    loadOptions={loadOptionsProvince1}
                    defaultOptions
                    className={classes.input21}
                    options={this.state.province}
                    onChange={(val) => {
                      this.handleChange(val, 'residence_province');
                      this.setState({ same: false });
                    }}
                  />
                </Grid>
                <Grid
                  item
                  lg={12}
                  xl={12}
                  md={12}
                  xs={12}
                  xs={12}
                  style={{ marginTop: 10 }}
                >
                  <Select2
                    name="form-field-name-error"
                    isDisabled={
                      this.state.value.residence_province == undefined
                      || this.state.value.residence_province == '-'
                    }
                    value={this.state.value.residence_city}
                    placeholder="Pilih Kota"
                    onFocus={() => {
                      this.removeValidate('residence_city');
                    }}
                    error
                    styles={{
                      control: (provided) => ({
                        ...provided,
                        borderColor: this.state.validator.residence_city
                          ? 'red'
                          : '#CACACA',
                        borderRadius: '0.25rem',
                      }),
                    }}
                    className={classes.input21}
                    onInputChange={(val) => {
                      this.getResidenceCity(
                        val,
                        this.state.value.residence_province.id
                      );
                    }}
                    cacheOptions
                    loadOptions={loadOptionsCity2}
                    defaultOptions
                    className={classes.input21}
                    options={this.state.cities2}
                    onChange={(val) => {
                      this.setState({ same: false });
                      this.handleChange(val, 'residence_city');
                    }}
                  />
                </Grid>
                <Grid
                  item
                  lg={12}
                  xl={12}
                  md={12}
                  xs={12}
                  xs={12}
                  style={{ marginTop: 10 }}
                >
                  <Select2
                    name="form-field-name-error"
                    isDisabled={
                      this.state.value.residence_city == undefined
                      || this.state.value.residence_city == '-'
                    }
                    value={this.state.value.residence_region}
                    placeholder="Pilih Kecamatan"
                    onFocus={() => {
                      this.removeValidate('residence_region');
                    }}
                    error
                    styles={{
                      control: (provided) => ({
                        ...provided,
                        borderColor: this.state.validator.residence_region
                          ? 'red'
                          : '#CACACA',
                        borderRadius: '0.25rem',
                      }),
                    }}
                    className={classes.input21}
                    onInputChange={(val) => {
                      this.getResidenceRegion(
                        val,
                        this.state.value.residence_city.id
                      );
                    }}
                    cacheOptions
                    loadOptions={loadOptionsRegional2}
                    defaultOptions
                    className={classes.input21}
                    options={this.state.regional2}
                    onChange={(val) => {
                      this.setState({ same: false });
                      this.handleChange(val, 'residence_region');
                    }}
                  />
                </Grid>
                <Grid
                  item
                  lg={12}
                  xl={12}
                  md={12}
                  xs={12}
                  xs={12}
                  style={{ marginTop: 10 }}
                >
                  <Select2
                    name="form-field-name-error"
                    isDisabled={
                      this.state.value.residence_region == undefined
                      || this.state.value.residence_region == '-'
                      || this.state.value.residence_region == ''
                    }
                    value={this.state.value.residence_urbans}
                    placeholder="Pilih Kelurahan"
                    onFocus={() => {
                      this.removeValidate('residence_urbans');
                    }}
                    error
                    styles={{
                      control: (provided) => ({
                        ...provided,
                        borderColor: this.state.validator.residence_urbans
                          ? 'red'
                          : '#CACACA',
                        borderRadius: '0.25rem',
                      }),
                    }}
                    className={classes.input21}
                    onInputChange={(val) => {
                      this.getResidenceUrbans(
                        val,
                        this.state.value.residence_region.id
                      );
                    }}
                    cacheOptions
                    loadOptions={loadOptionsUrbans2}
                    defaultOptions
                    className={classes.input21}
                    options={this.state.urbans2}
                    onChange={(val) => {
                      this.setState({ same: false });
                      this.handleChange(val, 'residence_urbans');
                    }}
                  />
                </Grid>
                <Grid
                  item
                  lg={12}
                  xl={12}
                  md={12}
                  xs={12}
                  xs={12}
                  style={{ marginTop: 10 }}
                >
                  <div>
                    <TextField
                      size="small"
                      className={classes.input21}
                      disabled={
                        this.state.value.residence_urbans == undefined
                        || this.state.value.residence_urbans == '-'
                        || this.state.value.residence_urbans == ''
                      }
                      variant="outlined"
                      autoComplete="off"
                      placeholder="Kode Pos"
                      onFocus={() => {
                        this.removeValidate('residence_postal_code');
                      }}
                      error={this.state.validator.residence_postal_code}
                      value={this.state.value.residence_postal_code}
                      onChange={(event) => {
                        this.setState({ same: false });
                        this.handleChange(
                          event.target.value,
                          'residence_postal_code'
                        );
                      }}
                      name="residence_postal_code"
                    />
                  </div>
                </Grid>
                <div style={{ marginTop: 10 }}>
                  <text className={classes.label1}>Jalan dan Nomor Rumah</text>
                  <text className={classes.starts1}>*</text>
                </div>
                <Grid
                  item
                  lg={12}
                  xl={12}
                  md={12}
                  xs={12}
                  xs={12}
                  style={{ marginTop: 10 }}
                >
                  <TextareaAutosize
                    className={
                      this.state.validator.residence_address
                        ? classes.textArea2
                        : classes.textArea
                    }
                    variant="outlined"
                    margin="normal"
                    rows={4}
                    autoComplete="off"
                    onFocus={() => {
                      this.removeValidate('residence_address');
                    }}
                    error={this.state.validator.residence_address}
                    value={this.state.value.residence_address}
                    onChange={(event) => {
                      this.setState({ same: false });
                      this.handleChange(
                        event.target.value,
                        'residence_address'
                      );
                    }}
                    name="residence_address"
                    InputProps={{
                      endAdornment: this.state.validator.residence_address ? (
                        <InputAdornment position="start">
                          <img src={Icon.warning} />
                        </InputAdornment>
                      ) : (
                        <div />
                      ),
                    }}
                  />
                </Grid>
                <FormHelperText className={classes.error}>
                  {this.state.validator.residence_province
                    || this.state.validator.residence_city
                    || this.state.validator.residence_region
                    || this.state.validator.residence_urbans
                    || this.state.validator.residence_postal_code
                    || this.state.validator.residence_address
                    ? 'Harap isi kotak berwarna merah'
                    : ''}
                </FormHelperText>
                <FormControlLabel
                  className={classes.cbx}
                  control={(
                    <Checkbox
                      onChange={() => {
                        const val = this.state.value;

                        if (this.state.same) {
                          val.residence_province = '-';
                          val.residence_city = '-';
                          val.residence_region = '-';
                          val.residence_urbans = '-';
                          val.residence_postal_code = '';
                          val.residence_address = '';
                          this.setState({
                            value: val,
                            same: false,
                          });
                        } else {
                          val.residence_province = this.state.value.identity_province;
                          val.residence_city = this.state.value.identity_city;
                          val.residence_region = this.state.value.identity_region;
                          val.residence_urbans = this.state.value.identity_urbans;
                          val.residence_postal_code = this.state.value.identity_postal_code;
                          val.residence_address = this.state.value.identity_address;
                          if (this.state.value.identity_province != undefined) {
                            this.getResidenceCity(
                              '',
                              this.state.value.identity_province.value
                            );
                            this.getResidenceRegion(
                              '',
                              this.state.value.identity_city.value
                            );
                            this.getResidenceUrbans(
                              '',
                              this.state.value.identity_region.value
                            );
                          }

                          this.setState({
                            value: val,
                            same: true,
                          });
                          this.removeValidate('residence_province');
                          this.removeValidate('residence_city');
                          this.removeValidate('residence_region');
                          this.removeValidate('residence_postal_code');
                          this.removeValidate('residence_address');
                        }
                      }}
                      checked={this.state.same}
                      value="remember"
                      color="primary"
                    />
                  )}
                  label="Sama dengan alamat ID"
                />
                <FormHelperText className={classes.error}>
                  {this.state.validator.residence_province
                    || this.state.validator.residence_city
                    || this.state.validator.residence_region
                    || this.state.validator.residence_urbans
                    || this.state.validator.residence_postal_code
                    || this.state.validator.residence_address
                    ? 'Harap isi kotak berwarna merah'
                    : ''}
                </FormHelperText>
              </Grid>
              <Grid
                item
                lg={4}
                xl={4}
                md={4}
                xs={4}
                xs={12}
                style={{ marginTop: 9 }}
              >
                <Grid item lg={12} xl={12} md={12} xs={12} xs={12}>
                  <div>
                    <text className={classes.label1}>Nomor HP</text>
                    <text className={classes.starts1}>*</text>
                  </div>
                  <InputMask
                    mask="9999 9999 999999"
                    value={this.state.validator.phone_number}
                    onFocus={() => {
                      this.removeValidate('phone_number');
                    }}
                    value={this.state.value.phone_number}
                    onChange={(event) => {
                      this.handleChange(
                        event.target.value.toString(),
                        'phone_number'
                      );
                    }}
                    name="phone_number"
                    disabled={false}
                    maskChar=" "
                  >
                    {() => (
                      <TextField
                        size="small"
                        className={classes.input2}
                        error={this.state.validator.phone_number}
                        helperText={this.state.validator.phone_number}
                        variant="outlined"
                      />
                    )}
                  </InputMask>
                </Grid>
                <Grid
                  item
                  lg={12}
                  xl={12}
                  style={{ marginTop: 25 }}
                  md={12}
                  xs={12}
                  xs={12}
                >
                  <div>
                    <text className={classes.label1}>Nomor Telepon</text>
                  </div>
                  <InputMask
                    mask="9999 9999 999999"
                    value={this.state.validator.telephone_number}
                    onFocus={() => {
                      this.removeValidate('telephone_number');
                    }}
                    value={this.state.value.telephone_number}
                    onChange={(event) => {
                      this.handleChange(
                        event.target.value.toString(),
                        'telephone_number'
                      );
                    }}
                    name="telephone_number"
                    disabled={false}
                    maskChar=" "
                  >
                    {() => (
                      <TextField
                        size="small"
                        className={classes.input2}
                        error={this.state.validator.telephone_number}
                        helperText={this.state.validator.telephone_number}
                        variant="outlined"
                      />
                    )}
                  </InputMask>
                </Grid>
                <Grid
                  item
                  style={{ marginTop: 10 }}
                  lg={12}
                  xl={12}
                  md={12}
                  xs={12}
                  xs={12}
                >
                  <div>
                    <text className={classes.label1}>Email</text>
                  </div>
                  <TextField
                    size="small"
                    className={classes.input24}
                    variant="outlined"
                    autoComplete="off"
                    placeholder="Tidak wajib diisi"
                    onFocus={() => {
                      this.removeValidate('email');
                    }}
                    error={this.state.validator.email}
                    helperText={this.state.validator.email}
                    value={this.state.value.email}
                    onChange={(event) => {
                      this.handleChange(event.target.value, 'email');
                    }}
                    name="email"
                  />
                </Grid>
              </Grid>
            </Grid>
            <Divider className={classes.divider} />
            <div className={classes.BodytitleMdl2}>
              <text className={classes.titleMdl}>
                Pekerjaan Dan Penghasilan
              </text>
            </div>
            <Grid
              container
              direction="row"
              item
              lg={12}
              xl={12}
              md={12}
              xs={12}
              xs={12}
            >
              <Grid container item lg={12} xl={12} md={12} sm={12} xs={12}>
                <Grid container item lg={12} xl={12} md={12} sm={12} xs={12}>
                  <Grid
                    item
                    lg={4}
                    xl={4}
                    md={4}
                    sm={4}
                    xs={12}
                    className={classes.formPad}
                  >
                    <div>
                      <text className={classes.label1}>Jenis Pekerjaan</text>
                      <text className={classes.starts1}>*</text>
                    </div>
                    <Select2
                      name="form-field-name-error"
                      value={this.state.value.job_type}
                      placeholder="Pilih"
                      onFocus={() => {
                        this.removeValidate('job_type');
                      }}
                      error
                      styles={{
                        control: (provided) => ({
                          ...provided,
                          borderColor: this.state.validator.job_type
                            ? 'red'
                            : '#CACACA',
                          borderRadius: '0.25rem',
                        }),
                      }}
                      className={classes.input2}
                      options={this.state.job.map(({ name }) => ({
                        value: name,
                        label: name,
                      }))}
                      onChange={(val) => {
                        this.handleChange(val, 'job_type');
                      }}
                    />
                    <FormHelperText className={classes.error}>
                      {this.state.validator.job_type}
                    </FormHelperText>
                  </Grid>
                  <Grid
                    item
                    lg={4}
                    xl={4}
                    md={4}
                    sm={4}
                    xs={12}
                    className={classes.formPad}
                  >
                    <div>
                      <text className={classes.label1}>Sumber Penghasilan</text>
                      <text className={classes.starts1}>*</text>
                    </div>
                    <Select2
                      name="form-field-name-error"
                      value={this.state.value.income_source}
                      placeholder="Pilih"
                      onFocus={() => {
                        this.removeValidate('income_source');
                      }}
                      error
                      styles={{
                        control: (provided) => ({
                          ...provided,
                          borderColor: this.state.validator.income_source
                            ? 'red'
                            : '#CACACA',
                          borderRadius: '0.25rem',
                        }),
                      }}
                      className={classes.input2}
                      options={[
                        {
                          value: '-',
                          label: 'Pilih',
                          isDisabled: true,
                        },
                        {
                          value: 'Menjadi karyawan',
                          label: 'Menjadi karyawan',
                        },
                        {
                          value: 'Menjual barang dan jasa',
                          label: 'Menjual barang dan jasa',
                        },
                        {
                          value: 'Menjual keahlian',
                          label: 'Menjual keahlian',
                        },
                        {
                          value: 'Ikut network marketing',
                          label: 'Ikut network marketing',
                        },
                        {
                          value: 'Investasi bagi hasil',
                          label: 'Investasi bagi hasil',
                        },
                        {
                          value: 'Investasi pendapatan tetap',
                          label: 'Investasi pendapatan tetap',
                        },
                        {
                          value: 'Jual beli investasi',
                          label: 'Jual beli investasi',
                        },
                      ]}
                      onChange={(val) => {
                        this.handleChange(val, 'income_source');
                      }}
                    />
                    <FormHelperText className={classes.error}>
                      {this.state.validator.income_source}
                    </FormHelperText>
                  </Grid>
                  <Grid
                    item
                    lg={4}
                    xl={4}
                    md={4}
                    sm={4}
                    xs={12}
                    className={classes.formPad}
                  >
                    <div>
                      <text className={classes.label1}>
                        Penghasilan per Bulan
                      </text>
                      <text className={classes.starts1}>*</text>
                    </div>
                    <Select2
                      name="form-field-name-error"
                      value={this.state.value.salary_amount}
                      placeholder="Pilih"
                      onFocus={() => {
                        this.removeValidate('salary_amount');
                      }}
                      error
                      styles={{
                        control: (provided) => ({
                          ...provided,
                          borderColor: this.state.validator.salary_amount
                            ? 'red'
                            : '#CACACA',
                          borderRadius: '0.25rem',
                        }),
                      }}
                      className={classes.input2}
                      options={[
                        {
                          value: '-',
                          label: 'Pilih',
                          isDisabled: true,
                        },
                        {
                          value: 1,
                          label: 'Kurang dari Rp 3.000.000',
                        },
                        {
                          value: 2,
                          label: 'Diatas dari Rp 3.000.000',
                        },
                        {
                          value: 3,
                          label: 'Diatas dari Rp 6.000.000',
                        },
                      ]}
                      onChange={(val) => {
                        this.handleChange(val, 'salary_amount');
                      }}
                    />
                    <FormHelperText className={classes.error}>
                      {this.state.validator.salary_amount}
                    </FormHelperText>
                  </Grid>
                </Grid>
                <Grid container item lg={12} xl={12} md={12} sm={12} xs={12}>
                  <Grid
                    item
                    lg={4}
                    xl={4}
                    md={4}
                    sm={4}
                    xs={12}
                    className={classes.formPad}
                  >
                    <div>
                      <text className={classes.label1}>Perusahaan</text>
                    </div>
                    <TextField
                      size="small"
                      className={classes.input2}
                      variant="outlined"
                      autoComplete="off"
                      onFocus={() => {
                        this.removeValidate('company_name');
                      }}
                      error={this.state.validator.company_name}
                      helperText={this.state.validator.company_name}
                      value={this.state.value.company_name}
                      onChange={(event) => {
                        this.handleChange(event.target.value, 'company_name');
                      }}
                      name="company_name"
                    />
                  </Grid>

                  <Grid
                    item
                    lg={4}
                    xl={4}
                    md={4}
                    sm={4}
                    xs={12}
                    className={classes.formPad}
                  >
                    <div>
                      <text className={classes.label1}>Tujuan Gadai</text>
                      <text className={classes.starts1}>*</text>
                    </div>
                    <Select2
                      name="form-field-name-error"
                      value={this.state.value.financing_purpose}
                      placeholder="Pilih"
                      onFocus={() => {
                        this.removeValidate('financing_purpose');
                      }}
                      error
                      styles={{
                        control: (provided) => ({
                          ...provided,
                          borderColor: this.state.validator.financing_purpose
                            ? 'red'
                            : '#CACACA',
                          borderRadius: '0.25rem',
                        }),
                      }}
                      className={classes.input2}
                      options={[
                        {
                          value: '-',
                          label: 'Pilih',
                          isDisabled: true,
                        },
                        {
                          value: 'Biaya Konsumtif',
                          label: 'Biaya Konsumtif',
                        },
                        {
                          value: 'Biaya Pendidikan',
                          label: 'Biaya Pendidikan',
                        },
                        {
                          value: 'Biaya Rumah Tangga',
                          label: 'Biaya Rumah Tangga',
                        },
                        {
                          value: 'Lainnya',
                          label: 'Lainnya',
                        },
                      ]}
                      onChange={(val) => {
                        this.handleChange(val, 'financing_purpose');
                      }}
                    />
                    <FormHelperText className={classes.error}>
                      {this.state.validator.financing_purpose}
                    </FormHelperText>
                  </Grid>

                  <Grid
                    item
                    lg={4}
                    xl={4}
                    md={4}
                    sm={4}
                    xs={12}
                    className={classes.formPad}
                  >
                    <div>
                      <text className={classes.label1}>Status Pernikahan</text>
                      <text className={classes.starts1}>*</text>
                    </div>
                    <Select2
                      name="form-field-name-error"
                      value={this.state.value.marital_status}
                      placeholder="Pilih"
                      onFocus={() => {
                        this.removeValidate('marital_status');
                      }}
                      error
                      styles={{
                        control: (provided) => ({
                          ...provided,
                          borderColor: this.state.validator.marital_status
                            ? 'red'
                            : '#CACACA',
                          borderRadius: '0.25rem',
                        }),
                      }}
                      className={classes.input2}
                      options={[
                        {
                          value: '-',
                          label: 'Pilih',
                          isDisabled: true,
                        },
                        {
                          value: 'married',
                          label: 'Kawin',
                        },
                        {
                          value: 'single',
                          label: 'Belum Kawin',
                        },
                        {
                          value: 'divorced',
                          label: 'Cerai',
                        },
                      ]}
                      onChange={(val) => {
                        this.handleChange(val, 'marital_status');
                      }}
                    />
                    <FormHelperText className={classes.error}>
                      {this.state.validator.marital_status}
                    </FormHelperText>
                  </Grid>
                </Grid>

                <Grid container item lg={12} xl={12} md={12} sm={12} xs={12}>
                  <Grid
                    item
                    lg={4}
                    xl={4}
                    md={4}
                    sm={4}
                    xs={12}
                    className={classes.formPad}
                  >
                    <div>
                      <text className={classes.label1}>NPWP</text>
                    </div>
                    <InputMask
                      mask="99.999.999.9-999.999"
                      value={this.state.validator.tax_number}
                      onFocus={() => {
                        this.removeValidate('tax_number');
                      }}
                      value={this.state.value.tax_number}
                      onChange={(event) => {
                        this.handleChange(
                          event.target.value.toString(),
                          'tax_number'
                        );
                      }}
                      name="tax_number"
                      disabled={false}
                      helperText={this.state.validator.tax_number}
                      maskChar=" "
                    >
                      {() => (
                        <TextField
                          className={classes.input2}
                          error={this.state.validator.tax_number}
                          variant="outlined"
                          size="small"
                        />
                      )}
                    </InputMask>
                  </Grid>
                  <Grid
                    item
                    lg={4}
                    xl={4}
                    md={4}
                    sm={4}
                    xs={12}
                    className={classes.formPad}
                  >
                    <div>
                      <text className={classes.label1}>Nama Ibu Kandung</text>
                      <text className={classes.starts1}>*</text>
                    </div>
                    <TextField
                      size="small"
                      className={classes.input2}
                      variant="outlined"
                      autoComplete="off"
                      onFocus={() => {
                        this.removeValidate('mother_name');
                      }}
                      error={this.state.validator.mother_name}
                      helperText={this.state.validator.mother_name}
                      value={this.state.value.mother_name}
                      onChange={(event) => {
                        this.handleChange(event.target.value, 'mother_name');
                      }}
                      name="mother_name"
                    />
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
            <Divider className={classes.divider} />
            <div className={classes.BodytitleMdl2}>
              <text className={classes.titleMdl}>Scan ID</text>
            </div>
            <div className={classes.BodytitleMdl}>
              <Box
                borderColor={
                  this.state.validator.img ? 'error.main' : 'grey.500'
                }
                border={1}
                onClick={() => {
                  this.removeValidate('img');
                }}
                className={classes.imgScan}
              >
                {this.state.value.img ? (
                  <img
                    className={classes.imgScan2}
                    onClick={() => {
                      this.removeValidate('img');
                    }}
                    src={this.state.value.img}
                  />
                ) : null}
              </Box>
              <FormHelperText className={classes.error22}>
                {this.state.validator.img}
              </FormHelperText>
              <FormHelperText
                style={{
                  marginLeft: '25px',
                }}
              >
                Maximum File 2Mb
              </FormHelperText>
            </div>
            <div className={classes.BodytitleMdl22}>
              <img
                src={Icon.deleteImg}
                onClick={() => {
                  const dataSet = this.state.value;
                  dataSet.img = null;
                  this.setState({ value: dataSet, ImgBase44: '' });
                }}
              />
              <div />
              <input
                type="file"
                accept="image/*"
                name="file"
                title="Pilih Gambar"
                onChange={this.handleChangeImg}
              />
            </div>
          </div>
        </div>
      );
    }
    if (this.state.form == 2) {
      var content = (
        <div className={classes.scrool}>
          {Func.toLogin(this.state.redirect)}
          <div className={classes.root}>
            <div className={classes.BodytitleMdl2}>
              <text className={classes.titleMdl}>Kontak Darurat</text>
            </div>
            {this.renderListContact()}
            <Button
              style={{
                backgroundColor: '#C4A643',
                color: 'white',
                marginTop: 15,
                marginLeft: 15,
              }}
              disabled={this.state.loader}
              onClick={() => {
                this.addContact();
              }}
              variant="contained"
            >
              <Typography variant="button" style={{ color: '#FFFFFF' }}>
                Tambah Kontak
              </Typography>
            </Button>
          </div>
        </div>
      );
    }
    return (
      <div>
        <Dialog
          scroll="paper"
          open
          maxWidth="md"
          aria-labelledby="scroll-dialog-title"
          aria-describedby="scroll-dialog-description"
        >
          <DialogTitle id="scroll-dialog-title">
            <Typography variant="h7" className={classes.tittleModal}>
              {this.props.type + ' Nasabah Perorangan'}
            </Typography>
            {this.props.state === 2 ? (
              <img src={Icon.check2} className={classes.space} />
            ) : null}
            <Typography
              variant="h8"
              className={
                this.props.state === 1 ? classes.space : classes.space3
              }
            >
              {' Langkah 1'}
            </Typography>
            <img src={Icon.line} className={classes.space2} />
            <Typography variant="h8" className={classes.space}>
              {' Langkah 2'}
            </Typography>
            <IconButton
              aria-label="close"
              className={classes.closeButton}
              disabled={this.state.loader}
              onClick={() => {
                this.props.handleModal();
              }}
            >
              <CloseIcon />
            </IconButton>
          </DialogTitle>
          <DialogContent dividers>
            <DialogContentText id="scroll-dialog-description" tabIndex={-1}>
              {content}
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button
              style={{
                backgroundColor: '#862C3A',
                color: 'white',
              }}
              disabled={this.state.loader}
              onClick={() => {
                this.handleSubmit(this.props.type);
              }}
              variant="contained"
            >
              <Typography variant="button" style={{ color: '#FFFFFF' }}>
                {this.state.loader
                  ? (
                    <CircularProgress
                      style={{
                        color: 'white',
                        marginLeft: '18px',
                        marginRight: '18px',
                        marginTop: 5,
                      }}
                      size={15}
                    />
                  )
                  : this.state.form === 1 ? 'Lanjutkan' : 'Simpan'}
              </Typography>
            </Button>
            {this.state.form == 2 ? (
              <Button
                style={{
                  backgroundColor: '#e0e0e0',
                  color: 'black',
                }}
                disabled={this.state.loader}
                onClick={() => {
                  this.setState({ form: 1 });
                  this.props.OnNext('1');
                }}
                variant="contained"
              >
                <Typography variant="button">Kembali</Typography>
              </Button>
            ) : null}
            <Button
              style={{
                color: 'black',
              }}
              disabled={this.state.loader}
              onClick={() => {
                this.props.handleModal();
              }}
              variant="outlined"
            >
              <Typography variant="button">Batal</Typography>
            </Button>
          </DialogActions>
        </Dialog>
        ;
      </div>
    );
  }
}

export default withStyles(styles.CoustomsStyles, { name: 'Form' })(Form);
