/* eslint-disable no-unused-vars */
/* eslint-disable react/no-access-state-in-setstate */
/* eslint-disable block-scoped-var */
/* eslint-disable no-var */
/* eslint-disable vars-on-top */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable no-undef */
/* eslint-disable class-methods-use-this */
/* eslint-disable react/sort-comp */
/* eslint-disable react/no-unused-state */
/* eslint-disable camelcase */
/* eslint-disable no-sparse-arrays */
/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable array-callback-return */
import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import Select2 from 'react-select';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import InputBase from '@material-ui/core/InputBase';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import Pagination from '@material-ui/lab/Pagination';
import { Hidden, Button } from '@material-ui/core';
import BeatLoader from 'react-spinners/BeatLoader';
import EditIcon from '@material-ui/icons/Edit';
import FilterListIcon from '@material-ui/icons/FilterList';
import NumberFormat from 'react-number-format';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Divider from '@material-ui/core/Divider';
import PrintIcon from '@material-ui/icons/Print';
import Tooltip from '@material-ui/core/Tooltip';
import Icon from '../../../../components/icon';
import FormCountry from './form/index';
import styles from './css';
import Func from '../../../../functions/index';

function createData(id, cif_number, name, identity_number, hp, os, status, created_by, created_at) {
  return {
    id,
    cif_number,
    name,
    identity_number,
    hp,
    os,
    status,
    created_by,
    created_at,
  };
}

const headCells = [
  {
    id: 'cif_number',
    numeric: false,
    disablePadding: false,
    label: 'No.CIF'
  }, {
    id: 'name',
    numeric: false,
    disablePadding: false,
    label: 'Nasabah'
  }, {
    id: 'id_ktp',
    numeric: false,
    disablePadding: false,
    label: 'ID KTP'
  }, {
    id: 'hp',
    numeric: false,
    disablePadding: false,
    label: 'No.HP'
  }, {
    id: 'os',
    numeric: false,
    disablePadding: false,
    label: 'Total OS'
  }, {
    id: 'created_by',
    numeric: false,
    disablePadding: false,
    label: 'Dibuat Oleh'
  }, {
    id: 'created_at',
    numeric: false,
    disablePadding: false,
    label: 'Tgl. Dibuat'
  }, {
    id: 'status',
    numeric: false,
    disablePadding: false,
    label: 'Status'
  }, {
    id: 'action',
    numeric: false,
    disablePadding: false,
    label: ''
  },

  ,,
];

const BootstrapInput = withStyles((theme) => ({
  root: {
    'label + &': {
      marginTop: theme.spacing(3)
    }
  },
  input: {
    borderRadius: 4,
    position: 'relative',
    backgroundColor: theme.palette.background.paper,
    border: '1px solid #ced4da',
    fontSize: 16,
    padding: '10px 26px 10px 12px',
    transition: theme
      .transitions
      .create(['border-color', 'box-shadow']),
    fontFamily: [
      '-apple-system',
      'BlinkMacSystemFont',
      '"Segoe UI"',
      'Roboto',
      '"Helvetica Neue"',
      'Arial',
      'sans-serif',
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"'
    ].join(','),
    '&:focus': {
      borderRadius: 4,
      borderColor: '#80bdff',
      boxShadow: '0 0 0 0.2rem rgba(0,123,255,.25)'
    }
  }
}))(InputBase);

class Tables extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      total_data: 0,
      total_page: 1,
      current_page: 1,
      prev_page: null,
      data: [],
      next_page: 1,
      rowsPerPage: 10,
      openSearch: false,
      rowFocus: '',
      page: 1,
      filter: false,
      count: 1,
      // eslint-disable-next-line no-dupe-keys
      data: [],
      filterValue: [],
      loading: true,
      modal: false,
      selectAll: false,
      order: 'asc',
      rowSelected: [],
      bulk: false,
      redirect: false,
      failure: false
    };
    this.ChangePage = this
      .ChangePage
      .bind(this);
  }

  handleClick(id) {
    alert(id);
  }

  ChangePage(event, page) {
    this.setState({
      page
    }, () => {
      this.getData();
    });
  }

  setFocus(row) {
    this.setState({ rowFocus: row.id });
  }

  setUnfocus() {
    setTimeout(() => {
      this.setState({ rowFocus: '' });
    }, 5500);
  }

  handleModal(row) {
    this.setState({
      modal: true,
      row
    }, () => {
      this.setState({ modal: false, bulk: false, row: undefined });
    });
  }

  componentDidMount() {
    this.getData('first');
  }

    getData = () => {
      this.setState({ loading: true });
      Func
        .getDataTransaction2(this.props.path, this.state.rowsPerPage, this.state.page, this.state.filterValue)
        .then((res) => {
          if (res.json.code === '403') {
            if (Func.Clear_Token() === true) {
              if (!localStorage.getItem('token')) {
                this.setState({ redirect: true });
              }
            }
          } else if (res.json.status === 500) {
            this.setState({ failure: true });
          } else {
            const datas = [];
            res
              .json
              .data
              .map((data) => {
                datas.push(createData(data.id, data.cif_number, data.name, data.identity_number, data.phone, data.os, data.status, data.created_by_name, Func.FormatDate(data.created_at)));
              });
            this.setState({
              con: res.json.data.length,
              data: datas,
              loading: false,
              total_data: res.json.total_data,
              page: res.json.current_page,
              next_page: res.json.next_page,
              prev_page: res.json.prev_page,
              current_page: res.json.current_page,
              total_page: res.json.total_page,
              filter: false
            });
          }
        });
    };

    Short(orderKey) {
      if (orderKey === this.state.order) {
        this.setState({ order: '' });
        var library = this.state.data;
        library.sort((a, b) => (a[orderKey] < b[orderKey]
          ? 1
          : b[orderKey] < a[orderKey]
            ? -1
            : 0));
      } else {
        this.setState({ order: orderKey });
        // eslint-disable-next-line no-redeclare
        var library = this.state.data;
        library.sort((a, b) => (a[orderKey] > b[orderKey]
          ? 1
          : b[orderKey] > a[orderKey]
            ? -1
            : 0));
      }
    }

    handleChange(event, name) {
      const dataSet = this.state.filterValue;
      dataSet[name] = event;
      this.setState({ filterValue: dataSet });
    }

    render() {
      const { classes } = this.props;
      let con = 0;
      this.state.data.map((row) => {
        if (this.state.rowSelected.indexOf(row.id) > -1) {
          con += 1;
        }
      });
      return (
        <div>
          <TableContainer
            style={{ width: this.props.open ? window.innerWidth - this.props.width : window.innerWidth - 115 }}
            component={Paper}
          >
            <div style={{
              width: '100%'
            }}
            >
              <Box display="flex">
                <Box flexGrow={1}>
                  <Typography variant="h6" className={classes.paginationTxt2}>
                    {this.props.title}
                  </Typography>
                </Box>
              </Box>
            </div>
            <div style={{
              width: '100%'
            }}
            >
              <Box display="flex">
                <Box flexGrow={1}>
                  <Typography className={classes.paginationTxt}>
                    {this.props.subtitle}
                  </Typography>
                </Box>
              </Box>
            </div>
            <Hidden smUp>
              <div
                style={{
                  width: '100%'
                }}
              >
                <Box display="flex">
                  <Box flexGrow={1}>
                    <Typography variant="h9" className={classes.textperdata}>
                      Tampilkan
                    </Typography>
                    <Select
                      size="small"
                      className={classes.selectperdata}
                      labelId="demo-customized-select-label"
                      id="demo-customized-select"
                      value={this.state.rowsPerPage}
                      onChange={(val) => {
                        this.setState({
                          rowsPerPage: val.target.value
                        }, () => {
                          this.getData();
                        });
                      }}
                      input={<BootstrapInput />}
                    >
                      <MenuItem value={10}>10 data</MenuItem>
                      <MenuItem value={50}>50 data</MenuItem>
                      <MenuItem value={100}>100 data</MenuItem>
                    </Select>
                    <Typography variant="h9" className={classes.textperdata}>
                      Total:
                      {' '}
                      {this.state.total_data}
                      {' '}
                      nasabah
                    </Typography>
                  </Box>
                </Box>
              </div>
              <div
                style={{
                  width: '100%'
                }}
              >
                <Box display="flex">
                  <Box>
                    <Button
                      name="filterButton"
                      style={{ marginRight: 15, marginBottom: 15 }}
                      startIcon={<FilterListIcon />}
                      variant="outlined"
                      onClick={() => {
                        this.setState({
                          filter: !this.state.filter
                        });
                      }}
                      color="primary"
                    >
                      Filter
                    </Button>
                  </Box>
                </Box>
              </div>
            </Hidden>
            <Dialog
              open={this.state.filter}
              onClose={() => {
                this.setState({
                  filter: !this.state.filter
                });
              }}
              aria-labelledby="alert-dialog-title"
              aria-describedby="alert-dialog-description"
            >
              <DialogTitle id="alert-dialog-title">Filter</DialogTitle>
              <Divider style={{ marginBottom: 20 }} />
              <DialogContent>
                <DialogContentText id="alert-dialog-description">
                  <div>
                    <div>
                      <Select2
                        name="form-field-name-error"
                        value={this.state.filterValue.status_true}
                        placeholder="Pilih"
                        error
                        styles={{
                          control: (provided, state) => ({
                            ...provided,
                            borderColor: this.state.filterValue.status_true
                              ? 'red'
                              : '#CACACA',
                            borderRadius: '0.25rem'
                          })
                        }}
                        className={classes.search2}
                        options={[
                          {
                            value: '-',
                            label: 'Pilih Status',
                            isDisabled: true
                          }, {
                            value: 1,
                            label: 'Aktif'
                          }, {
                            value: 0,
                            label: 'Tidak Aktif'
                          }
                        ]}
                        onChange={(val) => {
                          this.handleChange(val, 'status_true');
                        }}
                      />
                    </div>
                    <div>
                      <BootstrapInput
                        value={this.state.filterValue.name_cont}
                        placeholder="Nama Nasabah"
                        onChange={(event) => {
                          this.handleChange(event.target.value, 'name_cont');
                        }}
                        on
                        className={classes.search2}
                        id="demo-customized-textbox"
                      />
                    </div>
                    <div>
                      <BootstrapInput
                        placeholder="Email"
                        value={this.state.filterValue.email_cont}
                        onChange={(event) => {
                          this.handleChange(event.target.value, 'email_cont');
                        }}
                        on
                        className={classes.search2}
                        id="demo-customized-textbox"
                      />
                    </div>
                    <div>
                      <BootstrapInput
                        value={this.state.filterValue.customer_contact_phone_number_cont}
                        placeholder="No. Telepon"
                        onChange={(event) => {
                          this.handleChange(event.target.value, 'customer_contact_phone_number_cont');
                        }}
                        on
                        className={classes.search2}
                        id="demo-customized-textbox"
                      />
                    </div>
                    <div>
                      <BootstrapInput
                        value={this.state.filterValue.identity_number}
                        placeholder="No. Indentitas"
                        onChange={(event) => {
                          this.handleChange(event.target.value, 'identity_number');
                        }}
                        on
                        className={classes.search2}
                        id="demo-customized-textbox"
                      />
                    </div>
                    <div>
                      <BootstrapInput
                        value={this.state.filterValue.cif_number_cont}
                        placeholder="No. CIF"
                        onChange={(event) => {
                          this.handleChange(event.target.value, 'cif_number_cont');
                        }}
                        on
                        className={classes.search2}
                        id="demo-customized-textbox"
                      />
                    </div>
                  </div>
                </DialogContentText>
              </DialogContent>
              <DialogActions>
                <Button
                  onClick={() => {
                    this.setState({
                      filterValue: []
                    }, () => {
                      this.getData();
                    });
                  }}
                  color="primary"
                >
                  Hapus
                </Button>
                <Button
                  onClick={() => {
                    this.getData();
                  }}
                  color="primary"
                  autoFocus
                >
                  Filter
                </Button>
              </DialogActions>
            </Dialog>
            <Hidden xsDown>
              <div
                style={{
                  width: '100%'
                }}
              >
                <Box display="flex">
                  <Box flexGrow={1}>
                    <Typography variant="h9" className={classes.textperdata}>
                      Tampilkan
                    </Typography>
                    <Select
                      size="small"
                      className={classes.selectperdata}
                      labelId="demo-customized-select-label"
                      id="demo-customized-select"
                      value={this.state.rowsPerPage}
                      onChange={(val) => {
                        this.setState({
                          rowsPerPage: val.target.value
                        }, () => {
                          this.getData();
                        });
                      }}
                      input={<BootstrapInput />}
                    >
                      <MenuItem value={10}>10 data</MenuItem>
                      <MenuItem value={50}>50 data</MenuItem>
                      <MenuItem value={100}>100 data</MenuItem>
                    </Select>
                    <Typography variant="h9" className={classes.textperdata}>
                      Total:
                      {' '}
                      {this.state.total_data}
                      {' '}
                      nasabah
                    </Typography>
                  </Box>
                  <Box>
                    <Button
                      name="filterButton"
                      style={{ marginRight: 15, marginBottom: 15 }}
                      startIcon={<FilterListIcon />}
                      variant="outlined"
                      color="primary"
                      onClick={() => {
                        this.setState({
                          filter: !this.state.filter
                        });
                      }}
                    >
                      Filter
                    </Button>
                  </Box>
                </Box>
              </div>
            </Hidden>
            <FormCountry type="Ubah" modal={this.state.modal} row={this.state.row} />
          </TableContainer>
          <TableContainer style={{ width: this.props.open ? window.innerWidth - this.props.width : window.innerWidth - 115 }} component={Paper}>
            <Table
              size="small"
              onMouseOut={() => {
                this.setUnfocus();
              }}
              className={classes.table}
              aria-label="simple table"
            >
              <TableHead className={classes.headTable}>
                <TableRow>
                  {headCells.map((headCell) => (
                    <TableCell
                      key={headCell.id}
                      align={headCell.numeric
                        ? 'right'
                        : 'left'}
                      sortDirection={this.state.order === headCell.id
                        ? this.state.order
                        : false}
                    >
                      <TableSortLabel
                        active={this.state.order === headCell.id}
                        direction={this.state.order === headCell.id
                          ? this.state.order
                          : 'asc'}
                        onClick={() => {
                          this.Short(headCell.id);
                        }}
                      >
                        {headCell.label}
                        {this.state.order === headCell.id
                          ? (
                            <span className={classes.visuallyHidden} />
                          )
                          : null}
                      </TableSortLabel>
                    </TableCell>
                  ))}
                </TableRow>
              </TableHead>
              {this.state.data.length === 0 && !this.state.loading
                ? (
                  <TableBody>
                    <TableRow>
                      <TableCell colSpan={10} align="center">
                        <Typography>
                          Tidak Ada Data
                        </Typography>
                      </TableCell>
                    </TableRow>
                  </TableBody>
                )
                : null}
              <TableBody>
                {!this.state.loading
                  ? this
                    .state
                    .data
                    .map((row) => (
                      <TableRow
                        hover
                        onMouseOver={() => {
                          this.setFocus(row);
                        }}
                        role="checkbox"
                        tabIndex={-1}
                        key={row.id}
                      >
                        <TableCell align="left">{row.cif_number}</TableCell>
                        <TableCell align="left">{row.name}</TableCell>
                        <TableCell align="left">{row.identity_number}</TableCell>
                        <TableCell align="left">{row.hp}</TableCell>
                        <TableCell align="left">
                          <NumberFormat
                            value={row.os}
                            type="text"
                            displayType="text"
                            thousandSeparator="."
                            prefix="Rp. "
                            decimalSeparator={false}
                          />
                        </TableCell>
                        <TableCell align="left">{row.created_by}</TableCell>
                        <TableCell align="left">{row.created_at}</TableCell>
                        <TableCell align="left">
                          {row.status === true
                            ? (<img src={Icon.selectActive} />)
                            : (<img src={Icon.selectNonActive} />)}
                        </TableCell>
                        <TableCell align="left">
                          <div className={classes.action}>
                            {
                              Func.checkPermission('customer#individual#update') ? (
                                <Tooltip title="Ubah" aria-label="Ubah">
                                  <IconButton
                                    onClick={() => {
                                      this.handleModal(row);
                                    }}
                                    aria-label="Cari"
                                  >
                                    <EditIcon />
                                  </IconButton>
                                </Tooltip>
                              )
                                : null
                            }
                            {
                              Func.checkPermission('customer#individual#print') ? (
                                <Tooltip title="Cetak" aria-label="Cetak">
                                  <IconButton
                                    onClick={() => {
                                      window.open(process.env.REACT_APP_BASE_URL_MANAJEMENT + process.env.REACT_APP_API_PREFIX_V1 + '/customer/' + row.id + '/customer_form_print.pdf?token=' + localStorage.getItem('token'));
                                    }}
                                  >
                                    <PrintIcon />
                                  </IconButton>
                                </Tooltip>
                              ) : null
                            }
                          </div>
                        </TableCell>
                      </TableRow>
                    ))

                  : null}
              </TableBody>
            </Table>
            {this.state.loading
              ? (
                <div className={classes.loader}>
                  <BeatLoader size={15} color="#3F3F3F" loading />
                </div>
              )
              : null}
          </TableContainer>
          <TableContainer style={{ width: this.props.open ? window.innerWidth - this.props.width : window.innerWidth - 115, marginBottom: 40 }} component={Paper}>
            <div className={classes.col}>
              <text>
                Halaman
                {' '}
                {' ' + this.state.current_page + ' '}
                dari
                {' '}
                {' ' + this.state.total_page + ' '}
                halaman
              </text>
            </div>
            <Pagination
              className={classes.row}
              color="secondary"
              count={this.state.total_page}
              defaultPage={this.state.page}
              onChange={this.ChangePage}
              siblingCount={0}
            />
          </TableContainer>
        </div>
      );
    }
}

export default withStyles(styles.CoustomsStyles, { name: 'Tables' })(Tables);
