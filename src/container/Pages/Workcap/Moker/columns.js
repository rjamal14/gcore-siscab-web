/* eslint-disable react/jsx-one-expression-per-line */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable no-trailing-spaces */
/* eslint-disable semi */
/* eslint-disable react/prop-types */
import React from 'react';
import Func from '../../../../functions';
import MokerAction from './mokerAction';

const setColor = (val) => {
  let color = '#000000'
  let backgroundColor = '#F1F1F1'
  let status = 'Draft'

  if (val === 'waiting_approval') {
    color = '#FFFFFF'
    backgroundColor = '#2196F3'
    status = 'Menunggu Persetujuan'
  } else if (val === 'in_progress') {
    color = '#FFFFFF'
    backgroundColor = '#FF9800'
    status = 'Dalam Proses'
  } else if (val === 'done') {
    color = '#FFFFFF'
    backgroundColor = '#4CAF50'
    status = 'Selesai'
  } else if (val === 'rejected') {
    color = '#FFFFFF'
    backgroundColor = '#F44336'
    status = 'Ditolak'
  } else if (val === 'need_confirmation') {
    color = '#343A40'
    backgroundColor = '#FFC107'
    status = 'Butuh Konfirmasi'
  }
  return (
    <div style={{
      backgroundColor,
      width: 'auto',
      height: '40px',
      color,
      borderRadius: 5,
      padding: '10px 0',
      textAlign: 'center',
    }}
    >
      {status.toUpperCase()}
    </div>
  )
}

const columns = [
  {
    name: 'id',
    label: 'ID',
    display: false,
    options: {
      filter: false,
      sort: false
    }
  },
  {
    name: 'requested_office_name',
    label: 'Nama Kantor',
    options: {
      filter: true,
      sort: false
    }
  },
  {
    name: 'amount',
    label: 'Jumlah',
    customBodyRender: (evt, val) => Func.currencyFormatter(val),
    options: {
      filter: true,
      sort: false
    }
  },
  {
    name: 'status',
    label: 'Status',
    customBodyRender: (evt, val) => setColor(val),
    options: {
      filter: true,
      sort: false
    }
  },
  {
    name: '',
    label: 'Aksi',
    customBodyRender: (evt, val, row) => <MokerAction row={row} />,
  }
];

export default columns;
