/* eslint-disable camelcase */
/* eslint-disable semi */
/* eslint-disable import/prefer-default-export */
import axios from 'axios';
import env from '../../../../config/env';
import { SetAuthTokenRequest, checkTokenExpired } from '../../../../config/interceptor';

const api = axios.create({
  baseURL: env.financialApi + env.apiPrefixV1
});
api.interceptors.request.use(SetAuthTokenRequest, null);
api.interceptors.response.use(response => response, checkTokenExpired);

// eslint-disable-next-line camelcase
/* --------------------------- api accounting ------------------------------------- */
export const getApi = (params) => api.get('/working_capitals/histories', { params });
export const publish = (id) => api.put(`/working_capitals/${id}/publish`);
export const receiveStatement = (id, payload) => api.put(`/working_capitals/${id}/statement/receive`, payload);