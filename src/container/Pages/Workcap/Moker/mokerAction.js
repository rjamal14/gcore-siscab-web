/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState, Fragment } from 'react';
import { Dropdown } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import swal from 'sweetalert';
import ReceiveStatement from './receiveStatement';
import { publish } from './api';

const MokerAction = ({ row }) => {
  const [option, setOption] = useState(null);
  const [open, setOpen] = useState(false);

  useEffect(() => {
    if (row.status === 'need_confirmation') setOption(['Terima Statement']);
    else if (row.status === 'draft') setOption(['Publish']);
  }, []);

  const handleReceiveStatement = () => {
    swal({
      title: 'Apa Anda yakin?',
      text: 'Mempublish pengajuan modal',
      icon: 'info',
      buttons: ['Batal', 'Yakin'],
    })
      .then((willDelete) => {
        if (willDelete) {
          publish(row.id)
            .then((res) => {
              const response = res?.data || res?.response?.data;
              if (res?.status === 200) {
                swal('Sukses!', response.message, 'success')
                  .then(() => { window.location.reload(); });
              }
            });
        }
      });
  };

  return (
    <Fragment>
      {
        option && (
          <Dropdown>
            <Dropdown.Toggle variant="info" id="dropdown-basic">
              PILIH
            </Dropdown.Toggle>
            <Dropdown.Menu>
              {option.map((opt) => (
                <Dropdown.Item
                  eventKey={opt}
                  key={opt}
                  onClick={() => {
                    if (opt === 'Terima Statement') setOpen(true);
                    if (opt === 'Publish') handleReceiveStatement();
                  }}
                >
                  {opt}
                </Dropdown.Item>
              ))}
            </Dropdown.Menu>
          </Dropdown>
        )
      }
      <ReceiveStatement open={open} setOpen={setOpen} id={row.id} amountval={row.amount} />
    </Fragment>
  );
};

export default MokerAction;
