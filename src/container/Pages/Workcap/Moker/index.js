/* eslint-disable array-callback-return */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable camelcase */
/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable no-trailing-spaces */
import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import Crud from '../../../../components/Crud';
import columns from './columns';
import { getApi } from './api';
import { changePageTitle } from '../../../../redux/actions/changePageTitle';

const Cashflow = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(changePageTitle('Modal Kerja'));
  }, []);

  return (
    <Crud
      columns={columns}
      description="History Modal Kerja"
      getApi={getApi}
      disableAdd
      disableFilter
      disableExport
      disableDelete
      disableEdit
    />
  ); 
};

export default Cashflow;
