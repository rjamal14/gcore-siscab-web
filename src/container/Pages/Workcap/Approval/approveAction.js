/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect } from 'react';
import { Dropdown } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import swal from 'sweetalert';
import { approval } from './api';

const ApproveAction = ({ row }) => {
  const [status, setStatus] = useState(); // approve or reject

  const option = [
    'Terima',
    'Tolak',
  ];

  useEffect(() => {
    if (status) {
      swal({
        title: 'Apa Anda yakin?',
        text: `${status === 'approve' ? 'Menerima' : 'Menolak'} pengajuan`,
        icon: 'info',
        buttons: ['Batal', 'Yakin'],
      })
        .then((willDelete) => {
          if (willDelete) {
            approval({ type: status, id: row.id })
              .then((res) => {
                const response = res?.data || res?.response?.data;
                if (res?.status === 200) {
                  swal('Sukses!', response.message, 'success')
                    .then(() => { window.location.reload(); });
                }
              });
          }
        });
    }
  }, [status]);

  return (
    <Dropdown>
      <Dropdown.Toggle variant="info" id="dropdown-basic">
        PILIH
      </Dropdown.Toggle>

      <Dropdown.Menu>
        {option.map((opt) => (
          <Dropdown.Item
            eventKey={opt}
            key={opt}
            onClick={() => {
              if (opt === 'Terima') setStatus('approve');
              if (opt === 'Tolak') setStatus('reject');
            }}
          >
            {opt}
          </Dropdown.Item>
        ))}
      </Dropdown.Menu>
    </Dropdown>
  );
};

export default ApproveAction;
