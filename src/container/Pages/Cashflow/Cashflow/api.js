/* eslint-disable camelcase */
/* eslint-disable semi */
/* eslint-disable import/prefer-default-export */
import axios from 'axios';
import env from '../../../../config/env';
import { SetAuthTokenRequest, checkTokenExpired } from '../../../../config/interceptor';

// api to accounting
const api = axios.create({
  baseURL: env.financialApi + env.apiPrefixV1
});
api.interceptors.request.use(SetAuthTokenRequest, null);
api.interceptors.response.use(response => response, error => error);

// api to master
const apiMaster = axios.create({
  baseURL: env.masterApi + env.apiPrefixV1
});
apiMaster.interceptors.request.use(SetAuthTokenRequest, null);
apiMaster.interceptors.response.use(null, checkTokenExpired);
/* ---------------------------------------------------------------- */
const path = '/daily_cashes';

const mapParams = (params) => {
  const newParams = {};

  newParams.opening_balance = params.opening_balance;
  newParams.closing_balance = params.closing_balance;
  newParams.cash_report_items_attributes = params.cash_report_items_attributes;

  return newParams;
};

// eslint-disable-next-line camelcase
/* --------------------------- api accounting ------------------------------------- */
export const getApi = (params) => api.get(path, { params });
export const getCashStatus = () => api.get(`${path}/history_status`); // buat page cashflow
export const getCashStatus2 = (office_id) => api.post(`cashiers/status?office_id=${office_id}`, {
  balance: 0,
  type: 'credit'
}); // buat page transaction
export const getOpeningBalance = () => api.get(`${path}/opening_balance`);
export const postApi = (params, cash_endpoint) => api.post(`${path}/${cash_endpoint}`, mapParams(params));
export const showApi = (id) => api.get(`${path}/${id}`);
export const getTodayOpenedCashStatus = () => api.get(`${path}/today`);
export const approveCash = () => api.put('/cashiers/approve')
export const approveCloseCash = () => api.put('/cashiers/approve/close')
export const postWokcap = (params) => api.post('/working_capitals', params);
/* --------------------------- api master ------------------------------------- */
export const getApiMoneyBills = () => apiMaster.get('/money_bills/list')
