/* eslint-disable no-else-return */
/* eslint-disable react/jsx-one-expression-per-line */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable no-trailing-spaces */
/* eslint-disable semi */
/* eslint-disable react/prop-types */

import React from 'react'
import * as dayjs from 'dayjs';
import Func from '../../../../functions'

const formatingDate = (val) => (Func.FormatDate(val))
const formatingTime = (val) => (dayjs(val).locale('id').format('HH:mm'))
const setColor = (val, rowItem) => {
  let flag;

  if (!rowItem?.status && rowItem?.cashier_status && rowItem?.is_closing_approved) {
    flag = true
  }

  if (!rowItem?.status && rowItem?.cashier_status && !rowItem?.is_closing_approved) {
    flag = false
  }

  if (rowItem?.status && rowItem?.cashier_status && rowItem?.is_closing_approved === 'cashier_still_open') {
    flag = true
  }

  if (rowItem?.status && !rowItem?.cashier_status && rowItem?.is_closing_approved === 'cashier_still_open') {
    flag = false
  }

  if (flag === undefined) {
    return (
      <div style={{ textAlign: 'center', }}> - </div>
    )
  }
  return (
    <div style={{
      backgroundColor: (!flag ? '#CACACA' : '#28a745'),
      width: '200px',
      height: '40px',
      color: 'white',
      borderRadius: 5,
      padding: '10px 0',
      textAlign: 'center',
    }}
    >
      {!flag ? 'Menunggu Persetujuan' : 'Disetujui'}
    </div>
  )
}
const setKeterangan = (val) => (val ? 'Masih Buka' : 'Sudah Tutup')

const columns = [
  {
    name: 'id',
    label: 'ID',
    display: false,
    options: {
      filter: false,
      sort: false
    }
  },
  {
    name: 'cashier_name',
    label: 'Nama Kasir',
    options: {
      filter: true,
      sort: false
    }
  },
  {
    name: 'opening_time',
    label: 'Tanggal',
    customBodyRender: (evt, val) => formatingDate(val),
    options: {
      filter: true,
      sort: false
    }
  },
  {
    name: 'opening_time',
    label: 'Jam',
    customBodyRender: (evt, val) => formatingTime(val),
    options: {
      filter: true,
      sort: false
    }
  },
  {
    name: 'status',
    label: 'Status Kasir',
    customBodyRender: (evt, val) => setKeterangan(val),
    options: {
      filter: true,
      sort: false
    }
  },
  {
    name: 'cashier_status',
    label: 'Status Aprroval',
    customBodyRender: (evt, val, rowItem) => setColor(val, rowItem),
    options: {
      filter: true,
      sort: false
    }
  },
  {
    name: 'detail',
  },
  {
    name: 'pointerCursor'
  },

];

export default columns;
