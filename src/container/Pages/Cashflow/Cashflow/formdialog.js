/* eslint-disable no-restricted-globals */
/* eslint-disable radix */
/* eslint-disable array-callback-return */
/* eslint-disable react/jsx-closing-tag-location */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable padded-blocks */
/* eslint-disable no-param-reassign */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-use-before-define */
/* eslint-disable no-trailing-spaces */
/* eslint-disable react/jsx-one-expression-per-line */
import React, { useState } from 'react';
import {
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Grid,
  Button,
  TextField,
  Divider,
  IconButton,
  CircularProgress
} from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import swal from 'sweetalert';
import { 
  currencyLabelForm,
  mainFormProps
} from './tabledata';
import Func from '../../../../functions';
import { opencashSchema } from '../../../../validation/opencashSchema';
import { postApi } from './api';
import '../../../../styles/date_field.css';
import useStyle from './css';

function CashflowDialog(props) {
  const { 
    buttonStyle,
    buttonIcon,
    apiValue,
    paperMoney,
    coinMoney,
    setPaperMoney,
    setCoinMoney,
    cashTitle,
    cashStatus,
    formPermission
  } = props;
  const style = useStyle();
  const [openDialog, setOpenDialog] = useState(false);
  const [totalCash, setTotalCash] = useState(0);
  const [loading, setLoading] = useState(false);
  const [openCashInput, setOpenCashInput] = useState({
    marginCash: 0, description: ''

  });
  const buttonProps = [
    ['contained', 'secondary', '', 'Simpan', () => handleSubmit()],
    ['outlined', '', style.blackTextButton, 'Batal', () => setOpenDialog(false)]
  ];

  const handleFormDialog = () => {
    // kalau hari ini di kantor cabangnya sudah buka kas, form dialog tdk bs muncul
    if (cashStatus === 'close' && !formPermission) {
      swal({
        text: 'Maaf, kas sudah dibuka hari ini',
        icon: 'error',
      });
    } else setOpenDialog(true);
  };

  // set jumlah mata uang & total uang jadi kosong
  const setEmptyStates = () => {
    setPaperMoney(paperMoney.map(({ nominal }) => ({
      nominal,
      quantity: '',
      total: 0
    })));
    setCoinMoney(coinMoney.map(({ nominal }) => ({
      nominal,
      quantity: '',
      total: 0
    })));
    setTotalCash(0);
  };

  // handle close form dialog
  const handleClose = () => {
    setEmptyStates();
    setOpenDialog(false);
  };

  // Handle change jumlah uang
  const handleChange = (type, idx, val) => {
    let state = '';
    if (type === 'paper') state = paperMoney;
    else state = coinMoney;
      
    // validating amount/jumlah, tdk boleh masuk huruf
    const validateResult = opencashSchema.validate({
      quantity: val
    });

    if (!validateResult.error) {
      // total = amount * pecahan
      const { nominal } = state[idx];
      state[idx].quantity = val;
      state[idx].total = val * nominal;
    } else {
      state[idx].quantity = '';
      state[idx].total = 0;
    }

    // simpan ke state
    if (type === 'paper') setPaperMoney(state);
    else setCoinMoney(state);

    // hitung total uang
    countAllTotal();
  };

  // Handle change for total, selisih and description
  const handleChangeCashInput = (propName, val) => {
    const state = openCashInput;
    state[propName] = val;
    setOpenCashInput(state);
  };

  // hitung total uang
  const countAllTotal = () => {
    const mergeMoneyArr = paperMoney.concat(coinMoney);
    let newTotal = 0;
    mergeMoneyArr.map(({ total }) => {
      newTotal += total;
    });
    /*
    * margin :
    * jika tutup kas : margin = saldo akhir - jumlah uang
    * jika buka kas : margin = saldo awal - jumlah uang
    */
    const cashInput = openCashInput;
    let balanceUsed = 0;
    if (cashStatus === 'open') balanceUsed = apiValue.remaining_balance;
    else balanceUsed = apiValue.opening_balance;
    cashInput.marginCash = balanceUsed - newTotal;
    setOpenCashInput(cashInput);
    setTotalCash(newTotal);
  };

  const handleSubmit = () => {
    setLoading(true);
    
    // menyatukan paperMoney & coinMoney dalam 1 array
    // tp bedakan money_type nya
    const mergeMoney = [];
    paperMoney.map(({ nominal, quantity }) => {
      if (quantity) {
        mergeMoney.push({
          nominal, money_type: 'paper', quantity: parseInt(quantity)
        });
      }
    });
    coinMoney.map(({ nominal, quantity }) => {
      if (quantity) {
        mergeMoney.push({
          nominal, money_type: 'coin', quantity: parseInt(quantity)
        });
      }
    });

    // jika cashStatus true = tutup kasir
    // membedakan api endpoint & data totalCash sesuai cashStatus(true/false)
    const dataForPostApi = { cash_report_items_attributes: mergeMoney };
    let cashEndpoint;
    if (cashStatus === 'open') {
      cashEndpoint = 'close_cashier';
      dataForPostApi.closing_balance = totalCash;
    } else {
      cashEndpoint = 'open_cashier';
      dataForPostApi.opening_balance = totalCash;
    }

    // save to db
    saveToDB(dataForPostApi, cashEndpoint);
  };

  const saveToDB = (dataForPostApi, cashEndpoint) => {
    postApi(dataForPostApi, cashEndpoint)
      .then(res => {
        const response = res.data || res.response.data;
        if (response.code === 201 || response.code === 200) {
          swal({
            title: 'Sukses',
            text: response.message,
            icon: 'success',
            buttons: 'OK'
          })
            .then(() => { location.reload(); });
          handleClose();
        } else {
          swal({
            title: 'Kesalahan',
            text: response.message,
            icon: 'error',
          });
        }
        setLoading(false);
      });
  };

  return (
    <> 
      <Button
        variant="contained"
        color="primary"
        disabled={coinMoney.length === 0 && paperMoney.length === 0}
        className={buttonStyle}
        startIcon={buttonIcon}
        onClick={() => handleFormDialog()}
      >
        {cashTitle}
      </Button>
      {/* --------------------------- Dialog --------------------------- */}
      <Dialog maxWidth="md" scroll="paper" open={openDialog}>
        <DialogTitle className={style.dialogTitle}>
          <h3 className={style.blackText}>{cashTitle}</h3>
          <IconButton className={style.buttonClose} onClick={handleClose}>
            <CloseIcon />
          </IconButton>
        </DialogTitle>
        <DialogContent dividers>
          <Grid container direction="row" spacing={3}>
            {/* ----------------------- Main Form ----------------------- */}
            {mainFormProps.map(({ label, ...val }) => (
              <Grid item lg={3} xl={3} md={3} sm={3} xs={12}>
                <div className={style.label}>
                  <text>{label}</text>
                </div>
                <TextField
                  disabled={!!apiValue.opening_balance}
                  type={val.type}
                  fullWidth
                  size="small"
                  variant="outlined"
                  defaultValue={
                    label === 'Saldo Awal'
                      ? Func.currencyFormatter(apiValue.opening_balance)
                      : val.value
                  }
                />
              </Grid>
            ))}
            {
              (cashStatus === 'open') && (
                <Grid item lg={3} xl={3} md={3} sm={3} xs={12}>
                  <div className={style.label}>
                    <text>Saldo Akhir</text>
                  </div>
                  <TextField
                    disabled
                    fullWidth
                    size="small"
                    variant="outlined"
                    value={Func.currencyFormatter(apiValue.remaining_balance)}
                  />
                </Grid>
              )
            }
            <Grid item lg={12} xl={12} md={12} sm={12} xs={12}>
              <Divider />
            </Grid>
            {/* ---------------------- Second Form ------------------------------ */}
            {(paperMoney[0]) && (<>
              <Grid item lg={6} xl={6} md={6} sm={6} xs={12} className={style.grid}>
                <h3 className={style.blackText}>Uang Kertas</h3>
              </Grid>
            </>)}
            {(!coinMoney[0] || !paperMoney[0]) && (<Grid item lg={6} xl={6} md={6} sm={6} xs={12} className={style.grid} />)}
            {coinMoney[0] && (
              <Grid item lg={6} xl={6} md={6} sm={6} xs={12} className={style.grid}>
                <h3 className={style.blackText}>Uang Logam</h3>
              </Grid>
            )}
            {(paperMoney[0]) && currencyLabelForm.map(label => (<>
              <Grid item lg={2} xl={2} md={2} sm={2} xs={12} className={style.grid}>
                <div className={style.label}>
                  <text> {label} </text>
                </div>
              </Grid>
            </>))}
            {(!coinMoney[0] || !paperMoney[0]) && (<Grid item lg={6} xl={6} md={6} sm={6} xs={12} className={style.grid} />)}
            {(coinMoney[0]) && currencyLabelForm.map(label => (<>
              <Grid item lg={2} xl={2} md={2} sm={2} xs={12} className={style.grid}>
                <div className={style.label}>
                  <text> {label} </text>
                </div>
              </Grid>
            </>))}
            {/* ----------------------- Form Uang Kertas ------------------------------ */}
            <Grid container lg={6} xl={6} md={6} sm={6} xs={12} style={{ marginTop: '10px', marginBottom: '10px' }} className={style.grid}>
              {paperMoney.map((val, idx) => (
                <>
                  <Grid item lg={12} xl={12} md={12} sm={12} xs={12} style={{ marginLeft: '20px' }}>
                    <TextField
                      disabled
                      className={style.currencyField}
                      value={Func.currencyFormatter(val.nominal)}
                      size="small"
                    />
                    <TextField
                      className={style.amountField}
                      value={val.quantity}
                      size="small"
                      variant="outlined"
                      onChange={(evt) => handleChange('paper', idx, evt.target.value)}
                    />
                    <TextField
                      disabled
                      className={style.totalField}
                      value={Func.currencyFormatter(val.total)}
                      size="small"
                    />
                  </Grid>
                </>
              ))}
            </Grid>
            {/* ----------------------- Form Uang Logam ------------------------------ */}
            <Grid container lg={6} xl={6} md={6} sm={6} xs={12} style={{ marginTop: '10px', marginBottom: '10px' }} className={style.grid}>
              {coinMoney.map((val, idx) => (
                <>
                  <Grid item lg={12} xl={12} md={12} sm={12} xs={12} style={{ marginLeft: '20px' }}>
                    <TextField
                      disabled
                      className={style.currencyField}
                      value={Func.currencyFormatter(val.nominal)}
                      size="small"
                    />
                    <TextField
                      className={style.amountField}
                      value={val.quantity}
                      size="small"
                      variant="outlined"
                      onChange={(evt) => handleChange('coin', idx, evt.target.value)}
                    />
                    <TextField
                      disabled
                      className={style.totalField}
                      value={Func.currencyFormatter(val.total)}
                      size="small"
                    />
                  </Grid>
                </>
              ))}

            </Grid>
            {/* ----------------------- Total & Selisih ------------------------------ */}
            <Grid item lg={12} xl={12} md={12} sm={12} xs={12}>
              <Divider />
            </Grid>
            <Grid item lg={3} xl={3} md={3} sm={3} xs={12}>
              <text className={style.label}>
                Total uang kertas dan uang logam
              </text>
            </Grid>
            <Grid item lg={3} xl={3} md={3} sm={3} xs={12}>
              <TextField
                disabled
                size="small"
                value={Func.currencyFormatter(totalCash)}                  
              />
            </Grid>
            <Grid item lg={3} xl={3} md={3} sm={3} xs={12}>
              <text className={style.label}>
                Selisih uang fisik dengan saldo {cashStatus === 'open' ? 'akhir' : 'awal'}
              </text>
            </Grid>
            <Grid item lg={3} xl={3} md={3} sm={3} xs={12}>
              <TextField
                size="small"
                disabled
                value={`${openCashInput.marginCash < 0 ? 'lebih' : (openCashInput.marginCash > 0) ? 'kurang' : ''}`
                + ` ${Func.currencyFormatter(Math.abs(openCashInput.marginCash))}`}
                error={openCashInput.marginCash}
              />
            </Grid>
            {/* ----------------------- Description ------------------------------ */}
            <Grid item lg={12} xl={12} md={12} sm={12} xs={12}>
              <Divider />
            </Grid>
            <Grid item lg={12} xl={12} md={12} sm={12} xs={12}>
              <text className={style.label}> Keterangan </text>
              <text className={style.label}> (Note: Jika ada selisih sertakan pada lampiran keterangan) </text>
              <TextField
                fullWidth
                multiline
                size="small"
                variant="outlined"
                name="expired_date"
                rows={4}
                onChange={(evt) => handleChangeCashInput('description', evt.target.value)}
              />
            </Grid>
            {/* ----------------------- End ------------------------------ */}
          </Grid>
        </DialogContent>
        <DialogActions>
          {buttonProps.map(([variant, color, classStyle, title, onclick]) => (
            <Button
              variant={variant}
              color={color}
              className={classStyle}
              onClick={onclick}
            >
              {(title === 'Simpan' && loading) 
                ? <CircularProgress size={22} />
                : title}
            </Button>
          ))}
        </DialogActions>
      </Dialog>
    </>
  );
}

export default CashflowDialog;
