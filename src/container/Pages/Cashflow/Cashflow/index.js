/* eslint-disable array-callback-return */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable camelcase */
/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable no-trailing-spaces */
import React, { Fragment, useEffect, useState } from 'react';
import dayjs from 'dayjs';
import Crud from '../../../../components/Crud';
import columns from './columns';
import {
  getApi,
  showApi,
  getApiMoneyBills,
  getCashStatus,
  getOpeningBalance,
  getTodayOpenedCashStatus,
} from './api';
import formfields from './formfields';
import useStyle from './css';
import Icon from '../../../../components/icon';
import Func from '../../../../functions/index';
import CashflowDialog from './formdialog';
import ApproveDialog from './approvedialog';
import MokerDropdown from './mokerDropdown';
import MokerStatus from './mokerStatus';

const Cashflow = () => {
  const { root, button, img } = useStyle();
  const [apiValue, setApiValue] = useState({
    opening_balance: 0,
    remaining_balance: 0,
  });
  const [paperMoney, setPaperMoney] = useState([]);
  const [coinMoney, setCoinMoney] = useState([]);
  const [cashStatus, setCashStatus] = useState('open');
  const [approveDialog, setApproveDialog] = useState(false);
  const [formPermission, setFormPermission] = useState(true);
  const [approveData, setApproveData] = useState(null);
  const [workCap, setWorkCap] = useState();

  useEffect(() => {
    const apiState = apiValue;
    // cek ke api apa hr ini sudah buka kas atau belum
    // ciri sudah buka kas = sudah ada opening time
    getTodayOpenedCashStatus().then((res) => {
      const response = res?.data || res?.response?.data;
      const dateToday = dayjs().format('DD MMMM YYYY');
      let openingDate = '';

      if (response?.code === 200) openingDate = dayjs(response.data.opening_time).format('DD MMMM YYYY');
      else openingDate = '';

      if (openingDate === dateToday) setFormPermission(false);
    });
    // Ambil saldo akhir/remaining balance
    getCashStatus().then((res) => {
      const response = res?.data || res?.response?.data;
      if (response?.code === 400) setCashStatus('close');
      apiState.remaining_balance = response?.data || 0;
    });

    getOpeningBalance().then((res) => {
      const response = res?.data || res?.response?.data;
      if (response?.code === 200) apiState.opening_balance = response?.message || 0;
    });

    // Ambil daftar uang
    getApiMoneyBills().then((res) => {
      const response = res?.data || res?.response?.data;
      if (response?.code === 200) {
        const moneyData = res.data.data;
        const paperList = [];
        const coinList = [];
        moneyData.sort((a, b) => b.nominal - a.nominal);
        moneyData.length > 0
          && moneyData.map(({ money_type, nominal }) => {
            if (money_type === 'paper') {
              paperList.push({
                nominal,
                quantity: '',
                total: 0,
              });
              setPaperMoney(paperList);
            } else {
              coinList.push({
                nominal,
                quantity: '',
                total: 0,
              });
              setCoinMoney(coinList);
            }
          });
      }
    });

    setApiValue(apiState);
  }, []);

  const mapParams = (params) => {
    const newParams = params;
    newParams.id = params.id;
    return newParams;
  };

  const parseResponse = (response) => {
    const { data } = response.data;

    return data;
  };

  const parseId = (object) => {
    const { id } = object;

    return id;
  };

  const handleRowClick = (event, row) => {
    setApproveData(row);
    setApproveDialog(true);
  };

  return (
    <Fragment>
      {/* --------------- Open & Close cashflow button --------------------- */}
      <div className={root}>
        {Func.checkPermission('cashflow#all#open-and-close-cash') && (
          <Fragment>
            <CashflowDialog
              cashStatus={cashStatus}
              cashTitle={cashStatus === 'open' ? 'Tutup Kas' : 'Buka Kas'}
              buttonStyle={button}
              buttonIcon={(
                <img
                  className={img}
                  src={
                    cashStatus === 'open'
                      ? Icon.closeCashflow
                      : Icon.openCashflow
                  }
                />
              )}
              apiValue={apiValue}
              paperMoney={paperMoney}
              setPaperMoney={setPaperMoney}
              coinMoney={coinMoney}
              setCoinMoney={setCoinMoney}
              formPermission={formPermission}
            />
            <MokerDropdown />
            <MokerStatus data={workCap} />
            <ApproveDialog
              approveDialog={approveDialog}
              handleModal={() => setApproveDialog(false)}
              approveData={approveData}
            />
          </Fragment>
        )}
      </div>
      {/* --------------- Open & Close cashflow button --------------------- */}
      <Crud
        columns={columns}
        formField={formfields}
        description="Pencatatan buka dan tutup kas."
        getApi={getApi}
        showApi={showApi}
        parseResponse={parseResponse}
        parseId={parseId}
        mapParams={mapParams}
        disableAdd
        disableFilter
        disableExport
        onRowClick={handleRowClick}
        setWorkCap={setWorkCap}
      />
    </Fragment>
  );
};

export default Cashflow;
