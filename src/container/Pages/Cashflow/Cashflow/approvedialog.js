/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
/* eslint-disable no-restricted-globals */
/* eslint-disable radix */
/* eslint-disable array-callback-return */
/* eslint-disable react/jsx-closing-tag-location */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable padded-blocks */
/* eslint-disable no-param-reassign */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-use-before-define */
/* eslint-disable no-trailing-spaces */
/* eslint-disable react/jsx-one-expression-per-line */
import React, { useState, useEffect } from 'react';
import {
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Grid,
  Button,
  Divider,
  IconButton,
  Typography,
  DialogContentText,
  makeStyles,
} from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import swal from 'sweetalert';
import Func from '../../../../functions';
import '../../../../styles/date_field.css';
import styles from './cssModal';
import { approveCash, approveCloseCash } from './api';

const useStyles = makeStyles((theme) => styles.CoustomsStyles);

function ApproveDialog(props) {
  const { approveDialog, handleModal, approveData } = props;
  const classes = useStyles();
  const [redirect, setRedirect] = useState(false);
  const [value, setValue] = useState([]);
  const [loader, setLoader] = useState(false);
  const [cashItem, setCashItem] = useState([]);

  useEffect(() => {
    setValue(approveData);
    setCashItem(approveData?.cash_report_items);
  }, [approveDialog]);

  const handleCashApprove = () => {
    swal({
      title: 'Anda yakin?',
      icon: 'warning',
      buttons: true,
    })
      .then((confirm) => {
        if (confirm) {
          approveCash()
            .then(res => {
              const response = res.data || res.response.data;
              if (response.code === 200) {
                swal('Berhasil Approve!', {
                  icon: 'success',
                })
                  .then(() => { location.reload(); });
              } else if (response.code === 400) {
                swal(response.message, {
                  icon: 'error',
                });
              }
            });
        }
      });
  };

  const handleCloseCashApprove = () => {
    swal({
      title: 'Anda yakin?',
      icon: 'warning',
      buttons: true,
    })
      .then((confirm) => {
        if (confirm) {
          approveCloseCash()
            .then(res => {
              const response = res.data || res.response.data;
              if (response.code === 200) {
                swal('Berhasil Approve!', {
                  icon: 'success',
                })
                  .then(() => { location.reload(); });
              } else if (response.code === 400) {
                swal(response.message, {
                  icon: 'error',
                });
              }
            });
        }
      });
  };

  return (
    <Dialog
      scroll="paper"
      open={approveDialog}
      maxWidth="md"
      aria-labelledby="scroll-dialog-title"
      aria-describedby="scroll-dialog-description"
    >
      <DialogTitle id="scroll-dialog-title">
        <Typography variant="h7" className={classes.tittleModal}>
          Informasi Arus Kas
        </Typography>
        <IconButton
          aria-label="close"
          className={classes.closeButton}
          onClick={() => {
            handleModal();
          }}
        >
          <CloseIcon />
        </IconButton>
      </DialogTitle>
      <DialogContent dividers>
        <DialogContentText id="scroll-dialog-description" tabIndex={-1}>
          <div className={classes.scrool}>
            <div>
              {Func.toLogin(redirect)}
              <div className={classes.root}>
                <div className={classes.BodytitleMdl2}>
                  <text className={classes.titleMdl}>Data Kas</text>
                </div>
                <Grid container direction="row">
                  <Grid
                    item
                    lg={4}
                    xl={4}
                    md={4}
                    sm={4}
                    xs={12}
                    className={classes.formPad}
                  >
                    <div className={classes.label111}>
                      <text className={classes.label121}>Nama Kasir</text>
                    </div>
                    <div className={classes.label1112}>
                      <text className={classes.label1}>
                        {value?.cashier_name}
                      </text>
                    </div>
                  </Grid>
                  <Grid
                    item
                    lg={4}
                    xl={4}
                    md={4}
                    sm={4}
                    xs={12}
                    className={classes.formPad}
                  >
                    <div className={classes.label111}>
                      <text className={classes.label121}>Nama Kantor</text>
                    </div>
                    <div className={classes.label1112}>
                      <text className={classes.label1}>
                        {value?.office_name}
                      </text>
                    </div>
                  </Grid>
                  <Grid
                    item
                    lg={4}
                    xl={4}
                    md={4}
                    sm={4}
                    xs={12}
                    className={classes.formPad}
                  >
                    <div className={classes.label111}>
                      <text className={classes.label121}>Sisa Saldo</text>
                    </div>
                    <div className={classes.label1112}>
                      <text className={classes.label1}>
                        {Func.currencyFormatter(value?.remaining_balance)}
                      </text>
                    </div>
                  </Grid>
                </Grid>
                <Divider className={classes.divider} />
                <div className={classes.BodytitleMdl2}>
                  <text className={classes.titleMdl}>Laporan Kas</text>
                </div>
                <Grid container direction="row">
                  <Grid
                    item
                    lg={6}
                    xl={6}
                    md={6}
                    sm={6}
                    xs={12}
                    className={classes.formPad}
                  >
                    <div className={classes.label111}>
                      <text className={classes.label121}>Nominal Uang</text>
                    </div>
                  </Grid>
                  <Grid
                    item
                    lg={6}
                    xl={6}
                    md={6}
                    sm={6}
                    xs={12}
                    className={classes.formPad}
                  >
                    <div className={classes.label111}>
                      <text className={classes.label121}>Jumlah</text>
                    </div>
                  </Grid>
                  {cashItem?.length >= 1
                    && cashItem.map(({ money_type, nominal, quantity, total }) => (
                      <>
                        <Grid
                          item
                          lg={6}
                          xl={6}
                          md={6}
                          sm={6}
                          xs={12}
                          className={classes.formPad}
                        >
                          <div className={classes.label1112}>
                            <text className={classes.label1}>
                              {quantity} {money_type === 'paper' ? 'lembar uang' : 'buah koin'} {Func.currencyFormatter(nominal)}
                            </text>
                          </div>
                        </Grid>
                        <Grid
                          item
                          lg={6}
                          xl={6}
                          md={6}
                          sm={6}
                          xs={12}
                          className={classes.formPad}
                        >
                          <div className={classes.label1112}>
                            <text className={classes.label1}>
                              {Func.currencyFormatter(total)}
                            </text>
                          </div>
                        </Grid>
                      </>
                    ))}
                </Grid>
              </div>
            </div>
          </div>
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        {(!value?.is_closing_approved)
        && ((localStorage.getItem('role_name') === 'Kepala Unit'))
        && (
          <Button
            style={{
              backgroundColor: '#862C3A',
              color: 'white',
            }}
            onClick={() => {
              handleModal();
              handleCloseCashApprove();
            }}
            variant="contained"
          >
            <Typography variant="button" style={{ color: '#FFFFFF' }}>
              Setuju
            </Typography>
          </Button>
        )}
        {(!value?.cashier_status)
          && ((localStorage.getItem('roleId') === '5f589b7cd77fab2771b2ac0d'))
          && (
            <Button
              style={{
                backgroundColor: '#862C3A',
                color: 'white',
              }}
              onClick={() => {
                handleModal();
                handleCashApprove();
              }}
              variant="contained"
            >
              <Typography variant="button" style={{ color: '#FFFFFF' }}>
                Setujui
              </Typography>
            </Button>
          )}

        <Button
          style={{
            color: 'black',
          }}
          disabled={loader}
          onClick={() => {
            handleModal();
          }}
          variant="outlined"
        >
          <Typography variant="button">Batal</Typography>
        </Button>
      </DialogActions>
    </Dialog>
  );
}

export default ApproveDialog;
