/* eslint-disable semi */
import dayjs from 'dayjs'
import Func from '../../../../functions/index'

export const currencyTitleForm = ['Uang Kertas', 'Uang Logam']

export const currencyLabelForm = ['Pecahan', 'Jumlah', 'Total']

export const mainFormProps = [
  {
    disabled: true,
    type: '',
    value: Func.FormatDate(dayjs().format('YYYY-MM-DD')),
    label: 'Tanggal'
  },
  {
    disabled: true,
    type: '',
    value: localStorage.getItem('name'),
    label: 'Kasir'
  },
  {
    disabled: false,
    type: '',
    value: '',
    label: 'Saldo Awal'
  }
]
