/* eslint-disable consistent-return */
import React, { Fragment, useState } from 'react';
import {
  Button,
  Menu,
  MenuItem,
  Divider,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  TextField,
  CircularProgress,
  InputAdornment,
  Table,
  TableBody,
  TableRow,
  withStyles,
} from '@material-ui/core';
import MuiTableCell from '@material-ui/core/TableCell';
import swal from 'sweetalert';
import { useHistory } from 'react-router-dom';
import useStyle from './css';
import Func from '../../../../functions';
import { postWokcap } from './api';

const TableCell = withStyles({
  root: {
    borderBottom: 'none',
  },
})(MuiTableCell);

const MokerDropdown = () => {
  const style = useStyle();
  const history = useHistory();
  const [anchorEl, setAnchorEl] = useState(null);
  const [dialog, setDialog] = useState(false);
  const [loading, setLoading] = useState(false);
  const [amount, setAmount] = useState(0);
  const [error, setError] = useState('');
  const [selectedFile, setSelected] = useState(null);
  const [attachment, setAttachment] = useState('');
  const scrHeight = window.screen.height;

  const handleSubmit = () => {
    if (amount && attachment) {
      setLoading(true);
      const payload = {
        working_capital: {
          amount,
          attachment

        }
      };
      swal({
        title: 'Apa Anda yakin?',
        text: 'Melakukan Pengajuan Modal Kerja',
        icon: 'info',
        buttons: ['Batal', 'Ajukan'],
      })
        .then((confirm) => {
          if (confirm) {
            postWokcap(payload)
              .then((res) => {
                // const response = res?.data || res?.response?.data;
                if (res.status === 201) {
                  swal('Sukses!', '', 'success')
                    .then(() => { history.push('/app/workcap/moker'); });
                  setDialog(false);
                }
              });
          }
          setLoading(false);
          setSelected(null);
        });
    } else setError('Tidak boleh kosong');
  };

  const buttonProps = [
    ['contained', 'secondary', '', 'Simpan', () => handleSubmit()],
    ['outlined', '', style.blackTextButton, 'Batal', () => {
      setDialog(false);
      setSelected(null);
    }]
  ];

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const onChange = (evt) => {
    const { value } = evt.target;
    setAmount(value);
  };

  const selectFile = (evt) => {
    setSelected(evt.target.files);
    const file = evt.target.files[0];
    if (file.size > 2.9e6) {
      swal({ title: 'File Terlalu Besar', text: 'Maximal File 2Mb', icon: 'error', buttons: 'OK' });
      return false;
    }
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onloadend = () => setAttachment(reader.result);
  };

  const dialogMoker = () => (
    <Dialog scroll="paper" open={dialog}>
      <DialogTitle>Request Modal Kerja</DialogTitle>
      <DialogContent dividers>
        <Table>
          <TableBody>
            <TableRow>
              <TableCell>Amount</TableCell>
              <TableCell>
                <TextField
                  defaultValue={Func.currencyFormatter(amount)}
                  fullWidth
                  size="small"
                  variant="outlined"
                  type="number"
                  onChange={onChange}
                  error={(error && !amount)}
                  helperText={amount ? '' : error}
                  onKeyDown={() => setError('')}
                  InputProps={{
                    startAdornment: <InputAdornment position="start"> Rp </InputAdornment>
                  }}
                />
              </TableCell>
            </TableRow>
            <TableRow>
              <TableCell>Lampiran</TableCell>
              <TableCell>
                <Button
                  variant="contained"
                  component="label"
                >
                  Pilih File
                  <input
                    type="file"
                    hidden
                    onChange={selectFile}
                  />
                </Button>
                {selectedFile && selectedFile.length > 0 ? selectedFile[0].name : null}
                {!attachment && error}
              </TableCell>
            </TableRow>
          </TableBody>
        </Table>
      </DialogContent>
      <DialogActions>
        {buttonProps.map(([variant, color, classStyle, title, onclick]) => (
          <Button
            variant={variant}
            color={color}
            className={classStyle}
            onClick={onclick}
          >
            {(title === 'Simpan' && loading)
              ? <CircularProgress size={22} />
              : title}
          </Button>
        ))}
      </DialogActions>
    </Dialog>
  );

  return (
    <Fragment>
      <Button
        variant="contained"
        color="primary"
        className={style.button}
        onClick={handleClick}
      >
        Modal Kerja
      </Button>
      <Menu
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
        anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
        transformOrigin={{ vertical: 'top', horizontal: 'center' }}
        PaperProps={{ style: { marginTop: (scrHeight >= 1080) ? '3%' : '4%', } }}
      >
        <MenuItem
          onClick={() => setDialog(true)}
        >
          Request Modal Kerja
        </MenuItem>
        <Divider />
        <MenuItem onClick={() => history.replace('/app/workcap/moker')}>
          History Modal Kerja
        </MenuItem>
      </Menu>
      {dialogMoker()}
    </Fragment>
  );
};

export default MokerDropdown;
