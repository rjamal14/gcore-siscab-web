/* eslint-disable react/jsx-one-expression-per-line */
import React, { Fragment } from 'react';
import Alert from '@material-ui/lab/Alert';
import { Grid, withStyles } from '@material-ui/core';
import MuiDivider from '@material-ui/core/Divider';
import Func from '../../../../functions';
import useStyle from './css';

const Divider = withStyles({
  root: {
    backgroundColor: '#FFFFFF',
    margin: '0px 10px',
  },
})(MuiDivider);

const MokerStatus = ({ data }) => {
  const style = useStyle();

  const setColor = (val) => {
    let color = '#000000';
    let backgroundColor = '#F1F1F1';
    let status = 'Draft';

    if (val === 'waiting_approval') {
      color = '#FFFFFF';
      backgroundColor = '#2196F3';
      status = 'Menunggu Persetujuan';
    } else if (val === 'in_progress') {
      color = '#FFFFFF';
      backgroundColor = '#FF9800';
      status = 'Dalam Proses';
    } else if (val === 'done') {
      color = '#FFFFFF';
      backgroundColor = '#4CAF50';
      status = 'Selesai';
    } else if (val === 'rejected') {
      color = '#FFFFFF';
      backgroundColor = '#F44336';
      status = 'Ditolak';
    }
    return (
      <span
        style={{
          backgroundColor,
          width: 'auto',
          height: '40px',
          color,
          borderRadius: 5,
          padding: '2px 20px',
          textAlign: 'center',
        }}
      >
        {status.toUpperCase()}
      </span>
    );
  };

  return (
    <Fragment>
      {data && (
        <Alert
          elevation={1}
          severity="warning"
          icon={false}
          className={style.button}
          classes={{
            root: style.alertRoot,
            message: style.alertMsg,
            standardWarning: style.alertStandar,
          }}
        >
          <Grid container alignItems="center">
            <span>
              <b>No</b>: GC-8980932840234890
            </span>
            <Divider orientation="vertical" flexItem />
            <span>
              <b>Jumlah</b>: {Func.currencyFormatter(data?.amount || 0)}
            </span>
            <Divider orientation="vertical" flexItem />
            <span>
              {setColor(data?.status)}
            </span>
          </Grid>
        </Alert>
      )}
    </Fragment>
  );
};

export default MokerStatus;
