/* eslint-disable array-callback-return */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable camelcase */
/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable no-trailing-spaces */
import React, { Fragment, useState } from 'react';
import dayjs from 'dayjs';
import Crud from '../../../../components/Crud';
import columns from './columns';
import { 
  getApi,
  getExport,
  getPrint,
  getJsonExport
} from './api';
import Func from '../../../../functions';
import CashflowStatus from './cashflowStatus';
import useStyle from './css';

const CashJournal = () => {
  const style = useStyle();
  const [apiGetType, setApiGetType] = useState('normal'); // export or normal
  const [cashflow, setCashflow] = useState({ opening_balance: 0, remaining_balance: 0 });

  const mapParams = (params) => {
    const newParams = params;
    newParams.id = params.id;
    return newParams;
  };

  const parseResponse = (response) => {
    const { data } = response.data;
    
    return data;
  };

  const parseId = (object) => {
    const { id } = object;

    return id;
  };

  const handleJsonExport = () => setApiGetType('export');
  
  const handleCustomReset = () => setApiGetType('normal');

  const handleExport = (startDate, endDate) => {
    const start = dayjs(startDate).format('YYYY-MM-DD');
    const end = dayjs(endDate).format('YYYY-MM-DD');
    getExport(start, end);
  };

  const handlePrint = (startDate, endDate) => {
    const start = dayjs(startDate).format('YYYY-MM-DD');
    const end = dayjs(endDate).format('YYYY-MM-DD');
    getPrint(start, end);
  };

  return (
    <Fragment>
      <div className={style.root}>
        <CashflowStatus data={cashflow} />
      </div>
      <Crud
        columns={columns}
        description="Buku Kas"
        getApi={apiGetType === 'normal' ? getApi : getJsonExport}
        parseResponse={parseResponse}
        parseId={parseId}
        mapParams={mapParams}
        disableAdd
        disableFilter
        disableSearch
        handleExport={handleExport}
        disableExport={!Func.checkPermission('cashflow#cash-journal#export')}
        customDateExport
        enablePrint
        handlePrint={handlePrint}
        enableJsonExport
        handleJsonExport={handleJsonExport}
        handleCustomReset={handleCustomReset}
        setCashflow={setCashflow}
      />
      
    </Fragment>
  );
};

CashJournal.propTypes = ({

});

export default CashJournal;
