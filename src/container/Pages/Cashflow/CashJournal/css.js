import { makeStyles } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    paddingTop: theme.spacing(1),
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(2),
  },
  button: {
    color: '#FFFFFF',
    height: '30px',
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1),
    marginRight: theme.spacing(2)
  },
  buttonClose: {
    position: 'absolute',
    right: theme.spacing(1),
    top: theme.spacing(1),
  },
  img: {
    height: '20px',
    width: '20px'
  },
  currencyField: {
    width: '140px',
  },
  amountField: {
    width: '80px',
    marginRight: theme.spacing(1),
    marginLeft: theme.spacing(1),
    marginBottom: theme.spacing(1)
  },
  totalField: {
    width: '180px',
    marginBottom: theme.spacing(1)
  },
  label: {
    fontSize: '10pt',
    marginLeft: 5,
    marginBottom: '10px',
    fontWeight: 'normal'
  },
  dialog: {
    width: '1000px'
  },
  dialogTitle: {
    marginTop: theme.spacing(-3),
    marginBottom: theme.spacing(-3)
  },
  blackText: {
    fontSize: '1.25rem',
    fontWeight: 'normal',
  },
  blackTextButton: {
    color: '#000000'
  },
  divider: {
    marginBottom: '-100px',
  },
  dividerMainTitle: {
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(3),
  },
  grid: {
    marginTop: theme.spacing(-3.5),
  },
  input: {
    borderColor: 'red'
  },
  alertRoot: {
    padding: 0,
  },
  alertMsg: {
    padding: '6px 20px',
  },
  alertStandar: {
    backgroundColor: '#C4A643',
  },
}));

export default useStyles;
