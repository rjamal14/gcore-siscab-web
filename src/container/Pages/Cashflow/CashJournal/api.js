/* eslint-disable camelcase */
/* eslint-disable semi */
/* eslint-disable import/prefer-default-export */
import axios from 'axios';
import env from '../../../../config/env';
import { SetAuthTokenRequest, checkTokenExpired } from '../../../../config/interceptor';

const api = axios.create({
  baseURL: env.financialApi + env.apiPrefixV1
});

api.interceptors.request.use(SetAuthTokenRequest, null);
api.interceptors.response.use(response => response, (error) => {
  checkTokenExpired(error);
  return error;
});

const token = localStorage.getItem('token');
const path = '/ledgers/items.json';
const exportPath = `${env.financialApi + env.apiPrefixV1}/ledgers/items/export`;

export const getApi = (params) => api.get(path, { params });
export const getExport = (startDate, endDate) => {
  window.open(`${exportPath}.xlsx?q[created_at_gteq]=${startDate}&q[created_at_lteq]=${endDate}&token=${token}`);
}
export const getJsonExport = (params) => api.get(`/ledgers/items`, {
  params: {
    page: params.page,
    per_page: params.per_page,
    'q[created_at_lteq]': params.q.created_at_lteq,
    'q[created_at_gteq]': params.q.created_at_gteq,
    token
  }
});
export const getPrint = (startDate, endDate) => {
  window.open(`${exportPath}.pdf?q[created_at_gteq]=${startDate}&q[created_at_lteq]=${endDate}&token=${token}`);
};
