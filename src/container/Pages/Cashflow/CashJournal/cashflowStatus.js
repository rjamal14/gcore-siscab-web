/* eslint-disable react/jsx-one-expression-per-line */
import React, { Fragment } from 'react';
import Alert from '@material-ui/lab/Alert';
import { Grid, withStyles } from '@material-ui/core';
import MuiDivider from '@material-ui/core/Divider';
import Func from '../../../../functions';
import useStyle from './css';

const Divider = withStyles({
  root: {
    backgroundColor: '#FFFFFF',
    margin: '0px 10px',
  },
})(MuiDivider);

const CashflowStatus = ({ data }) => {
  const style = useStyle();
  return (
    <Fragment>
      {data && (
        <Alert
          elevation={1}
          severity="warning"
          icon={false}
          className={style.button}
          classes={{
            root: style.alertRoot,
            message: style.alertMsg,
            standardWarning: style.alertStandar,
          }}
        >
          <Grid container alignItems="center">
            <span>
              Saldo Awal: <b>{Func.currencyFormatter(data?.opening_balance || 0)}</b>
            </span>
            <Divider orientation="vertical" flexItem />
            <span>
              Saldo Akhir: <b>{Func.currencyFormatter(data?.remaining_balance || 0)}</b>
            </span>
          </Grid>
        </Alert>
      )}
    </Fragment>
  );
};

export default CashflowStatus;
