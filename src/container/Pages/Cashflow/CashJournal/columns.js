/* eslint-disable no-trailing-spaces */
/* eslint-disable semi */
/* eslint-disable react/prop-types */

import Func from '../../../../functions'
import * as dayjs from 'dayjs'
import 'dayjs/locale/id'


const formatingDate = (val) => {
  const locale = dayjs(val).locale('id')
  const date = locale.format('DD MMM YYYY')
  const time = locale.format('HH:mm')
  return (
    `${date} | ${time}`
  )
}
const formatingCurrency = (val) => (val ? Func.currencyFormatter(val) : 'Rp. 0')

const columns = [
  {
    name: 'id',
    label: 'ID',
    display: false,
    options: {
      filter: false,
      sort: false
    }
  },
  {
    name: 'ref',
    label: 'No. Referensi',
    options: {
      filter: true,
      sort: false
    }
  },
  {
    name: 'publish_date',
    label: 'Tanggal',
    customBodyRender: (evt, val) => formatingDate(val),
    options: {
      filter: true,
      sort: false
    }
  },
  {
    name: 'description',
    label: 'Keterangan',
    options: {
      filter: true,
      sort: false
    }
  },
  {
    name: 'debit',
    label: 'Debit',
    customBodyRender: (evt, val) => formatingCurrency(val),
    options: {
      filter: true,
      sort: false
    }
  },
  {
    name: 'credit',
    label: 'Kredit',
    customBodyRender: (evt, val) => formatingCurrency(val),
    options: {
      filter: true,
      sort: false
    }
  }
];

export default columns;
