/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable radix */
/* eslint-disable react/button-has-type */
/* eslint-disable camelcase */
/* eslint-disable prefer-destructuring */
/* eslint-disable eqeqeq */
/* eslint-disable react/sort-comp */
/* eslint-disable react/no-access-state-in-setstate */
/* eslint-disable react/no-unused-state */
/* eslint-disable no-shadow */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable no-unused-vars */
import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Divider from '@material-ui/core/Divider';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import DialogContentText from '@material-ui/core/DialogContentText';
import MuiDialogActions from '@material-ui/core/DialogActions';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Button from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress';
import Func from '../../../../../functions/index';
import Icon from '../../../../../components/icon';
import styles from '../css';
import env from '../../../../../config/env';
import '../../../../../styles/date_field.css';

const DialogActions = withStyles((theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(1),
  },
}))(MuiDialogActions);

class Form extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: false,
      validator: [],
      value: {
        amount: '',
      },
      office_name: '',
      name: '',
    };
  }

  componentDidMount() {
    this.getAkun('', '');
  }

  getAkun(val) {
    this.setState({ loader: true });
    fetch(
      env.financialApi + env.apiPrefixV1 + '/accounts/autocomplete?query=' + val,
      {
        method: 'GET',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + localStorage.getItem('token'),
        },
      }
    )
      .then((response) => response.json())
      .then((json) => {
        if (json.code == '403') {
          if (Func.Clear_Token() == true) {
            if (!localStorage.getItem('token')) {
              this.setState({ redirect: true });
            }
          }
        } else {
          const datas = [];
          datas.push({
            value: '-',
            label: json.data.length > 0 ? 'Pilih' : 'Tidak ditemukan',
            isDisabled: true,
          });
          // eslint-disable-next-line array-callback-return
          json.data.map((value) => {
            datas.push({
              label: value.account_number + ' - ' + value.item,
              value: value.account_number,
            });
          });

          this.setState(
            {
              data5: datas,
              data5_ori: json.data,
            },
            () => {
              if (this.props.type == 'Ubah') {
                fetch(
                  env.managementApi + env.apiPrefixV1 + '/cashout_transactions/' + this.props.id,
                  {
                    method: 'GET',
                    headers: {
                      Accept: 'application/json',
                      'Content-Type': 'application/json',
                      Authorization: 'Bearer ' + localStorage.getItem('token'),
                    },
                  }
                )
                  .then((response) => response.json())
                  .then((json) => {
                    if (json.code == '403') {
                      if (Func.Clear_Token() == true) {
                        if (!localStorage.getItem('token')) {
                          this.setState({ redirect: true });
                        }
                      }
                    }
                    const val = [];
                    const data = json.data.cashout_transactions;
                    const search_debit = this.state.data5_ori.find(
                      (o) => o.account_number === parseInt(data.debit.coa)
                    );
                    const search_credit = this.state.data5_ori.find(
                      (o) => o.account_number === parseInt(data.credit.coa)
                    );
                    const search_account = this.state.data5_ori.find(
                      (o) => o.account_number === parseInt(data.account.coa)
                    );
                    val.date = Func.FormatDate(data.date);
                    val.description = data.description;
                    val.ref = data.ref;
                    val.amount = data.amount;
                    val.coa_debit = {
                      value: search_debit.account_number,
                      label:
                        search_debit.account_number + ' - ' + search_debit.item,
                    };
                    val.desc_debit = data.debit.description;
                    val.id_debit = data.debit.id;
                    val.coa_credit = {
                      value: search_credit.account_number,
                      label:
                        search_credit.account_number + ' - ' + search_credit.item,
                    };
                    val.desc_credit = data.credit.description;
                    val.id_credit = data.credit.id;
                    val.coa_account = {
                      value: data.account.coa,
                      label:
                        data.account.coa + ' - ' + data.account.description,
                    };
                    val.coa_account = {
                      value: search_account.account_number,
                      label:
                        search_account.account_number + ' - ' + search_account.item,
                    };
                    val.desc_account = data.account.description;
                    val.id_account = data.account.id;

                    this.setState({ value: val, loader: false });
                  })
                  .catch((error) => {})
                  .finally(() => {});
              }
            }
          );
        }
      })
      .catch((error) => {})
      .finally(() => {});
  }

  render() {
    const loadOptions2 = (inputValue, callback) => {
      setTimeout(() => {
        callback(this.state.data5);
      }, 4000);
    };

    const { classes } = this.props;
    const ExampleCustomInput = ({ value, onClick }) => (
      <img src={Icon.icon_date} onClick={onClick} />
    );

    if (this.state.loader) {
      return (
        <div className={classes.root2}>
          <Dialog
            disablePortal
            disableEnforceFocus
            disableAutoFocus
            open
            scroll="paper"
            maxWidth="md"
            aria-labelledby="server-modal-title"
            aria-describedby="server-modal-description"
            container={() => {}}
          >
            <div
              style={{
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
                padding: '20px',
                borderRadius: '50px',
              }}
            >
              <CircularProgress
                style={{ color: '#85203B', margin: '18px' }}
                size={40}
              />
              Mohon Tunggu
              <div style={{ marginTop: '10px' }} />
            </div>
          </Dialog>
        </div>
      );
    }
    return (
      <Dialog
        scroll="paper"
        open
        maxWidth="md"
        aria-labelledby="scroll-dialog-title"
        aria-describedby="scroll-dialog-description"
      >
        <DialogTitle id="scroll-dialog-title">
          <Typography variant="h7" className={classes.tittleModal}>
            Detail Pengeluaran Kas
          </Typography>
          <IconButton
            disabled={this.state.loader_button}
            aria-label="close"
            className={classes.closeButton}
            onClick={() => {
              this.props.handleModal();
            }}
          >
            <CloseIcon />
          </IconButton>
        </DialogTitle>
        <DialogContent dividers>
          <DialogContentText id="scroll-dialog-description" tabIndex={-1}>
            <div>
              <div className={classes.scrool}>
                {Func.toLogin(this.state.redirect)}
                <div className={classes.root}>
                  <Grid
                    container
                    direction="row"
                    item
                    lg={12}
                    xl={12}
                    md={12}
                    xs={12}
                  >
                    <Grid
                      container
                      item
                      lg={12}
                      xl={12}
                      md={12}
                      sm={12}
                      xs={12}
                    >
                      <Grid item lg={6} xl={6} md={6} sm={6} xs={12}>
                        <div>
                          <text className={classes.label121}>Tanggal</text>
                          <text className={classes.starts1}>*</text>
                        </div>
                        <div className={classes.label1112}>
                          <text className={classes.label1}>
                            {this.state.value.date}
                          </text>
                        </div>
                      </Grid>
                      <Grid item lg={6} xl={6} md={6} sm={6} xs={12}>
                        <div>
                          <text className={classes.label121}>No Referensi</text>
                        </div>
                        <div className={classes.label1112}>
                          <text className={classes.label1}>
                            {this.state.value.ref}
                          </text>
                        </div>
                      </Grid>
                    </Grid>
                    <Divider className={classes.divider} />
                    <Grid
                      container
                      item
                      lg={12}
                      xl={12}
                      md={12}
                      sm={12}
                      xs={12}
                    >
                      <Grid item lg={6} xl={6} md={6} sm={6} xs={12}>
                        <div>
                          <text className={classes.label121}>
                            No. Akun Debit
                          </text>
                          <text className={classes.starts1}>*</text>
                        </div>
                        <div className={classes.label1112}>
                          <text className={classes.label1}>
                            {this.state.value.coa_debit == undefined
                              ? ' '
                              : this.state.value.coa_debit.label}
                          </text>
                        </div>
                      </Grid>
                      <Grid item lg={6} xl={6} md={6} sm={6} xs={12}>
                        <div>
                          <text className={classes.label121}>
                            Keterangan Akun Debit
                          </text>
                          <text className={classes.starts1}>*</text>
                        </div>
                        <div className={classes.label1112}>
                          <text className={classes.label1}>
                            {this.state.value.desc_debit}
                          </text>
                        </div>
                      </Grid>
                    </Grid>
                    <Grid
                      container
                      item
                      lg={12}
                      xl={12}
                      md={12}
                      sm={12}
                      xs={12}
                    >
                      <Grid item lg={6} xl={6} md={6} sm={6} xs={12}>
                        <div>
                          <text className={classes.label121}>
                            No. Akun Kredit
                          </text>
                          <text className={classes.starts1}>*</text>
                        </div>
                        <div className={classes.label1112}>
                          <text className={classes.label1}>
                            {this.state.value.coa_credit == undefined
                              ? ' '
                              : this.state.value.coa_credit.label}
                          </text>
                        </div>
                      </Grid>
                      <Grid item lg={6} xl={6} md={6} sm={6} xs={12}>
                        <div>
                          <text className={classes.label121}>
                            Keterangan Akun Kredit
                          </text>
                          <text className={classes.starts1}>*</text>
                        </div>
                        <div className={classes.label1112}>
                          <text className={classes.label1}>
                            {this.state.value.desc_credit}
                          </text>
                        </div>
                      </Grid>
                    </Grid>
                    <Grid
                      container
                      item
                      lg={12}
                      xl={12}
                      md={12}
                      sm={12}
                      xs={12}
                    >
                      <Grid item lg={6} xl={6} md={6} sm={6} xs={12}>
                        <div>
                          <text className={classes.label121}>No. Akun</text>
                          <text className={classes.starts1}>*</text>
                        </div>
                        <div className={classes.label1112}>
                          <text className={classes.label1}>
                            {this.state.value.coa_account == undefined
                              ? ' '
                              : this.state.value.coa_account.label}
                          </text>
                        </div>
                      </Grid>
                      <Grid item lg={6} xl={6} md={6} sm={6} xs={12}>
                        <div>
                          <text className={classes.label121}>
                            Keterangan Akun
                          </text>
                          <text className={classes.starts1}>*</text>
                        </div>
                        <div className={classes.label1112}>
                          <text className={classes.label1}>
                            {this.state.value.desc_account}
                          </text>
                        </div>
                      </Grid>
                    </Grid>
                    <Grid item lg={12} xl={12} md={12} sm={12} xs={12}>
                      <div>
                        <text className={classes.label121}>Nominal</text>
                        <text className={classes.starts1}>*</text>
                      </div>
                      <div className={classes.label1112}>
                        <text className={classes.label1}>
                          {Func.FormatNumber(this.state.value.amount)}
                        </text>
                      </div>
                    </Grid>
                    <Grid item lg={12} xl={12} md={12} sm={12} xs={12}>
                      <div>
                        <text className={classes.label121}>Keterangan</text>
                        <text className={classes.starts1}>*</text>
                      </div>
                      <div className={classes.label1112}>
                        <text className={classes.label1}>
                          {this.state.value.description}
                        </text>
                      </div>
                    </Grid>
                  </Grid>
                </div>
              </div>
            </div>
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button
            style={{
              color: 'black',
            }}
            disabled={this.state.loader_button}
            onClick={() => {
              this.props.handleModal();
            }}
            variant="outlined"
          >
            <Typography variant="button">Batal</Typography>
          </Button>
        </DialogActions>
      </Dialog>
    );
  }
}

export default withStyles(styles.CoustomsStyles, { name: 'Form' })(Form);
