/* eslint-disable react/jsx-closing-tag-location */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable array-callback-return */
/* eslint-disable consistent-return */
/* eslint-disable no-redeclare */
/* eslint-disable no-var */
/* eslint-disable block-scoped-var */
/* eslint-disable vars-on-top */
/* eslint-disable radix */
/* eslint-disable react/button-has-type */
/* eslint-disable camelcase */
/* eslint-disable prefer-destructuring */
/* eslint-disable eqeqeq */
/* eslint-disable react/sort-comp */
/* eslint-disable react/no-access-state-in-setstate */
/* eslint-disable react/no-unused-state */
/* eslint-disable no-shadow */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable no-unused-vars */
import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import InputAdornment from '@material-ui/core/InputAdornment';
import Divider from '@material-ui/core/Divider';
import Typography from '@material-ui/core/Typography';
import swal from 'sweetalert';
import FormHelperText from '@material-ui/core/FormHelperText';
import Grid from '@material-ui/core/Grid';
import TextareaAutosize from '@material-ui/core/TextareaAutosize';
import DialogContentText from '@material-ui/core/DialogContentText';
import MuiDialogActions from '@material-ui/core/DialogActions';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Button from '@material-ui/core/Button';
import AsyncSelect from 'react-select/async';
import { CircularProgress } from '@material-ui/core';
import Func from '../../../../../functions/index';
import Icon from '../../../../../components/icon';
import styles from '../css';
import env from '../../../../../config/env';
import '../../../../../styles/date_field.css';

const DialogActions = withStyles((theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(1)
  }
}))(MuiDialogActions);

class Form extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: false,
      validator: [],
      value: {
        amount: ''
      },
      office_name: '',
      name: ''
    };
    this.timeout = 0;
    this.handleChangeImg = this
      .handleChangeImg
      .bind(this);
  }

  removeValidate(name) {
    const data = this.state.validator;
    delete data[name];
    this.setState({ validator: data });
  }

  handleChange(event, name) {
    const dataSet = this.state.value;
    if (name == 'coa_debit') {
      dataSet.desc_debit = event.desc;
      this.removeValidate('desc_debit');
    }
    if (name == 'coa_credit') {
      dataSet.desc_credit = event.desc;
      this.removeValidate('desc_credit');
    }
    if (name == 'coa_account') {
      dataSet.desc_account = event.desc;
      this.removeValidate('desc_account');
    }
    dataSet[name] = event;
    this.setState({ value: dataSet });
  }

  handleChangeDate(date, name, name2) {
    const dt = new Date(date);
    const dataSet = this.state.value;
    dataSet[name2] = dt;
    dataSet[name] = dt.getFullYear() + '-' + parseInt(dt.getMonth() + 1) + '-' + dt.getDate();
    this.setState({ value: dataSet });
  }

  componentDidMount() {
    this.setState({ loader: true });
    this.getAkun('', '');
  }

  handleSubmit(type) {
    this.setState({ loader_button: true });
    const validator = [
      { name: 'date',
        type: 'required'
      }, {
        name: 'amount',
        type: 'required'
      }, {
        name: 'description',
        type: 'required'
      }, {
        name: 'coa_debit',
        type: 'required'
      }, {
        name: 'desc_debit',
        type: 'required'
      }, {
        name: 'coa_credit',
        type: 'required'
      }, {
        name: 'desc_credit',
        type: 'required'
      }, {
        name: 'coa_account',
        type: 'required'
      }, {
        name: 'desc_account',
        type: 'required'
      }, {
        name: 'description',
        type: 'required'
      }, {
        name: 'amount',
        type: 'required'
      }
    ];

    const validate = Func.Validator(this.state.value, validator);
    if (validate.success) {
      if (type == 'Tambah') {
        var nested = [
          {
            coa: this.state.value.coa_debit.value,
            description: this.state.value.desc_debit,
            type_account: 'debit',
          },
          {
            coa: this.state.value.coa_credit.value,
            description: this.state.value.desc_credit,
            type_account: 'credit',
          },
          {
            coa: this.state.value.coa_account.value,
            description: this.state.value.desc_account,
            type_account: 'account',
          }
        ];
      } else {
        var nested = [
          {
            id: this.state.value.id_debit,
            coa: this.state.value.coa_debit.value,
            description: this.state.value.desc_debit,
            type_account: 'debit',
          },
          {
            id: this.state.value.id_credit,
            coa: this.state.value.coa_credit.value,
            description: this.state.value.desc_credit,
            type_account: 'credit',
          },
          {
            id: this.state.value.id_account,
            coa: this.state.value.coa_account.value,
            description: this.state.value.desc_account,
            type_account: 'account',
          }
        ];
      }
      fetch(env.managementApi + env.apiPrefixV1 + '/cashout_transactions/' + (type == 'Tambah'
        ? ''
        : this.props.id), {
        method: type == 'Tambah'
          ? 'POST'
          : 'PUT',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + localStorage.getItem('token')
        },
        body: JSON.stringify({
          date: this.state.value.date,
          ref: this.state.value.ref,
          amount: this.state.value.amount,
          description: this.state.value.description,
          cashout_accounts_attributes: nested
        })
      }).then((response) => response.json()).then((json) => {
        if (json.code == '403') {
          Func.Refresh_Token();
          if (Func.Refresh_Token() == true) {
            this.handleSubmit();
          }
        }
        if (type == 'Tambah') {
          if (json.created) {
            this
              .props
              .OnNext(json.message);
            this.setState({ loader_button: false });
          } else {
            Func.AlertError(json.message);
            this.setState({ loader_button: false });
          }
        } else if (json.code == 200) {
          this
            .props
            .OnNext(json.message);
          this.setState({ loader_button: false });
        } else {
          Func.AlertError(json.message);
          this.setState({ loader_button: false });
        }
      }).catch((error) => {})
        . finally(() => {});
    } else {
      this.setState({ validator: validate.error });
      this.setState({ loader_button: false });
    }
  }

  handleChangeImg(event) {
    const file = event.target.files[0];
    if (file.size > 2.9e6) {
      swal({ title: 'File Terlalu Besar', text: 'Maximal File 2Mb', icon: 'error', buttons: 'OK' });
      return false;
    }
    this.removeValidate('img');
    const dataSet = this.state.value;
    dataSet.img = URL.createObjectURL(event.target.files[0]);
    this.setState({ value: dataSet });
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onloadend = () => {
      this.setState({ ImgBase44: reader.result });
    };
  }

  getAkun(val, name) {
    fetch(env.financialApi + env.apiPrefixV1 + '/accounts/autocomplete?query=' + val, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + localStorage.getItem('token')
      }
    }).then((response) => response.json()).then((json) => {
      if (json.code == '403') {
        if (Func.Clear_Token() == true) {
          if (!localStorage.getItem('token')) {
            this.setState({ redirect: true });
          }
        }
      } else {
        const datas = [];
        datas.push({
          value: '-',
          label: json.data.length > 0
            ? 'Pilih'
            : 'Tidak ditemukan',
          isDisabled: true
        });
        json
          .data
          .map((value) => {
            datas.push({
              label: value.account_number,
              value: value.account_number,
              desc: value.item
            });
          });

        this.setState({
          data5: datas,
          data5_ori: json.data
        }, () => {
          if (this.props.type == 'Ubah') {
            fetch(env.managementApi + env.apiPrefixV1 + '/cashout_transactions/' + this.props.id, {
              method: 'GET',
              headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + localStorage.getItem('token')
              }
            }).then((response) => response.json()).then((json) => {
              if (json.code == '403') {
                if (Func.Clear_Token() == true) {
                  if (!localStorage.getItem('token')) {
                    this.setState({ redirect: true });
                  }
                }
              }
              const val = [];
              const data = json.data.cashout_transactions;
              const search_debit = this.state.data5_ori.find(o => o.account_number === parseInt(data.debit.coa));
              const search_credit = this.state.data5_ori.find(o => o.account_number === parseInt(data.credit.coa));
              const search_account = this.state.data5_ori.find(o => o.account_number === parseInt(data.account.coa));
              const d = new Date(data.date);
              val.date = d.getFullYear() + '-' + parseInt(d.getMonth() + 1) + '-' + (parseInt(d.getDate()) < 9 ? '0' + parseInt(d.getDate()) : parseInt(d.getDate()));
              val.description = data.description;
              val.ref = data.ref;
              val.amount = data.amount;
              val.coa_debit = {
                value: search_debit.account_number,
                label: search_debit.account_number,
                desc: search_debit.item
              };
              val.desc_debit = data.debit.description;
              val.id_debit = data.debit.id;
              val.coa_credit = {
                value: search_credit.account_number,
                label: search_credit.account_number,
                desc: search_credit.item
              };
              val.desc_credit = data.credit.description;
              val.id_credit = data.credit.id;
              val.coa_account = {
                value: data.account.coa,
                label: data.account.coa,
                desc: data.account.description
              };
              val.coa_account = {
                value: search_account.account_number,
                label: search_account.account_number,
                desc: search_account.item
              };
              val.desc_account = data.account.description;
              val.id_account = data.account.id;

              this.setState({ value: val });
              this.setState({ loader: false });
            }).catch((error) => {})
              . finally(() => {});
          } else {
            this.setState({ loader: false });
          }
        });
      }
    }).catch((error) => {})
      . finally(() => {});
  }

  render() {
    const loadOptions2 = (inputValue, callback) => {
      setTimeout(() => {
        callback(this.state.data5);
      }, 600);
    };

    const { classes } = this.props;
    const ExampleCustomInput = ({ value, onClick }) => (<img src={Icon.icon_date} onClick={onClick} />);

    if (this.state.loader) {
      return (
        <div className={classes.root2}>
          <Dialog
            disablePortal
            disableEnforceFocus
            disableAutoFocus
            open
            scroll="paper"
            maxWidth="md"
            aria-labelledby="server-modal-title"
            aria-describedby="server-modal-description"
            container={() => {}}
          >
            <div
              style={{
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
                padding: '20px',
                borderRadius: '50px',
              }}
            >
              <CircularProgress
                style={{ color: '#85203B', margin: '18px' }}
                size={40}
              />
              Mohon Tunggu
              <div
                style={{
                  marginTop: '10px',
                }}
              />
            </div>
          </Dialog>
        </div>
      );
    }
    return (
      <Dialog
        scroll="paper"
        open
        maxWidth="md"
        aria-labelledby="scroll-dialog-title"
        aria-describedby="scroll-dialog-description"
      >
        <DialogTitle id="scroll-dialog-title">
          <Typography variant="h7" className={classes.tittleModal}>
            {this.props.type + ' Pengeluaran Kas'}
          </Typography>
          <IconButton
            disabled={this.state.loader_button}
            aria-label="close"
            className={classes.closeButton}
            onClick={() => {
              this.props.handleModal();
            }}
          >
            <CloseIcon />
          </IconButton>
        </DialogTitle>
        <DialogContent dividers>
          <DialogContentText id="scroll-dialog-description" tabIndex={-1}>
            <div className={classes.scrool}>
              {Func.toLogin(this.state.redirect)}
              <div className={classes.root}>
                <Grid container direction="row" item lg={12} xl={12} md={12} xs={12}>
                  <Grid container item lg={12} xl={12} md={12} sm={12} xs={12}>
                    <Grid item lg={6} xl={6} md={6} sm={6} xs={12}>
                      <div>
                        <text className={classes.label1}>
                          Tanggal
                        </text>
                        <text className={classes.starts1}>*</text>
                      </div>
                      <TextField
                        size="small"
                        fullWidth
                        type="date"
                        className={classes.input2}
                        variant="outlined"
                        autoComplete="off"
                        onFocus={() => {
                          this.removeValidate('date');
                        }}
                        error={this.state.validator.date}
                        helperText={this.state.validator.date}
                        value={this.state.value.date}
                        onChange={(event) => {
                          this.handleChange(event.target.value, 'date');
                        }}
                        name="date"
                      />
                    </Grid>
                    <Grid item lg={6} xl={6} md={6} sm={6} xs={12}>
                      <div>
                        <text className={classes.label1}>
                          No Referensi
                        </text>
                      </div>
                      <TextField
                        size="small"
                        fullWidth
                        className={classes.input2}
                        variant="outlined"
                        autoComplete="off"
                        onFocus={() => {
                          this.removeValidate('ref');
                        }}
                        error={this.state.validator.ref}
                        helperText={this.state.validator.ref}
                        value={this.state.value.ref}
                        onChange={(event) => {
                          this.handleChange(event.target.value, 'ref');
                        }}
                        name="ref"
                      />
                    </Grid>
                  </Grid>
                  <Divider className={classes.divider} />
                  <Grid container item lg={12} xl={12} md={12} sm={12} xs={12}>
                    <Grid item lg={6} xl={6} md={6} sm={6} xs={12}>
                      <div>
                        <text className={classes.label1}>No. Akun Debit</text>
                        <text className={classes.starts1}>*</text>
                      </div>
                      <AsyncSelect
                        name="form-field-name-error"
                        placeholder="Cari Akun"
                        value={this.state.value.coa_debit}
                        onFocus={() => {
                          this.removeValidate('coa_debit');
                        }}
                        styles={{
                          control: (provided, state) => ({
                            ...provided,
                            borderColor: this.state.validator.coa_debit
                              ? 'red'
                              : '#CACACA',
                            borderRadius: '0.25rem'
                          })
                        }}
                        onInputChange={(val) => {
                          if (val) {
                            if (this.timeout) clearTimeout(this.timeout);
                            this.timeout = setTimeout(() => {
                              this.getAkun(val, '');
                            }, 500);
                          }
                        }}
                        cacheOptions
                        loadOptions={loadOptions2}
                        defaultOptions
                        className={classes.input2}
                        options={this.state.data5}
                        onChange={(val) => {
                          this.handleChange(val, 'coa_debit');
                        }}
                      />
                      <FormHelperText className={classes.error}>
                        {this.state.validator.coa_debit}
                      </FormHelperText>
                    </Grid>
                    <Grid item lg={6} xl={6} md={6} sm={6} xs={12}>
                      <div>
                        <text className={classes.label1}>
                          Keterangan Akun Debit
                        </text>
                        <text className={classes.starts1}>*</text>
                      </div>
                      <TextField
                        size="small"
                        fullWidth
                        className={classes.input2}
                        variant="outlined"
                        autoComplete="off"
                        onFocus={() => {
                          this.removeValidate('desc_debit');
                        }}
                        error={this.state.validator.desc_debit}
                        helperText={this.state.validator.desc_debit}
                        value={this.state.value.desc_debit}
                        onChange={(event) => {
                          this.handleChange(event.target.value, 'desc_debit');
                        }}
                        name="desc_debit"
                      />
                    </Grid>
                  </Grid>
                  <Grid container item lg={12} xl={12} md={12} sm={12} xs={12}>
                    <Grid item lg={6} xl={6} md={6} sm={6} xs={12}>
                      <div>
                        <text className={classes.label1}>No. Akun Kredit</text>
                        <text className={classes.starts1}>*</text>
                      </div>
                      <AsyncSelect
                        name="form-field-name-error"
                        placeholder="Cari Akun"
                        value={this.state.value.coa_credit}
                        onFocus={() => {
                          this.removeValidate('coa_credit');
                        }}
                        styles={{
                          control: (provided, state) => ({
                            ...provided,
                            borderColor: this.state.validator.coa_credit
                              ? 'red'
                              : '#CACACA',
                            borderRadius: '0.25rem'
                          })
                        }}
                        onInputChange={(val) => {
                          if (val) {
                            if (this.timeout) clearTimeout(this.timeout);
                            this.timeout = setTimeout(() => {
                              this.getAkun(val, '');
                            }, 500);
                          }
                        }}
                        cacheOptions
                        loadOptions={loadOptions2}
                        defaultOptions
                        className={classes.input2}
                        options={this.state.data5}
                        onChange={(val) => {
                          this.handleChange(val, 'coa_credit');
                        }}
                      />
                      <FormHelperText className={classes.error}>
                        {this.state.validator.coa_credit}
                      </FormHelperText>
                    </Grid>
                    <Grid item lg={6} xl={6} md={6} sm={6} xs={12}>
                      <div>
                        <text className={classes.label1}>
                          Keterangan Akun Kredit
                        </text>
                        <text className={classes.starts1}>*</text>
                      </div>
                      <TextField
                        size="small"
                        fullWidth
                        className={classes.input2}
                        variant="outlined"
                        autoComplete="off"
                        onFocus={() => {
                          this.removeValidate('desc_credit');
                        }}
                        error={this.state.validator.desc_credit}
                        helperText={this.state.validator.desc_credit}
                        value={this.state.value.desc_credit}
                        onChange={(event) => {
                          this.handleChange(event.target.value, 'desc_credit');
                        }}
                        name="desc_credit"
                      />
                    </Grid>
                  </Grid>
                  <Grid container item lg={12} xl={12} md={12} sm={12} xs={12}>
                    <Grid item lg={6} xl={6} md={6} sm={6} xs={12}>
                      <div>
                        <text className={classes.label1}>No. Akun</text>
                        <text className={classes.starts1}>*</text>
                      </div>
                      <AsyncSelect
                        name="form-field-name-error"
                        placeholder="Cari Akun"
                        value={this.state.value.coa_account}
                        onFocus={() => {
                          this.removeValidate('coa_account');
                        }}
                        styles={{
                          control: (provided, state) => ({
                            ...provided,
                            borderColor: this.state.validator.coa_account
                              ? 'red'
                              : '#CACACA',
                            borderRadius: '0.25rem'
                          })
                        }}
                        onInputChange={(val) => {
                          if (val) {
                            if (this.timeout) clearTimeout(this.timeout);
                            this.timeout = setTimeout(() => {
                              this.getAkun(val, '');
                            }, 500);
                          }
                        }}
                        cacheOptions
                        loadOptions={loadOptions2}
                        defaultOptions
                        className={classes.input2}
                        options={this.state.data5}
                        onChange={(val) => {
                          this.handleChange(val, 'coa_account');
                        }}
                      />
                      <FormHelperText className={classes.error}>
                        {this.state.validator.coa_account}
                      </FormHelperText>
                    </Grid>
                    <Grid item lg={6} xl={6} md={6} sm={6} xs={12}>
                      <div>
                        <text className={classes.label1}>
                          Keterangan Akun
                        </text>
                        <text className={classes.starts1}>*</text>
                      </div>
                      <TextField
                        size="small"
                        fullWidth
                        className={classes.input2}
                        variant="outlined"
                        autoComplete="off"
                        onFocus={() => {
                          this.removeValidate('desc_account');
                        }}
                        error={this.state.validator.desc_account}
                        helperText={this.state.validator.desc_account}
                        value={this.state.value.desc_account}
                        onChange={(event) => {
                          this.handleChange(event.target.value, 'desc_account');
                        }}
                        name="desc_account"
                      />
                    </Grid>
                  </Grid>
                  <Grid item lg={12} xl={12} md={12} sm={12} xs={12}>
                    <div>
                      <text className={classes.label1}>
                        Nominal
                      </text>
                      <text className={classes.starts1}>*</text>
                    </div>
                    <TextField
                      size="small"
                      fullWidth
                      className={classes.input2}
                      variant="outlined"
                      autoComplete="off"
                      onFocus={() => {
                        this.removeValidate('amount');
                      }}
                      error={this.state.validator.amount}
                      helperText={this.state.validator.amount}
                      value={Func.FormatNumber(this.state.value.amount)}
                      onChange={(event) => {
                        this.handleChange(Func.UnFormatRp(event.target.value), 'amount');
                      }}
                      name="amount"
                      InputProps={{
                        startAdornment: <InputAdornment position="start">
                          Rp
                        </InputAdornment>
                      }}
                    />
                  </Grid>
                  <Grid item lg={12} xl={12} md={12} sm={12} xs={12}>
                    <div>
                      <text className={classes.label1}>
                        Keterangan
                      </text>
                      <text className={classes.starts1}>*</text>
                    </div>
                    <div>
                      <TextareaAutosize
                        className={this.state.validator.description
                          ? classes.textArea2
                          : classes.textArea}
                        variant="outlined"
                        margin="normal"
                        rows={8}
                        autoComplete="off"
                        onFocus={() => {
                          this.removeValidate('description');
                        }}
                        error={this.state.validator.description}
                        value={this.state.value.description}
                        onChange={(event) => {
                          this.handleChange(event.target.value, 'description');
                        }}
                        name="description"
                        InputProps={{
                          endAdornment: this.state.validator.description
                            ? (
                              <InputAdornment position="start">
                                <img src={Icon.warning} />
                              </InputAdornment>
                            )
                            : (<div />)
                        }}
                      />
                      <FormHelperText className={classes.error}>
                        {this.state.validator.description}
                      </FormHelperText>
                    </div>
                  </Grid>
                </Grid>
              </div>
            </div>
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button
            style={{
              backgroundColor: '#862C3A',
              color: 'white',
            }}
            disabled={this.state.loader_button}
            onClick={() => {
              this.handleSubmit(this.props.type);
            }}
            variant="contained"
          >
            <Typography variant="button" style={{ color: '#FFFFFF' }}>
              {this.state.loader_button ? (
                <CircularProgress
                  style={{
                    color: 'white',
                    marginLeft: '18px',
                    marginRight: '18px',
                    marginTop: 5,
                  }}
                  size={15}
                />
              ) : (
                'Simpan'
              )}
            </Typography>
          </Button>
          <Button
            style={{
              color: 'black',
            }}
            disabled={this.state.loader_button}
            onClick={() => {
              this.props.handleModal();
            }}
            variant="outlined"
          >
            <Typography variant="button">Batal</Typography>
          </Button>
        </DialogActions>
      </Dialog>
    );
  }
}

export default withStyles(styles.CoustomsStyles, { name: 'Form' })(Form);
