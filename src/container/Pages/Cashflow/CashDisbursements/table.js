/* eslint-disable react/jsx-indent */
/* eslint-disable no-unused-vars */
/* eslint-disable react/no-access-state-in-setstate */
/* eslint-disable no-redeclare */
/* eslint-disable block-scoped-var */
/* eslint-disable no-var */
/* eslint-disable vars-on-top */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable class-methods-use-this */
/* eslint-disable react/sort-comp */
/* eslint-disable no-dupe-keys */
/* eslint-disable react/no-unused-state */
/* eslint-disable camelcase */
/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable array-callback-return */
import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import InputBase from '@material-ui/core/InputBase';
import FilterListIcon from '@material-ui/icons/FilterList';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import Swal from 'sweetalert2';
import Pagination from '@material-ui/lab/Pagination';
import { Hidden, Button } from '@material-ui/core';
import BeatLoader from 'react-spinners/BeatLoader';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import Tooltip from '@material-ui/core/Tooltip';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Divider from '@material-ui/core/Divider';
import Select2 from 'react-select';
import styles from './css';
import FormCountry from './form/index';
import Icon from '../../../../components/icon';
import Func from '../../../../functions/index';
import env from '../../../../config/env';
import './date_field.css';

function createData(id, user_name, date, coa, description, amount, status) {
  return {
    id,
    user_name,
    date,
    coa,
    description,
    amount,
    status,
  };
}

const headCells = [
  {
    id: 'id',
    numeric: false,
    disablePadding: false,
    label: 'Kasir',
  },
  {
    id: 'name',
    numeric: false,
    disablePadding: false,
    label: 'Tanggal',
  },
  {
    id: 'hp',
    numeric: false,
    disablePadding: false,
    label: 'No. Akun',
  },
  {
    id: 'description',
    numeric: false,
    disablePadding: false,
    label: 'Description',
  },
  {
    id: 'os',
    numeric: false,
    disablePadding: false,
    label: 'Nominal',
  },
  {
    id: 'os',
    numeric: false,
    disablePadding: false,
    label: 'Status',
  },
  {
    id: 'action',
    numeric: false,
    disablePadding: false,
    label: '',
  },
];

const BootstrapInput = withStyles((theme) => ({
  root: {
    'label + &': {
      marginTop: theme.spacing(3),
    },
  },
  input: {
    borderRadius: 4,
    position: 'relative',
    backgroundColor: theme.palette.background.paper,
    border: '1px solid #ced4da',
    fontSize: 16,
    padding: '10px 26px 10px 12px',
    transition: theme.transitions.create(['border-color', 'box-shadow']),
    fontFamily: [
      '-apple-system',
      'BlinkMacSystemFont',
      '"Segoe UI"',
      'Roboto',
      '"Helvetica Neue"',
      'Arial',
      'sans-serif',
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(','),
    '&:focus': {
      borderRadius: 4,
      borderColor: '#80bdff',
      boxShadow: '0 0 0 0.2rem rgba(0,123,255,.25)',
    },
  },
}))(InputBase);

class Tables extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      total_data: 0,
      total_page: 1,
      current_page: 1,
      prev_page: null,
      data: [],
      next_page: 1,
      rowsPerPage: 10,
      openSearch: false,
      filterValue: {
        amount: '',
      },
      rowFocus: '',
      page: 1,
      filter: false,
      count: 1,
      data: [],
      loading: true,
      modal: false,
      selectAll: false,
      order: 'asc',
      rowSelected: [],
      bulk: false,
      redirect: false,
      failure: false,
    };
    this.ChangePage = this.ChangePage.bind(this);
  }

  handleClick(id) {
    alert(id);
  }

  ChangePage(event, page) {
    this.setState(
      {
        page,
      },
      () => {
        this.getData();
      }
    );
  }

  setFocus(row) {
    this.setState({ rowFocus: row.id });
  }

  setUnfocus() {
    setTimeout(() => {
      this.setState({ rowFocus: '' });
    }, 5500);
  }

  handleModal(row) {
    this.setState(
      {
        modal2: false,
        modal: true,
        row,
      },
      () => {
        this.setState({ modal: false, bulk: false });
      }
    );
  }

  handleShow(row) {
    this.setState(
      {
        modal2: true,
        modal: false,
        row,
      },
      () => {
        this.setState({ modal2: false, bulk: false });
      }
    );
  }

  componentDidMount() {
    this.getData('first');
  }

  handleClickDelete = (row) => {
    Swal.fire({
      title: 'Apakah Anda yakin?',
      text: 'Akan menghapus data yang dipilih',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Ya, Hapus!',
      cancelButtonText: 'Batal',
    }).then((result) => {
      if (result.value) {
        fetch(
          env.managementApi + env.apiPrefixV1 + '/' + this.props.path + '/' + row,
          {
            method: 'DELETE',
            headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json',
              Authorization: 'Bearer ' + localStorage.getItem('token'),
            },
          }
        )
          .then((response) => response.json())
          .then((json) => {
            if (json.success) {
              this.setState({ rowSelected: [] });
              Swal.fire('Berhasil!', 'Data berhasil dihapus.', 'success');
            } else {
              Swal.fire('Gagal!', json.message, 'warning');
            }
            this.getData('first');
          })
          .catch(() => {})
          .finally(() => {});
      }
    });
  };

  getData = () => {
    this.setState({ loading: true });
    const q = this.state.filterValue;
    const a = {
      date_eq: q.date === undefined ? '' : q.date,
      user_name_cont: q.user_name === undefined ? '' : q.user_name,
      status_eq: q.status === undefined ? '' : q.status.value,
      cashout_accounts_coa_eq: q.coa === undefined ? '' : q.coa,
      amount_eq: q.amount === undefined ? '' : q.amount,
    };
    const serialize = '&' + Func.serialize(a, 'q');
    Func.getDataTransaction3(
      this.props.path,
      this.state.rowsPerPage,
      this.state.page,
      serialize
    ).then((res) => {
      if (res.error) {
        this.setState({
          con: 0,
          data: [],
          loading: false,
          total_data: 0,
          page: 0,
          next_page: 0,
          prev_page: 0,
          current_page: 0,
          total_page: 0,
          filter: false,
        });
      } else if (res.json.code === '403') {
        if (Func.Clear_Token() === true) {
          if (!localStorage.getItem('token')) {
            this.setState({ redirect: true });
          }
        }
      } else if (res.json.status === 500) {
        this.setState({ failure: true });
      } else {
        const datas = [];
        res.json.data.map((data) => {
          datas.push(
            createData(
              data.id,
              data.user_name,
              data.date,
              data.account.coa,
              data.account.description,
              data.amount,
              data.status
            )
          );
        });
        this.setState({
          con: res.json.data.length,
          data: datas,
          loading: false,
          total_data: res.json.total_data,
          page: res.json.current_page,
          next_page: res.json.next_page,
          prev_page: res.json.prev_page,
          current_page: res.json.current_page,
          total_page: res.json.total_page,
          filter: false,
        });
      }
    });
  };

  Short(orderKey) {
    if (orderKey === this.state.order) {
      this.setState({ order: '' });
      var library = this.state.data;
      library.sort((a, b) => (a[orderKey] < b[orderKey] ? 1 : b[orderKey] < a[orderKey] ? -1 : 0)
      );
    } else {
      this.setState({ order: orderKey });
      var library = this.state.data;
      library.sort((a, b) => (a[orderKey] > b[orderKey] ? 1 : b[orderKey] > a[orderKey] ? -1 : 0)
      );
    }
  }

  handleChange(event, name) {
    const dataSet = this.state.filterValue;
    dataSet[name] = event;
    this.setState({ filterValue: dataSet });
  }

  handleClickDeleteAll = () => {
    Swal.fire({
      title: 'Apakah Anda yakin?',
      text: 'Akan menghapus data yang dipilih',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Ya, Hapus!',
      cancelButtonText: 'Batal',
    }).then((result) => {
      if (result.value) {
        this.state.rowSelected.map((row) => {
          fetch(
            env.managementApi
              + env.apiPrefixV1
              + '/'
              + this.props.path
              + '/'
              + row,
            {
              method: 'DELETE',
              headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + localStorage.getItem('token'),
              },
            }
          )
            .then((response) => response.json())
            .then(() => {})
            .catch(() => {})
            .finally(() => {});
        });
        Swal.fire('Berhasil!', 'Data berhasil dihapus.', 'success');
        this.getData();
        this.setState({ rowSelected: [] });
      }
    });
  };

  DeleteBulk() {
    this.handleClickDeleteAll();
  }

  generateColor(x) {
    if (x === 'in_progress') {
      var color = '#FFEB85';
    } else if (x === 'received') {
      var color = '#69FBBA';
    } else if (x === 'approved') {
      var color = '#A8EAFF';
    } else if (x === 'rejected') {
      var color = '#CB3B3B';
    } else if (x === 'not_accepted') {
      var color = 'rgba(43, 68, 80, 0.5)';
    } else if (x === 'cancelled') {
      var color = '#E5E5E5';
    } else {
      var color = 'transparent';
    }
    return color;
  }

  generateText(x) {
    if (x === 'approved') {
      var text = 'Disetujui';
    } else {
      var text = 'Menunggu';
    }
    return text;
  }

  render() {
    const { classes } = this.props;
    let con = 0;
    this.state.data.map((row) => {
      if (this.state.rowSelected.indexOf(row.id) > -1) {
        con += 1;
      }
    });
    return (
      <div>
        <TableContainer
          style={{
            width: this.props.open
              ? window.innerWidth - this.props.width
              : window.innerWidth - 115,
          }}
          component={Paper}
        >
          <div
            style={{
              width: '100%',
            }}
          >
            <Box display="flex">
              <Box flexGrow={1}>
                <Typography variant="h6" className={classes.paginationTxt2}>
                  {this.props.title}
                </Typography>
              </Box>
            </Box>
          </div>
          <div
            style={{
              width: '100%',
            }}
          >
            <Box display="flex">
              <Box flexGrow={1}>
                <Typography className={classes.paginationTxt}>
                  {this.props.subtitle}
                </Typography>
              </Box>
            </Box>
          </div>
          <Hidden smUp>
            <div
              style={{
                width: '100%',
              }}
            >
              <Box display="flex">
                <Box flexGrow={1}>
                  <Typography variant="h9" className={classes.textperdata}>
                    Tampilkan
                  </Typography>
                  <Select
                    size="small"
                    className={classes.selectperdata}
                    labelId="demo-customized-select-label"
                    id="demo-customized-select"
                    value={this.state.rowsPerPage}
                    onChange={(val) => {
                      this.setState(
                        {
                          rowsPerPage: val.target.value,
                        },
                        () => {
                          this.getData();
                        }
                      );
                    }}
                    input={<BootstrapInput />}
                  >
                    <MenuItem value={10}>10 data</MenuItem>
                    <MenuItem value={50}>50 data</MenuItem>
                    <MenuItem value={100}>100 data</MenuItem>
                  </Select>
                  <Typography variant="h9" className={classes.textperdata}>
                    Total:
                    {' '}
                    {this.state.total_data}
                    {' '}
                    pengeluaran kas
                  </Typography>
                </Box>
              </Box>
            </div>
            <div
              style={{
                width: '100%',
              }}
            >
              <Button
                name="filterButton"
                style={{
                  marginRight: 15,
                  marginBottom: 15,
                }}
                startIcon={<FilterListIcon />}
                variant="outlined"
                onClick={() => {
                  this.setState({
                    filter: !this.state.filter,
                  });
                }}
                color="primary"
              >
                Filter
              </Button>
            </div>
          </Hidden>
          <Hidden xsDown>
            <div
              style={{
                width: '100%',
              }}
            >
              <Box display="flex">
                <Box flexGrow={1}>
                  <Typography variant="h9" className={classes.textperdata}>
                    Tampilkan
                  </Typography>
                  <Select
                    size="small"
                    className={classes.selectperdata}
                    labelId="demo-customized-select-label"
                    id="demo-customized-select"
                    value={this.state.rowsPerPage}
                    onChange={(val) => {
                      this.setState(
                        {
                          rowsPerPage: val.target.value,
                        },
                        () => {
                          this.getData();
                        }
                      );
                    }}
                    input={<BootstrapInput />}
                  >
                    <MenuItem value={10}>10 data</MenuItem>
                    <MenuItem value={50}>50 data</MenuItem>
                    <MenuItem value={100}>100 data</MenuItem>
                  </Select>
                  <Typography variant="h9" className={classes.textperdata}>
                    Total:
                    {' '}
                    {this.state.total_data}
                    {' '}
                    pengeluaran kas
                  </Typography>
                </Box>
                <Box>
                  <Button
                    name="filterButton"
                    style={{
                      marginRight: 15,
                      marginBottom: 15,
                    }}
                    startIcon={<FilterListIcon />}
                    variant="outlined"
                    onClick={() => {
                      this.setState({
                        filter: !this.state.filter,
                      });
                    }}
                    color="primary"
                  >
                    Filter
                  </Button>
                </Box>
              </Box>
            </div>
          </Hidden>
          <Dialog
            open={this.state.filter}
            onClose={() => {
              this.setState({
                filter: !this.state.filter,
              });
            }}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
          >
            <DialogTitle id="alert-dialog-title">Filter</DialogTitle>
            <Divider
              style={{
                marginBottom: 20,
              }}
            />
            <DialogContent>
              <DialogContentText id="alert-dialog-description">
                <div>
                  <div>
                    <Select2
                      name="form-field-name-error"
                      value={this.state.filterValue.status}
                      placeholder="Pilih"
                      error
                      styles={{
                        control: (provided, state) => ({
                          ...provided,
                          borderColor: this.state.filterValue.status
                            ? 'red'
                            : '#CACACA',
                          borderRadius: '0.25rem',
                        }),
                      }}
                      className={classes.search2}
                      options={[
                        {
                          value: '-',
                          label: 'Pilih Status',
                          isDisabled: true,
                        },
                        {
                          value: 0,
                          label: 'Disetujui',
                        },
                        {
                          value: 1,
                          label: 'Menunggu Persetujuan',
                        },
                        {
                          value: 2,
                          label: 'Ditolak',
                        },
                      ]}
                      onChange={(val) => {
                        this.handleChange(val, 'status');
                      }}
                    />
                  </div>
                  <div>
                    <BootstrapInput
                      value={this.state.filterValue.user_name}
                      placeholder="Kasir"
                      onChange={(event) => {
                        this.handleChange(event.target.value, 'user_name');
                      }}
                      on
                      className={classes.search2}
                      id="demo-customized-textbox"
                    />
                  </div>
                  <div>
                    <BootstrapInput
                      value={this.state.filterValue.date}
                      placeholder="Tanggal"
                      type="date"
                      onChange={(event) => {
                        this.handleChange(event.target.value, 'date');
                      }}
                      on
                      className={classes.search2}
                      id="demo-customized-textbox"
                    />
                  </div>
                  <div>
                    <BootstrapInput
                      value={this.state.filterValue.coa}
                      placeholder="COA"
                      onChange={(event) => {
                        this.handleChange(event.target.value, 'coa');
                      }}
                      on
                      className={classes.search2}
                      id="demo-customized-textbox"
                    />
                  </div>
                  <div>
                    <BootstrapInput
                      placeholder="Nominal"
                      value={Func.FormatNumber(this.state.filterValue.amount)}
                      onChange={(event) => {
                        this.handleChange(
                          Func.UnFormatRp(event.target.value),
                          'amount'
                        );
                      }}
                      on
                      className={classes.search2}
                      id="demo-customized-textbox"
                    />
                  </div>
                </div>
              </DialogContentText>
            </DialogContent>
            <DialogActions>
              <Button
                onClick={() => {
                  this.setState({ filter: false });
                }}
                color="primary"
              >
                Tutup
              </Button>
              <Button
                onClick={() => {
                  this.setState(
                    {
                      filterValue: {
                        amount: '',
                      },
                    },
                    () => {
                      this.setState({ filter: false });
                      this.getData();
                    }
                  );
                }}
                color="primary"
              >
                Hapus
              </Button>
              <Button
                onClick={() => {
                  this.setState({ filter: false });
                  this.getData();
                }}
                color="primary"
                autoFocus
              >
                Filter
              </Button>
            </DialogActions>
          </Dialog>
          <FormCountry
            type="Ubah"
            modal={this.state.modal}
            modal2={this.state.modal2}
            row={this.state.row}
          />
        </TableContainer>
        <TableContainer
          style={{
            width: this.props.open
              ? window.innerWidth - this.props.width
              : window.innerWidth - 115,
          }}
          component={Paper}
        >
          <Table
            size="small"
            onMouseOut={() => {
              this.setUnfocus();
            }}
            className={classes.table}
            aria-label="simple table"
          >
            <TableHead className={classes.headTable}>
              <TableRow>
                {headCells.map((headCell) => (
                  <TableCell
                    key={headCell.id}
                    align={headCell.numeric ? 'right' : 'left'}
                    sortDirection={
                      this.state.order === headCell.id
                        ? this.state.order
                        : false
                    }
                  >
                    <TableSortLabel
                      active={this.state.order === headCell.id}
                      direction={
                        this.state.order === headCell.id
                          ? this.state.order
                          : 'asc'
                      }
                      onClick={() => {
                        this.Short(headCell.id);
                      }}
                    >
                      {headCell.label}
                      {this.state.order === headCell.id ? (
                        <span className={classes.visuallyHidden} />
                      ) : null}
                    </TableSortLabel>
                  </TableCell>
                ))}
              </TableRow>
            </TableHead>
            {this.state.data.length === 0 && !this.state.loading ? (
              <TableBody>
                <TableRow>
                  <TableCell colSpan={8} align="center">
                    <Typography>Tidak Ada Data</Typography>
                  </TableCell>
                </TableRow>
              </TableBody>
            ) : null}
            <TableBody>
              {!this.state.loading
                ? this.state.data.map((row) => (
                  <TableRow
                    hover
                    onMouseOver={() => {
                      this.setFocus(row);
                    }}
                    role="checkbox"
                    tabIndex={-1}
                    key={row.id}
                  >
                    <TableCell
                      onClick={() => {
                        this.handleShow(row);
                      }}
                      style={{ cursor: 'pointer' }}
                      align="left"
                    >
                      {row.user_name}
                    </TableCell>
                    <TableCell
                      onClick={() => {
                        this.handleShow(row);
                      }}
                      style={{ cursor: 'pointer' }}
                      align="left"
                    >
                      {Func.FormatDate(row.date)}
                    </TableCell>
                    <TableCell
                      onClick={() => {
                        this.handleShow(row);
                      }}
                      style={{ cursor: 'pointer' }}
                      align="left"
                    >
                      {row.coa}
                    </TableCell>
                    <TableCell
                      onClick={() => {
                        this.handleShow(row);
                      }}
                      style={{ cursor: 'pointer' }}
                      align="left"
                    >
                      {row.description}
                    </TableCell>
                    <TableCell
                      onClick={() => {
                        this.handleShow(row);
                      }}
                      style={{ cursor: 'pointer' }}
                      align="left"
                    >
                      {Func.FormatRp(row.amount)}
                    </TableCell>
                    <TableCell
                      onClick={() => {
                        this.handleShow(row);
                      }}
                      style={{ cursor: 'pointer' }}
                      align="left"
                    >
                      <div
                        style={{
                          backgroundColor: this.generateColor(row.status),
                          width: '172px',
                          height: '40px',
                          borderRadius: 5,
                          padding: '10px 0',
                          textAlign: 'center',
                        }}
                      >
                        {this.generateText(row.status)}
                      </div>
                    </TableCell>
                    <TableCell align="left">
                      {!row.status && (
                        <div className={classes.action}>
                          <div>
                            {Func.checkPermission(
                              'cashflow#cash-disbursements#update'
                            ) ? (
                                <Tooltip title="Ubah" aria-label="Ubah">
                                  <IconButton
                                    onClick={() => {
                                      this.handleModal(row);
                                    }}
                                    aria-label="Cari"
                                  >
                                    <EditIcon />
                                  </IconButton>
                                </Tooltip>
                              ) : null}
                            {Func.checkPermission(
                              'cashflow#cash-disbursements#delete'
                            ) ? (
                                <Tooltip title="Hapus" aria-label="Hapus">
                                  <IconButton
                                    onClick={() => {
                                      this.handleClickDelete(row.id);
                                    }}
                                    aria-label="Cari"
                                  >
                                    <DeleteIcon />
                                  </IconButton>
                                </Tooltip>
                              ) : null}
                          </div>
                        </div>
                      )}
                    </TableCell>
                  </TableRow>
                ))
                : null}
            </TableBody>
          </Table>
          {this.state.loading ? (
            <div className={classes.loader}>
              <BeatLoader size={15} color="#3F3F3F" loading />
            </div>
          ) : null}
        </TableContainer>
        <TableContainer
          style={{
            width: this.props.open
              ? window.innerWidth - this.props.width
              : window.innerWidth - 115,
            marginBottom: 40,
          }}
          component={Paper}
        >
          <div className={classes.col}>
            <text>
              Halaman
              {' '}
              {' ' + this.state.current_page + ' '}
              dari
              {' '}
              {' ' + this.state.total_page + ' '}
              halaman
            </text>
          </div>
          <Pagination
            color="secondary"
            className={classes.row}
            count={this.state.total_page}
            defaultPage={this.state.page}
            onChange={this.ChangePage}
            siblingCount={0}
          />
        </TableContainer>
        {this.state.rowSelected.length > 0 ? (
          <div className={classes.popupv2}>
            <Box display="flex" justifyContent="center" flexWrap="wrap">
              <Box flexWrap="wrap">
                <div className={classes.popup}>
                  <Box display="flex" justifyContent="center" flexWrap="wrap">
                    <Box>
                      <Typography className={classes.popupTxt}>
                        Pilih aksi:
                      </Typography>
                    </Box>
                    <Box>
                      <IconButton
                        onClick={() => {
                          this.DeleteBulk();
                        }}
                        aria-label="Cari"
                      >
                        <img src={Icon.popupDelete} />
                      </IconButton>
                    </Box>
                    <Box>
                      <IconButton
                        className={classes.popupClose}
                        onClick={() => {
                          this.setState({ rowSelected: [] });
                        }}
                        aria-label="Cari"
                      >
                        <img src={Icon.close} />
                      </IconButton>
                    </Box>
                  </Box>
                </div>
              </Box>
            </Box>
          </div>
        ) : null}
      </div>
    );
  }
}

export default withStyles(styles.CoustomsStyles, { name: 'Tables' })(Tables);
