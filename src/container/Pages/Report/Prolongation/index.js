/* eslint-disable react/no-access-state-in-setstate */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/sort-comp */
/* eslint-disable react/no-unused-state */
import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import styles from './css';
import Tables from './tables/index';
import Form from './form/index';

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      open: true,
      master: false,
      validator: [],
      value: [],
      mdlShow: true,
      mobile: false,
      redirect: false,
      active: 'Pencairan',
      show: 'password',
      id_cust: null
    };
  }

  handleModal() {
    this.setState({
      modal: true
    }, () => {
      this.setState({ modal: false });
    });
  }

  handleDrawer() {
    this.setState({
      open: !this.state.open
    });
  }

  componentDidMount() {}

  render() {
    return (
      <main style={{ marginTop: 10 }}>
        <Form type="Tambah" modal={this.state.modal} />
        <div style={{ display: 'flex', justifyContent: 'center' }}>
          <Tables open={this.state.open} active={this.state.active} />
        </div>
      </main>
    );
  }
}

export default withStyles(styles.CoustomsStyles, { name: 'Login' })(Login);
