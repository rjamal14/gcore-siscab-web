/* eslint-disable react/destructuring-assignment */
import React from 'react';
import Hidden from '@material-ui/core/Hidden';
import { withStyles } from '@material-ui/core/styles';
import styles from '../css';
import Melting from './melting';

class Index extends React.Component {
  render() {
    return (
      <div>
        <div>
          <Hidden only={['lg', 'xl']}>
            <Melting
              width={60}
              open={this.props.open}
              filter={false}
              title="Laporan Transaksi Semua"
              subtitle=""
              path="transactions/published.json?q[created_at_gteq]=2020-10-1&q[created_at_lteq]=2020-10-30"
            />
          </Hidden>
          <Hidden only={['sm', 'md', 'xs']}>
            <Melting
              width={285}
              open={this.props.open}
              filter={false}
              title="Laporan Transaksi Semua"
              subtitle=""
              path="transactions/published.json?q[created_at_gteq]=2020-10-1&q[created_at_lteq]=2020-10-30"
            />
          </Hidden>
        </div>
      </div>
    );
  }
}

export default withStyles(styles.CoustomsStyles, { name: 'Index' })(Index);
