/* eslint-disable no-plusplus */
/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable no-shadow */
/* eslint-disable no-param-reassign */
/* eslint-disable no-cond-assign */
/* eslint-disable no-constant-condition */
/* eslint-disable array-callback-return */
/* eslint-disable camelcase */
/* eslint-disable react/sort-comp */
/* eslint-disable react/no-access-state-in-setstate */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/no-unused-state */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import MuiDialogContent from '@material-ui/core/DialogContent';
import Divider from '@material-ui/core/Divider';
import MuiDialogActions from '@material-ui/core/DialogActions';
import FormHelperText from '@material-ui/core/FormHelperText';
import { BeatLoader } from 'react-spinners';
import Func from '../../../../../functions/index';
import Icon from '../../../../../components/icon';
import styles from '../css';

const DialogContent = withStyles((theme) => ({
  root: {
    padding: theme.spacing(2)
  }
}))(MuiDialogContent);

const DialogActions = withStyles((theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(1)
  }
}))(MuiDialogActions);

class Form extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: false,
      validator: [],
      value: { gender: { label: '' } },
      type: '',
      activeTabs: 0,
      Proses: false,
      ImgBase124: '',
      section: 0,
      id: null,
      redirect: false,
      date: new Date(),
      provin: [],
      data3: [],
      estimated_value: [],
      ltv_value: [],
      ltv: [],
      monthly_fee: [],
      dataBJ: [],
      cont: 0,
      approvals: [],
      download: null,
      loading: false
    };
    this.handleChangeImg = this
      .handleChangeImg
      .bind(this);
  }

  removeValidate(name) {
    const data = this.state.validator;
    delete data[name];
    this.setState({ validator: data });
  }

  getInsurance(id) {
    fetch(process.env.REACT_APP_URL_MASTER + process.env.REACT_APP_API_PREFIX_V1 + '/product/' + id + '/insurance_item/', {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + localStorage.getItem('token')
      }
    }).then((response) => response.json()).then((json) => {
      if (json.code === '403') {
        if (Func.Clear_Token() === true) {
          if (!localStorage.getItem('token')) {
            this.setState({ redirect: true });
          }
        }
      }

      const datas = [];
      datas.push({
        value: '-',
        label: json.data.insurance_item_product.insurance_items.length > 0
          ? 'Pilih'
          : 'Tidak ditemukan',
        isDisabled: true
      });

      json
        .data
        .insurance_item_product
        .insurance_items
        .map((value) => {
          datas.push({ value: value.id.$oid, label: value.name });
        });
      this.setState({ data3: datas, data3_ori: json.data.insurance_item_product.insurance_items });
    }).catch(() => {})
      .finally(() => {});
  }

  getRentalCosts(value) {
    const id = this.state.value.product_id.value;
    const insurance_item_id = this.state.value.insurance_item_id.value;
    fetch(process.env.REACT_APP_URL_MASTER + process.env.REACT_APP_API_PREFIX_V1 + '/rental_costs?product_id=' + id + '&insurance_item_id=' + insurance_item_id + '&loan_value=' + value, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + localStorage.getItem('token')
      }
    }).then((response) => response.json()).then((json) => {
      if (json.code === '403') {
        if (Func.Clear_Token() === true) {
          if (!localStorage.getItem('token')) {
            this.setState({ redirect: true });
          }
        }
      }
      const val = this.state.value;
      val.monthly_fee = Math.ceil(json.data.rental_cost_nominal);
      this.setState({ value: val });
    }).catch(() => {})
      .finally(() => {});
  }

  getAdminFee(value) {
    const id = this.state.value.product_id.value;
    const insurance_item_id = this.state.value.insurance_item_id.value;
    fetch(process.env.REACT_APP_URL_MASTER + process.env.REACT_APP_API_PREFIX_V1 + '/admin_fees?product_id=' + id + '&insurance_item_id=' + insurance_item_id + '&loan_nominal=' + value, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + localStorage.getItem('token')
      }
    }).then((response) => response.json()).then((json) => {
      if (json.code === '403') {
        if (Func.Clear_Token() === true) {
          if (!localStorage.getItem('token')) {
            this.setState({ redirect: true });
          }
        }
      }
      const val = this.state.value;
      val.admin_fee = Math.ceil(value * parseFloat('0.' + json.data[0].cost_percentage));
      this.setState({ value: val });
    }).catch(() => {})
      .finally(() => {});
  }

  getDueDates(date) {
    const id = this.state.value.product_id.value;
    fetch(process.env.REACT_APP_URL_MASTER + process.env.REACT_APP_API_PREFIX_V1 + '/due_dates?product_id=' + id + '&contract_date=' + date, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + localStorage.getItem('token')
      }
    }).then((response) => response.json()).then((json) => {
      if (json.code === '403') {
        if (Func.Clear_Token() === true) {
          if (!localStorage.getItem('token')) {
            this.setState({ redirect: true });
          }
        }
      }

      const val = this.state.value;
      val.due_date = json.data.due_date;
      val.auction_date = json.data.auction_date;
      this.setState({ value: val });
    }).catch(() => {})
      .finally(() => {});
  }

  getInsuranceProd(id, id2, idx) {
    fetch(process.env.REACT_APP_URL_MASTER + process.env.REACT_APP_API_PREFIX_V1 + '/product/' + id, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + localStorage.getItem('token')
      }
    }).then((response) => response.json()).then((json) => {
      if (json.code === '403') {
        if (Func.Clear_Token() === true) {
          if (!localStorage.getItem('token')) {
            this.setState({ redirect: true });
          }
        }
      }

      const datas = [];
      datas.push({
        value: '-',
        label: json.data.product_detail.product_insurance_items.length > 0
          ? 'Pilih'
          : 'Tidak ditemukan',
        isDisabled: true
      });
      const _id = id2;
      if (id2 = !'') {
        const search = json
          .data
          .product_detail
          .product_insurance_items.find(o => o.id.$oid === _id);
        if (search !== undefined) {
          const val = this.state.value;
          val['product_insurance_item_id' + idx] = { value: search.id.$oid, label: search.name };
          this.setState({ value: val });
        }
      }

      json
        .data
        .product_detail
        .product_insurance_items
        .map((value) => {
          datas.push({ value: value.id.$oid, label: value.name });
        });
      this.setState({ data5: datas });
    }).catch(() => {})
      .finally(() => {});
  }

  handleChangeDate(date, name) {
    const dt = new Date(date);
    const dataSet = this.state.value;
    dataSet[name] = dt;
    this.setState({ value: dataSet });
  }

  componentDidMount() {
    this.getProd('', '', '');
    // this.getCusto('', '', '');
    if (this.props.type === 'Ubah') {
      this.setState({ loading: true });
      fetch(process.env.REACT_APP_URL_MANAGEMENT + process.env.REACT_APP_API_PREFIX_V1 + '/transactions/' + this.props.id, {
        method: 'GET',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + localStorage.getItem('token')
        }
      }).then((response) => response.json()).then((json) => {
        this.setState({ loading: false });
        if (json.code === '403') {
          if (Func.Clear_Token() === true) {
            if (!localStorage.getItem('token')) {
              this.setState({ redirect: true });
            }
          }
        }
        // this.getCusto('', '', json.data.customer.name);
        this.getProd('', json.data.product_id, '');
        const val = this.state.value;

        try {
          val.product_name = json.data.product_name;
          val.insurance_item_id = { value: json.data.insurance_item_id, label: json.data.insurance_item_name };
          val.contract_date = json.data.contract_date;
          val.due_date = json.data.due_date;
          val.auction_date = json.data.auction_date;
          val.admin_fee = json.data.admin_fee;
          val.loan_amount = json.data.loan_amount;
          val.monthly_fee = json.data.monthly_fee;
          json.data.transaction_insurance_items.map((value, index) => {
            this.addBJ(value.id);
            this.getEstimate(
              index,
              value.product_insurance_item_id,
              value.weight,
              value.carats,
              value.amount
            );
            this.getInsuranceProd(json.data.product_id, value.product_insurance_item_id, index);
            val['name' + index] = value.name;
            val['ownership' + index] = value.ownership;
            val['amount' + index] = value.amount;
            val['weight' + index] = value.weight;
            val['carats' + index] = value.carats;
            val['description' + index] = value.description;
            val['Insurance_item_image' + index] = value.insurance_item_image.url;
          });
        } catch (err) {
          this.setState({ loading: false });
          alert('terjadi kesalahan sistem');
        }
        const mapData = Object.assign(val, json.data.customer);

        this.setState({
          value: mapData,
          // data2:
          data4: json.data.customer,
          approvals: json.data.approvals,
          download: json.data.download === undefined ? null : json.data.download
        });
      }).catch(() => {})
        .finally(() => {});
    } else {
      this.addBJ();
    }
  }

  getProd = (val, id, name) => {
    this.setState({ isLoading: true });
    Func
      .getData('product', 10, 1, val)
      .then((res) => {
        if (res.json.code === '403') {
          if (Func.Clear_Token() === true) {
            if (!localStorage.getItem('token')) {
              this.setState({ redirect: true });
            }
          }
        } else {
          const datas = [];
          datas.push({
            value: '-',
            label: res.json.data.length > 0
              ? 'Pilih'
              : 'Tidak ditemukan',
            isDisabled: true
          });

          res
            .json
            .data
            .map((value) => {
              datas.push({ value: value.id.$oid, label: value.name });
            });
          if (id !== '') {
            const search = res
              .json
              .data
              .find(o => o.id.$oid === id);
            if (search === undefined) {
              datas.push({ value: id, label: name });
            }
            if (search !== undefined) {
              const val = this.state.value;
              val.product_id = { value: search.id.$oid, label: search.name };
            }
          }

          this.setState({ data2: datas, data2_ori: res.json.data });
        }
      });
  };

  getCusto = (val, id, name) => {
    this.setState({ isLoading: true });
    Func
      .getDataTransaction('customer', 10, 1, val)
      .then((res) => {
        if (res.json.code === '403') {
          if (Func.Clear_Token() === true) {
            if (!localStorage.getItem('token')) {
              this.setState({ redirect: true });
            }
          }
        } else {
          const datas = [];
          datas.push({
            value: '-',
            label: res.json.data.length > 0
              ? 'Pilih'
              : 'Tidak ditemukan',
            isDisabled: true
          });
          res
            .json
            .data
            .map((value) => {
              datas.push({ value: value.id, label: value.name });
            });
          if (name !== '') {
            const search = res
              .json
              .data
              .find(o => o.name === name);
            if (search === undefined) {
              datas.push({ value: id, label: name });
            }

            if (search !== undefined) {
              const val = this.state.value;
              val.customer_id = { value: search.id, label: search.name };
            }
          }

          this.setState({ data4: datas, data4_ori: res.json.data });
        }
      });
  };

  handleSubmit(type) {
    const validator = [
      {
        name: 'img',
        type: 'required'
      }
    ];
    const validate = Func.Validator(this.state.value, validator);
    if (validate.success) {
      fetch(type, {
        method: 'PATCH',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + localStorage.getItem('token')
        },
        body: JSON.stringify({
          transaction_approval: {
            attachment: this.state.ImgBase44
          }
        })
      }).then((response) => response.json()).then((json) => {
        if (json.code === '403') {
          Func.Refresh_Token();
          if (Func.Refresh_Token() === true) {
            this.handleSubmit();
          }
        }
        if (json.code === 200) {
          this.props.OnNext(json.message);
        } else {
          this.setState({ validator: json.status });
        }
      }).catch(() => {})
        .finally(() => {});
    } else {
      this.setState({ validator: validate.error });
    }
  }

  handleChangeImg(event) {
    this.removeValidate('img');
    const dataSet = this.state.value;
    dataSet.img = URL.createObjectURL(event.target.files[0]);
    this.setState({ value: dataSet });

    const file = event.target.files[0];
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onloadend = () => {
      this.setState({ ImgBase44: reader.result });
    };
  }

  getEstimate(index, id, weight, carats, amount) {
    if (id === undefined) {
      id = this.state.value['product_insurance_item_id' + index] === undefined ? 0 : this.state.value['product_insurance_item_id' + index].value;
      weight = this.state.value['weight' + index] === undefined ? 0 : this.state.value['weight' + index];
      carats = this.state.value['carats' + index] === undefined ? 0 : this.state.value['carats' + index];
      amount = this.state.value['amount' + index] === undefined ? 1 : this.state.value['amount' + index];
    }

    fetch(process.env.REACT_APP_URL_MASTER + process.env.REACT_APP_API_PREFIX_V1 + '/estimate_values?carats=' + carats + '&weight=' + weight + '&product_insurance_item_id=' + id, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + localStorage.getItem('token')
      }
    }).then((response) => response.json()).then((json) => {
      if (json.code === '403') {
        if (Func.Clear_Token() === true) {
          if (!localStorage.getItem('token')) {
            this.setState({ redirect: true });
          }
        }
      }

      const { estimated_value } = this.state;
      const { ltv } = this.state;
      const { ltv_value } = this.state;

      estimated_value[index] = Math.ceil(json.data.estimated_value * amount);
      ltv_value[index] = Math.ceil(json.data.ltv_value * amount);
      ltv[index] = Math.ceil(json.data.ltv);

      this.setState({ estimated_value, ltv_value, ltv }, () => {
        this.removeValidate('loan_amount');
      });
    }).catch(() => {})
      .finally(() => {});
  }

  renderBJ() {
    const { classes } = this.props;
    const data = [];
    let key = 1;
    this
      .state
      .dataBJ
      .map((value) => {
        if (!value._destroy) {
          data.push(
            <div>
              <div
                className={classes.BodytitleMdl2}
                style={{
                  marginTop: 50
                }}
              >
                <text className={classes.titleMdl}>
                  BARANG JAMINAN
                  {' '}
                  {key}
                </text>
              </div>
              <Grid container direction="row" item lg={12} xl={12} md={12} xs={12}>
                <Grid container item lg={8} xl={8} md={8} sm={8} xs={12}>
                  <Grid container item lg={12} xl={12} md={12} sm={12} xs={12}>
                    <Grid item lg={6} xl={6} md={6} sm={6} xs={12} className={classes.formPad}>
                      <Grid container item lg={12} xl={12} md={12} sm={12} xs={12}>
                        <Grid item lg={8} xl={8} md={8} sm={8} xs={12} className={classes.formPad}>
                          <div>
                            <text className={classes.label121}>Nama Barang Jaminan</text>
                          </div>
                          <div>
                            <div className={classes.label1112}>
                              <text className={classes.label1}>{this.state.value['name' + value.key] }</text>
                            </div>
                          </div>
                        </Grid>
                        <Grid item lg={4} xl={4} md={4} sm={4} xs={12} className={classes.formPad}>
                          <div>
                            <text className={classes.label121}>Jumlah</text>
                          </div>
                          <div>
                            <div className={classes.label1112}>
                              <text className={classes.label1}>{this.state.value['amount' + value.key] }</text>
                            </div>
                          </div>
                        </Grid>
                      </Grid>
                    </Grid>
                    <Grid item lg={6} xl={6} md={6} sm={6} xs={12} className={classes.formPad}>
                      <div>
                        <text className={classes.label121}>Kepemilikan Barang Jaminan</text>
                      </div>
                      <div>
                        <div className={classes.label1112}>
                          <text className={classes.label1}>{this.state.value['ownership' + value.key] }</text>
                        </div>
                      </div>
                    </Grid>
                  </Grid>
                  <Grid container item lg={12} xl={12} md={12} sm={12} xs={12}>
                    <Grid item lg={6} xl={6} md={6} sm={6} xs={12} className={classes.formPad}>
                      <div>
                        <text className={classes.label121}>Kategori Barang Jaminan</text>
                      </div>
                      <div>
                        <div className={classes.label1112}>
                          <text className={classes.label1}>{this.state.value['product_insurance_item_id' + value.key] === undefined ? '' : this.state.value['product_insurance_item_id' + value.key].label }</text>
                        </div>
                      </div>
                    </Grid>
                    <Grid container lg={6} xl={6} md={6} sm={6} xs={12} className={classes.formPad}>
                      <Grid item lg={6} xl={6} md={6} sm={6} xs={12} className={classes.formPad}>
                        <div>
                          <text className={classes.label121}>Berat</text>
                        </div>
                        <div>
                          <div className={classes.label1112}>
                            <text className={classes.label1}>{this.state.value['weight' + value.key]}</text>
                          </div>
                        </div>
                      </Grid>
                      <Grid item lg={6} xl={6} md={6} sm={6} xs={12} className={classes.formPad}>
                        <div>
                          <text className={classes.label121}>Karatase</text>
                        </div>
                        <div>
                          <div className={classes.label1112}>
                            <text className={classes.label1}>{this.state.value['carats' + value.key]}</text>
                          </div>
                        </div>
                      </Grid>
                    </Grid>
                  </Grid>
                </Grid>
                <Grid container item lg={4} xl={4} md={4} sm={4} xs={12} spacing={3}>
                  <Grid item lg={12} xl={12} md={12} sm={12} xs={12} className={classes.formPad}>
                    <div>
                      <text className={classes.label121}>Deskripsi</text>
                    </div>
                    <div>
                      <div className={classes.label1112}>
                        <text className={classes.label1}>{this.state.value['description' + value.key]}</text>
                      </div>
                    </div>
                  </Grid>
                </Grid>
              </Grid>
              <Grid item lg={4} xl={4} md={4} sm={4} xs={12}>
                <Box
                  borderColor={this.state.validator['Insurance_item_image' + value.key]
                    ? 'error.main'
                    : 'grey.500'}
                  border={1}
                  onClick={() => {
                    this.removeValidate('Insurance_item_image' + value.key);
                  }}
                  className={classes.imgScan}
                >
                  {this.state.value['Insurance_item_image' + value.key]
                    ? (
                      <img
                        className={classes.imgScan2}
                        onClick={() => {
                          this.removeValidate('Insurance_item_image' + value.key);
                        }}
                        src={this.state.value['Insurance_item_image' + value.key]}
                      />
                    )
                    : null}
                </Box>
              </Grid>
            </div>
          );
          key++;
        }
      });
    return data;
  }

  addBJ(id) {
    const data = this.state.dataBJ;
    data.push({ _destroy: false, key: this.state.cont, id: id === undefined ? null : id });
    this.setState({
      dataBJ: data,
      cont: this.state.cont + 1
    });
  }

  render() {
    let estimated_value = 0;
    for (let index = 0; index < this.state.estimated_value.length; index++) {
      estimated_value += this.state.estimated_value[index];
    }

    let ltv_value = 0;
    for (let index = 0; index < this.state.ltv_value.length; index++) {
      ltv_value += this.state.ltv_value[index];
    }

    let ltv = 0;
    for (let index = 0; index < this.state.ltv.length; index++) {
      ltv += this.state.ltv[index];
    }

    if (this.state.value.loan_amount > ltv_value && this.state.validator.loan_amount === undefined) {
      const validate = this.state.validator;
      validate.loan_amount = 'Pinjaman melebihi batas maksimum';
      this.setState({ validator: validate });
    }

    const { classes } = this.props;
    return (
      <div>
        <DialogContent>
          {
            this.state.loading
              ? (
                <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
                  Mohon Tunggu
                  <BeatLoader />
                </div>
              )
              : (
                <div className={classes.scrool}>
                  <div>
                    {Func.toLogin(this.state.redirect)}
                    <div className={classes.root}>
                      <div className={classes.BodytitleMdl2}>
                        <text className={classes.titleMdl}>Data Nasabah</text>
                      </div>
                      {' '}
                      <Grid container direction="row" item lg={12} xl={12} md={12} xs={12}>
                        <Grid item lg={12} xl={12} md={12} sm={12} xs={12}>
                          <Grid container direction="row" item lg={12} xl={12} md={12} xs={12}>
                            <Grid container item lg={8} xl={8} md={8} sm={8} xs={12}>
                              <Grid container item lg={12} xl={12} md={12} sm={12} xs={12}>
                                <Grid item lg={6} xl={6} md={6} sm={6} xs={12} className={classes.formPad}>
                                  <div className={classes.label111}>
                                    <text className={classes.label121}>Nama Lengkap</text>
                                  </div>
                                  <div className={classes.label1112}>
                                    <text className={classes.label1}>{this.state.value.name }</text>
                                  </div>
                                </Grid>
                                <Grid item lg={6} xl={6} md={6} sm={6} xs={12} className={classes.formPad}>
                                  <div className={classes.label111}>
                                    <text className={classes.label121}>Nomor HP</text>
                                  </div>
                                  <div className={classes.label1112}>
                                    <text className={classes.label1}>{this.state.value.phone_number}</text>
                                  </div>
                                </Grid>
                              </Grid>
                              <Grid container item lg={12} xl={12} md={12} sm={12} xs={12}>
                                <Grid item lg={6} xl={6} md={6} sm={6} xs={12} className={classes.formPad}>
                                  <div className={classes.label111}>
                                    <text className={classes.label121}>Jenis Kelamin</text>
                                  </div>
                                  <div className={classes.label1112}>
                                    <text className={classes.label1}>
                                      { this.state.value.gender === 'p' && 'Perempuan' }
                                      { this.state.value.gender === 'l' && 'Laki - laki' }
                                    </text>
                                  </div>
                                </Grid>
                                <Grid item lg={6} xl={6} md={6} sm={6} xs={12} className={classes.formPad}>
                                  <div className={classes.label111}>
                                    <text className={classes.label121}>ID</text>
                                    <text className={classes.label121} />
                                    <text className={classes.label121} />
                                    <text className={classes.label121} />
                                    <text className={classes.label121}>Nomor ID</text>
                                  </div>
                                  <div className={classes.label1112}>
                                    <text className={classes.label1}>{this.state.value.identity_type === undefined ? ' ' : this.state.value.identity_type.toUpperCase()}</text>
                                    <text style={{ marginLeft: 50 }} className={classes.label1}>{this.state.value.identity_number}</text>
                                  </div>
                                </Grid>
                              </Grid>
                              <Grid container item lg={12} xl={12} md={12} sm={12} xs={12} />
                            </Grid>
                            <Grid container item lg={4} xl={4} md={4} sm={4} xs={12} spacing={3}>
                              <Grid item lg={12} xl={12} md={12} sm={12} xs={12} className={classes.formPad}>
                                <div className={classes.label111}>
                                  <text className={classes.label121}>Alamat</text>
                                </div>
                                <div className={classes.label1112}>
                                  <text className={classes.label1}>{this.state.value.address}</text>
                                </div>
                              </Grid>
                            </Grid>
                          </Grid>
                          <Divider className={classes.divider} />
                          <Grid container direction="row" item lg={12} xl={12} md={12} xs={12}>
                            <Grid container item lg={4} xl={4} md={4} sm={4} xs={12}>
                              <div className={classes.label111}>
                                <text className={classes.label121}>Produk Gadai</text>
                              </div>
                            </Grid>
                            <Grid container item lg={4} xl={4} md={4} sm={4} xs={12}>
                              <div className={classes.label111}>
                                <text className={classes.label121}>Kategori Barang Jaminan</text>
                              </div>
                            </Grid>
                          </Grid>
                          <Grid container direction="row" item lg={12} xl={12} md={12} xs={12}>
                            <Grid container item lg={4} xl={4} md={4} sm={4} xs={12}>
                              <div className={classes.label1112}>
                                <text className={classes.label1}>{this.state.value.product_name }</text>
                              </div>
                            </Grid>
                            <Grid container item lg={4} xl={4} md={4} sm={4} xs={12}>
                              <div className={classes.label1112}>
                                <text className={classes.label1}>{this.state.value.insurance_item_id === undefined ? '' : this.state.value.insurance_item_id.label }</text>
                              </div>
                            </Grid>
                          </Grid>
                          <div
                            style={{
                              marginBottom: 50
                            }}
                          >
                            {this.renderBJ()}
                          </div>
                          <div className={classes.BodytitleMdl2}>
                            <text className={classes.titleMdl}>Perhitungan Barang Jaminan</text>
                          </div>
                          <Grid container direction="row" item lg={12} xl={12} md={12} xs={12}>
                            <Grid container item lg={4} xl={4} md={4} sm={4} xs={12}>
                              <div className={classes.label111}>
                                <text className={classes.label121}>Nilai Taksiran</text>
                              </div>
                            </Grid>
                            <Grid container item lg={4} xl={4} md={4} sm={4} xs={12}>
                              <div className={classes.label111}>
                                <text className={classes.label121}>Pinjaman yang Diajukan</text>
                              </div>
                            </Grid>
                            <Grid container item lg={4} xl={4} md={4} sm={4} xs={12}>
                              <div className={classes.label111}>
                                <text className={classes.label121}>Biaya Admin</text>
                              </div>
                            </Grid>
                          </Grid>

                          <Grid container direction="row" item lg={12} xl={12} md={12} xs={12}>
                            <Grid container item lg={4} xl={4} md={4} sm={4} xs={12}>
                              <div className={classes.label111}>
                                <text className={classes.label1}>
                                  Rp
                                  {' '}
                                  {estimated_value !== undefined
                                    ? Func.FormatNumber(estimated_value)
                                    : ''}

                                </text>
                              </div>
                            </Grid>
                            <Grid container item lg={4} xl={4} md={4} sm={4} xs={12}>
                              <div className={classes.label111}>
                                <text className={classes.label1}>
                                  Rp
                                  {' '}
                                  {this.state.value.loan_amount !== undefined
                                    ? Func.FormatNumber(this.state.value.loan_amount)
                                    : ''}

                                </text>
                              </div>
                            </Grid>
                            <Grid container item lg={4} xl={4} md={4} sm={4} xs={12}>
                              <div className={classes.label111}>
                                <text className={classes.label1}>
                                  Rp
                                  {' '}
                                  {this.state.value.admin_fee !== undefined
                                    ? Func.FormatNumber(this.state.value.admin_fee)
                                    : ''}

                                </text>
                              </div>
                            </Grid>
                          </Grid>

                          <Grid container direction="row" item lg={12} xl={12} md={12} xs={12}>
                            <Grid container item lg={4} xl={4} md={4} sm={4} xs={12}>
                              <div className={classes.label111}>
                                <text className={classes.label121}>Maksimum Pinjaman</text>
                              </div>
                            </Grid>
                            <Grid container item lg={4} xl={4} md={4} sm={4} xs={12}>
                              <div className={classes.label111}>
                                <text className={classes.label121}>Rasio Pinjaman</text>
                              </div>
                            </Grid>
                            <Grid container item lg={4} xl={4} md={4} sm={4} xs={12}>
                              <div className={classes.label111}>
                                <text className={classes.label121}>Biaya Sewa Perbulan</text>
                              </div>
                            </Grid>
                          </Grid>

                          <Grid container direction="row" item lg={12} xl={12} md={12} xs={12}>
                            <Grid container item lg={4} xl={4} md={4} sm={4} xs={12}>
                              <div className={classes.label111}>
                                <text className={classes.label1}>
                                  Rp
                                  {' '}
                                  {ltv_value !== undefined
                                    ? Func.FormatNumber(ltv_value)
                                    : ''}

                                </text>
                              </div>
                            </Grid>
                            <Grid container item lg={4} xl={4} md={4} sm={4} xs={12}>
                              <div className={classes.label111}>
                                <text className={classes.label1}>
                                  {ltv}
                                  {' '}
                                  %
                                </text>
                              </div>
                            </Grid>
                            <Grid container item lg={4} xl={4} md={4} sm={4} xs={12}>
                              <div className={classes.label111}>
                                <text className={classes.label1}>
                                  Rp
                                  {' '}
                                  {this.state.value.monthly_fee !== undefined
                                    ? Func.FormatNumber(this.state.value.monthly_fee)
                                    : ''}

                                </text>
                              </div>
                            </Grid>
                          </Grid>
                          <Divider className={classes.divider} />
                          <div className={classes.BodytitleMdl2}>
                            <text className={classes.titleMdl}>Tanggal-Tanggal Penting</text>
                          </div>
                          <Grid container direction="row" item lg={12} xl={12} md={12} xs={12}>
                            <Grid container item lg={4} xl={4} md={4} sm={4} xs={12}>
                              <div className={classes.label111}>
                                <text className={classes.label121}>Tanggal Akad</text>
                              </div>
                            </Grid>
                            <Grid container item lg={4} xl={4} md={4} sm={4} xs={12}>
                              <div className={classes.label111}>
                                <text className={classes.label121}>Tanggal Jatuh Tempo</text>
                              </div>
                            </Grid>
                            <Grid container item lg={4} xl={4} md={4} sm={4} xs={12}>
                              <div className={classes.label111}>
                                <text className={classes.label121}>Tanggal Lelang</text>
                              </div>
                            </Grid>
                          </Grid>

                          <Grid container direction="row" item lg={12} xl={12} md={12} xs={12}>
                            <Grid container item lg={4} xl={4} md={4} sm={4} xs={12}>
                              <div className={classes.label1112}>
                                <text className={classes.label1}>{Func.FormatDate(this.state.value.contract_date)}</text>
                              </div>
                            </Grid>
                            <Grid container item lg={4} xl={4} md={4} sm={4} xs={12}>
                              <div className={classes.label1112}>
                                <text className={classes.label1}>{Func.FormatDate(this.state.value.due_date)}</text>
                              </div>
                            </Grid>
                            <Grid container item lg={4} xl={4} md={4} sm={4} xs={12}>
                              <div className={classes.label1112}>
                                <text className={classes.label1}>{Func.FormatDate(this.state.value.auction_date)}</text>
                              </div>
                            </Grid>
                          </Grid>
                        </Grid>
                      </Grid>
                    </div>
                  </div>
                  <div>
                    {
                      this.state.approvals.length > 0
                        ? (
                          <div>
                            <Divider className={classes.divider} />
                            <div className={classes.BodytitleMdl2}>
                              <text className={classes.titleMdl}>Lampiran</text>
                            </div>
                            <div className={classes.BodytitleMdl}>
                              <Box
                                borderColor={this.state.validator.img
                                  ? 'error.main'
                                  : 'grey.500'}
                                border={1}
                                onClick={() => {
                                  this.removeValidate('img');
                                }}
                                className={classes.imgScan3}
                              >
                                {this.state.value.img
                                  ? (
                                    <img
                                      className={classes.imgScan4}
                                      onClick={() => {
                                        this.removeValidate('img');
                                      }}
                                      src={this.state.value.img}
                                    />
                                  )
                                  : null}
                              </Box>
                              <FormHelperText className={classes.error22}>
                                {this.state.validator.img}
                              </FormHelperText>
                            </div>
                            <div className={classes.BodytitleMdl22}>
                              <img
                                src={Icon.deleteImg}
                                onClick={() => {
                                  const dataSet = this.state.value;
                                  dataSet.img = null;
                                  this.setState({ value: dataSet, ImgBase44: '' });
                                }}
                              />
                              <div />
                              <input
                                type="file"
                                accept="image/*"
                                name="file"
                                title="Pilih Gambar"
                                onChange={this.handleChangeImg}
                              />
                            </div>
                          </div>
                        )
                        : null
                    }
                  </div>
                </div>
              )
          }
        </DialogContent>
        <DialogActions>
          <Grid container className={classes.scrool2}>
            <Grid item xs={12}>
              <div
                style={{
                  marginTop: 20
                }}
              >
                <text style={styles.starts} />
                <text className={styles.label}>
                  {' '}

                </text>
              </div>
              <div>
                <text style={styles.starts} />
                <text className={styles.label}>
                  {' '}

                </text>
              </div>
            </Grid>
            <Grid item xs={12} />
          </Grid>
        </DialogActions>
      </div>
    );
  }
}

export default withStyles(styles.CoustomsStyles, { name: 'Form' })(Form);
