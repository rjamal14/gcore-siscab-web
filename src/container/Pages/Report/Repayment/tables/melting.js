/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable no-unused-vars */
/* eslint-disable array-callback-return */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable no-useless-concat */
/* eslint-disable radix */
/* eslint-disable class-methods-use-this */
/* eslint-disable react/sort-comp */
/* eslint-disable no-dupe-keys */
/* eslint-disable react/no-unused-state */
/* eslint-disable camelcase */
import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import InputBase from '@material-ui/core/InputBase';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import Swal from 'sweetalert2';
import Pagination from '@material-ui/lab/Pagination';
import { Hidden } from '@material-ui/core';
import BeatLoader from 'react-spinners/BeatLoader';
import DatePicker from 'react-datepicker';
import Button from '@material-ui/core/Button';
import Func from '../../../../../functions/index';
import Icon from '../../../../../components/icon';
import FormCountry from '../form';
import styles from '../css';
import 'react-datepicker/dist/react-datepicker.css';
import env from '../../../../../config/env';

function createData(
  id,
  insurance_item_name,
  cif_number,
  sge,
  name,
  loan_amount,
  ltv,
  admin_fee,
  monthly_fee,
  estimate_value,
  contract_date,
  due_date,
  auction_date
) {
  return {
    id,
    insurance_item_name,
    cif_number,
    sge,
    name,
    loan_amount,
    ltv,
    admin_fee,
    monthly_fee,
    estimate_value,
    contract_date,
    due_date,
    auction_date
  };
}

const headCells = [
  {
    id: 'insurance_item_name',
    numeric: false,
    disablePadding: false,
    label: 'KategoriBJ'
  }, {
    id: 'sge',
    numeric: false,
    disablePadding: false,
    label: 'No. SGE'
  }, {
    id: 'name',
    numeric: false,
    disablePadding: false,
    label: 'Nasabah'
  }, {
    id: 'loan_amount',
    numeric: false,
    disablePadding: false,
    label: 'Pinjaman'
  }, {
    id: 'ltv',
    numeric: false,
    disablePadding: false,
    label: 'Rasio'
  }, {
    id: 'admin_fee',
    numeric: false,
    disablePadding: false,
    label: 'Admin '
  },
  {
    id: 'monthly_fee',
    numeric: false,
    disablePadding: false,
    label: 'Sewa'
  }, {
    id: 'contract_date',
    numeric: false,
    disablePadding: false,
    label: 'Tgl. Akad'
  }
];

const BootstrapInput = withStyles((theme) => ({
  root: {
    'label + &': {
      marginTop: theme.spacing(3)
    }
  },
  input: {
    borderRadius: 4,
    position: 'relative',
    backgroundColor: theme.palette.background.paper,
    border: '1px solid #ced4da',
    fontSize: 16,
    padding: '10px 26px 10px 12px',
    transition: theme
      .transitions
      .create(['border-color', 'box-shadow']),
    fontFamily: [
      '-apple-system',
      'BlinkMacSystemFont',
      '"Segoe UI"',
      'Roboto',
      '"Helvetica Neue"',
      'Arial',
      'sans-serif',
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"'
    ].join(','),
    '&:focus': {
      borderRadius: 4,
      borderColor: '#80bdff',
      boxShadow: '0 0 0 0.2rem rgba(0,123,255,.25)'
    }
  }
}))(InputBase);

class Tables extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      total_data: 0,
      total_sge: 0,
      total_loan_amount: 0,
      total_admin_fee: 0,
      total_page: 1,
      current_page: 1,
      prev_page: null,
      data: [],
      next_page: 1,
      rowsPerPage: 10,
      openSearch: false,
      rowFocus: '',
      page: 1,
      count: 1,
      data: [],
      loading: true,
      modal: false,
      selectAll: false,
      order: 'asc',
      rowSelected: [],
      bulk: false,
      redirect: false,
      failure: false,
    };
    this.ChangePage = this
      .ChangePage
      .bind(this);
  }

  handleClick(id) {
    alert(id);
  }

  ChangePage(event, page) {
    this.setState({
      page
    }, () => {
      this.getData();
    });
  }

  setFocus(row) {
    this.setState({ rowFocus: row.id });
  }

  setUnfocus() {
    setTimeout(() => {
      this.setState({ rowFocus: '' });
    }, 5500);
  }

  handleModal(row) {
    this.setState({
      modal2: false,
      modal: true,
      row
    }, () => {
      this.setState({ modal: false, bulk: false });
    });
  }

  handleApprove(row) {
    this.setState({
      modal2: true,
      modal: false,
      row
    }, () => {
      this.setState({ modal2: false, bulk: false });
    });
  }

  componentDidMount() {
    if (localStorage.getItem('delete_product') != null) {
      this.handleDelete(localStorage.getItem('delete_product'));
    } else {
      const d = new Date();
      this.setState({
        startDate: new Date(parseInt(d.getMonth()) + 1 + '-01-' + d.getFullYear()),
        endDate: d
      }, () => {
        this.getData('first');
      });
    }
  }

    handleClickDelete = (row) => {
      Swal
        .fire({
          title: 'Apakah Anda yakin?',
          text: 'Akan menghapus data yang dipilih',
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Ya, Hapus!',
          cancelButtonText: 'Batal'
        })
        .then((result) => {
          if (result.value) {
            fetch(process.env.REACT_APP_URL_MANAGEMENT + process.env.REACT_APP_API_PREFIX_V1 + '/transactions' + '/' + row, {
              method: 'DELETE',
              headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + localStorage.getItem('token')
              }
            }).then((response) => response.json()).then((json) => {
              this.getData('first');
              this.setState({ rowSelected: [] });
              Swal.fire('Berhasil!', 'Data berhasil dihapus.', 'success');
            }).catch((error) => {})
              .finally(() => {});
          }
        });
    };

    handleDelete = (row) => {
      fetch(process.env.REACT_APP_URL_MANAGEMENT + process.env.REACT_APP_API_PREFIX_V1 + '/transactions' + '/' + row, {
        method: 'DELETE',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + localStorage.getItem('token')
        }
      }).then((response) => response.json()).then((json) => {
        localStorage.removeItem('delete_product');
        this.getData('first');
      }).catch((error) => {})
        .finally(() => {});
    };

    formatDate(date) {
      const d = new Date(date);
      let month = '' + (d.getMonth() + 1);
      let day = '' + d.getDate();
      const year = d.getFullYear();

      if (month.length < 2) month = '0' + month;
      if (day.length < 2) day = '0' + day;

      return [year, month, day].join('-');
    }

    getData = () => {
      this.setState({ loading: true });
      Func
        .getDataTransactionV2('transactions/published.json?q[created_at_gteq]=' + this.formatDate(this.state.startDate) + '&q[created_at_lteq]=' + this.formatDate(this.state.endDate) + '&type=repayment', this.state.rowsPerPage, this.state.page, this.state.search)
        .then((res) => {
          if (res.json.code === '403') {
            if (Func.Clear_Token() === true) {
              if (!localStorage.getItem('token')) {
                this.setState({ redirect: true });
              }
            }
          } else if (res.json.status === 500) {
            this.setState({ failure: true });
          } else {
            const datas = [];
            res.json.data.map((data) => {
              if (data.company === null) {
                datas.push(
                  createData(
                    data.id,
                    data.insurance_item_name,
                    data.customer.cif_number,
                    data.sge,
                    data.customer.name,
                    data.loan_amount,
                    data.maximum_loan_percentage,
                    data.admin_fee,
                    data.monthly_fee,
                    data.estimate_value,
                    data.contract_date,
                    data.due_date,
                    data.auction_date
                  )
                );
              }
            });
            this.setState({
              con: res.json.data.length,
              data: datas,
              loading: false,
              total_data: res.json.total_data,
              total_sge: res.json.total_sge,
              total_loan_amount: Func.FormatRp(res.json.total_loan_amount),
              total_admin_fee: Func.FormatRp(res.json.total_admin_fee),
              page: res.json.page,
              next_page: res.json.next_page,
              prev_page: res.json.prev_page,
              current_page: res.json.current_page,
              total_page: res.json.total_page
            });
          }
        });
    };

    Short(orderKey) {
      if (orderKey === this.state.order) {
        this.setState({ order: '' });
        const library = this.state.data;
        library.sort((a, b) => (a[orderKey] < b[orderKey]
          ? 1
          : b[orderKey] < a[orderKey]
            ? -1
            : 0));
      } else {
        this.setState({ order: orderKey });
        const library = this.state.data;
        library.sort((a, b) => (a[orderKey] > b[orderKey]
          ? 1
          : b[orderKey] > a[orderKey]
            ? -1
            : 0));
      }
    }

    handleClickDeleteAll = () => {
      Swal
        .fire({
          title: 'Apakah Anda yakin?',
          text: 'Akan menghapus data yang dipilih',
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Ya, Hapus!',
          cancelButtonText: 'Batal'
        })
        .then((result) => {
          if (result.value) {
            this.state.rowSelected.map((row) => {
              fetch(process.env.REACT_APP_URL_MANAGEMENT + process.env.REACT_APP_API_PREFIX_V1 + '/transactions' + '/' + row, {
                method: 'DELETE',
                headers: {
                  Accept: 'application/json',
                  'Content-Type': 'application/json',
                  Authorization: 'Bearer ' + localStorage.getItem('token')
                }
              }).then((response) => response.json()).then((json) => {

              }).catch((error) => {})
                .finally(() => {});
            });
            this.getData('first');
            this.setState({ rowSelected: [] });
            Swal.fire('Berhasil!', 'Data berhasil dihapus.', 'success');
          }
        });
    };

    DeleteBulk() {
      this.handleClickDeleteAll();
    }

    render() {
      const { classes } = this.props;
      let con = 0;
      this.state.data.map((row) => {
        if (this.state.rowSelected.indexOf(row.id) > -1) {
          con += 1;
        }
      });
      return (
        <div>
          {Func.toLogin(this.state.redirect)}
          <TableContainer style={{ width: this.props.open ? window.innerWidth - this.props.width : window.innerWidth - 115 }} component={Paper}>
            <div style={{
              width: '100%'
            }}
            >
              <Box display="flex">
                <Box flexGrow={1}>
                  <Typography variant="h6" className={classes.paginationTxt2}>
                    {this.props.title}
                  </Typography>
                </Box>
              </Box>
            </div>
            <div style={{
              width: '100%'
            }}
            >
              <Box display="flex">
                <Box flexGrow={1}>
                  <Typography className={classes.paginationTxt}>
                    {this.props.subtitle}
                  </Typography>
                </Box>
              </Box>
            </div>
            <Hidden smUp>
              <div
                style={{
                  width: '100%'
                }}
              >
                <Box display="flex">
                  <Box flexGrow={1}>
                    <Typography variant="h9" className={classes.textperdata}>
                      Tampilkan
                    </Typography>
                    <Select
                      size="small"
                      className={classes.selectperdata}
                      labelId="demo-customized-select-label"
                      id="demo-customized-select"
                      value={this.state.rowsPerPage}
                      onChange={(val) => {
                        this.setState({
                          rowsPerPage: val.target.value
                        }, () => {
                          this.getData();
                        });
                      }}
                      input={<BootstrapInput />}
                    >
                      <MenuItem value={10}>10 data</MenuItem>
                      <MenuItem value={50}>50 data</MenuItem>
                      <MenuItem value={100}>100 data</MenuItem>
                    </Select>
                  </Box>
                </Box>
              </div>
              <div>
                <Typography variant="h9" className={classes.textperdata}>
                  Total:
                  {' '}
                  {this.state.total_data}
                  {' '}
                  produk
                </Typography>
              </div>
              <div
                style={{
                  width: '100%'
                }}
              >
                <Box display="flex">
                  <Box>
                    <BootstrapInput
                      style={{
                        marginBottom: 20,
                        marginLeft: -0
                      }}
                      value={this.state.search}
                      onChange={(event) => {
                        this.setState({
                          search: event.target.value
                        }, () => {
                          this.getData();
                        });
                      }}
                      on
                      className={classes.search2}
                      id="demo-customized-textbox"
                    />
                  </Box>
                </Box>
              </div>
            </Hidden>
            <Hidden xsDown>
              <Box display="flex" style={{ marginTop: -40 }} flexDirection="row-reverse" p={1} m={1}>
                <Box p={1}>
                  <text style={{ fontWeight: 'bold' }}>Total SGE : </text>
                  <text>{this.state.total_sge}</text>
                </Box>
                <Box p={1}>
                  <text style={{ fontWeight: 'bold' }}>Total Pinjaman : </text>
                  {this.state.total_loan_amount}
                </Box>
                <Box p={1}>
                  <text style={{ fontWeight: 'bold' }}>Total Admin Fee : </text>
                  {this.state.total_admin_fee}
                </Box>
              </Box>
              <div
                style={{
                  width: '100%',
                  // position:'absolute',
                  right: 20
                }}
              >
                <Box style={{ marginBottom: 20 }} display="flex">
                  <Box flexGrow={1}>
                    <Typography variant="h9" className={classes.textperdata}>
                      Tampilkan
                    </Typography>
                    <Select
                      size="small"
                      className={classes.selectperdata}
                      labelId="demo-customized-select-label"
                      id="demo-customized-select"
                      value={this.state.rowsPerPage}
                      onChange={(val) => {
                        this.setState({
                          rowsPerPage: val.target.value
                        }, () => {
                          this.getData();
                        });
                      }}
                      input={<BootstrapInput />}
                    >
                      <MenuItem value={10}>10 data</MenuItem>
                      <MenuItem value={50}>50 data</MenuItem>
                      <MenuItem value={100}>100 data</MenuItem>
                    </Select>
                  </Box>
                  <Box>
                    <DatePicker
                      className={classes.Startdate}
                      selected={this.state.startDate}
                      onChange={(startDate) => {
                        if (Object.prototype.toString.call(startDate) === '[object Date]') {
                          this.setState({ startDate });
                        }
                      }}
                      selectsStart
                      selectsEnd
                      disabledKeyboardNavigation
                      value={Func.FormatDate(this.state.startDate)}
                      placeholderText="Tanggal Awal"
                      startDate={this.state.startDate}
                      endDate={this.state.endDate}
                    />
                  </Box>
                  <Box>
                    <DatePicker
                      className={classes.date}
                      selected={this.state.endDate}
                      selectsEnd
                      onChange={(endDate) => {
                        if (Object.prototype.toString.call(endDate) === '[object Date]') {
                          this.setState({ endDate });
                        }
                      }}
                      value={Func.FormatDate(this.state.endDate)}
                      placeholderText="Tanggal Akhir"
                      startDate={this.state.startDate}
                      endDate={this.state.endDate}
                      disabledKeyboardNavigation
                      minDate={this.state.startDate}
                    />
                  </Box>
                  <Box style={{ marginRight: 20 }}>
                    <Button
                      variant="contained"
                      onClick={() => {
                        this.getData();
                      }}
                    >
                      Terapkan
                    </Button>
                  </Box>
                  {
                    Func.checkPermission('report#repayment#export')
                      ? (
                        <Box style={{ marginRight: 20 }}>
                          <Button
                            variant="contained"
                            color="primary"
                            component="a"
                            href={env.managementApi + env.apiPrefixV1 + '/transactions/published.xlsx?q[created_at_gteq]=' + this.formatDate(this.state.startDate) + '&q[created_at_lteq]=' + this.formatDate(this.state.endDate) + '&token=' + localStorage.getItem('token')}
                            target="_blank"
                          >
                            Export
                          </Button>
                        </Box>
                      )
                      : null
                  }
                </Box>
              </div>
            </Hidden>
            <FormCountry type="Ubah" modal={this.state.modal} modal2={this.state.modal2} row={this.state.row} />
          </TableContainer>
          <TableContainer style={{ width: this.props.open ? window.innerWidth - this.props.width : window.innerWidth - 115 }} component={Paper}>
            <Table
              size="small"
              onMouseOut={() => {
                this.setUnfocus();
              }}
              className={classes.table}
              aria-label="simple table"
            >
              <TableHead className={classes.headTable}>
                <TableRow>
                  {headCells.map((headCell) => (
                    <TableCell
                      key={headCell.id}
                      align={headCell.numeric
                        ? 'right'
                        : 'left'}
                      sortDirection={this.state.order === headCell.id
                        ? this.state.order
                        : false}
                    >
                      <TableSortLabel
                        active={this.state.order === headCell.id}
                        direction={this.state.order === headCell.id
                          ? this.state.order
                          : 'asc'}
                        onClick={() => {
                          this.Short(headCell.id);
                        }}
                      >
                        {headCell.label}
                        {this.state.order === headCell.id
                          ? (
                            <span className={classes.visuallyHidden} />
                          )
                          : null}
                      </TableSortLabel>
                    </TableCell>
                  ))}
                </TableRow>
              </TableHead>
              {this.state.data.length === 0 && !this.state.loading
                ? (
                  <TableBody>
                    <TableRow>
                      <TableCell colSpan={12} align="center">
                        <Typography>
                          Tidak Ada Data
                        </Typography>
                      </TableCell>
                    </TableRow>
                  </TableBody>
                )
                : null}
              <TableBody>
                {!this.state.loading
                  ? this
                    .state
                    .data
                    .map((row) => (
                      <TableRow
                        hover
                        onMouseOver={() => {
                          this.setFocus(row);
                        }}
                        role="checkbox"
                        tabIndex={-1}
                        key={row.id}
                      >
                        <TableCell onClick={() => { this.handleApprove(row); }} align="left">{row.insurance_item_name}</TableCell>
                        <TableCell onClick={() => { this.handleApprove(row); }} align="left">{row.sge}</TableCell>
                        <TableCell onClick={() => { this.handleApprove(row); }} align="left">{row.name}</TableCell>
                        <TableCell onClick={() => { this.handleApprove(row); }} align="left">{Func.FormatRp(row.loan_amount)}</TableCell>
                        <TableCell onClick={() => { this.handleApprove(row); }} align="left">
                          {row.ltv}
                          {' '}
                          %
                        </TableCell>
                        <TableCell onClick={() => { this.handleApprove(row); }} align="left">{Func.FormatRp(row.admin_fee)}</TableCell>
                        <TableCell onClick={() => { this.handleApprove(row); }} align="left">{Func.FormatRp(row.monthly_fee)}</TableCell>
                        <TableCell onClick={() => { this.handleApprove(row); }} align="left">{Func.FormatDate(row.contract_date)}</TableCell>
                      </TableRow>
                    ))

                  : null}
              </TableBody>
            </Table>
            {this.state.loading
              ? (
                <div className={classes.loader}>
                  <BeatLoader size={15} color="#3F3F3F" loading />
                </div>
              )
              : null}
          </TableContainer>
          <TableContainer style={{ width: this.props.open ? window.innerWidth - this.props.width : window.innerWidth - 115, marginBottom: 40 }} component={Paper}>
            <div className={classes.col}>
              <text>
                Halaman
                {' '}
                {' ' + this.state.current_page + ' '}
                dari
                {' '}
                {' ' + this.state.total_page + ' '}
                halaman
              </text>
            </div>
            <Pagination
              color="secondary"
              className={classes.row}
              count={this.state.total_page}
              defaultPage={this.state.page}
              onChange={this.ChangePage}
              siblingCount={0}
            />
          </TableContainer>
          {this.state.rowSelected.length > 0
            ? (
              <div className={classes.popupv2}>
                <Box display="flex" justifyContent="center" flexWrap="wrap">
                  <Box flexWrap="wrap">
                    <div className={classes.popup}>
                      <Box display="flex" justifyContent="center" flexWrap="wrap">
                        <Box>
                          <Typography className={classes.popupTxt}>
                            Pilih aksi:
                          </Typography>
                        </Box>
                        <Box>
                          <IconButton
                            onClick={() => {
                              this.DeleteBulk();
                            }}
                            aria-label="Cari"
                          >
                            <img src={Icon.popupDelete} />
                          </IconButton>
                        </Box>
                        <Box>
                          <IconButton
                            className={classes.popupClose}
                            onClick={() => {
                              this.setState({ rowSelected: [] });
                            }}
                            aria-label="Cari"
                          >
                            <img src={Icon.close} />
                          </IconButton>
                        </Box>
                      </Box>
                    </div>
                  </Box>
                </Box>
              </div>
            )
            : null}
        </div>
      );
    }
}

export default withStyles(styles.CoustomsStyles, { name: 'Tables' })(Tables);
