/* eslint-disable react/destructuring-assignment */
import React from 'react';
import Hidden from '@material-ui/core/Hidden';
import { withStyles } from '@material-ui/core/styles';
import styles from '../css';
import Melting from './melting';

class Index extends React.Component {
  render() {
    return (
      <div>
        <div>
          <Hidden only={['lg', 'xl']}>
            <Melting
              width={60}
              open={this.props.open}
              filter={false}
              title="Laporan Transaksi Outstanding"
              subtitle=""
            />
          </Hidden>
          <Hidden only={['sm', 'md', 'xs']}>
            <Melting
              width={285}
              open={this.props.open}
              filter={false}
              title="Laporan Transaksi Outstanding"
              subtitle=""
            />
          </Hidden>
        </div>
      </div>
    );
  }
}

export default withStyles(styles.CoustomsStyles, { name: 'Index' })(Index);
