/* eslint-disable react/jsx-one-expression-per-line */
import React from 'react';
import { Card, CardContent, Box } from '@material-ui/core';
import clsx from 'clsx';
import useStyles from './dashboard-jss';
import Chart from '../../../components/Chart';

const SectionChart = ({ data }) => {
  const classes = useStyles();

  return (
    <Card classes={{ root: classes.root }}>
      <CardContent>
        <div className={clsx(classes.userText, classes.summaryTitle)}>
          Statistik Transaksi
        </div>
        <div className={classes.summaryBox}>
          <div className={classes.summaryBodyChart}>
            <Box borderColor="#C4C4C4" p={3} borderRight={1} borderBottom={1}>
              <Chart
                total={data?.outstanding?.length || 0}
                type="bar"
                title="Outstanding"
                color="rgba(147,196,125,1)"
                data={data?.outstanding || []}
              />
            </Box>
          </div>
          <div className={classes.summaryBodyChart}>
            <Box borderColor="#C4C4C4" p={3} borderBottom={1}>
              <Chart
                total={data?.disbursement?.length || 0}
                type="line"
                title="Pencairan"
                color="rgba(147,196,125,1)"
                data={data?.disbursement || []}
              />
            </Box>
          </div>
        </div>
        <div className={classes.summaryBox}>
          <div className={classes.summaryBodyChart}>
            <Box borderColor="#C4C4C4" p={3} borderRight={1} borderBottom={1}>
              <Chart
                total={data?.repayment?.length || 0}
                type="bar"
                title="Pelunasan"
                color="rgba(111,168,220,1)"
                data={data?.repayment || []}
              />
            </Box>
          </div>
          <div className={classes.summaryBodyChart}>
            <Box borderColor="#C4C4C4" p={3} borderBottom={1}>
              <Chart
                total={data?.installment?.length || 0}
                title="Cicilan"
                type="line"
                color="rgba(111,168,220,1)"
                data={data?.installment || []}
              />
            </Box>
          </div>
        </div>
        <div className={classes.summaryBox}>
          <div className={classes.summaryBodyChart}>
            <Box borderColor="#C4C4C4" p={3} borderRight={1}>
              <Chart
                total={data?.income?.length || 0}
                title="Pendapatan"
                type="bar"
                color="rgba(111,168,220,1)"
                data={data?.income || []}
              />
            </Box>
          </div>
          <div className={classes.summaryBodyChart}>
            <Box borderColor="#C4C4C4" p={3}>
              <Chart
                total={data?.dpd_graph?.length || 0}
                title="DPD"
                type="line"
                color="rgba(111,168,220,1)"
                data={data?.dpd_graph || []}
              />
            </Box>
          </div>
        </div>
      </CardContent>
    </Card>
  );
};

export default SectionChart;
