/* eslint-disable react/jsx-one-expression-per-line */
import React, { useState, useEffect } from 'react';
import { Alert, AlertTitle } from '@material-ui/lab';
import { makeStyles } from '@material-ui/core/styles';
import swal from 'sweetalert';
import SectionSummary from './SectionSummary';
import SectionCashflow from './SectionCashflow';
import SectionChart from './SectionChart';
import { getApi } from './api';
import { getCashStatus2 } from '../Cashflow/Cashflow/api';

const useStyles = makeStyles((theme) => ({
  root: {
    margin: theme.spacing(2),
  },
  dashboardItem: {
    borderRadius: 8,
    marginBottom: 16,
  },
}));

const Dashboard = () => {
  const classes = useStyles();
  const [dashboardData, setDashboardData] = useState({});

  useEffect(() => {
    getCashStatus2(localStorage.getItem('office_id')).then((res) => {
      const response = res?.data || res?.response?.data;
      if (res?.status !== 200) {
        localStorage.setItem('cashStatus', 'Tertutup');
      } else {
        localStorage.setItem('cashStatus', 'Terbuka');
      }
      localStorage.setItem('balance', response.balance);
    });
    getApi().then((res) => {
      const response = res?.data || res?.response?.data;
      if (res?.status !== 200) {
        swal({
          title: 'Kesalahan',
          text: response?.message,
          icon: 'Error',
        });
      } else setDashboardData(response);
    });
  }, []);

  return (
    <div className={classes.root}>
      <div className={classes.content}>
        <Alert
          severity="warning"
          classes={{ root: classes.dashboardItem }}
          onClose={() => { }}
        >
          <AlertTitle>Sampel Push Notifikasi</AlertTitle>
          This is an error alert — <strong>check it out!</strong>
        </Alert>
      </div>
      <SectionSummary data={dashboardData} />
      <SectionCashflow data={dashboardData.cash_flow} />
      <SectionChart data={dashboardData} />
    </div>
  );
};

export default Dashboard;
