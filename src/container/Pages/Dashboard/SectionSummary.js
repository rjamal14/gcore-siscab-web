/* eslint-disable react/jsx-one-expression-per-line */
import React from 'react';
import { Card, CardContent, Typography, Divider } from '@material-ui/core';
import clsx from 'clsx';
import AccountBalanceWalletIcon from '@material-ui/icons/AccountBalanceWallet';
import InboxIcon from '@material-ui/icons/Inbox';
import TodayIcon from '@material-ui/icons/Today';
import useStyles from './dashboard-jss';
import Func from '../../../functions';

const FirstBox = ({ classes, data }) => (
  <div className={classes.summaryBody}>
    <div className={clsx(classes.userText, classes.summaryTitle)}>
      SALDO
    </div>
    <div className={classes.summaryItem}>
      <AccountBalanceWalletIcon color="secondary" style={{ fontSize: 45 }} />
      <div className={classes.description}>
        <Typography style={{ fontSize: 22 }}>
          Petty Cash
        </Typography>
        <Typography className={classes.cashText}>
          {Func.currencyFormatter(data?.petty_cash?.remaining_balance)}
        </Typography>
        <Typography style={{ fontSize: 22 }}>
          Kas Operasional
        </Typography>
        <Typography className={classes.cashText}>
          {Func.currencyFormatter(data?.cash?.remaining_balance)}
        </Typography>
      </div>
    </div>
  </div>
);

const SecondBox = ({ classes, data }) => (
  <div className={classes.summaryBodySecond}>
    <div className={clsx(classes.userText, classes.summaryTitle)}>
      OS
    </div>
    <div className={classes.summaryItem}>
      <InboxIcon color="secondary" style={{ fontSize: 45 }} />
      <div className={classes.description}>
        <Typography style={{ fontSize: 22 }}>
          Reguler
        </Typography>
        <Typography className={classes.cashText}>
          {Func.currencyFormatter(data?.transactions?.os)}
          <span style={{ color: '#000000', fontSize: 22 }}> of </span>
          {data?.transactions?.noa}
          <span style={{ color: '#000000', fontSize: 22 }}> Noa</span>
        </Typography>
        <Typography style={{ fontSize: 22 }}>
          Cicilan
        </Typography>
        <Typography className={classes.cashText}>
          Rp. 50,400,150
          <span style={{ color: '#000000', fontSize: 22 }}> of </span>
          177 <span style={{ color: '#000000', fontSize: 22 }}>Noa</span>
        </Typography>
      </div>
    </div>
  </div>
);

const ThirdBox = ({ classes, data }) => (
  <div className={classes.summaryBodySecond}>
    <div className={clsx(classes.userText, classes.summaryTitle)}>
      DPD
    </div>
    <div className={classes.summaryItem}>
      <TodayIcon color="secondary" style={{ fontSize: 45 }} />
      <div className={classes.description}>
        <Typography style={{ fontSize: 22 }}>
          DPD
        </Typography>
        <Typography className={classes.cashText}>
          {Func.currencyFormatter(data?.dpd?.os)}
          <span style={{ color: '#000000', fontSize: 22 }}> of </span>
          {data?.dpd?.noa}
          <span style={{ color: '#000000', fontSize: 22 }}> Noa</span>
        </Typography>
      </div>
    </div>
  </div>
);

const SectionSummary = ({ data }) => {
  const classes = useStyles();

  return (
    <Card classes={{ root: classes.root }}>
      <CardContent>
        <Typography className={classes.welcomeText}>
          Halo, Selamat datang kembali!
        </Typography>
        <Typography className={classes.userText}>
          {localStorage.getItem('name')}
        </Typography>
        <div className={classes.summaryBox}>
          <FirstBox classes={classes} data={data} />
          <Divider orientation="vertical" flexItem />
          <SecondBox classes={classes} data={data} />
          <Divider orientation="vertical" flexItem />
          <ThirdBox classes={classes} data={data} />
        </div>
      </CardContent>
    </Card>
  );
};

export default SectionSummary;
