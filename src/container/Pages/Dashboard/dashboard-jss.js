import { makeStyles } from '@material-ui/core';

const useStyles = makeStyles(theme => ({
  root: {
    padding: '16px 16px 22px',
    borderRadius: 8,
    width: '100%',
    marginBottom: 16,
  },
  root2: {
    padding: '16px 16px 22px',
    borderRadius: 8,
    width: '100%',
    marginBottom: 16,
  },
  // content: {
  //   marginLeft: '20px'
  // },
  dashboardItem: {
    borderRadius: 8,
  },
  welcomeText: {
    fontSize: '18px',
    fontWeight: '400',
    color: '#808080',
  },
  userText: {
    fontWeight: '400',
    fontSize: '27px',
    lineHeight: '2',
    color: '#6E6868'
  },
  summaryBody: {
    width: '33.333%',
  },
  summaryBodyChart: {
    width: '100%',
  },
  summaryBodySecond: {
    width: '33.333%',
    paddingLeft: '2%',
    marginBottom: '2%'
  },
  summaryBox: {
    display: 'flex',
    flexDirection: 'row',
    width: '100%',
  },
  summaryTitle: {
    fontSize: '22px',
    fontWeight: '600',
    color: '#6E6868',
  },
  summaryItem: {
    display: 'flex',
    alignSelf: 'left',
    margin: -3,
  },
  summaryIcon: {
    display: 'flex',
    flexDirection: 'column',
  },
  description: {
    paddingLeft: 15,
  },
  cashText: {
    fontWeight: 'bold',
    fontSize: 25,
    color: theme.palette.secondary.main,
    lineHeight: '2',
  },
  tableText: {
    fontSize: 18,
    color: '#808080'
  }
}));

export default useStyles;
