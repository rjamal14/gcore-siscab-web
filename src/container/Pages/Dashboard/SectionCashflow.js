/* eslint-disable react/jsx-one-expression-per-line */
import React from 'react';
import { Card, CardContent, Table, TableBody, TableHead, TableRow, TableCell } from '@material-ui/core';
import clsx from 'clsx';
import * as dayjs from 'dayjs';
import useStyles from './dashboard-jss';
import 'dayjs/locale/id';
import Func from '../../../functions';

const SectionCashflow = ({ data }) => {
  const classes = useStyles();

  return (
    <Card classes={{ root: classes.root2 }}>
      <CardContent>
        <div className={clsx(classes.userText, classes.summaryTitle)}>
          Arus Kas
        </div>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell className={classes.tableText}>Tanggal</TableCell>
              <TableCell className={classes.tableText}>Jam</TableCell>
              <TableCell className={classes.tableText}>No. Referensi</TableCell>
              <TableCell className={classes.tableText}>Keterangan</TableCell>
              <TableCell className={classes.tableText}>Debit</TableCell>
              <TableCell className={classes.tableText}>Kredit</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {
              (data?.length > 0) && data.map((cols) => (
                <TableRow>
                  <TableCell className={classes.tableText}>{dayjs(cols.publish_date).locale('id').format('DD MMMM YYYY')}</TableCell>
                  <TableCell className={classes.tableText}>{dayjs(cols.publish_date).locale('id').format('HH:mm')}</TableCell>
                  <TableCell className={classes.tableText}>{cols.ref || '-'}</TableCell>
                  <TableCell className={classes.tableText}>{cols.description}</TableCell>
                  <TableCell className={classes.tableText}>{Func.currencyFormatter(cols.debit)}</TableCell>
                  <TableCell className={classes.tableText}>{Func.currencyFormatter(cols.credit)}</TableCell>
                </TableRow>
              ))
            }
            {
              (data?.length <= 0) && (
                <TableRow>
                  <TableCell cols={6}>Tidak Ada Catatan Arus Kas</TableCell>
                </TableRow>
              )
            }
          </TableBody>
        </Table>
      </CardContent>
    </Card>
  );
};

export default SectionCashflow;
