/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable no-shadow */
/* eslint-disable array-callback-return */
/* eslint-disable react/no-unused-state */
/* eslint-disable consistent-return */
/* eslint-disable radix */
/* eslint-disable react/no-access-state-in-setstate */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/sort-comp */
import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import InputAdornment from '@material-ui/core/InputAdornment';
import Typography from '@material-ui/core/Typography';
import FormHelperText from '@material-ui/core/FormHelperText';
import Grid from '@material-ui/core/Grid';
import TextareaAutosize from '@material-ui/core/TextareaAutosize';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import CloseIcon from '@material-ui/icons/Close';
import IconButton from '@material-ui/core/IconButton';
import Button from '@material-ui/core/Button';
import AsyncSelect from 'react-select/async';
import swal from 'sweetalert';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import { CircularProgress } from '@material-ui/core';
import env from '../../../../../config/env';
import styles from '../css';
import Icon from '../../../../../components/icon';
import Func from '../../../../../functions/index';

class Form extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      validator: [],
      value: { req_amount: '' },
    };
    this.handleChangeImg = this
      .handleChangeImg
      .bind(this);
    this.timeout = 0;
  }

  removeValidate(name) {
    const data = this.state.validator;
    delete data[name];
    this.setState({ validator: data });
  }

  handleChange(event, name) {
    const dataSet = this.state.value;
    dataSet[name] = event;
    this.setState({ value: dataSet });
  }

  handleChangeDate(date, name, name2) {
    const dt = new Date(date);
    const dataSet = this.state.value;
    dataSet[name2] = dt;
    dataSet[name] = dt.getFullYear() + '-' + parseInt(dt.getMonth() + 1) + '-' + dt.getDate();
    this.setState({ value: dataSet });
  }

  componentDidMount() {
    this.setState({ loader: true });
    this.getAkun('', '');
  }

  handleSubmit(type) {
    this.setState({ loader_button: true });
    const validator = [
      {
        name: 'req_subject',
        type: 'required'
      }, {
        name: 'req_description',
        type: 'required'
      }, {
        name: 'req_coa',
        type: 'required'
      }, {
        name: 'applicant_name',
        type: 'required'
      }, {
        name: 'req_amount',
        type: 'required'
      },
    ];

    const validate = Func.Validator(this.state.value, validator);
    if (validate.success) {
      fetch(env.managementApi + env.apiPrefixV1 + '/cash_transactions/' + (type === 'Tambah'
        ? ''
        : this.props.id), {
        method: type === 'Tambah'
          ? 'POST'
          : 'PUT',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + localStorage.getItem('token')
        },
        body: JSON.stringify({
          req_subject: this.state.value.req_subject,
          req_description: this.state.value.req_description,
          req_coa: this.state.value.req_coa.value,
          applicant_name: this.state.value.applicant_name,
          req_amount: parseInt(this.state.value.req_amount),
        })
      }).then((response) => response.json()).then((json) => {
        if (json.code === '403') {
          Func.Refresh_Token();
          if (Func.Refresh_Token() === true) {
            this.handleSubmit();
          }
        }
        if (type === 'Tambah') {
          if (json.created) {
            this
              .props
              .OnNext(json.message);
            this.setState({ loader_button: false });
          } else {
            Func.AlertError(json.message);
            this.setState({ loader_button: false });
          }
        } else if (json.code === 200) {
          this
            .props
            .OnNext(json.message);
          this.setState({ loader_button: false });
        } else {
          Func.AlertError(json.message);
          this.setState({ loader_button: false });
        }
      }).catch(() => {})
        .finally(() => {});
    } else {
      this.setState({ validator: validate.error });
      this.setState({ loader_button: false });
    }
  }

  handleChangeImg(event) {
    const file = event.target.files[0];
    if (file.size > 2.9e6) {
      swal({
        title: 'File Terlalu Besar',
        text: 'Maximal File 2Mb',
        icon: 'error',
        buttons: 'OK'
      });
      return false;
    }
    this.removeValidate('img');
    const dataSet = this.state.value;
    dataSet.img = URL.createObjectURL(event.target.files[0]);
    this.setState({ value: dataSet });
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onloadend = () => {
      this.setState({ ImgBase44: reader.result });
    };
  }

  getAkun(val) {
    fetch(env.financialApi + env.apiPrefixV1 + '/accounts/autocomplete?query=' + val, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + localStorage.getItem('token')
      }
    }).then((response) => response.json()).then((json) => {
      if (json.code === '403') {
        if (Func.Clear_Token() === true) {
          if (!localStorage.getItem('token')) {
            this.setState({ redirect: true });
          }
        }
      } else {
        const datas = [];
        datas.push({
          value: '-',
          label: json.data.length > 0
            ? 'Pilih'
            : 'Tidak ditemukan',
          isDisabled: true
        });
        json
          .data
          .map((value) => {
            datas.push({ label: value.account_number + ' - ' + value.item, value: value.account_number });
          });

        this.setState({ data5: datas, data5_ori: json.data }, () => {
          if (this.props.type === 'Ubah') {
            this.setState({ loader: true });
            fetch(env.managementApi + env.apiPrefixV1 + '/cash_transactions/' + this.props.id, {
              method: 'GET',
              headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + localStorage.getItem('token')
              }
            }).then((response) => response.json()).then((json) => {
              if (json.code === '403') {
                if (Func.Clear_Token() === true) {
                  if (!localStorage.getItem('token')) {
                    this.setState({ redirect: true });
                  }
                }
              }
              const val = [];
              const search = this.state.data5_ori.find(o => o.account_number === parseInt(json.data.cash_transactions.req_coa));
              val.req_subject = json.data.cash_transactions.req_subject;
              val.req_description = json.data.cash_transactions.req_description;
              val.req_amount = json.data.cash_transactions.req_amount;
              val.applicant_name = json.data.cash_transactions.applicant_name;
              val.req_coa = { value: search.account_number, label: search.account_number + ' - ' + search.item };

              this.setState({
                value: val,
                loader: false
              });
            }).catch(() => {})
              .finally(() => {});
          } else {
            this.setState({ loader: false });
          }
        });
      }
    }).catch(() => {})
      . finally(() => {});
  }

  render() {
    const loadOptions2 = (inputValue, callback) => {
      setTimeout(() => {
        callback(this.state.data5);
      }, 600);
    };
    const { classes } = this.props;
    if (this.state.loader) {
      return (
        <div className={classes.root2}>
          <Dialog
            disablePortal
            disableEnforceFocus
            disableAutoFocus
            open
            scroll="paper"
            maxWidth="md"
            aria-labelledby="server-modal-title"
            aria-describedby="server-modal-description"
            container={() => {}}
          >
            <div
              style={{
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
                padding: '20px',
                borderRadius: '50px',
              }}
            >
              <CircularProgress
                style={{ color: '#85203B', margin: '18px' }}
                size={40}
              />
              Mohon Tunggu
              <div
                style={{
                  marginTop: '10px',
                }}
              />
            </div>
          </Dialog>
        </div>
      );
    }

    return (
      <Dialog
        scroll="paper"
        open
        maxWidth="md"
        aria-labelledby="scroll-dialog-title"
        aria-describedby="scroll-dialog-description"
      >
        <DialogTitle id="scroll-dialog-title">
          <Typography variant="h7" className={classes.tittleModal}>
            {this.props.type + ' Permintaan Kantor Cabang'}
          </Typography>
          <IconButton
            disabled={this.state.loader_button}
            aria-label="close"
            className={classes.closeButton}
            onClick={() => {
              this.props.handleModal();
            }}
          >
            <CloseIcon />
          </IconButton>
        </DialogTitle>
        <DialogContent dividers>
          <DialogContentText id="scroll-dialog-description" tabIndex={-1}>
            <div className={classes.scrool}>
              {Func.toLogin(this.state.redirect)}
              <div className={classes.root}>
                <Grid container direction="row" item lg={12} xl={12} md={12} xs={12}>
                  <Grid container item lg={12} xl={12} md={12} sm={12} xs={12}>
                    <Grid item lg={12} xl={12} md={12} sm={12} xs={12}>
                      <div>
                        <text className={classes.label1}>Nomor COA</text>
                        <text className={classes.starts1}>*</text>
                      </div>
                      <AsyncSelect
                        name="form-field-name-error"
                        placeholder="Cari Akun"
                        value={this.state.value.req_coa}
                        onFocus={() => {
                          this.removeValidate('req_coa');
                        }}
                        styles={{
                          control: (provided) => ({
                            ...provided,
                            borderColor: this.state.validator.req_coa
                              ? 'red'
                              : '#CACACA',
                            borderRadius: '0.25rem'
                          })
                        }}
                        onInputChange={(val) => {
                          if (val) {
                            if (this.timeout) clearTimeout(this.timeout);
                            this.timeout = setTimeout(() => {
                              this.getAkun(val, '');
                            }, 500);
                          }
                        }}
                        cacheOptions
                        loadOptions={loadOptions2}
                        defaultOptions
                        className={classes.input2}
                        options={this.state.data5}
                        onChange={(val) => {
                          this.handleChange(val, 'req_coa');
                        }}
                      />
                      <FormHelperText className={classes.error}>
                        {this.state.validator.budget_request_type_id}
                      </FormHelperText>
                    </Grid>
                    <Grid item lg={12} xl={12} md={12} sm={12} xs={12}>
                      <div>
                        <text className={classes.label1}>
                          Nama Pemohon
                        </text>
                        <text className={classes.starts1}>*</text>
                      </div>
                      <TextField
                        size="small"
                        fullWidth
                        className={classes.input2}
                        variant="outlined"
                        autoComplete="off"
                        onFocus={() => {
                          this.removeValidate('applicant_name');
                        }}
                        error={this.state.validator.applicant_name}
                        helperText={this.state.validator.applicant_name}
                        value={this.state.value.applicant_name}
                        onChange={(event) => {
                          this.handleChange(event.target.value, 'applicant_name');
                        }}
                        name="applicant_name"
                      />
                    </Grid>
                    <Grid item lg={12} xl={12} md={12} sm={12} xs={12}>
                      <div>
                        <text className={classes.label1}>
                          Nominal
                        </text>
                        <text className={classes.starts1}>*</text>
                      </div>
                      <TextField
                        size="small"
                        fullWidth
                        className={classes.input2}
                        variant="outlined"
                        autoComplete="off"
                        onFocus={() => {
                          this.removeValidate('req_amount');
                        }}
                        error={this.state.validator.req_amount}
                        helperText={this.state.validator.req_amount}
                        value={Func.FormatNumber(this.state.value.req_amount)}
                        onChange={(event) => {
                          this.handleChange(Func.UnFormatRp(event.target.value), 'req_amount');
                        }}
                        name="req_amount"
                        InputProps={{
                          startAdornment: <InputAdornment position="start"> Rp </InputAdornment>
                        }}
                      />
                    </Grid>
                    <Grid item lg={12} xl={12} md={12} sm={12} xs={12}>
                      <div>
                        <text className={classes.label1}>
                          Nama Biaya
                        </text>
                        <text className={classes.starts1}>*</text>
                      </div>
                      <TextField
                        size="small"
                        fullWidth
                        className={classes.input2}
                        variant="outlined"
                        autoComplete="off"
                        onFocus={() => {
                          this.removeValidate('req_subject');
                        }}
                        error={this.state.validator.req_subject}
                        helperText={this.state.validator.req_subject}
                        value={this.state.value.req_subject}
                        onChange={(event) => {
                          this.handleChange(event.target.value, 'req_subject');
                        }}
                        name="req_subject"
                      />
                    </Grid>
                    <Grid item lg={12} xl={12} md={12} sm={12} xs={12}>
                      <div>
                        <text className={classes.label1}>
                          Keterangan Biaya
                        </text>
                        <text className={classes.starts1}>*</text>
                      </div>
                      <div>
                        <TextareaAutosize
                          className={this.state.validator.req_description
                            ? classes.textArea2
                            : classes.textArea}
                          variant="outlined"
                          margin="normal"
                          rows={8}
                          autoComplete="off"
                          onFocus={() => {
                            this.removeValidate('req_description');
                          }}
                          error={this.state.validator.req_description}
                          value={this.state.value.req_description}
                          onChange={(event) => {
                            this.handleChange(event.target.value, 'req_description');
                          }}
                          name="req_description"
                          InputProps={{
                            endAdornment: this.state.validator.req_description
                              ? (
                                <InputAdornment position="start">
                                  <img src={Icon.warning} />
                                </InputAdornment>
                              )
                              : (<div />)
                          }}
                        />
                        <FormHelperText className={classes.error}>
                          {this.state.validator.req_description}
                        </FormHelperText>
                      </div>
                    </Grid>
                  </Grid>
                </Grid>
              </div>
            </div>
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button
            style={{
              backgroundColor: '#862C3A',
              color: 'white',
            }}
            disabled={this.state.loader_button}
            onClick={() => {
              this.handleSubmit(this.props.type);
            }}
            variant="contained"
          >
            <Typography variant="button" style={{ color: '#FFFFFF' }}>
              {this.state.loader_button ? (
                <CircularProgress
                  style={{
                    color: 'white',
                    marginLeft: '18px',
                    marginRight: '18px',
                    marginTop: 5,
                  }}
                  size={15}
                />
              ) : (
                'Simpan'
              )}
            </Typography>
          </Button>
          <Button
            style={{
              color: 'black',
            }}
            disabled={this.state.loader_button}
            onClick={() => {
              this.props.handleModal();
            }}
            variant="outlined"
          >
            <Typography variant="button">Batal</Typography>
          </Button>
        </DialogActions>
      </Dialog>
    );
  }
}

export default withStyles(styles.CoustomsStyles, { name: 'Form' })(Form);
