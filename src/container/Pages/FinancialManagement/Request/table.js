/* eslint-disable no-unused-vars */
/* eslint-disable react/no-access-state-in-setstate */
/* eslint-disable no-lonely-if */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable class-methods-use-this */
/* eslint-disable react/sort-comp */
/* eslint-disable no-dupe-keys */
/* eslint-disable react/no-unused-state */
/* eslint-disable camelcase */
/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable array-callback-return */
import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import InputBase from '@material-ui/core/InputBase';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import Pagination from '@material-ui/lab/Pagination';
import { Hidden } from '@material-ui/core';
import BeatLoader from 'react-spinners/BeatLoader';
import EditIcon from '@material-ui/icons/Edit';
import FormCountry from './form/index';
import Func from '../../../../functions/index';
import styles from './css';

function createData(id, code, budget_request_type, request_date, requested_user_name, nominal, status) {
  return { id, code, budget_request_type, request_date, requested_user_name, nominal, status };
}

const headCells = [
  {
    id: 'code',
    numeric: false,
    disablePadding: false,
    label: 'Kode'
  }, {
    id: 'name',
    numeric: false,
    disablePadding: false,
    label: 'Jenis Permintaan'
  }, {
    id: 'hp',
    numeric: false,
    disablePadding: false,
    label: 'Tanggal'
  }, {
    id: 'os',
    numeric: false,
    disablePadding: false,
    label: 'Akun SisCab'
  }, {
    id: 'os',
    numeric: false,
    disablePadding: false,
    label: 'Nominal'
  }, {
    id: 'status',
    numeric: false,
    disablePadding: false,
    label: 'Status'
  }, {
    id: 'action',
    numeric: false,
    disablePadding: false,
    label: ''
  }
];

const BootstrapInput = withStyles((theme) => ({
  root: {
    'label + &': {
      marginTop: theme.spacing(3)
    }
  },
  input: {
    borderRadius: 4,
    position: 'relative',
    backgroundColor: theme.palette.background.paper,
    border: '1px solid #ced4da',
    fontSize: 16,
    padding: '10px 26px 10px 12px',
    transition: theme
      .transitions
      .create(['border-color', 'box-shadow']),
    fontFamily: [
      '-apple-system',
      'BlinkMacSystemFont',
      '"Segoe UI"',
      'Roboto',
      '"Helvetica Neue"',
      'Arial',
      'sans-serif',
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"'
    ].join(','),
    '&:focus': {
      borderRadius: 4,
      borderColor: '#80bdff',
      boxShadow: '0 0 0 0.2rem rgba(0,123,255,.25)'
    }
  }
}))(InputBase);

class Tables extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      total_data: 0,
      total_page: 1,
      current_page: 1,
      prev_page: null,
      data: [],
      next_page: 1,
      rowsPerPage: 10,
      openSearch: false,
      rowFocus: '',
      page: 1,
      filter: false,
      count: 1,
      data: [],
      filterValue: [],
      loading: true,
      modal: false,
      selectAll: false,
      order: 'asc',
      rowSelected: [],
      bulk: false,
      redirect: false,
      failure: false,
    };
    this.ChangePage = this
      .ChangePage
      .bind(this);
  }

  handleClick(id) {
    alert(id);
  }

  ChangePage(event, page) {
    this.setState({
      page
    }, () => {
      this.getData();
    });
  }

  setFocus(row) {
    this.setState({ rowFocus: row.id });
  }

  setUnfocus() {
    setTimeout(() => {
      this.setState({ rowFocus: '' });
    }, 5500);
  }

  handleModal(row) {
    this.setState({
      modal2: false,
      modal: true,
      row
    }, () => {
      this.setState({ modal: false, bulk: false });
    });
  }

  handleShow(row) {
    this.setState({
      modal2: true,
      modal: false,
      row
    }, () => {
      this.setState({ modal2: false, bulk: false });
    });
  }

  componentDidMount() {
    this.getData('first');
  }

    getData = () => {
      this.setState({ loading: true });
      Func
        .getDataFinancial(this.props.path, this.state.rowsPerPage, this.state.page, this.state.search)
        .then((res) => {
          if (res.error) {
            this.setState({
              con: 0,
              data: [],
              loading: false,
              total_data: 0,
              page: 0,
              next_page: 0,
              prev_page: 0,
              current_page: 0,
              total_page: 0,
              filter: false
            });
          } else {
            if (res.json.code === '403') {
              if (Func.Clear_Token() === true) {
                if (!localStorage.getItem('token')) {
                  this.setState({ redirect: true });
                }
              }
            } else if (res.json.status === 500) {
              this.setState({ failure: true });
            } else {
              const datas = [];
              res
                .json
                .data
                .map((data) => {
                  datas.push(createData(
                    data.id,
                    data.code,
                    data.budget_request_type.name,
                    Func.FormatDate(data.request_date),
                    data.requested_user_name,
                    Func.FormatRp(data.nominal),
                    data.status
                  ));
                });
              this.setState({
                con: res.json.data.length,
                data: datas,
                loading: false,
                total_data: res.json.total_data,
                page: res.json.current_page,
                next_page: res.json.next_page,
                prev_page: res.json.prev_page,
                current_page: res.json.current_page,
                total_page: res.json.total_page,
                filter: false
              });
            }
          }
        });
    };

    Short(orderKey) {
      if (orderKey === this.state.order) {
        this.setState({ order: '' });
        const library = this.state.data;
        library.sort((a, b) => (a[orderKey] < b[orderKey]
          ? 1
          : b[orderKey] < a[orderKey]
            ? -1
            : 0));
      } else {
        this.setState({ order: orderKey });
        // eslint-disable-next-line no-redeclare
        const library = this.state.data;
        library.sort((a, b) => (a[orderKey] > b[orderKey]
          ? 1
          : b[orderKey] > a[orderKey]
            ? -1
            : 0));
      }
    }

    handleChange(event, name) {
      const dataSet = this.state.filterValue;
      dataSet[name] = event;
      this.setState({ filterValue: dataSet });
    }

    generateColor(x) {
      let color = '';
      if (x === 'in_progress') {
        color = '#FFEB85';
      } else if (x === 'received') {
        color = '#69FBBA';
      } else if (x === 'approved') {
        color = '#A8EAFF';
      } else if (x === 'rejected') {
        color = '#CB3B3B';
      } else if (x === 'not_accepted') {
        color = 'rgba(43, 68, 80, 0.5)';
      } else if (x === 'cancelled') {
        color = '#E5E5E5';
      } else {
        color = 'transparent';
      }
      return color;
    }

    generateText(x) {
      let text = '';
      if (x === 'in_progress') {
        text = 'Menunggu Persetujuan';
      } else if (x === 'received') {
        text = 'Diterima';
      } else if (x === 'approved') {
        text = 'Disetujui';
      } else if (x === 'rejected') {
        text = 'Ditolak';
      } else if (x === 'not_accepted') {
        text = 'Tidak Diterima';
      } else if (x === 'cancelled') {
        text = 'Batal';
      } else {
        text = '';
      }
      return text;
    }

    render() {
      const { classes } = this.props;
      let con = 0;
      this.state.data.map((row) => {
        if (this.state.rowSelected.indexOf(row.id) > -1) {
          con += 1;
        }
      });
      return (
        <div>
          <TableContainer
            style={{ width: this.props.open ? window.innerWidth - this.props.width : window.innerWidth - 115 }}
            component={Paper}
          >
            <div style={{
              width: '100%'
            }}
            >
              <Box display="flex">
                <Box flexGrow={1}>
                  <Typography variant="h6" className={classes.paginationTxt2}>
                    {this.props.title}
                  </Typography>
                </Box>
              </Box>
            </div>
            <div style={{
              width: '100%'
            }}
            >
              <Box display="flex">
                <Box flexGrow={1}>
                  <Typography className={classes.paginationTxt}>
                    {this.props.subtitle}
                  </Typography>
                </Box>
              </Box>
            </div>
            <Hidden smUp>
              <div
                style={{
                  width: '100%'
                }}
              >
                <Box display="flex">
                  <Box flexGrow={1}>
                    <Typography variant="h9" className={classes.textperdata}>
                      Tampilkan
                    </Typography>
                    <Select
                      size="small"
                      className={classes.selectperdata}
                      labelId="demo-customized-select-label"
                      id="demo-customized-select"
                      value={this.state.rowsPerPage}
                      onChange={(val) => {
                        this.setState({
                          rowsPerPage: val.target.value
                        }, () => {
                          this.getData();
                        });
                      }}
                      input={<BootstrapInput />}
                    >
                      <MenuItem value={10}>10 data</MenuItem>
                      <MenuItem value={50}>50 data</MenuItem>
                      <MenuItem value={100}>100 data</MenuItem>
                    </Select>
                    <Typography variant="h9" className={classes.textperdata}>
                      Total:
                      {' '}
                      {this.state.total_data}
                      {' '}
                      {' '}
                      permintaan
                    </Typography>
                  </Box>
                </Box>
              </div>
              <div
                style={{
                  width: '100%'
                }}
              >
                <Box>
                  <BootstrapInput
                    placeholder="Pencarian Kode"
                    value={this.state.search}
                    style={{
                      marginBottom: 20,
                      marginLeft: -0
                    }}
                    onChange={(event) => {
                      this.setState({
                        search: event.target.value
                      }, () => {
                        this.getData();
                      });
                    }}
                    on
                    className={classes.search2}
                    id="demo-customized-textbox"
                  />
                </Box>
              </div>
            </Hidden>
            <Hidden xsDown>
              <div
                style={{
                  width: '100%'
                }}
              >
                <Box display="flex">
                  <Box flexGrow={1}>
                    <Typography variant="h9" className={classes.textperdata}>
                      Tampilkan
                    </Typography>
                    <Select
                      size="small"
                      className={classes.selectperdata}
                      labelId="demo-customized-select-label"
                      id="demo-customized-select"
                      value={this.state.rowsPerPage}
                      onChange={(val) => {
                        this.setState({
                          rowsPerPage: val.target.value
                        }, () => {
                          this.getData();
                        });
                      }}
                      input={<BootstrapInput />}
                    >
                      <MenuItem value={10}>10 data</MenuItem>
                      <MenuItem value={50}>50 data</MenuItem>
                      <MenuItem value={100}>100 data</MenuItem>
                    </Select>
                    <Typography variant="h9" className={classes.textperdata}>
                      Total:
                      {' '}
                      {this.state.total_data}
                      {' '}
                      {' '}
                      permintaan
                    </Typography>
                  </Box>
                  <Box>
                    <BootstrapInput
                      placeholder="Pencarian Kode"
                      value={this.state.search}
                      style={{
                        marginBottom: 20,
                        marginLeft: -0
                      }}
                      onChange={(event) => {
                        this.setState({
                          search: event.target.value
                        }, () => {
                          this.getData();
                        });
                      }}
                      on
                      className={classes.search2}
                      id="demo-customized-textbox"
                    />
                  </Box>
                </Box>
              </div>
            </Hidden>
            <FormCountry type="Ubah" modal={this.state.modal} modal2={this.state.modal2} row={this.state.row} />
          </TableContainer>
          <TableContainer style={{ width: this.props.open ? window.innerWidth - this.props.width : window.innerWidth - 115 }} component={Paper}>
            <Table
              size="small"
              onMouseOut={() => {
                this.setUnfocus();
              }}
              className={classes.table}
              aria-label="simple table"
            >
              <TableHead className={classes.headTable}>
                <TableRow>
                  {headCells.map((headCell) => (
                    <TableCell
                      key={headCell.id}
                      align={headCell.numeric
                        ? 'right'
                        : 'left'}
                      sortDirection={this.state.order === headCell.id
                        ? this.state.order
                        : false}
                    >
                      <TableSortLabel
                        active={this.state.order === headCell.id}
                        direction={this.state.order === headCell.id
                          ? this.state.order
                          : 'asc'}
                        onClick={() => {
                          this.Short(headCell.id);
                        }}
                      >
                        {headCell.label}
                        {this.state.order === headCell.id
                          ? (
                            <span className={classes.visuallyHidden} />
                          )
                          : null}
                      </TableSortLabel>
                    </TableCell>
                  ))}
                </TableRow>
              </TableHead>
              {this.state.data.length === 0 && !this.state.loading
                ? (
                  <TableBody>
                    <TableRow>
                      <TableCell colSpan={8} align="center">
                        <Typography>
                          Tidak Ada Data
                        </Typography>
                      </TableCell>
                    </TableRow>
                  </TableBody>
                )
                : null}
              <TableBody>
                {!this.state.loading
                  ? this
                    .state
                    .data
                    .map((row) => (
                      <TableRow
                        hover
                        onMouseOver={() => {
                          this.setFocus(row);
                        }}
                        role="checkbox"
                        tabIndex={-1}
                        key={row.id}
                      >
                        <TableCell onClick={() => { this.handleShow(row); }} align="left">{row.code}</TableCell>
                        <TableCell onClick={() => { this.handleShow(row); }} align="left">{row.budget_request_type}</TableCell>
                        <TableCell onClick={() => { this.handleShow(row); }} align="left">{row.request_date}</TableCell>
                        <TableCell onClick={() => { this.handleShow(row); }} align="left">{row.requested_user_name}</TableCell>
                        <TableCell onClick={() => { this.handleShow(row); }} align="left">{row.nominal}</TableCell>
                        <TableCell onClick={() => { this.handleShow(row); }} align="left">
                          <div style={{
                            backgroundColor: this.generateColor(row.status),
                            width: '172px',
                            height: '40px',
                            borderRadius: 5,
                            padding: '10px 0',
                            textAlign: 'center',
                          }}
                          >
                            {this.generateText(row.status)}
                          </div>
                        </TableCell>
                        <TableCell align="left">
                          <div className={classes.action}>
                            {
                              Func.checkPermission('financial-management#request#update') && row.status === 'in_progress' ? (
                                <IconButton
                                  onClick={() => {
                                    this.handleModal(row);
                                  }}
                                  aria-label="Cari"
                                >
                                  <EditIcon />
                                </IconButton>
                              )
                                : null
                            }
                          </div>
                        </TableCell>
                      </TableRow>
                    ))
                  : null}
              </TableBody>
            </Table>
            {this.state.loading
              ? (
                <div className={classes.loader}>
                  <BeatLoader size={15} color="#3F3F3F" loading />
                </div>
              )
              : null}
          </TableContainer>
          <TableContainer style={{ width: this.props.open ? window.innerWidth - this.props.width : window.innerWidth - 115, marginBottom: 40 }} component={Paper}>
            <div className={classes.col}>
              <text>
                Halaman
                {' '}
                {' ' + this.state.current_page + ' '}
                dari
                {' '}
                {' ' + this.state.total_page + ' '}
                halaman
              </text>
            </div>
            <Pagination
              color="secondary"
              className={classes.row}
              count={this.state.total_page}
              defaultPage={this.state.page}
              onChange={this.ChangePage}
              siblingCount={0}
            />
          </TableContainer>
        </div>
      );
    }
}

export default withStyles(styles.CoustomsStyles, { name: 'Tables' })(Tables);
