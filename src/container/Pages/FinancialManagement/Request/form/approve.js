/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable class-methods-use-this */
/* eslint-disable eqeqeq */
/* eslint-disable array-callback-return */
/* eslint-disable react/no-unused-state */
/* eslint-disable radix */
/* eslint-disable react/no-access-state-in-setstate */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/sort-comp */
import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import Box from '@material-ui/core/Box';
import CloseIcon from '@material-ui/icons/Close';
import IconButton from '@material-ui/core/IconButton';
import Button from '@material-ui/core/Button';
import swal from 'sweetalert';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import { CircularProgress } from '@material-ui/core';
import env from '../../../../../config/env';
import styles from '../css';
import Func from '../../../../../functions/index';

class Form extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      validator: [],
      value: { email: '', nominal: '' },
      office_name: '',
      name: ''
    };
    this.handleChangeImg = this
      .handleChangeImg
      .bind(this);
  }

  removeValidate(name) {
    const data = this.state.validator;
    delete data[name];
    this.setState({ validator: data });
  }

  handleChange(event, name) {
    const dataSet = this.state.value;
    dataSet[name] = event;
    this.setState({ value: dataSet });
  }

  handleChangeDate(date, name, name2) {
    const dt = new Date(date);
    const dataSet = this.state.value;
    dataSet[name2] = dt;
    dataSet[name] = dt.getFullYear() + '-' + parseInt(dt.getMonth() + 1) + '-' + dt.getDate();
    this.setState({ value: dataSet });
  }

    getTypes= (val, id, name) => {
      this.setState({ isLoading: true });
      fetch(env.financialApi + env.apiPrefixV1 + '/budget_request_types/autocomplete' + (val === '' ? '' : '?query=' + val), {
        method: 'GET',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + localStorage.getItem('token')
        }
      }).then((response) => response.json()).then((json) => {
        if (json.code === '403') {
          if (Func.Clear_Token() === true) {
            if (!localStorage.getItem('token')) {
              this.setState({ redirect: true });
            }
          }
        }
        const datas = [];
        datas.push({
          value: '-',
          label: json.data.length > 0
            ? 'Pilih'
            : 'Tidak ditemukan',
          isDisabled: true
        });
        json
          .data
          .map((value) => {
            datas.push({ value: value.id, label: value.name });
          });
        if (id != '') {
          const search = json
            .data
            .find(o => o._id.$oid === id);
          if (search === undefined) {
            datas.push({ value: id, label: name });
          }
        }

        this.setState({ data4: datas, data4_ori: json.data });
      }).catch(() => { })
        .finally(() => { });
    };

    getUSer() {
      fetch(env.financialApi + env.apiPrefixV1 + '/user/me', {
        method: 'GET',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + localStorage.getItem('token')
        }
      }).then((response) => response.json()).then((json) => {
        if (json.code === '403') {
          if (Func.Clear_Token() === true) {
            if (!localStorage.getItem('token')) {
              this.setState({ redirect: true });
            }
          }
        }
        this.setState({
          name: json.name,
          office_name: json.office_name,
          loader: false
        });
      }).catch(() => { })
        .finally(() => { });
    }

    componentDidMount() {
      this.setState({ loader: true });
      this.getUSer();
      this.getTypes('', '', '');
      if (this.props.type === 'Ubah') {
        fetch(env.financialApi + env.apiPrefixV1 + '/budget_requests/' + this.props.id, {
          method: 'GET',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: 'Bearer ' + localStorage.getItem('token')
          }
        }).then((response) => response.json()).then((json) => {
          if (json.code === '403') {
            if (Func.Clear_Token() === true) {
              if (!localStorage.getItem('token')) {
                this.setState({ redirect: true });
              }
            }
          }
          const val = [];

          val.code = json.data.code;
          val.nominal = json.data.nominal;
          val.description = json.data.description;
          val.status = json.data.status;
          val.img = json.data.attachment.url;
          val.budget_request_type_id = { value: json.data.budget_request_type.id, label: json.data.budget_request_type.name };

          this.setState({
            value: val,
            loader: false
          });
        }).catch(() => {})
          .finally(() => {});
      }
    }

    cancel() {
      this.setState({ loader_button: true });
      fetch(env.financialApi + env.apiPrefixV1 + '/budget_requests/' + this.props.id + '/cancel', {
        method: 'PUT',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + localStorage.getItem('token')
        },
        body: JSON.stringify({
        })
      }).then((response) => response.json()).then((json) => {
        if (json.code === '403') {
          Func.Refresh_Token();
          if (Func.Refresh_Token() === true) {
            this.cancel();
          }
        }
        if (json.code === 200) {
          this.props.OnNext(json.message);
          this.setState({ loader_button: false });
        } else {
          Func.AlertError('Maaf, Terjadi kesalahan pada sistem');
          this.setState({ loader_button: false });
          this.setState({ validator: json.status });
        }
      }).catch(() => {})
        .finally(() => {});
    }

    handleChangeImg(event) {
      this.removeValidate('img');
      const dataSet = this.state.value;
      dataSet.img = URL.createObjectURL(event.target.files[0]);
      this.setState({ value: dataSet });

      const file = event.target.files[0];
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onloadend = () => {
        this.setState({ ImgBase44: reader.result });
      };
    }

    generateColor(x) {
      let color = '';
      if (x === 'in_progress') {
        color = '#FFEB85';
      } else if (x === 'received') {
        color = '#69FBBA';
      } else if (x === 'approved') {
        color = '#A8EAFF';
      } else if (x === 'rejected') {
        color = '#CB3B3B';
      } else if (x === 'not_accepted') {
        color = 'rgba(43, 68, 80, 0.5)';
      } else if (x === 'cancelled') {
        color = '#E5E5E5';
      } else {
        color = 'transparent';
      }
      return color;
    }

    generateText(x) {
      let text = '';
      if (x === 'in_progress') {
        text = 'Menunggu Persetujuan';
      } else if (x === 'received') {
        text = 'Diterima';
      } else if (x === 'approved') {
        text = 'Disetujui';
      } else if (x === 'rejected') {
        text = 'Ditolak';
      } else if (x === 'not_accepted') {
        text = 'Tidak Diterima';
      } else if (x === 'cancelled') {
        text = 'Batal';
      } else {
        text = '';
      }
      return text;
    }

    render() {
      const { classes } = this.props;
      if (this.state.loader) {
        return (
          <div className={classes.root2}>
            <Dialog
              disablePortal
              disableEnforceFocus
              disableAutoFocus
              open
              scroll="paper"
              maxWidth="md"
              aria-labelledby="server-modal-title"
              aria-describedby="server-modal-description"
              container={() => {}}
            >
              <div
                style={{
                  display: 'flex',
                  flexDirection: 'column',
                  alignItems: 'center',
                  padding: '20px',
                  borderRadius: '50px',
                }}
              >
                <CircularProgress
                  style={{ color: '#85203B', margin: '18px' }}
                  size={40}
                />
                Mohon Tunggu
                <div
                  style={{
                    marginTop: '10px',
                  }}
                />
              </div>
            </Dialog>
          </div>
        );
      }

      return (
        <Dialog
          scroll="paper"
          open
          maxWidth="md"
          aria-labelledby="scroll-dialog-title"
          aria-describedby="scroll-dialog-description"
        >
          <DialogTitle id="scroll-dialog-title">
            <Typography variant="h7" className={classes.tittleModal}>
              {this.props.type + ' Permintaan Kantor Cabang'}
            </Typography>
            <IconButton
              disabled={this.state.loader_button}
              aria-label="close"
              className={classes.closeButton}
              onClick={() => {
                this.props.handleModal();
              }}
            >
              <CloseIcon />
            </IconButton>
          </DialogTitle>
          <DialogContent dividers>
            <DialogContentText id="scroll-dialog-description" tabIndex={-1}>
              <div className={classes.scrool}>
                {Func.toLogin(this.state.redirect)}
                <div className={classes.root}>
                  <Box display="flex">
                    <Box flexGrow={1}>
                      <Typography variant="h6">{this.state.value.code}</Typography>
                    </Box>
                    <Box>
                      <div style={{
                        backgroundColor: this.generateColor(this.state.value.status),
                        width: '200px',
                        height: '40px',
                        borderRadius: 5,
                        padding: '10px 0',
                        textAlign: 'center',
                      }}
                      >
                        {this.generateText(this.state.value.status)}
                      </div>
                    </Box>
                  </Box>
                  <Grid container direction="row" item lg={12} xl={12} md={12} xs={12}>
                    <Grid container item lg={12} xl={12} md={12} sm={12} xs={12}>
                      <Grid item lg={12} xl={12} md={12} sm={12} xs={12}>
                        <div className={classes.label111} style={{ marginTop: 20 }}>
                          <text className={classes.Labelvalue2}>Nama Akun SisCab</text>
                        </div>
                        <div className={classes.label1112} style={{ marginTop: 10 }}>
                          <text className={classes.value2}>{this.state.name}</text>
                        </div>
                      </Grid>
                      <Grid item lg={12} xl={12} md={12} sm={12} xs={12}>
                        <div className={classes.label111} style={{ marginTop: 20 }}>
                          <text className={classes.Labelvalue2}>Kantor Cabang</text>
                        </div>
                        <div className={classes.label1112} style={{ marginTop: 10 }}>
                          <text className={classes.value2}>{this.state.office_name}</text>
                        </div>
                      </Grid>
                      <Grid item lg={12} xl={12} md={12} sm={12} xs={12}>
                        <div className={classes.label111} style={{ marginTop: 20 }}>
                          <text className={classes.Labelvalue2}>Deskripsi</text>
                        </div>
                        <div className={classes.label1112} style={{ marginTop: 10 }}>
                          <text className={classes.value2}>{this.state.value.description}</text>
                        </div>
                      </Grid>
                      <Grid item lg={12} xl={12} md={12} sm={12} xs={12}>
                        <div className={classes.label111} style={{ marginTop: 20 }}>
                          <text className={classes.Labelvalue2}>Nominal</text>
                        </div>
                        <div className={classes.label1112} style={{ marginTop: 10 }}>
                          <text className={classes.value2}>{Func.FormatRp(this.state.value.nominal)}</text>
                        </div>
                      </Grid>
                      <Grid item lg={12} xl={12} md={12} sm={12} xs={12}>
                        <div style={{ marginTop: 30 }} className={classes.BodytitleMdl2}>
                          <text className={classes.label1}>Lampiran</text>
                          <text className={classes.starts1}>*</text>
                        </div>
                        <div style={{ marginBottom: 20 }}>
                          <Box>
                            {this.state.value.img
                              ? (
                                <img
                                  className={classes.imgScan4}
                                  onClick={() => {
                                    this.removeValidate('img');
                                  }}
                                  src={this.state.value.img}
                                />
                              )
                              : null}
                          </Box>
                        </div>
                      </Grid>
                    </Grid>
                  </Grid>
                </div>
              </div>
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            {Func.checkPermission('financial-management#request#cancel') && this.state.value.status === 'in_progress' ? (
              <Button
                style={{
                  backgroundColor: '#862C3A',
                  color: 'white',
                }}
                disabled={this.state.loader_button}
                onClick={() => {
                  swal({
                    title: 'Apakah Anda Yakin?',
                    text: 'data yang anda batalkan tidak bisa di rubah kembali!',
                    icon: 'warning',
                    buttons: true,
                    dangerMode: true,
                  })
                    .then((willDelete) => {
                      if (willDelete) {
                        this.cancel();
                      }
                    });
                }}
                variant="contained"
              >
                <Typography variant="button" style={{ color: '#FFFFFF' }}>
                  {this.state.loader_button ? (
                    <CircularProgress
                      style={{
                        color: 'white',
                        marginLeft: '18px',
                        marginRight: '18px',
                        marginTop: 5,
                      }}
                      size={15}
                    />
                  ) : (
                    'Batalkan Permintaan'
                  )}
                </Typography>
              </Button>
            ) : null}
            <Button
              style={{
                color: 'black',
              }}
              disabled={this.state.loader_button}
              onClick={() => {
                this.props.handleModal();
              }}
              variant="outlined"
            >
              <Typography variant="button">Batal</Typography>
            </Button>
          </DialogActions>
        </Dialog>
      );
    }
}

export default withStyles(styles.CoustomsStyles, { name: 'Form' })(Form);
