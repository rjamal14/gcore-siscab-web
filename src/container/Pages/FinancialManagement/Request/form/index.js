/* eslint-disable react/no-access-state-in-setstate */
/* eslint-disable eqeqeq */
/* eslint-disable react/no-deprecated */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/sort-comp */
/* eslint-disable react/no-unused-state */
import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Func from '../../../../../functions/index';
import styles from '../css';
import Icon from '../../../../../components/icon';
import Field from './field';
import Approve from './approve';

class Form extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: false,
      modal2: false,
      validator: [],
      value: [],
      type: '',
      activeTabs: 0,
      Proses: 0,
      imgPath: Icon.blank,
      count: 0,
      id: null,
    };
    this.handleChangeImg = this.handleChangeImg.bind(this);
  }

  onPage(type) {
    let val = '';
    if (type === '-') {
      val = this.state.activeTabs - 1;
    } else {
      val = this.state.activeTabs + 1;
    }
    this.setState({
      activeTabs: val,
      Proses: val
    });
  }

  componentWillReceiveProps() {
    if (this.props.row != 'undefined') {
      if (this.props.modal === true) {
        this.setState({
          modal: this.props.modal,
          modal2: false,
          type: this.props.type
        });
      } else if (this.props.modal2 === true) {
        this.setState({
          modal: false,
          modal2: this.props.modal2,
          type: this.props.type
        });
      }
    }
    if (this.props.type == 'Ubah' && this.props.row != undefined) {
      this.setState({ id: this.props.row.id });
    }
  }

  removeValidate(name) {
    const data = this.state.validator;
    delete data[name];
    this.setState({ validator: data });
  }

  handleModal() {
    this.setState({
      modal: false,
      modal2: false,
    });
  }

  handleChange(event, name) {
    const dataSet = this.state.value;
    dataSet[name] = event.target.value;
    this.setState({ value: dataSet });
  }

  handleSubmit() {
    if (this.state.activeTabs == 6) {
      this.setState({
        modal: !this.state.modal,
        activeTabs: 0,
      });
    } else {
      this.setState({
        Proses: this.state.activeTabs,
      });
    }
  }

  handleChangeImg(event) {
    this.setState({
      imgPath: URL.createObjectURL(event.target.files[0]),
    });
  }

  render() {
    if (this.state.modal) {
      return (
        <Field
          Proses={this.state.Proses}
          value={this.state.value}
          type={this.state.type}
          handleModal={() => {
            this.handleModal();
          }}
          id={this.state.id}
          count={this.state.count}
          OnNext={(res) => {
            this.setState({ modal: false });
            Func.AlertForm('Berhasil', res, 'success');
          }}
        />
      );
    } if (this.state.modal2) {
      return (
        <Approve
          Proses={this.state.Proses}
          value={this.state.value}
          handleModal={() => {
            this.handleModal();
          }}
          type={this.state.type}
          id={this.state.id}
          count={this.state.count}
          OnNext={(res) => {
            this.setState({ modal: false, modal2: false });
            Func.AlertForm('Berhasil', res, 'success');
          }}
        />
      );
    }
    return <div />;
  }
}

export default withStyles(styles.CoustomsStyles, {
  name: 'Form',
})(Form);
