/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable consistent-return */
/* eslint-disable react/no-unused-state */
/* eslint-disable array-callback-return */
/* eslint-disable eqeqeq */
/* eslint-disable radix */
/* eslint-disable react/no-access-state-in-setstate */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/sort-comp */
import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import InputAdornment from '@material-ui/core/InputAdornment';
import Typography from '@material-ui/core/Typography';
import FormHelperText from '@material-ui/core/FormHelperText';
import Grid from '@material-ui/core/Grid';
import TextareaAutosize from '@material-ui/core/TextareaAutosize';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import Box from '@material-ui/core/Box';
import CloseIcon from '@material-ui/icons/Close';
import IconButton from '@material-ui/core/IconButton';
import Button from '@material-ui/core/Button';
import SelectAsync from 'react-select/async';
import Divider from '@material-ui/core/Divider';
import swal from 'sweetalert';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import { CircularProgress } from '@material-ui/core';
import env from '../../../../../config/env';
import styles from '../css';
import Icon from '../../../../../components/icon';
import Func from '../../../../../functions/index';

class Form extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      validator: [],
      value: { email: '', nominal: '' },
      office_name: '',
      name: '',
    };
    this.handleChangeImg = this.handleChangeImg.bind(this);
  }

  removeValidate(name) {
    const data = this.state.validator;
    delete data[name];
    this.setState({ validator: data });
  }

  handleChange(event, name) {
    const dataSet = this.state.value;
    dataSet[name] = event;
    this.setState({ value: dataSet });
  }

  handleChangeDate(date, name, name2) {
    const dt = new Date(date);
    const dataSet = this.state.value;
    dataSet[name2] = dt;
    dataSet[name] = dt.getFullYear() + '-' + parseInt(dt.getMonth() + 1) + '-' + dt.getDate();
    this.setState({ value: dataSet });
  }

  getTypes = (val, id, name) => {
    fetch(
      env.financialApi
        + env.apiPrefixV1
        + '/budget_request_types/autocomplete'
        + (val === '' ? '' : '?query=' + val),
      {
        method: 'GET',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + localStorage.getItem('token'),
        },
      }
    )
      .then((response) => response.json())
      .then((json) => {
        if (json.code === '403') {
          if (Func.Clear_Token() === true) {
            if (!localStorage.getItem('token')) {
              this.setState({ redirect: true });
            }
          }
        }
        const datas = [];
        datas.push({
          value: '-',
          label: json.data.length > 0 ? 'Pilih' : 'Tidak ditemukan',
          isDisabled: true,
        });
        json.data.map((value) => {
          datas.push({ value: value.id, label: value.name });
        });
        if (id != '') {
          const search = json.data.find((o) => o._id.$oid === id);
          if (search == undefined) {
            datas.push({ value: id, label: name });
          }
        }

        this.setState({ data4: datas, data4_ori: json.data });
      })
      .catch(() => {})
      .finally(() => {});
  };

  getUSer() {
    fetch(env.financialApi + env.apiPrefixV1 + '/user/me', {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + localStorage.getItem('token'),
      },
    })
      .then((response) => response.json())
      .then((json) => {
        if (json.code == '403') {
          if (Func.Clear_Token() == true) {
            if (!localStorage.getItem('token')) {
              this.setState({ redirect: true });
            }
          }
        }
        this.setState({
          name: json.name,
          office_name: json.office_name,
          loader: false,
        });
      })
      .catch(() => {})
      .finally(() => {});
  }

  componentDidMount() {
    this.setState({ loader: true });
    this.getUSer();
    this.getTypes('', '', '');
    if (this.props.type == 'Ubah') {
      fetch(
        env.financialApi
          + env.apiPrefixV1
          + '/budget_requests/'
          + this.props.id,
        {
          method: 'GET',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: 'Bearer ' + localStorage.getItem('token'),
          },
        }
      )
        .then((response) => response.json())
        .then((json) => {
          if (json.code == '403') {
            if (Func.Clear_Token() == true) {
              if (!localStorage.getItem('token')) {
                this.setState({ redirect: true });
              }
            }
          }
          const val = [];

          val.code = json.data.code;
          val.nominal = json.data.nominal;
          val.description = json.data.description;
          val.img = json.data.attachment.url;
          val.budget_request_type_id = {
            value: json.data.budget_request_type.id,
            label: json.data.budget_request_type.name,
          };

          this.setState({
            value: val,
            loader: false,
          });
        })
        .catch(() => {})
        .finally(() => {});
    }
  }

  handleSubmit(type) {
    this.setState({ loader_button: true });
    const validator = [
      {
        name: 'budget_request_type_id',
        type: 'required',
      },
      {
        name: 'description',
        type: 'required',
      },
      {
        name: 'nominal',
        type: 'required',
      },
      {
        name: 'img',
        type: 'required',
      },
    ];

    const validate = Func.Validator(this.state.value, validator);
    if (validate.success) {
      fetch(
        env.financialApi
          + env.apiPrefixV1
          + '/budget_requests/'
          + (type == 'Tambah' ? '' : this.props.id),
        {
          method: type == 'Tambah' ? 'POST' : 'PUT',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: 'Bearer ' + localStorage.getItem('token'),
          },
          body: JSON.stringify({
            description: this.state.value.description,
            nominal: parseInt(this.state.value.nominal),
            budget_request_type_id: this.state.value.budget_request_type_id
              .value,
            attachment: this.state.ImgBase44,
          }),
        }
      )
        .then((response) => response.json())
        .then((json) => {
          if (json.code == '403') {
            Func.Refresh_Token();
            if (Func.Refresh_Token() == true) {
              this.handleSubmit();
            }
          }
          if (type == 'Tambah') {
            if (json.created) {
              this.props.OnNext(json.message);
              this.setState({ loader_button: false });
            } else {
              this.setState({ validator: json.status });
              this.setState({ loader_button: false });
            }
          } else if (json.code == 200) {
            this.props.OnNext(json.message);
            this.setState({ loader_button: false });
          } else {
            Func.AlertError('Maaf, Terjadi kesalahan pada sistem');
            this.setState({ loader_button: false });
            this.setState({ validator: json.status });
          }
        })
        .catch(() => {})
        .finally(() => {});
    } else {
      this.setState({ validator: validate.error });
      this.setState({ loader_button: false });
    }
  }

  handleChangeImg(event) {
    const file = event.target.files[0];
    if (file.size > 2.9e6) {
      swal({
        title: 'File Terlalu Besar',
        text: 'Maximal File 2Mb',
        icon: 'error',
        buttons: 'OK',
      });
      return false;
    }
    this.removeValidate('img');
    const dataSet = this.state.value;
    dataSet.img = URL.createObjectURL(event.target.files[0]);
    this.setState({ value: dataSet });
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onloadend = () => {
      this.setState({ ImgBase44: reader.result });
    };
  }

  render() {
    const loadOptions = (inputValue, callback) => {
      setTimeout(() => {
        callback(this.state.data4);
      }, 1000);
    };

    const { classes } = this.props;
    if (this.state.loader) {
      return (
        <div className={classes.root2}>
          <Dialog
            disablePortal
            disableEnforceFocus
            disableAutoFocus
            open
            scroll="paper"
            maxWidth="md"
            aria-labelledby="server-modal-title"
            aria-describedby="server-modal-description"
            container={() => {}}
          >
            <div
              style={{
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
                padding: '20px',
                borderRadius: '50px',
              }}
            >
              <CircularProgress
                style={{ color: '#85203B', margin: '18px' }}
                size={40}
              />
              Mohon Tunggu
              <div
                style={{
                  marginTop: '10px',
                }}
              />
            </div>
          </Dialog>
        </div>
      );
    }

    return (
      <Dialog
        scroll="paper"
        open
        maxWidth="md"
        aria-labelledby="scroll-dialog-title"
        aria-describedby="scroll-dialog-description"
      >
        <DialogTitle id="scroll-dialog-title">
          <Typography variant="h7" className={classes.tittleModal}>
            {this.props.type + ' Permintaan Kantor Cabang'}
          </Typography>
          <IconButton
            disabled={this.state.loader_button}
            aria-label="close"
            className={classes.closeButton}
            onClick={() => {
              this.props.handleModal();
            }}
          >
            <CloseIcon />
          </IconButton>
        </DialogTitle>
        <DialogContent dividers>
          <DialogContentText id="scroll-dialog-description" tabIndex={-1}>
            <div className={classes.scrool}>
              {Func.toLogin(this.state.redirect)}
              <div className={classes.root}>
                <Grid
                  container
                  direction="row"
                  item
                  lg={12}
                  xl={12}
                  md={12}
                  xs={12}
                >
                  <Grid container item lg={12} xl={12} md={12} sm={12} xs={12}>
                    <Grid container lg={12} xl={12} md={12} sm={12} xs={12}>
                      <Grid item lg={6} xl={6} md={6} sm={12} xs={12}>
                        <div>
                          <text className={classes.Labelvalue2}>
                            Nama Akun SisCab
                          </text>
                        </div>
                        <div style={{ marginTop: '10px' }}>
                          <text className={classes.value2}>
                            {this.state.name}
                          </text>
                        </div>
                      </Grid>
                      <Grid item lg={6} xl={6} md={6} sm={12} xs={12}>
                        <div>
                          <text className={classes.Labelvalue2}>
                            Kantor Cabang
                          </text>
                        </div>
                        <div style={{ marginTop: '10px' }}>
                          <text className={classes.value2}>
                            {this.state.office_name}
                          </text>
                        </div>
                      </Grid>
                    </Grid>
                    <div style={{ marginTop: 80 }} />
                    <Grid item lg={12} xl={12} md={12} sm={12} xs={12}>
                      <div>
                        <text className={classes.label1}>Jenis Permintaan</text>
                        <text className={classes.starts1}>*</text>
                      </div>
                      <SelectAsync
                        name="form-field-name-error"
                        value={this.state.value.budget_request_type_id}
                        placeholder="Pilih"
                        onFocus={() => {
                          this.removeValidate('budget_request_type_id');
                        }}
                        styles={{
                          control: (provided) => ({
                            ...provided,
                            borderColor: this.state.validator.budget_request_type_id
                              ? 'red'
                              : '#CACACA',
                            borderRadius: '0.25rem',
                          }),
                        }}
                        className={classes.input2}
                        onInputChange={(val) => {
                          this.getTypes(val, '', '');
                        }}
                        onChange={(val) => {
                          this.handleChange(val, 'budget_request_type_id');
                        }}
                        cacheOptions
                        loadOptions={loadOptions}
                        defaultOptions
                      />
                      <FormHelperText className={classes.error}>
                        {this.state.validator.budget_request_type_id}
                      </FormHelperText>
                    </Grid>
                    <Grid item lg={12} xl={12} md={12} sm={12} xs={12}>
                      <div>
                        <text className={classes.label1}>Nominal</text>
                        <text className={classes.starts1}>*</text>
                      </div>
                      <TextField
                        size="small"
                        fullWidth
                        className={classes.input2}
                        variant="outlined"
                        autoComplete="off"
                        onFocus={() => {
                          this.removeValidate('nominal');
                        }}
                        error={this.state.validator.nominal}
                        helperText={this.state.validator.nominal}
                        value={Func.FormatNumber(this.state.value.nominal)}
                        onChange={(event) => {
                          this.handleChange(
                            Func.UnFormatRp(event.target.value),
                            'nominal'
                          );
                        }}
                        name="nominal"
                        InputProps={{
                          startAdornment: (
                            <InputAdornment position="start">Rp</InputAdornment>
                          ),
                        }}
                      />
                    </Grid>
                    <Grid item lg={12} xl={12} md={12} sm={12} xs={12}>
                      <div>
                        <text className={classes.label1}>Deskripsi</text>
                        <text className={classes.starts1}>*</text>
                      </div>
                      <div>
                        <TextareaAutosize
                          className={
                            this.state.validator.description
                              ? classes.textArea2
                              : classes.textArea
                          }
                          variant="outlined"
                          margin="normal"
                          rows={8}
                          autoComplete="off"
                          onFocus={() => {
                            this.removeValidate('description');
                          }}
                          error={this.state.validator.description}
                          value={this.state.value.description}
                          onChange={(event) => {
                            this.handleChange(
                              event.target.value,
                              'description'
                            );
                          }}
                          name="description"
                          InputProps={{
                            endAdornment: this.state.validator.description ? (
                              <InputAdornment position="start">
                                <img src={Icon.warning} />
                              </InputAdornment>
                            ) : (
                              <div />
                            ),
                          }}
                        />
                        <FormHelperText className={classes.error}>
                          {this.state.validator.description}
                        </FormHelperText>
                      </div>
                    </Grid>
                    <Grid item lg={12} xl={12} md={12} sm={12} xs={12}>
                      <Divider className={classes.divider} />
                      <div className={classes.BodytitleMdl2}>
                        <text className={classes.label1}>Lampiran</text>
                        <text className={classes.starts1}>*</text>
                      </div>
                      <div className={classes.BodytitleMdl}>
                        <Box
                          borderColor={
                            this.state.validator.img ? 'error.main' : 'grey.500'
                          }
                          border={1}
                          onClick={() => {
                            this.removeValidate('img');
                          }}
                          className={classes.imgScan}
                        >
                          {this.state.value.img ? (
                            <img
                              className={classes.imgScan2}
                              onClick={() => {
                                this.removeValidate('img');
                              }}
                              src={this.state.value.img}
                            />
                          ) : null}
                        </Box>
                        <FormHelperText className={classes.error22}>
                          {this.state.validator.img}
                        </FormHelperText>
                        <FormHelperText
                          style={{
                            marginLeft: '15px',
                          }}
                        >
                          Maximum File 2Mb
                        </FormHelperText>
                      </div>
                      <div className={classes.BodytitleMdl22}>
                        <img
                          src={Icon.deleteImg}
                          onClick={() => {
                            const dataSet = this.state.value;
                            dataSet.img = null;
                            this.setState({ value: dataSet, ImgBase44: '' });
                          }}
                        />
                        <div />
                        <input
                          type="file"
                          accept="image/*"
                          name="file"
                          title="Pilih Gambar"
                          onChange={this.handleChangeImg}
                        />
                      </div>
                    </Grid>
                  </Grid>
                </Grid>
              </div>
            </div>
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button
            style={{
              backgroundColor: '#862C3A',
              color: 'white',
            }}
            disabled={this.state.loader_button}
            onClick={() => {
              this.handleSubmit(this.props.type);
            }}
            variant="contained"
          >
            <Typography variant="button" style={{ color: '#FFFFFF' }}>
              {this.state.loader_button ? (
                <CircularProgress
                  style={{
                    color: 'white',
                    marginLeft: '18px',
                    marginRight: '18px',
                    marginTop: 5,
                  }}
                  size={15}
                />
              ) : (
                'Simpan'
              )}
            </Typography>
          </Button>
          <Button
            style={{
              color: 'black',
            }}
            disabled={this.state.loader_button}
            onClick={() => {
              this.props.handleModal();
            }}
            variant="outlined"
          >
            <Typography variant="button">Batal</Typography>
          </Button>
        </DialogActions>
      </Dialog>
    );
  }
}

export default withStyles(styles.CoustomsStyles, { name: 'Form' })(Form);
