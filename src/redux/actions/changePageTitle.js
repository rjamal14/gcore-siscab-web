/* eslint-disable import/prefer-default-export */
import { CHANGE_PAGE_TITLE } from '../constants';

export const changePageTitle = (title) => ({
  type: CHANGE_PAGE_TITLE,
  title
});
