import { combineReducers } from 'redux';
import example from './exampleReducer';
import pageTitle from './pageTitleReducer';
import notification from './notification';

const rootReducer = combineReducers({
  // ADD YOUR REDUCER HERE !!!
  example,
  pageTitle,
  notification,
});

export default rootReducer;
