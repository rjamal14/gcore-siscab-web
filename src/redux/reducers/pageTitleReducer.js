/* eslint-disable consistent-return */
/* eslint-disable no-param-reassign */
/* eslint-disable no-fallthrough */
const { default: produce } = require('immer');
const { CHANGE_PAGE_TITLE } = require('../constants');

const initState = {
  title: ''
};

const reducer = (state = initState, action = {}) => produce(
  state, draft => {
    switch (action.type) {
      case CHANGE_PAGE_TITLE:
        draft.title = action.title;
        break;
      default:
        return state;
    }
  }
);

export default reducer;
