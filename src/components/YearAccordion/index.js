/* eslint-disable no-unused-vars */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
import React, { Fragment, useState } from 'react';
import PropTypes from 'prop-types';
import { Grow, List, ListItem, Typography } from '@material-ui/core';
import clsx from 'clsx';
import _ from 'lodash';
import useStyles from './yearAccordion-jss';

const months = ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Des'];
const years = [2020, 2019, 2018, 2017];

const Accordion = ({ primaryMenu, secondaryMenu, index }) => {
  const [collapse, setCollapse] = useState(false);
  const classes = useStyles();
  return (
    <Fragment>
      <div className={classes.arrowContainer} onClick={() => setCollapse(!collapse)}>
        <Typography variant="button">
          {primaryMenu}
        </Typography>
      </div>
      <div className={clsx({
        [classes.collapseIn]: collapse,
        [classes.collapseOut]: !collapse,
      })}
      >
        <List className={classes.list} disablePadding>
          {
            _.map(secondaryMenu, (item, idx) => (
              <Grow in={collapse}>
                <ListItem button key={idx} style={{ width: 50 }}>
                  {item}
                </ListItem>
              </Grow>
            )
            )
          }
        </List>
      </div>
    </Fragment>
  );
};

const YearAccordion = () => {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <Accordion index={1} primaryMenu="2020" secondaryMenu={months} />
      <Accordion index={2} primaryMenu="2019" secondaryMenu={months} />
      <Accordion index={3} primaryMenu="2018" secondaryMenu={months} />
      <Accordion index={4} primaryMenu="2017" secondaryMenu={months} />
    </div>
  );
};

YearAccordion.propTypes = {

};

export default YearAccordion;
