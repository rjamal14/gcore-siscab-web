import { darken, lighten, makeStyles } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    display: 'flex',
    backgroundColor: theme.palette.background.paper,
    marginTop: 4
  },
  arrowContainer: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    clipPath: 'polygon(75% 0%, 100% 50%, 75% 100%, 0% 100%, 25% 50%, 0% 0%)',
    minWidth: 80,
    marginLeft: -19,
    boxShadow: '3px 3px 3px 3px black',
    backgroundColor: theme.palette.secondary.main,
    color: '#FFFFFF',
    flexWrap: 'wrap',
    padding: theme.spacing(1),
    backgroundPosition: 'center',
    transition: 'background 0.8s',
    '&:hover': {
      backgroundColor: darken(theme.palette.secondary.main, 0.5),
      cursor: 'pointer'
    },
    '&:active': {
      backgroundColor: lighten(theme.palette.secondary.main, 0.5),
      backgroundSize: '100%',
      transition: 'background 0s'
    }
  },
  collapseIn: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexWrap: 'wrap',
  },
  collapseOut: {
    display: 'none'
  },
  list: {
    display: 'flex',
    flexDirection: 'row',
    padding: 0,
  }
}));

export default useStyles;
