import { makeStyles } from '@material-ui/core'

export const drawerWidth = 300
export const appbarHeight = 80

const useStyles = makeStyles((theme) => ({
  notification: {
    width: 340,
    height: 340,
    borderRadius: 5,
    marginLeft: -150,
    border: 'solid 0.5px #cccccc',
    overflowY: 'auto',
    '&::-webkit-scrollbar': {
      width: 8,
    },
    '&::-webkit-scrollbar-track': {
      '-webkit-box-shadow': 'inset 0 0 6px rgba(0,0,0,0.3)',
    },
    '&::-webkit-scrollbar-thumb': {
      borderRadius: 10,
      backgroundColor: 'RGBA(134, 44, 58)',
    },
    [theme.breakpoints.down('sm')]: {
      marginLeft: 'auto',
    },
  },
  listItem: {
    position: 'relative',
  },
  secondaryText: {
    '&.MuiTypography-root': {
      '&.MuiListItemText-secondary': {
        color: 'red',
      },
    },
  },
}))

export default useStyles
