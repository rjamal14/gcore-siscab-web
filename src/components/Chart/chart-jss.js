import { makeStyles } from '@material-ui/core';

const useStyles = makeStyles(theme => ({
  root: {
    borderRadius: 8,
    width: '100%',
    marginBottom: 16,
  },
  dashboardItem: {
    borderRadius: 8,
  },
  welcomeText: {
    fontSize: 20,
    color: '#808080',
  },
  userText: {
    fontWeight: 'bold',
    fontSize: 20,
    lineHeight: '2',
  },
  summaryBox: {
    display: 'flex',
    marginTop: 15,
    justifyContent: 'space-around',
    flexWrap: 'wrap',
  },
  summaryTitle: {
    fontSize: 18,
    paddingBottom: 5,
  },
  summaryItem: {
    display: 'flex',
    margin: -3,
  },
  summaryIcon: {
    display: 'flex',
    flexDirection: 'column',
  },
  description: {
    paddingLeft: 15,
  },
  cashText: {
    fontWeight: 'bold',
    color: theme.palette.secondary.main,
    lineHeight: '2',
  }
}));

export default useStyles;
