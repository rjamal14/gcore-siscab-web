/* eslint-disable no-unused-vars */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react/jsx-one-expression-per-line */
import React, { useEffect, useRef, useState, Fragment } from 'react';
import { Typography, Select } from '@material-ui/core';
import Chartjs from 'chart.js';
import useStyles from './chart-jss';

const Chart = ({ title, color, data, total = 0, type }) => {
  const chartConfig = {
    type: 'bar',
    data: {
      labels: ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Agu', 'Sep', 'Okt', 'Nov', 'Des'],
      datasets: [{
        backgroundColor: color,
      }],
    },
    options: {
      legend: { display: false }
    }
  };
  const classes = useStyles();
  const chartContainer = useRef(null);
  const [chartInstance, setChartInstance] = useState(null);
  const [config, setConfig] = useState(chartConfig);
  const [amount, setAmount] = useState(0);

  function changeType(value) {
    const conf = config;
    conf.type = value;
    if (value === 'line') {
      conf.data.datasets[0].backgroundColor = 'transparent';
      conf.data.datasets[0].borderColor = color;
    } else {
      conf.data.datasets[0].backgroundColor = color;
      conf.data.datasets[0].borderColor = 'transparent';
    }
    setConfig(conf);
    const newChartInstance = new Chartjs(chartContainer.current, conf);
    setChartInstance(newChartInstance);
  }

  useEffect(() => {
    changeType(type);
    setAmount(total);
    if (data.length > 0) {
      const conf = config;
      conf.data.datasets[0].data = data.map((val) => (val?.value || 0));
      setConfig(conf);
    }
    if (chartContainer && chartContainer.current) {
      const newChartInstance = new Chartjs(chartContainer.current, config);
      setChartInstance(newChartInstance);
    }
  }, [chartContainer, data]);

  const handleChange = (event) => {
    const { value } = event.target;
    changeType(value);
  };

  return (
    <Fragment>
      <div style={{ width: '100%' }}>
        <div style={{ display: 'flex', justifyContent: 'space-between' }}>
          <Typography className={classes.welcomeText}>
            {title} <br />
            <b>{amount}</b> Transaksi
          </Typography>
          <Select
            native
            disableUnderline
            value={config.type}
            onChange={handleChange}
          >
            <option value="bar">Bar</option>
            <option value="line">Line</option>
          </Select>
        </div>
        <div style={{ paddingTop: 10 }}>
          <canvas ref={chartContainer} />
        </div>
      </div>
    </Fragment>
  );
};

export default Chart;
