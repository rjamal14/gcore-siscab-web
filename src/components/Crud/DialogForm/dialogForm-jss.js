import { makeStyles } from '@material-ui/core';

const useStyles = makeStyles(theme => ({
  root: {

  },
  dialogTitle: {
    display: 'flex',
    alignItems: 'center',
    padding: theme.spacing(2)
  },
  title: {
    flex: 1,
  },
  loadingSubmit: {
    display: 'flex',
    alignItems: 'center',
    marginRight: theme.spacing(1)
  }
}));

export default useStyles;
