import React from 'react';
import PropTypes from 'prop-types';
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  Icon,
  IconButton,
  Typography
} from '@material-ui/core';
import { Form, Formik } from 'formik';
import { useSelector } from 'react-redux';
import _ from 'lodash';
import { Alert } from '@material-ui/lab';
import { BeatLoader } from 'react-spinners';
import Components from '../../FormFields';
import useStyles from './dialogForm-jss';

const AddDialogForm = (props) => {
  const {
    open,
    onClose,
    initialValues,
    createData,
    formField,
    component,
    loading,
    errors
  } = props;

  const classes = useStyles();
  const { title } = useSelector(state => state.pageTitle);

  const onSubmit = async (value, action) => {
    await createData(value);
    action.setSubmitting(false);
  };

  return (
    <Dialog
      open={open}
      onClose={onClose}
      maxWidth="md"
      fullWidth
      aria-labelledby="dialog-form"
    >
      <Formik
        initialValues={initialValues}
        onSubmit={onSubmit}
        component={({ submitForm, isSubmitting }) => (
          <Form autoComplete="off" className={classes.container}>

            <div className={classes.dialogTitle}>
              <div className={classes.title}>
                <Typography variant="h5">
                  {`Tambah ${title}`}
                </Typography>
              </div>
              <IconButton onClick={onClose}>
                <Icon>close</Icon>
              </IconButton>
            </div>
            <DialogContent>
              {
                errors
                  && _.map(Object.keys(errors), (item) => (
                    <div style={{ marginBottom: 8 }}>
                      <Alert severity="error">
                        {`${item} ${errors[item]}`}
                      </Alert>
                    </div>
                  )
                  )
              }
              {
                component ? Components({ component, ...props }) : _.map(formField, (value, id) => Components({ id, ...value }))
              }
            </DialogContent>
            <DialogActions>
              { loading && (
                <div className={classes.loadingSubmit}>
                  <Typography style={{ marginRight: 8 }}>
                    Menyimpan Data
                  </Typography>

                  <BeatLoader />
                </div>
              )}
              <Button
                color="primary"
                disabled={isSubmitting}
                onClick={submitForm}
                variant="contained"
              >
                <Typography variant="button" style={{ color: '#FFFFFF' }}>
                  Simpan
                </Typography>
              </Button>
              <Button
                color="secondary"
                disabled={isSubmitting}
                onClick={onClose}
                variant="outlined"
              >
                <Typography variant="button">
                  Batal
                </Typography>
              </Button>
            </DialogActions>
          </Form>
        )}
      />
    </Dialog>
  );
};

AddDialogForm.defaultProps = ({
  errors: [],
});

AddDialogForm.propTypes = ({
  open: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  initialValues: PropTypes.oneOfType([
    PropTypes.array,
    PropTypes.object
  ]).isRequired,
  errors: PropTypes.oneOfType([
    PropTypes.array,
    PropTypes.object
  ]),
  createData: PropTypes.func.isRequired,
  formField: PropTypes.array.isRequired,
  loading: PropTypes.bool.isRequired,

});

export default AddDialogForm;
