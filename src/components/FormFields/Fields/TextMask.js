/* eslint-disable react/jsx-no-duplicate-props */
/* eslint-disable object-curly-newline */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/jsx-props-no-spreading */

import { TextField as BaseTextField } from 'formik-material-ui';
import { Field } from 'formik';
import PropTypes from 'prop-types';
import FormControl from '@material-ui/core/FormControl';
import React from 'react';
import ReactNumberFormat from 'react-number-format';

function FormatedInput({ maxLength, prefix, inputRef, onChange, onBlur, name, ...other }) {
  const fiOnBlur = () => onBlur();
  const fiOnChange = ({ value }) => onChange(value);
  return (
    <ReactNumberFormat
      name={name}
      {...other}
      getInputRef={inputRef}
      onValueChange={fiOnChange}
      onBlur={fiOnBlur}
      thousandSeparator
      isNumericString
      prefix={prefix}
    />
  );
}

FormatedInput.propTypes = {
  maxLength: PropTypes.any,
  prefix: PropTypes.string,
  inputRef: PropTypes.any.isRequired,
  onChange: PropTypes.func.isRequired,
  onBlur: PropTypes.func.isRequired,
  name: PropTypes.string.isRequired
};

FormatedInput.defaultProps = {
  maxLength: undefined,
  prefix: ''
};

const TextField = (props) => {
  const { field, form } = props;
  const { setFieldValue, setFieldTouched } = form;
  const { onChange, onBlur, name, ...restField } = field;
  const fieldChange = (e) => {
    setFieldValue(name, e, false);
    setFieldTouched(name, true);
  };
  const fieldBlur = () => onBlur(name);
  const newProps = { ...props, field: { name, ...restField, onChange: fieldChange, onBlur: fieldBlur } };
  return (
    <BaseTextField
      {...newProps}
    />
  );
};

TextField.propTypes = {
  field: PropTypes.object.isRequired,
  form: PropTypes.object.isRequired
};

const Component = ({ label, name, maxLength, prefix }) => {
  const inputProps = {};
  if (maxLength) inputProps.maxLength = maxLength;
  if (prefix) inputProps.prefix = prefix;
  return (
    <FormControl fullWidth>
      <Field
        component={TextField}
        name={name}
        spellCheck={false}
        label={label}
        InputProps={{
          inputComponent: FormatedInput
        }}
        inputProps={inputProps}
      />
    </FormControl>
  );
};

Component.propTypes = {
  label: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  maxLength: PropTypes.oneOfType([
    PropTypes.bool,
    PropTypes.number
  ]),
  prefix: PropTypes.oneOfType([
    PropTypes.bool,
    PropTypes.string
  ])
};

Component.defaultProps = {
  maxLength: false,
  prefix: false
};

export default Component;
