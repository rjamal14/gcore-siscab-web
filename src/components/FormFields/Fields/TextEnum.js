/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable object-curly-newline */
/* eslint-disable react/prop-types */

import { TextField as BaseTextField } from 'formik-material-ui';
import { Field } from 'formik';
import PropTypes from 'prop-types';
import FormControl from '@material-ui/core/FormControl';
import React from 'react';

const TextField = (props) => {
  const { form, field, parent, parentvalue } = props;
  const { value } = field;
  const currentValue = form.values[field.name];

  if (currentValue && !field.value) {
    form.setFieldValue(field.name, currentValue, false);
    form.setFieldTouched(field.name, true);
  }

  let length = 0;
  if (value && value.length) {
    length = value.length;
  }
  const maxLength = 50;

  if (form.values[parent] === parentvalue) {
    return (
      <BaseTextField
        inputProps={{
          maxLength
        }}
        helperText={`${length}/${maxLength}`}
        {...props}
      />
    );
  }

  return false;
};

const Component = (props) => {
  const { name, label, parent, parentvalue } = props;
  return (
    <FormControl fullWidth>
      <Field
        component={TextField}
        name={name}
        spellCheck={false}
        label={label}
        parent={parent}
        parentvalue={parentvalue}
      />
    </FormControl>
  );
};

Component.propTypes = {
  label: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired
};

export default Component;
