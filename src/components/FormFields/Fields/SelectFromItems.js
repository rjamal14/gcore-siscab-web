/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable react/prop-types */

import React, { useState } from 'react';
import MUISelect from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import { Field } from 'formik';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import { useIntl } from 'react-intl';

function BaseSelect(props) {
  const { formatMessage } = useIntl();
  const { label, field, form, items, itemsTranslation } = props;
  const { name } = field;
  const { values } = form;
  const [selected, setSelected] = useState(values[name] || '');
  const [emptyOption, setEmptyOption] = useState('\u00A0');

  function onChange(e) {
    setSelected(e.target.value);
    form.setFieldValue(field.name, e.target.value, false);
    form.setFieldTouched(field.name, true);
  }

  function onOpen() {
    setEmptyOption(label);
  }

  function onClose() {
    setEmptyOption('\u00A0');
  }

  return (
    <MUISelect displayEmpty value={selected} onChange={onChange} onOpen={onOpen} onClose={onClose}>
      <MenuItem value="" style={{ color: 'grey' }}>{emptyOption}</MenuItem>
      {items.map(
        (item) => (
          <MenuItem key={item} value={item}>
            {formatMessage(itemsTranslation.translation[itemsTranslation.scope][item])}
          </MenuItem>
        )
      )}
    </MUISelect>
  );
}

export default function Component(props) {
  const { label } = props;
  return (
    <div>
      <FormControl fullWidth>
        <InputLabel>{label}</InputLabel>
        <Field
          {...props}
          component={BaseSelect}
        />
      </FormControl>
    </div>
  );
}
