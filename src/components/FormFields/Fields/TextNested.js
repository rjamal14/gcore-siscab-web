/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable object-curly-newline */
/* eslint-disable react/prop-types */

import { TextField as BaseTextField } from 'formik-material-ui';
import { Field } from 'formik';
import PropTypes from 'prop-types';
import FormControl from '@material-ui/core/FormControl';
import React from 'react';

const TextField = (props) => {
  const { field, form } = props;
  const { value } = field;
  let length = 0;
  if (value && value.length) {
    length = value.length;
  }

  if (form.initialValues[field.name] !== '' && field.value === undefined) {
    field.value = form.initialValues[field.name];
  }

  const maxLength = 50;
  const subtitle = {
    color: '#FF8F00',
    fontSize: '14px',
    fontWeight: 'bold',
    lineHeight: '28px',
    margin: '10px 0 -5px'
  };

  return (
    <>
      {field.name === 'additional_address_attributes[address]' && <p style={subtitle}>Alamat Tempat Tinggal</p>}
      <BaseTextField
        inputProps={{
          maxLength
        }}
        helperText={`${length}/${maxLength}`}
        {...props}
      />
    </>
  );
};

const Component = ({ label, name }) => (
  <FormControl fullWidth>
    <Field
      component={TextField}
      name={name}
      spellCheck={false}
      label={label}
    />
  </FormControl>
);

Component.propTypes = {
  label: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired
};

export default Component;
