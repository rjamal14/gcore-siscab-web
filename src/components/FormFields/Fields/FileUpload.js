import React from 'react';
import { SimpleFileUpload } from 'formik-material-ui';
import { Field } from 'formik';

const Fileupload = () => <Field component={SimpleFileUpload} name="file" label="Bukti Setoran" />;

export default Fileupload;
