/* eslint-disable react/jsx-fragments */
/* eslint-disable no-unused-vars */
/* eslint-disable object-curly-newline */
/* eslint-disable react/jsx-props-no-spreading */

import MuiSelect from '@material-ui/core/Select';
import { Select as BaseSelect } from 'formik-material-ui';
import { Field } from 'formik';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import React, { useState, useEffect, Fragment, useRef } from 'react';
import MenuItem from '@material-ui/core/MenuItem';
import Checkbox from '@material-ui/core/Checkbox';
import PropTypes from 'prop-types';
import map from 'lodash.map';
import filter from 'lodash.filter';
import includes from 'lodash.includes';
import find from 'lodash.find';
import Input from '@material-ui/core/Input';
import ListItemText from '@material-ui/core/ListItemText';
import Chip from '@material-ui/core/Chip';
import SelectGet from './SelectGet';

function delay(ms) {
  let timeout;
  const promise = new Promise(
    (resolve => {
      timeout = setTimeout(() => {
        resolve('timeout done');
      }, ms);
    })
  );

  return {
    promise,
    cancel() { clearTimeout(timeout); }
  };
}

function SelectMultiple(props) {
  const [val, setVal] = useState('');
  const isMounted = useRef(false);
  const { items, ...rest } = props;

  async function longGet() {
    if (isMounted.current === true) {
      // isMounted.current = false;
    }
  }

  useEffect(() => {
    //
    longGet();
    return () => {
      isMounted.current = false;
    };
  }, [val]);

  function handleChange(e) {
    setVal(e.target.value);
  }
  return (
    <Fragment>
      <FormControl fullWidth>
        <InputLabel>Age</InputLabel>
        <MuiSelect
          value={val}
          onChange={handleChange}
          input={<Input />}
        >
          <MenuItem value="">
            <em>None</em>
          </MenuItem>
          <MenuItem value={10}>Ten</MenuItem>
          <MenuItem value={20}>Twenty</MenuItem>
          <MenuItem value={30}>Thirty</MenuItem>
        </MuiSelect>
      </FormControl>
      <FormControl fullWidth>
        <InputLabel>Age2</InputLabel>
        <MuiSelect
          value={val}
          onChange={handleChange}
          input={<Input />}
        >
          <MenuItem value="">
            <em>None</em>
          </MenuItem>
          <MenuItem value={10}>Ten</MenuItem>
          <MenuItem value={20}>Twenty</MenuItem>
          <MenuItem value={30}>Thirty</MenuItem>
        </MuiSelect>
      </FormControl>
    </Fragment>
  );
}

SelectMultiple.propTypes = {
  label: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  id: PropTypes.number.isRequired,
  items: PropTypes
    .arrayOf(PropTypes.oneOfType([
      PropTypes.number,
      PropTypes.string,
      PropTypes.shape({
        label: PropTypes.string,
        value: PropTypes.oneOfType([
          PropTypes.number,
          PropTypes.string
        ])
      })
    ])).isRequired,
  initialValue: PropTypes
    .oneOfType([
      PropTypes.string,
      PropTypes.number,
      PropTypes.arrayOf(PropTypes.oneOfType([
        PropTypes.number,
        PropTypes.string
      ]))
    ]).isRequired,
  chips: PropTypes.bool,
  checkbox: PropTypes.bool
};

SelectMultiple.defaultProps = {
  chips: false,
  checkbox: false,
};

export default SelectMultiple;
