import IconButton from '@material-ui/core/IconButton';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import PropTypes from 'prop-types';
import React from 'react';

const ShowPassword = ({ show, onToggle }) => (
  <IconButton
    aria-label="Toggle password visibility"
    onClick={() => onToggle(!show)}
    onMouseDown={(e) => e.preventDefault}
  >
    {show ? <VisibilityOff /> : <Visibility />}
  </IconButton>
);

ShowPassword.propTypes = {
  show: PropTypes.boolean.isRequired,
  onToggle: PropTypes.func.isRequired
};

export default ShowPassword;
