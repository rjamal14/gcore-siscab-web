/* eslint-disable react/jsx-props-no-spreading */

import React, { createElement } from 'react';
import definitions from './definitions';

export default function FormField(args) {
  let Element;
  const { id, component: Component, ...props } = args;
  if (typeof Component === 'string') Element = createElement(definitions[Component], { key: id, ...props });
  else {
    Element = (<Component key={id} {...props} />);
  }
  return Element;
}
