/* eslint-disable react/jsx-no-bind */
/* eslint-disable no-prototype-builtins */
import React from 'react';
import PropTypes from 'prop-types';
import { Button, Icon, IconButton, TableBody, TableCell, TableRow } from '@material-ui/core';
import { BeatLoader } from 'react-spinners';
import AppsIcon from '@material-ui/icons/Apps';
import _ from 'lodash';
import useStyles from './mainTable-jss';

const Tablebody = ({
  data,
  columns,
  loading,
  onRowClick,
  onEdit,
  onDelete,
  parseId,
  onColsClick,
  onDetailClick,
  accessDetail,
  disableDelete,
  disableEdit,
}) => {
  const classes = useStyles();
  const tableColumns = columns || [];
  const tableData = data || [];
  const filterCol = columns.filter(col => col.name === 'pointerCursor');
  const cursorStyle = filterCol ? 'pointer' : 'auto';

  const handleRowClick = (event, row, rowData) => {
    onRowClick && onRowClick(event, row, rowData);
  };

  const onClickEdit = (event, row, rowData) => {
    onEdit && onEdit(event, row, rowData);
  };

  const onClickDelete = (event, dataId) => {
    onDelete && onDelete(event, dataId);
  };

  const handleColsClick = (event, row, rowData) => {
    onColsClick && onColsClick(event, row, rowData);
  };

  const handleDetailClick = (event, row, rowData) => {
    onDetailClick && onDetailClick(event, row, rowData);
  };

  return (
    <TableBody>
      {
        loading
          ? (
            <TableRow hover>
              <TableCell
                className={classes.emptyRow}
                colSpan={tableColumns.length + 2}
              >
                <BeatLoader />
              </TableCell>
            </TableRow>
          )
          : tableData.length === 0
            ? (
              <TableRow hover>
                <TableCell
                  className={classes.emptyRow}
                  colSpan={tableColumns.length + 2}
                >
                  <Button
                    color="inherit"
                    startIcon={<AppsIcon />}
                  >
                    Tidak Ada Data
                  </Button>

                </TableCell>
              </TableRow>
            )
            : _.map(tableData, (rowItem, row) => (
              <TableRow hover key={row} onClick={handleRowClick.bind(null, row, rowItem)}>
                {
                  _.map(tableColumns, (colItem, col) => {
                    const show = colItem.hasOwnProperty('display') ? colItem.display : true;
                    const render = [];
                    if (show) {
                      if (colItem.name === 'action') {
                        render.push(
                          <TableCell key={col} style={{ cursor: cursorStyle }}>
                            <div className={classes.actionButton}>
                              <div style={{ marginRight: 20 }}>
                                {(colItem.customBodyRender) && colItem.customBodyRender(null, rowItem[colItem.name], rowItem)}
                              </div>
                              {
                                (!disableEdit) && (
                                  <IconButton onClick={onClickEdit.bind(null, row, rowItem)}>
                                    <Icon fontSize="small">edit</Icon>
                                  </IconButton>
                                )
                              }
                              {
                                (!disableDelete) && (
                                  <IconButton onClick={onClickDelete.bind(null, row, rowItem)}>
                                    <Icon fontSize="small">delete</Icon>
                                  </IconButton>
                                )
                              }
                              { accessDetail
                                && (
                                  <IconButton onClick={handleDetailClick.bind(null, row, rowItem)}>
                                    <Icon fontSize="small">visibility</Icon>
                                  </IconButton>
                                ) }
                            </div>
                          </TableCell>
                        );
                      } else if (colItem.name === 'detail') {
                        render.push(
                          <TableCell key={col} style={{ cursor: cursorStyle }}>
                            <div className={classes.actionButton}>
                              <IconButton onClick={handleDetailClick.bind(null, row, rowItem)}>
                                <Icon fontSize="small">visibility</Icon>
                              </IconButton>
                            </div>
                          </TableCell>
                        );
                      } else if (colItem.name === 'id') {
                        render.push(
                          <TableCell key={col} style={{ cursor: cursorStyle }}>
                            {parseId(rowItem) }
                          </TableCell>);
                      } else if (colItem.hasOwnProperty('customBodyRender')) {
                        return (
                          <TableCell onClick={handleColsClick.bind(null, row, rowItem)} key={col} style={{ cursor: cursorStyle }}>
                            {colItem.customBodyRender(null, rowItem[colItem.name], rowItem)}
                          </TableCell>
                        );
                      } else {
                        render.push(
                          <TableCell onClick={handleColsClick.bind(null, row, rowItem)} key={col} style={{ cursor: cursorStyle }}>
                            {rowItem[colItem.name]}
                          </TableCell>);
                      }
                    }

                    return render;
                  }
                  )
                }
              </TableRow>
            )
            )
      }
    </TableBody>
  );
};

Tablebody.defaultProps = ({
  onRowClick: null,
  onEdit: null,
  onDelete: null,
});

Tablebody.propTypes = {
  data: PropTypes.array.isRequired,
  columns: PropTypes.array.isRequired,
  loading: PropTypes.bool.isRequired,
  onRowClick: PropTypes.func,
  onEdit: PropTypes.func,
  onDelete: PropTypes.func,
  parseId: PropTypes.func.isRequired
};

export default Tablebody;
