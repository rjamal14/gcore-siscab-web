/* eslint-disable consistent-return */
/* eslint-disable no-prototype-builtins */
import React from 'react';
import PropTypes from 'prop-types';
import {
  TableCell,
  TableHead,
  TableRow,
} from '@material-ui/core';
import _ from 'lodash';
import useStyles from './mainTable-jss';

const Tablehead = ({ columns }) => {
  const classes = useStyles();

  return (
    <TableHead className={classes.tableHead}>
      <TableRow>
        {
          _.map(columns, (item, col) => {
            const show = item.hasOwnProperty('display') ? item.display : true;
            if (show) {
              return (
                <TableCell key={col}>
                  {item.label}
                </TableCell>
              );
            }
          }
          )
        }
      </TableRow>
    </TableHead>
  );
};
Tablehead.propTypes = {
  columns: PropTypes.array.isRequired
};

export default Tablehead;
