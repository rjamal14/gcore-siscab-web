/* eslint-disable consistent-return */
/* eslint-disable react/jsx-one-expression-per-line */
import React, { useState } from 'react';
import PropTypes from 'prop-types';
import {
  Button,
  FormControl,
  IconButton,
  InputAdornment,
  MenuItem,
  OutlinedInput,
  Popover,
  Select,
  TextField,
  Typography
} from '@material-ui/core';
import SearchIcon from '@material-ui/icons/Search';
import PrintIcon from '@material-ui/icons/Print';
import ImportExportIcon from '@material-ui/icons/ImportExport';
import FilterListIcon from '@material-ui/icons/FilterList';
import AssignmentIcon from '@material-ui/icons/Assignment';
import _ from 'lodash';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import { useHistory, useLocation } from 'react-router-dom';
import qs from 'qs';
import dayjs from 'dayjs';
import useStyles from './mainTable-jss';

const Tabletoolbar = ({
  title,
  description,
  totalData,
  columns,
  page,
  perPage,
  disableFilter,
  disableExport,
  disableSearch,
  handleExport,
  customDateExport,
  enablePrint,
  handlePrint,
  enableJsonExport,
  handleJsonExport,
  handleCustomReset
}) => {
  const dateObj = new Date();
  const classes = useStyles();
  const history = useHistory();
  const location = useLocation();
  const [startDate, setStartDate] = useState(new Date((dateObj.getMonth() + 1) + '-01-' + dateObj.getFullYear()));
  const [endDate, setEndDate] = useState(new Date((dateObj.getMonth() + 1) + '-28-' + dateObj.getFullYear()));
  const [filterParams, setFilterParams] = useState({ q: {} });
  const [searchValue, setSearchValue] = useState('');
  const [anchorEl, setAnchorEl] = React.useState(null);
  const openFilterMenu = Boolean(anchorEl);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const onFilterChange = (e) => {
    const newFilterParams = filterParams;
    const currentParams = qs.parse(location.search);
    newFilterParams.q[e.target.name] = e.target.value;

    const finalParams = _.assign(currentParams, newFilterParams);

    setFilterParams(finalParams);
  };

  const renderPerPage = () => (
    <Select
      name="perPage"
      value={perPage}
      onChange={e => history.push(`${location.pathname}?page=${page}&per_page=${e.target.value}`)}
      variant="outlined"
      className={classes.toolbarPage}
    >
      <MenuItem value={10}>10 Data</MenuItem>
      <MenuItem value={15}>15 Data</MenuItem>
      <MenuItem value={25}>25 Data</MenuItem>
    </Select>
  );

  const renderFilterMenu = () => (
    <Popover
      open={openFilterMenu}
      anchorEl={anchorEl}
      onClose={handleClose}
      anchorOrigin={{
        vertical: 'bottom',
        horizontal: 'right',
      }}
      transformOrigin={{
        vertical: 'top',
        horizontal: 'right',
      }}
    >
      <div className={classes.filterMenu}>
        <Typography
          variant="button"
          color="primary"
        >
          Filter Data
        </Typography>
        {
          _.map(columns, (col, key) => {
            if (col.options.filter) {
              return (
                <FormControl key={key} className={classes.filterField}>
                  <TextField
                    name={col.name + col.filterPredicate}
                    label={col.label}
                    variant="outlined"
                    onChange={onFilterChange}
                  />
                </FormControl>
              );
            }
          }
          )
        }
        <Button
          variant="contained"
          color="primary"
          fullWidth
          onClick={() => history.push(`${location.pathname}${qs.stringify(filterParams).replace('%3F', '?')}`)}
        >
          Filter
        </Button>
      </div>
    </Popover>
  );

  const onSearchChange = (e) => {
    setSearchValue(e.target.value);
  };

  const onKeyPress = (e) => {
    if (e.key === 'Enter') {
      history.push(`${location.pathname}?page=$1&per_page=10&search=${searchValue}`);
    }
  };

  // set samakan endDate dengan startDate kalau startDate melebihi endDate
  const handleStartDate = (date) => {
    const start = new Date(date).getTime();
    const end = new Date(endDate).getTime();
    if (start > end) setEndDate(date);
    setStartDate(date);
  };

  return (
    <div className={classes.tableToolbar}>
      <div className={classes.tableLeftContent}>
        <Typography variant="h6" className={classes.title} style={{ fontWeight: 'bold' }}>
          {title}
        </Typography>
        <Typography variant="body2" className={classes.title}>
          {description}
        </Typography>

        <div className={classes.leftToolbar}>
          Tampilkan: {renderPerPage()} Total: {totalData} {title}
        </div>
      </div>

      <div className={classes.rightToolbar}>
        {
          !disableSearch && (
            <>
              <OutlinedInput
                name="searchField"
                placeholder="Cari data..."
                onChange={onSearchChange}
                variant="outlined"
                value={searchValue}
                className={classes.searchField}
                onKeyPress={onKeyPress}
                endAdornment={(
                  <InputAdornment>
                    <IconButton
                      edge="end"
                      size="small"
                      onClick={() => history.push(`${location.pathname}?page=$1&per_page=10&search=${searchValue}`)}
                    >
                      <SearchIcon color="primary" />
                    </IconButton>
                  </InputAdornment>
                )}
              />
              <Button
                color="primary"
                variant="outlined"
                style={{ marginRight: 8 }}
                onClick={() => {
                  setSearchValue('');
                  history.push(`${location.pathname}?&page=${page}&per_page=${perPage}`);
                }}
              >
                Reset
              </Button>
            </>
          )
        }
        {
          !disableFilter
          && (
            <div>
              <Button
                name="filterButton"
                startIcon={<FilterListIcon />}
                style={{ marginRight: 8 }}
                variant="outlined"
                color="primary"
                onClick={handleClick}
              >
                Filter
              </Button>
              {renderFilterMenu()}
            </div>
          )
        }
        {
          customDateExport
          && (
            <>
              <DatePicker
                className={classes.Startdate}
                selected={startDate}
                onChange={date => handleStartDate(date)}
                disabledKeyboardNavigation
                placeholderText="Tanggal Awal"
                dateFormat="dd MMM yyyy"
                selectsStart
                startDate={startDate}
                endDate={endDate}
              />
              <DatePicker
                className={classes.date}
                selected={endDate}
                onChange={date => setEndDate(date)}
                disabledKeyboardNavigation
                placeholderText="Tanggal Akhir"
                dateFormat="dd MMM yyyy"
                minDate={startDate}
                showDisabledMonthNavigation
                selectsEnd
                startDate={startDate}
                endDate={endDate}
              />
            </>
          )
        }
        {
          enableJsonExport && (
            <div>
              <Button
                name="filterButton"
                startIcon={<AssignmentIcon />}
                variant="outlined"
                color="primary"
                style={{ marginRight: 8 }}
                onClick={() => {
                  handleJsonExport();
                  const start = dayjs(startDate).format('YYYY-MM-DD');
                  const end = dayjs(endDate).format('YYYY-MM-DD');
                  history.push(`${location.pathname}?&page=1&per_page=${perPage}&q[created_at_gteq]=${start}&q[created_at_lteq]=${end}&token`);
                }}
              >
                Terapkan
              </Button>
              <Button
                color="primary"
                variant="outlined"
                style={{ marginRight: 8 }}
                onClick={() => {
                  handleCustomReset();
                  history.push(`${location.pathname}?&page=${page}&per_page=${perPage}`);
                }}
              >
                Reset
              </Button>
            </div>
          )
        }
        {
          !disableExport
          && (
            <div>
              <Button
                name="filterButton"
                startIcon={<ImportExportIcon />}
                variant="outlined"
                color="primary"
                style={{ marginRight: 8 }}
                onClick={() => {
                  if (customDateExport) handleExport(startDate, endDate);
                  else handleExport();
                }}
              >
                Export
              </Button>
            </div>
          )
        }
        {
          enablePrint && (
            <div>
              <Button
                name="filterButton"
                startIcon={<PrintIcon />}
                variant="outlined"
                color="primary"
                onClick={() => handlePrint(startDate, endDate)}
              >
                Print
              </Button>
            </div>
          )
        }

      </div>

    </div>
  );
};

Tabletoolbar.defaultProps = ({
  title: 'Table',
  description: '',
  totalData: 0,
  page: 1,
  perPage: 10,
  disableFilter: false,
  disableExport: false
});

Tabletoolbar.propTypes = {
  columns: PropTypes.oneOfType([
    PropTypes.array,
    PropTypes.object
  ]).isRequired,
  title: PropTypes.string,
  description: PropTypes.string,
  totalData: PropTypes.number,
  page: PropTypes.number,
  perPage: PropTypes.number,
  disableFilter: PropTypes.bool,
  disableExport: PropTypes.bool
};

export default Tabletoolbar;
