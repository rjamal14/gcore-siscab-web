/* eslint-disable import/prefer-default-export */
import Joi from 'joi';

export const opencashSchema = Joi.object({
  /*  paperMoney: Joi.array().items(Joi.object({
    amount: Joi.number(),
    currency: Joi.number(),
    total: Joi.number()
  })), */
  quantity: Joi.number(),
  totalCash: Joi.number(),
  marginCash: Joi.number()
});
