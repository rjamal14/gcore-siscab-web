/* eslint-disable array-callback-return */
/* eslint-disable no-unused-vars */
import env from '../config/env';

const token = localStorage.getItem('token');
const service = {};

service.getCustomer = () => fetch(`${env.managementApi}${env.apiPrefixV1}/customer/autocomplete`, {
  method: 'GET',
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + localStorage.getItem('token')
  }
});

service.getProduct = () => fetch(`${env.masterApi}${env.apiPrefixV1}/product`, {
  method: 'GET',
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + localStorage.getItem('token')
  }
});

service.getTransactionFilter = ({ customerId, productId }) => {
  const url = `${env.managementApi}${env.apiPrefixV1}/transaction_disbursements?type=customer&q[customer_id_cont]=${customerId.id}&q[insurance_item_id_cont]='${productId.id}'`;
  return fetch(url, {
    method: 'GET',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + localStorage.getItem('token')
    }
  });
};

service.getJob = () => fetch(`${env.masterApi}${env.apiPrefixV1}/customer_jobs`, {
  method: 'GET',
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + localStorage.getItem('token')
  }
});

service.printStruk = (id) => window.open(`${env.managementApi}${env.apiPrefixV1}/transactions/${id}/print_disbursement.pdf?token=${token}`);

service.getDataInstallment = (rowsPerPage, page, search) => fetch(`${env.managementApi}${env.apiPrefixV1}/installments?type=customer`, {
  method: 'GET',
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + localStorage.getItem('token')
  }
});

service.installmentCostApi = (params) => fetch(`${env.masterApi}${env.apiPrefixV1}/installment/simulate?office_type=${localStorage.getItem('office_type')}&office_id=${localStorage.getItem('office_id')}`, {
  method: 'POST',
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + localStorage.getItem('token')
  },
  body: JSON.stringify(params),
});

service.getProductInstallment = () => fetch(`${env.masterApi}${env.apiPrefixV1}/product?type=`, {
  method: 'GET',
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + localStorage.getItem('token')
  }
});

service.checkInstallmentAttrb = (params) => fetch(`${env.masterApi}${env.apiPrefixV1}/installment/attribute?office_type=${localStorage.getItem('office_type')}&office_id=${localStorage.getItem('office_id')}`, {
  method: 'POST',
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + localStorage.getItem('token')
  },
  body: JSON.stringify(params),
});

export default service;
