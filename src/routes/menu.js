/* eslint-disable semi */
import React from 'react';
import {
  Home,
  Person,
  Receipt,
  AttachMoney,
  MenuBook,
  Assignment,
  LocalAtm,
} from '@material-ui/icons';
import Func from '../functions/index';
import Icon from '../components/icon';

const SetCustomIcon = (path, desc) => (
  <img
    src={path.path}
    width="20"
    height="20"
    alt={desc}
  />
)

const menu = [
  {
    title: 'Dashboard',
    link: '/home/dashboard',
    active: Func.checkPermission('home'),
    key: 'home',
    icon: <Home />,
    breadcrumb: {
      '/home': 'Home'
    },
    subMenu: [
      {
        title: 'Dashboard',
        key: 'home#dashboard',
        active: Func.checkPermission('home#dashboard'),
        link: '/home/dashboard',
        icon: 'lens',
        breadcrumb: {
          '/home/dashboard': 'Dashboard'
        },
      },
    ]
  },
  {
    title: 'Data Nasabah',
    link: '/customer/individual',
    active: Func.checkPermission('customer'),
    key: 'customer',
    icon: <Person />,
    breadcrumb: {
      '/customer': 'Data Nasabah'
    },
    subMenu: [
      {
        title: 'Perorangan',
        active: Func.checkPermission('customer#individual'),
        key: 'customer#individual',
        link: '/customer/individual',
        icon: 'lens',
        breadcrumb: {
          '/customer/individual': 'Perorangan'
        },
      }
    ]
  },
  {
    title: 'Manajemen Transaksi',
    link: '/transaction/individual',
    active: Func.checkPermission('transaction'),
    key: 'transaction',
    icon: <Receipt />,
    breadcrumb: {
      '/transaction': 'Transaksi'
    },
    subMenu: [
      {
        title: 'Perorangan',
        link: '/transaction/individual',
        active: Func.checkPermission('transaction#individual'),
        key: 'individual',
        icon: 'lens',
        breadcrumb: {
          '/transaction/individual': 'Perorangan'
        },
      },
      {
        title: 'Approval Deviasi',
        link: '/transaction/deviation',
        active: true,
        key: 'deviation',
        icon: 'lens',
        breadcrumb: {
          '/transaction/deviation': 'Approval Deviasi'
        },
      }
    ]
  },
  {
    title: 'Finansial Manajemen',
    link: '/financial-management/request',
    active: Func.checkPermission('financial-management'),
    key: 'financial-management',
    icon: <AttachMoney />,
    breadcrumb: {
      '/financial-management': 'Finansial Manajemen'
    },
    subMenu: [
      {
        key: 'permintaan',
        title: 'Permintaan',
        link: '/financial-management/request',
        active: Func.checkPermission('financial-management#request'),
        icon: 'lens',
        breadcrumb: {
          '/financial-management/request': 'Permintaan'
        },
      },
      {
        key: 'transaction-reports',
        title: 'Laporan Transaksi',
        link: '/financial-management/transaction-reports',
        active: Func.checkPermission('financial-management#transaction-reports'),
        icon: 'lens',
        breadcrumb: {
          '/financial-management/transaction-reports': 'Laporan Transaksi'
        },
      },
      {
        key: 'pembukuan',
        title: 'Pembukuan',
        active: Func.checkPermission('financial-management#bookkeeping'),
        link: '/financial-management/bookkeeping',
        icon: 'lens',
        breadcrumb: {
          '/financial-management/bookkeeping': 'Pembukuan'
        },
      },
      {
        key: 'miscellaneous-expense',
        title: 'Biaya Lain -  Lain',
        active: Func.checkPermission('financial-management#miscellaneous-expense'),
        link: '/financial-management/miscellaneous-expense',
        icon: 'lens',
        breadcrumb: {
          '/financial-management/miscellaneous-expense': 'Biaya Lain -  Lain'
        },
      },
    ]
  },
  {
    title: 'Jurnal',
    link: '/journal/all',
    active: Func.checkPermission('journal'),
    key: 'journal',
    icon: <MenuBook />,
    breadcrumb: {
      '/journal': 'Jurnal'
    },
    subMenu: [
      {
        key: 'all',
        title: 'Semua',
        active: Func.checkPermission('journal#all'),
        link: '/journal/all',
        icon: 'lens',
        breadcrumb: {
          '/journal/all': 'Semua'
        },
      }, {
        key: 'disbursement',
        title: 'Pencairan',
        link: '/journal/disbursement',
        active: Func.checkPermission('journal#disbursement'),
        icon: 'lens',
        breadcrumb: {
          '/journal/disbursement': 'Pencairan'
        },
      },
      {
        key: 'repayment',
        title: 'Pelunasan',
        link: '/journal/repayment',
        active: Func.checkPermission('journal#repayment'),
        icon: 'lens',
        breadcrumb: {
          '/journal/repayment': 'Pelunasan'
        },
      },
      {
        key: 'extension',
        title: 'Perpanjangan',
        link: '/journal/extension',
        active: Func.checkPermission('journal#extension'),
        icon: 'lens',
        breadcrumb: {
          '/journal/extension': 'Perpanjangan'
        },
      },
      {
        key: 'non-transactional',
        title: 'Non Transaksional',
        link: '/journal/non-transactional',
        active: Func.checkPermission('journal#non-transactional'),
        icon: 'lens',
        breadcrumb: {
          '/journal/non-transactional': 'Non Transaksional'
        },
      },
      {
        key: 'add',
        title: 'Tambah Jurnal',
        link: '/journal/add',
        active: false,
        icon: 'lens',
        breadcrumb: {
          '/journal/add': 'Tambah Jurnal'
        },
      },
    ]
  },
  {
    title: 'Laporan Transaksi',
    link: '/report/all',
    active: Func.checkPermission('report'),
    key: 'report',
    icon: <Assignment />,
    breadcrumb: {
      '/report': 'Laporan Transaksi'
    },
    subMenu: [
      {
        key: 'all',
        title: 'Semua',
        active: Func.checkPermission('report#all'),
        link: '/report/all',
        icon: 'lens',
        breadcrumb: {
          '/report/all': 'Semua'
        },
      },
      {
        key: 'disbursement',
        title: 'Pencairan',
        active: Func.checkPermission('report#disbursement'),
        link: '/report/disbursement',
        icon: 'lens',
        breadcrumb: {
          '/report/disbursement': 'Pencairan'
        },
      },
      {
        key: 'prolongations',
        title: 'Perpanjangan',
        active: Func.checkPermission('report#prolongations'),
        link: '/report/prolongation',
        icon: 'lens',
        breadcrumb: {
          '/report/prolongation': 'Perpanjangan'
        },
      },
      {
        key: 'repayment',
        title: 'Pelunasan',
        active: Func.checkPermission('report#repayment'),
        link: '/report/repayment',
        icon: 'lens',
        breadcrumb: {
          '/report-repayments/all': 'Pelunasan'
        },
      },
      {
        key: 'outstanding',
        title: 'Outstanding',
        active: Func.checkPermission('report#outstanding'),
        link: '/report/outstanding',
        icon: 'lens',
        breadcrumb: {
          '/report-outstandings/all': 'Pelunasan'
        },
      },

    ]
  },
  {
    title: 'Manajemen Kas',
    link: '/cashflow/all',
    active: Func.checkPermission('cashflow'),
    key: 'cashflow',
    icon: <SetCustomIcon path={Icon.cashflow} desc="cashflow" />,
    breadcrumb: {
      '/cashflow': 'Manajemen Kas'
    },
    subMenu: [
      {
        key: 'cashflow/all',
        title: 'Arus Kas',
        active: Func.checkPermission('cashflow#all'),
        link: '/cashflow/all?page=1&per_page=10',
        icon: 'lens',
        breadcrumb: {
          '/cashflow/all': 'Arus Kas'
        },
      },
      {
        key: 'cashflow/cash-disbursements',
        title: 'Pengeluaran Kas',
        active: Func.checkPermission('cashflow#cash-disbursements'),
        link: '/cashflow/cash-disbursements',
        icon: 'lens',
        breadcrumb: {
          '/cashflow/cash-disbursements': 'Pengeluaran Kas'
        },
      },
      {
        key: 'cashflow/cash-journal',
        title: 'Buku Kas',
        active: Func.checkPermission('cashflow#cash-journal'),
        link: '/cashflow/cash-journal?page=1&per_page=10',
        icon: 'lens',
        breadcrumb: {
          '/cashflow/cash-journal': 'Buku Kas'
        },
      }
    ]
  },
  {
    title: 'Modal Kerja',
    link: '/workcap',
    active: true, // belum daftar prefix
    key: 'workcap',
    icon: <LocalAtm />,
    breadcrumb: {
      '/workcap': 'Modal Kerja'
    },
    subMenu: [
      {
        title: 'History & Draft',
        key: 'workcap#moker',
        active: true, // belum daftar prefix
        link: '/workcap/moker',
        icon: 'lens',
        breadcrumb: {
          '/workcap/moker': 'History & Draft'
        },
      },
      {
        title: 'Approval',
        key: 'workcap#approval',
        active: true, // belum daftar prefix
        link: '/workcap/approval',
        icon: 'lens',
        breadcrumb: {
          '/workcap/approval': 'Approval'
        },
      },
    ]
  },
  {
    title: 'Pengaturan Profil',
    link: '/profile',
    active: false,
    key: 'profile',
    icon: <LocalAtm />,
    breadcrumb: {
      '/profile': 'Pengaturan Profil'
    }
  },
  {
    title: 'Ubah Kata Sandi',
    link: '/change-password',
    active: false,
    key: 'change-password',
    icon: <LocalAtm />,
    breadcrumb: {
      '/change-password': 'Ubah Kata Sandi'
    }
  },
  {
    title: 'Notifikasi',
    link: '/notification',
    active: false,
    key: 'notification',
    icon: <LocalAtm />,
    breadcrumb: {
      '/notification': 'Notifikasi'
    }
  },
];

export default menu;
