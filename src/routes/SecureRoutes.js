/* eslint-disable camelcase */
import React from 'react';
import { Switch, Route } from 'react-router-dom';
import Layout from '../container/Layout';
import PrivateRoute from '../components/PrivateRoute';
import {
  DashboardPage,
  CustomerIndividual,
  TransactionalReports,
  TransactionIndividual,
  Journal_Disbursement,
  Journal_Repayment,
  Journal_Extension,
  Request,
  Journal_All,
  TranscationReport,
  TranscationReport_disbursement,
  TranscationReport_prolongation,
  TranscationReport_repayment,
  TranscationReport_outstanding,
  Cashflow,
  CashDisbursements,
  CashJournal,
  MiscellaneousExpense,
  accessDened,
  Journal_nonTransactional,
  AllNotification,
  EntryJournalForm,
  MokerHistory,
  WorkcapApproval,
  DeviationApproval,
  Profile,
  ChangePassword,
} from '../container/Pages/asyncpages';
import application from '../config/application';
import NotFound from './NotFound';

const SecureRoutes = ({ history }) => {
  const securePath = application.privatePath;
  return (
    <Layout history={history}>
      <Switch>

        {/* Dashboard */}

        <PrivateRoute exact path={`${securePath}`} component={DashboardPage} />
        <PrivateRoute path={`${securePath}/home/dashboard`} component={DashboardPage} />

        {/* Data Nasabah */}

        <PrivateRoute path={`${securePath}/customer/individual`} component={CustomerIndividual} />

        {/* Manajemen Transaksi */}

        <PrivateRoute exact path={`${securePath}/transaction/individual`} component={TransactionIndividual} />
        <Route exact path={`${securePath}/transaction/deviation`} component={DeviationApproval} />

        {/* Finansial Manajemen */}

        <PrivateRoute path={`${securePath}/financial-management/request`} component={Request} />
        <PrivateRoute path={`${securePath}/financial-management/transaction-reports`} component={TransactionalReports} />
        <PrivateRoute path={`${securePath}/financial-management/miscellaneous-expense`} component={MiscellaneousExpense} />

        {/* Jurnal */}

        <PrivateRoute path={`${securePath}/journal/all`} component={Journal_All} />
        <PrivateRoute path={`${securePath}/journal/disbursement`} component={Journal_Disbursement} />
        <PrivateRoute path={`${securePath}/journal/repayment`} component={Journal_Repayment} />
        <PrivateRoute path={`${securePath}/journal/extension`} component={Journal_Extension} />
        <PrivateRoute path={`${securePath}/journal/non-transactional`} component={Journal_nonTransactional} />
        <Route path={`${securePath}/journal/add`} component={EntryJournalForm} />

        {/* Report */}

        <PrivateRoute path={`${securePath}/report/all`} component={TranscationReport} />
        <PrivateRoute path={`${securePath}/report/disbursement`} component={TranscationReport_disbursement} />
        <PrivateRoute path={`${securePath}/report/prolongation`} component={TranscationReport_prolongation} />
        <PrivateRoute path={`${securePath}/report/repayment`} component={TranscationReport_repayment} />
        <PrivateRoute path={`${securePath}/report/outstanding`} component={TranscationReport_outstanding} />

        {/* Laporan Transaksi */}

        <PrivateRoute path={`${securePath}/cashflow/all`} component={Cashflow} />
        <PrivateRoute path={`${securePath}/cashflow/cash-disbursements`} component={CashDisbursements} />
        <PrivateRoute path={`${securePath}/cashflow/cash-journal`} component={CashJournal} />
        <Route path={`${securePath}/cashflow/moker`} component={MokerHistory} />

        {/* Modal Kerja */}

        <Route path={`${securePath}/workcap/moker`} component={MokerHistory} />
        <Route path={`${securePath}/workcap/approval`} component={WorkcapApproval} />

        {/* Setting */}

        <Route path={`${securePath}/profile`} component={Profile} />
        <Route path={`${securePath}/change-password`} component={ChangePassword} />

        {/* etc */}

        <Route path={`${securePath}/page/access-dened`} component={accessDened} />
        <Route path={`${securePath}/notification`} component={AllNotification} />
        <Route path={`${securePath}/transaction/individual/:id/:type`} component={TransactionIndividual} />
        <Route component={NotFound} />

      </Switch>
    </Layout>
  );
};

export default SecureRoutes;
