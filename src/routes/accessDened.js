/* eslint-disable react/jsx-one-expression-per-line */
/* eslint-disable quotes */
/* eslint-disable react/no-unescaped-entities */
/* eslint-disable react/jsx-indent */
/* eslint-disable react/jsx-no-comment-textnodes */
/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
import { makeStyles } from '@material-ui/core';
import React from 'react';
import Link from '@material-ui/core/Link';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'fixed',
    width: '83%',
    height: '83%'
  },
  root2: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'fixed',
    width: '83%',
    height: '83%',
    marginTop: '25px'
  }
}));

const Access_Dened = () => {
  const classes = useStyles();
  return (
    <div>
      <div className={classes.root}>
          Oops, sorry you don't have access :(
      </div>
      <div className={classes.root2}>
        <Link href="/app/home/dashboard">
          {"\n"}Back to Home
        </Link>
      </div>
    </div>
  );
};
export default Access_Dened;
